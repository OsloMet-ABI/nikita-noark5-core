# Using postgresql

nikita has recently started to use postgresql and in the future will try to use the full text search capabilities, and
as such this file documents the setup of nikita with postgresql

> Note: Use postgresql version 17 or greater.

This recipe starts after postgresql 17.2 is installed and running, and assumes a fresh postgresql install. If you
already have set up postgresql you likely know what to ignore.

Check postgresql version

> sudo -u postgres psql -c "SELECT version();"

should produce something like

> version
> -----------------------------------------------------------------------------------------------------------------------------------
> PostgreSQL 17.2 (Ubuntu 17.2-1.pgdg22.04+1) on x86_64-pc-linux-gnu, compiled by gcc (Ubuntu 11.4.0-1ubuntu1~22.04)
> 11.4.0, 64-bit
(1 row)


Next, become the postgres user

> sudo -i -u postgres

Go into postgresql terminal

> psql

Set a password for postgresql server

> \password postgres

Then quit the postgresql terminal

> \q

Next, create a user for nikita

> createuser --interactive

> postgres@honeypot:~$ createuser --interactive
> Enter name of role to add: nikita_user
> Shall the new role be a superuser? (y/n) n
> Shall the new role be allowed to create databases? (y/n) n
> Shall the new role be allowed to create more new roles? (y/n) n

Next locate the application-postgres.yml file and update the username

> username: ${DB_USER:INSERT-USERNAME-HERE}

Note that you can also set the username at runtime using `-D` option

> -DDB_USER=nikita_user

Next create the database for nikita

> createdb nikita_noark5_prod

This should match the following value in application-postgres.yml

> url: ${DB_URI:jdbc:postgresql://localhost:5432/nikita_noark5_prod}

Now you can go back to the postgresql terminal and set the password for nikita_user

> psql
> ALTER USER nikita_user WITH PASSWORD 'INSERT-PASSWORD-HERE';

Note that you can also set the password at runtime using `-D` option

> -DDB_PASS=INSERT-PASSWORD-HERE

While still in the postgresql terminal grant all privileges on the nikita_noark5_prod database the user nikita_user

> GRANT ALL PRIVILEGES ON DATABASE nikita_noark5_prod TO nikita_user;

Then make `nikita_user` the owner of the `nikita_noark5_prod` database

> ALTER DATABASE nikita_noark5_prod OWNER TO nikita_user;

Next, quit the terminal and the postgres database should now be ready for nikita running the `postgres` profile

> \q

## Using postgreSQL in tests

It is possible to use a local standalone postgreSQL installation in the tests. Take a look at the file
`src/test/resources/application-local-postgres.yml`. The database for testing is different to the production
database.

You will need to follow the same steps as above and create a database `nikita_noark5_test` and make sure the
user has correct access rights. If all is OK, it should be possible to run the tests using postgreSQL using the
following maven command.

> mvn test -DDB_PROFILE=local-postgres -DDB_USER=INSERT-USERNAME-HERE -DDB_PASS=INSERT-PASSWORD-HERE

Note: You will find information in the git log for using testcontainers. We did not wish to include another
dependency in nikita that required the use of docker