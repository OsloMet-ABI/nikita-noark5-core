SET
AUTOCOMMIT OFF;
BEGIN
TRANSACTION;

SET
@ADMIN_UNIT_UUID = UUID();
SET
@USERUUID = UUID();
SET
@ADM_UNIT='Nikita AdministrativeUnit';
SET
@ADM_UNIT_ABBREV='NAU';
SET
@USERNAME='admin@example.com';
SET
@FIRSTNAME='Frank';
SET
@LASTNAME='Grimes';

-- Create an administrativeUnit

INSERT INTO system_id_entity(system_id,
                             created_date,
                             last_modified_date,
                             created_by,
                             organisation,
                             version,
                             dtype)
VALUES (@ADMIN_UNIT_UUID,
        NOW(),
        NOW(),
        null,
        'default',
        0,
        'AdministrativeUnit');

INSERT INTO ad_administrative_unit (system_id,
                                    default_administrative_unit,
                                    administrative_unit_name,
                                    short_name)
VALUES (@ADMIN_UNIT_UUID,
        true,
        @ADM_UNIT,
        @ADM_UNIT_ABBREV);

INSERT INTO system_id_entity(system_id,
                             created_date,
                             last_modified_date,
                             created_by,
                             organisation,
                             version,
                             dtype)
VALUES (@USERUUID,
        NOW(),
        NOW(),
        null,
        'default',
        0,
        'User');

INSERT INTO ad_user(system_id,
                    username,
                    firstname,
                    lastname)
VALUES (@USERUUID,
        @USERNAME,
        @FIRSTNAME,
        @LASTNAME);

INSERT INTO ad_administrative_unit_user(f_pk_administrative_unit_id,
                                        f_pk_user_id)
VALUES (@ADMIN_UNIT_UUID,
        @USERUUID);
COMMIT;