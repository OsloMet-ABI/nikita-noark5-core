package app.domain.filehandling;

import app.domain.noark5.NoarkGeneralEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.util.ArrayList;
import java.util.List;

import static app.utils.constants.Constants.TABLE_FILE_UPLOAD_REGISTER;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Not an actual Noark entity, but it makes sense to have those fields associated
 * with the file upload
 */
@Entity
@Table(name = TABLE_FILE_UPLOAD_REGISTER)
public class LargeFileUploadRegister
        extends NoarkGeneralEntity {

    @Column(name = CONTENT_LENGTH_ENG)
    Long contentLength;

    @OneToMany(mappedBy = "referenceLargeFileUploadRegister")
    private final List<FileChunkUpload> referenceFileChunkUpload = new ArrayList<>();
    @Column(name = UPLOAD_CONTENT_TYPE_ENG)
    String uploadContentType;
    @Column(name = UPLOAD_CONTENT_LENGTH_ENG)
    Long uploadContentLength;

    public LargeFileUploadRegister(Builder builder) {
        this.contentLength = builder.contentLength;
        this.uploadContentType = builder.uploadContentType;
        this.uploadContentLength = builder.uploadContentLength;
    }

    public LargeFileUploadRegister() {

    }

    public Long getUploadContentLength() {
        return uploadContentLength;
    }

    public void setUploadContentLength(Long uploadContentLength) {
        this.uploadContentLength = uploadContentLength;
    }

    public String getUploadContentType() {
        return uploadContentType;
    }

    public void setUploadContentType(String uploadContentType) {
        this.uploadContentType = uploadContentType;
    }

    public Long getContentLength() {
        return contentLength;
    }

    public void setContentLength(Long contentLength) {
        this.contentLength = contentLength;
    }

    public List<FileChunkUpload> getReferenceChunkFileUploadRegister() {
        return referenceFileChunkUpload;
    }

    public static class Builder {
        private Long contentLength;
        private String uploadContentType;
        private Long uploadContentLength;

        public Builder setContentLength(Long contentLength) {
            this.contentLength = contentLength;
            return this;
        }

        public Builder setUploadContentType(String uploadContentType) {
            this.uploadContentType = uploadContentType;
            return this;
        }

        public Builder setUploadContentLength(Long uploadContentLength) {
            this.uploadContentLength = uploadContentLength;
            return this;
        }

        public LargeFileUploadRegister build() {
            return new LargeFileUploadRegister(this);
        }
    }
}
