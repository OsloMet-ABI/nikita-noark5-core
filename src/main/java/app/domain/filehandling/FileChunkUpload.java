package app.domain.filehandling;

import app.domain.noark5.SystemIdEntity;
import jakarta.persistence.*;

import static app.utils.constants.Constants.PRIMARY_KEY_SYSTEM_ID;
import static app.utils.constants.Constants.TABLE_FILE_UPLOAD_CHUNKS;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.FetchType.LAZY;

/**
 * Not an actual Noark entity, but it makes sense to have those fields associated
 * with the file upload
 */
@Entity
@Table(name = TABLE_FILE_UPLOAD_CHUNKS)
public class FileChunkUpload
        extends SystemIdEntity {

    @Column(name = CONTENT_LENGTH_ENG)
    Long contentLength;

    @Column(name = CHUNK_CONTENT_LENGTH_TOTAL)
    Long contentLengthTotal;

    @Column(name = CHUNK_CONTENT_TYPE)
    String contentType;

    @Column(name = CHUNK_CONTENT_RANGE_FROM)
    Long contentRangeFrom;

    @Column(name = CHUNK_CONTENT_RANGE_TO)
    Long contentRangeTo;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "chunk_file_register",
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private LargeFileUploadRegister referenceLargeFileUploadRegister;

    public boolean isLastChunk() {
        return contentRangeTo == contentLengthTotal;
    }

    private FileChunkUpload(Builder builder) {
        this.contentLength = builder.contentLength;
        this.contentLengthTotal = builder.contentLengthTotal;
        this.contentType = builder.contentType;
        this.contentRangeFrom = builder.contentRangeFrom;
        this.contentRangeTo = builder.contentRangeTo;
    }

    public FileChunkUpload() {

    }

    // Getters
    public Long getContentLength() {
        return contentLength;
    }

    public Long getContentLengthTotal() {
        return contentLengthTotal;
    }

    public String getContentType() {
        return contentType;
    }

    public Long getContentRangeFrom() {
        return contentRangeFrom;
    }

    public Long getContentRangeTo() {
        return contentRangeTo;
    }

    // Builder class
    public static class Builder {
        private Long contentLength;
        private Long contentLengthTotal;
        private String contentType;
        private Long contentRangeFrom;
        private Long contentRangeTo;

        public Builder setContentLength(Long contentLength) {
            this.contentLength = contentLength;
            return this;
        }

        public Builder setContentLengthTotal(Long contentLengthTotal) {
            this.contentLengthTotal = contentLengthTotal;
            return this;
        }

        public Builder setContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public Builder setContentRangeFrom(Long contentRangeFrom) {
            this.contentRangeFrom = contentRangeFrom;
            return this;
        }

        public Builder setContentRangeTo(Long contentRangeTo) {
            this.contentRangeTo = contentRangeTo;
            return this;
        }

        public FileChunkUpload build() {
            return new FileChunkUpload(this);
        }
    }
}
