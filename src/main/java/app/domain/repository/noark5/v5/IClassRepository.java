package app.domain.repository.noark5.v5;

import app.domain.noark5.Class;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface IClassRepository
        extends CrudRepository<Class, UUID> {

    Optional<Class> findBySystemId(UUID systemId);

    long deleteByOrganisation(String organisation);
}
