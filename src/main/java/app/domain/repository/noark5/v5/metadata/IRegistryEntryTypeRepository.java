package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.RegistryEntryType;

public interface IRegistryEntryTypeRepository
        extends IMetadataRepository<RegistryEntryType, String> {

}
