package app.domain.repository.noark5.v5.admin;

import app.domain.noark5.admin.DeleteLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IDeleteLogRepository extends
        CrudRepository<DeleteLog, UUID> {

    DeleteLog findBySystemId(UUID systemId);
}
