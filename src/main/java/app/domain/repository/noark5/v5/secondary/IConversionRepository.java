package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.Conversion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IConversionRepository extends
        CrudRepository<Conversion, UUID> {
    Conversion findBySystemId(UUID systemId);
}
