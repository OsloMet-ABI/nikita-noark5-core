package app.domain.repository.noark5.v5;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.DocumentObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IDocumentObjectRepository extends
        PagingAndSortingRepository<DocumentObject, UUID>, CrudRepository<DocumentObject, UUID> {
    DocumentObject findBySystemId(UUID systemId);

    Page<DocumentObject> findByOrganisation(String organisation, Pageable pageable);
    Long countByReferenceDocumentDescriptionAndVariantFormatCode(
            DocumentDescription documentDescription, String variantFormatCode);
}
