package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.DocumentStatus;

public interface IDocumentStatusRepository
        extends IMetadataRepository<DocumentStatus, String> {

}
