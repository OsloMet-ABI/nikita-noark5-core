package app.domain.repository.noark5.v5;

import app.domain.noark5.File;
import app.domain.noark5.casehandling.RegistryEntry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface IRegistryEntryRepository extends
        CrudRepository<RegistryEntry, UUID> {

    RegistryEntry findBySystemId(UUID systemId);

    List<RegistryEntry> findByReferenceFile(File file);

    long deleteByOrganisation(String organisation);

    Long countByReferenceFile(File file);
}
