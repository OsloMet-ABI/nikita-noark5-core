package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.CoordinateSystem;

public interface ICoordinateSystemRepository
        extends IMetadataRepository<CoordinateSystem, String> {
}
