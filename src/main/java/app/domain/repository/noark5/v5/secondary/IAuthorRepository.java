package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IAuthorRepository
        extends CrudRepository<Author, UUID> {

    Author findBySystemId(UUID systemId);
}
