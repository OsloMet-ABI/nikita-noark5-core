package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.DocumentMedium;

public interface IDocumentMediumRepository
        extends IMetadataRepository<DocumentMedium, String> {

}
