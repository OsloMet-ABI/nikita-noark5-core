package app.domain.repository.noark5.v5;

import app.domain.noark5.ClassificationSystem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IClassificationSystemRepository extends
        CrudRepository<ClassificationSystem, UUID> {

    ClassificationSystem findBySystemId(UUID systemId);

    long deleteByOrganisation(String organisation);
}
