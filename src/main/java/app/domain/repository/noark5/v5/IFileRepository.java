package app.domain.repository.noark5.v5;

import app.domain.noark5.File;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IFileRepository extends
        CrudRepository<File, UUID> {

    File findBySystemId(UUID systemId);

    long deleteByOrganisation(String organisation);
}
