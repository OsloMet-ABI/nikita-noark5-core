package app.domain.repository.noark5.v5.casehandling;

import app.domain.noark5.File;
import app.domain.noark5.casehandling.RecordNote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface IRecordNoteRepository extends
        CrudRepository<RecordNote, String> {

    RecordNote findBySystemId(UUID systemId);

    List<RecordNote> findByReferenceFile(File file);

    long deleteByOrganisation(String organisation);

    long countByOrganisation(String organisation);
}
