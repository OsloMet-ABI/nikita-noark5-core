package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.ElectronicSignatureVerified;

public interface IElectronicSignatureVerifiedRepository
        extends IMetadataRepository<ElectronicSignatureVerified, String> {

}
