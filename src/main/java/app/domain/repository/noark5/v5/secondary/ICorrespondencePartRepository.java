package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.domain.repository.noark5.v5.NoarkEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface ICorrespondencePartRepository extends
        NoarkEntityRepository<CorrespondencePart, UUID> {

    int deleteByOrganisation(String organisation);
}
