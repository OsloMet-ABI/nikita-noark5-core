package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.MeetingRegistrationStatus;

public interface IMeetingRegistrationStatusRepository
        extends IMetadataRepository<MeetingRegistrationStatus, String> {

}
