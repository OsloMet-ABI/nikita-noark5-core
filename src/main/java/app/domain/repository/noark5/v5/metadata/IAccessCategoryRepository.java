package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.AccessCategory;

public interface IAccessCategoryRepository
        extends IMetadataRepository<AccessCategory, String> {

}
