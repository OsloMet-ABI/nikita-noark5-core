package app.domain.repository.noark5.v5.admin;

import app.domain.noark5.admin.AdministrativeUnit;
import app.domain.noark5.admin.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface IAdministrativeUnitRepository
        extends CrudRepository<AdministrativeUnit, UUID> {

    AdministrativeUnit findFirstByOrderByCreatedDateAsc();

    AdministrativeUnit findBySystemId(UUID systemId);

    List<AdministrativeUnit> findByAdministrativeUnitName(
            String administrativeUnitName);

    AdministrativeUnit findByUsersInAndDefaultAdministrativeUnit(
            Set<User> user, Boolean defaultAdministrativeUnit);

    long deleteByOrganisation(String organisation);
}
