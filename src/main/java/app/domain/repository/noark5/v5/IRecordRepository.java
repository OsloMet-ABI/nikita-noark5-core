package app.domain.repository.noark5.v5;

import app.domain.noark5.RecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IRecordRepository extends
        JpaRepository<RecordEntity, UUID> {

    RecordEntity getReferenceById(UUID systemId);

    RecordEntity findBySystemId(UUID systemId);

    long deleteByOrganisation(String organisation);
}
