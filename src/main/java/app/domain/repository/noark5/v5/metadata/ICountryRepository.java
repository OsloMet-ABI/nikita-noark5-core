package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.Country;

public interface ICountryRepository
        extends IMetadataRepository<Country, String> {

}
