package app.domain.repository.noark5.v5;

import app.domain.noark5.casehandling.CaseFile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ICaseFileRepository
        extends CrudRepository<CaseFile, UUID> {

    CaseFile findBySystemId(UUID systemId);

    long deleteByOrganisation(String organisation);
}
