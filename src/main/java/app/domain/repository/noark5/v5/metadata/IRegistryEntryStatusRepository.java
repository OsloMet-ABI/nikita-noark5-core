package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.RegistryEntryStatus;

public interface IRegistryEntryStatusRepository
        extends IMetadataRepository<RegistryEntryStatus, String> {

}
