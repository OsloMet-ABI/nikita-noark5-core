package app.domain.repository.noark5.v5;

import app.domain.noark5.FondsCreator;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IFondsCreatorRepository extends
        CrudRepository<FondsCreator, UUID> {

    FondsCreator findBySystemId(UUID systemId);

    long deleteByOrganisation(String organisation);
}
