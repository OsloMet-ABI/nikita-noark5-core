package app.domain.repository.noark5.v5;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.RecordEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IDocumentDescriptionRepository extends
        CrudRepository<DocumentDescription, UUID> {

    DocumentDescription findBySystemId(UUID systemId);

    Long countByReferenceRecordEntity(RecordEntity record);

    long deleteByOrganisation(String organisation);
}
