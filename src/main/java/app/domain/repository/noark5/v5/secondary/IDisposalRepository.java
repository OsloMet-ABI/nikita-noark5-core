package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.Disposal;
import app.domain.repository.noark5.v5.NoarkEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IDisposalRepository extends
        NoarkEntityRepository<Disposal, UUID> {
}
