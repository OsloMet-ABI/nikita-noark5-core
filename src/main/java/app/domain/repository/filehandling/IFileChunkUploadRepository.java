package app.domain.repository.filehandling;


import app.domain.filehandling.FileChunkUpload;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IFileChunkUploadRepository
        extends CrudRepository<FileChunkUpload, UUID> {
}
