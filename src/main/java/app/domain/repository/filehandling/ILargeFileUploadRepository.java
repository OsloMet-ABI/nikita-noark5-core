package app.domain.repository.filehandling;

import app.domain.filehandling.LargeFileUploadRegister;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ILargeFileUploadRepository
        extends CrudRepository<LargeFileUploadRegister, UUID> {
}
