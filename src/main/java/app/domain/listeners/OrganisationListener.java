package app.domain.listeners;

import app.domain.noark5.SystemIdEntity;
import app.service.IUserHandling;
import jakarta.persistence.PrePersist;
import org.springframework.stereotype.Component;

/**
 * Listener for handling organisation-related events for entities.
 * <p>
 * This class listens for the {@link PrePersist} event and sets the organisation
 * identifier on the provided {@link SystemIdEntity} before it is persisted to the database.
 * It utilises the {@link IUserHandling} interface to retrieve the current user's organisation Id.
 * </p>
 */
@Component
public class OrganisationListener {

    IUserHandling userHandling;

    public OrganisationListener(IUserHandling userHandling) {
        this.userHandling = userHandling;
    }

    /**
     * Sets the organisation identifier on the given {@link SystemIdEntity} before it is persisted.
     * <p>
     * This method is invoked automatically by the JPA provider when an entity is about to be persisted.
     * It retrieves the current user's organization ID using the {@link IUserHandling} service
     * and sets it on the provided systemIdEntity.
     * </p>
     *
     * @param systemIdEntity the {@link SystemIdEntity} instance to which the organization ID will be set
     */
    @PrePersist
    public void setOrganisationId(SystemIdEntity systemIdEntity) {
        String userId = userHandling.getOrganisation();
        systemIdEntity.setOrganisation(userId);
    }
}
