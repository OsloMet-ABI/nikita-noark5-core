package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.TABLE_MEETING_REGISTRATION_STATUS;
import static app.utils.constants.N5ResourceMappings.MEETING_REGISTRATION_STATUS;

// Noark 5v5 Møteregistreringsstatus
@Entity
@Table(name = TABLE_MEETING_REGISTRATION_STATUS)
public class MeetingRegistrationStatus
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public MeetingRegistrationStatus() {
    }

    public MeetingRegistrationStatus(String code, String codename) {
        super(code, codename);
    }

    public MeetingRegistrationStatus(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return MEETING_REGISTRATION_STATUS;
    }
}
