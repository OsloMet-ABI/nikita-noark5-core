package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_DOCUMENT_MEDIUM;
import static app.utils.constants.Constants.TABLE_DOCUMENT_MEDIUM;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_MEDIUM;

// Noark 5v5 dokumentmedium
@Entity
@Table(name = TABLE_DOCUMENT_MEDIUM)
public class DocumentMedium
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public DocumentMedium() {
    }

    public DocumentMedium(String code, String codename) {
        super(code, codename);
    }

    public DocumentMedium(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return DOCUMENT_MEDIUM;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_DOCUMENT_MEDIUM;
    }
}
