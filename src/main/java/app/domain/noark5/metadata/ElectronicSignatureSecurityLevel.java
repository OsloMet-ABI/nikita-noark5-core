package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_ELECTRONIC_SIGNATURE_SECURITY_LEVEL;
import static app.utils.constants.Constants.TABLE_ELECTRONIC_SIGNATURE_SECURITY_LEVEL;
import static app.utils.constants.N5ResourceMappings.ELECTRONIC_SIGNATURE_SECURITY_LEVEL;

// Noark 5v5 elektroniskSignaturSikkerhetsnivå
@Entity
@Table(name = TABLE_ELECTRONIC_SIGNATURE_SECURITY_LEVEL)
public class ElectronicSignatureSecurityLevel
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public ElectronicSignatureSecurityLevel() {
    }

    public ElectronicSignatureSecurityLevel(String code, String codename) {
        super(code, codename);
    }

    public ElectronicSignatureSecurityLevel(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return ELECTRONIC_SIGNATURE_SECURITY_LEVEL;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_ELECTRONIC_SIGNATURE_SECURITY_LEVEL;
    }
}
