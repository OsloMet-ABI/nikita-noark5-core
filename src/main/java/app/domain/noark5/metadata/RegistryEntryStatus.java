package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_REGISTRY_ENTRY_STATUS;
import static app.utils.constants.Constants.TABLE_REGISTRY_ENTRY_STATUS;
import static app.utils.constants.N5ResourceMappings.REGISTRY_ENTRY_STATUS;

// Noark 5v5 journalposttype
@Entity
@Table(name = TABLE_REGISTRY_ENTRY_STATUS)
public class RegistryEntryStatus
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public RegistryEntryStatus() {
    }

    public RegistryEntryStatus(String code, String codename) {
        super(code, codename);
    }

    public RegistryEntryStatus(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return REGISTRY_ENTRY_STATUS;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_REGISTRY_ENTRY_STATUS;
    }
}
