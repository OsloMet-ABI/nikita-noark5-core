package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_SCREENING_DOCUMENT;
import static app.utils.constants.Constants.TABLE_SCREENING_DOCUMENT;
import static app.utils.constants.N5ResourceMappings.SCREENING_DOCUMENT;

// Noark 5v5 Skjermingdokument
@Entity
@Table(name = TABLE_SCREENING_DOCUMENT)
public class ScreeningDocument
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public ScreeningDocument() {
    }

    public ScreeningDocument(String code, String codename) {
        super(code, codename);
    }

    public ScreeningDocument(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return SCREENING_DOCUMENT;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_SCREENING_DOCUMENT;
    }
}
