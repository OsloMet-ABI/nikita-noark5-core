package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.TABLE_MEETING_FILE_TYPE;
import static app.utils.constants.N5ResourceMappings.MEETING_FILE_TYPE;

// Noark 5v5 Møtesakstype
@Entity
@Table(name = TABLE_MEETING_FILE_TYPE)
public class MeetingFileType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public MeetingFileType() {
    }

    public MeetingFileType(String code, String codename) {
        super(code, codename);
    }

    public MeetingFileType(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return MEETING_FILE_TYPE;
    }
}
