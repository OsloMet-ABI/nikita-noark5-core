package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_COMMENT_TYPE;
import static app.utils.constants.Constants.TABLE_COMMENT_TYPE;
import static app.utils.constants.N5ResourceMappings.COMMENT_TYPE;

// Noark 5v5 Merknadstype
@Entity
@Table(name = TABLE_COMMENT_TYPE)
public class CommentType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public CommentType() {
    }

    public CommentType(String code, String codename) {
        super(code, codename);
    }

    public CommentType(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return COMMENT_TYPE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_COMMENT_TYPE;
    }
}
