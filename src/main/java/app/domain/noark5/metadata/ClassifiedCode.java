package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_CLASSIFIED_CODE;
import static app.utils.constants.Constants.TABLE_CLASSIFIED_CODE;
import static app.utils.constants.N5ResourceMappings.CLASSIFIED_CODE;

// Noark 5v5 graderingskode
@Entity
@Table(name = TABLE_CLASSIFIED_CODE)
public class ClassifiedCode
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public ClassifiedCode() {
    }

    public ClassifiedCode(String code, String codename) {
        super(code, codename);
    }

    public ClassifiedCode(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return CLASSIFIED_CODE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_CLASSIFIED_CODE;
    }
}
