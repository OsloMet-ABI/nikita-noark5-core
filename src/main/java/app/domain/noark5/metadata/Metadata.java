package app.domain.noark5.metadata;

//Can we make a different ID, but require code to be indexed and unique?

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IMetadataEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.NoarkEntity;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.payload.builder.noark5.metadata.MetadataLinksBuilder;
import app.webapp.payload.deserializers.noark5.metadata.MetadataDeserializer;
import app.webapp.payload.links.metadata.MetadataLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.NOARK_METADATA_PATH;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Created by tsodring on 3/23/17.
 */
@MappedSuperclass
@JsonDeserialize(using = MetadataDeserializer.class)
@LinksPacker(using = MetadataLinksBuilder.class)
@LinksObject(using = MetadataLinks.class)
public class Metadata
        extends NoarkEntity
        implements IMetadataEntity {

    /**
     * M??? - kode (xs:string)
     */
    @Id
    @Column(name = CODE_ENG, length = 60)
    @JsonProperty(CODE)
    protected String code;

    /**
     * M??? - inaktiv (xs:boolean)
     */
    @Column(name = "inactive")
    @JsonProperty(CODE_INACTIVE)
    protected Boolean inactive = false;

    /**
     * M??? - kodenavn (xs:string)
     */
    @Column(name = CODE_NAME_ENG)
    @JsonProperty(CODE_NAME)
    protected String codeName;

    public Metadata() {
    }

    public Metadata(String code, String codename) {
        setCode(code);
        setCodeName(codename);
    }

    public Metadata(String code) {
        setCode(code);
        setCodeName(null);
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getCodeName() {
        return codeName;
    }

    @Override
    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    @Override
    public Boolean getInactive() {
        return inactive;
    }

    @Override
    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }


    @Override
    public String toString() {
        return "Metadata{" +
                "code='" + code + '\'' +
                ", inactive='" + inactive + '\'' +
                ", codeName='" + codeName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Metadata rhs = (Metadata) other;
        return new EqualsBuilder()
                .append(code, rhs.code)
                .append(codeName, rhs.codeName)
                .append(inactive, rhs.inactive)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(code)
                .append(codeName)
                .append(inactive)
                .toHashCode();
    }

    @Override
    public String getBaseTypeName() {
        return "Metadata";
    }

    @Override
    public String getBaseRel() {
        return null;
    }

    @Override
    public String getIdentifier() {
        return getCode();
    }

    // All Metadata entities belong to "metadata".
    // All children pick up the value from here
    @Override
    public String getFunctionalTypeName() {
        return NOARK_METADATA_PATH;
    }

    @Override
    public void setBaseTypeName(String baseTypeName) {

    }

    @Override
    public void setBaseRel(String baseRel) {

    }

    @Override
    public String setFunctionalTypeName(String functionalTypeName) {
        return null;
    }

    public void createReference(@NotNull INoarkEntity entity,
                                @NotNull String referenceType) {
        // I really should be overridden. Currently throwing an Exception if I
        // am not overridden as nikita is unable to process this
        throw new NikitaMalformedInputDataException("Error when trying to " +
                "create a reference between entities");
    }
}
