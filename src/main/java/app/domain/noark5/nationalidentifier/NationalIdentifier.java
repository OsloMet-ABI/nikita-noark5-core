package app.domain.noark5.nationalidentifier;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.builder.noark5.nationalidentifier.NationalIdentifierLinksBuilder;
import app.webapp.payload.links.nationalidentifier.NationalIdentifierLinks;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.*;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_NATIONAL_IDENTIFIER)
@LinksPacker(using = NationalIdentifierLinksBuilder.class)
@LinksObject(using = NationalIdentifierLinks.class)
public class NationalIdentifier
        extends SystemIdEntity {

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = NATIONAL_IDENTIFIER_FILE_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private File referenceFile;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = NATIONAL_IDENTIFIER_RECORD_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private RecordEntity referenceRecordEntity;

    public File getReferenceFile() {
        return referenceFile;
    }

    public void setReferenceFile(File referenceFile) {
        this.referenceFile = referenceFile;
    }

    public RecordEntity getReferenceRecordEntity() {
        return referenceRecordEntity;
    }

    public void setReferenceRecord(RecordEntity referenceRecordEntity) {
        this.referenceRecordEntity = referenceRecordEntity;
    }
}
