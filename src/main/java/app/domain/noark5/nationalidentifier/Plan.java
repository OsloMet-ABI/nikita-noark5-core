package app.domain.noark5.nationalidentifier;

import app.domain.annotation.ANationalIdentifier;
import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.nationalidentifier.IPlanEntity;
import app.domain.noark5.metadata.Country;
import app.webapp.payload.builder.noark5.nationalidentifier.PlanLinksBuilder;
import app.webapp.payload.deserializers.noark5.nationalidentifier.PlanDeserializer;
import app.webapp.payload.links.nationalidentifier.PlanLinks;
import app.webapp.payload.serializers.noark5.nationalidentifier.PlanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_PLAN;
import static app.utils.constants.Constants.TABLE_PLAN;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Note this should be implemented shuch that only one of
 * kommunenummer, fylkenummer and land can have a value
 */
@Entity
@Table(name = TABLE_PLAN)
@JsonSerialize(using = PlanSerializer.class)
@JsonDeserialize(using = PlanDeserializer.class)
@LinksPacker(using = PlanLinksBuilder.class)
@LinksObject(using = PlanLinks.class)
@ANationalIdentifier(name = PLAN)
public class Plan
        extends NationalIdentifier
        implements IPlanEntity {

    /**
     * M??? - kommunenummer (xs:string)
     */
    @Column(name = MUNICIPALITY_NUMBER)
    @JsonProperty(MUNICIPALITY_NUMBER)

    String municipalityNumber;

    /**
     * M??? - fylkenummer (xs:string)
     */
    @Column(name = COUNTY_NUMBER_ENG)
    @JsonProperty(COUNTY_NUMBER)

    String countyNumber;

    /**
     * M??? - landkode code (xs:string)
     */
    @Column(name = "country_code")

            String countryCode;

    /**
     * M??? - landkode code name (xs:string)
     */
    @Column(name = "country_code_name")

            String countryCodeName;

    /**
     * M??? - planidentifikasjon (xs:string)
     */
    @Column(name = PLAN_IDENTIFICATION_ENG, nullable = false)
    @JsonProperty(PLAN_IDENTIFICATION)

    String planIdentification;

    @Override
    public String getMunicipalityNumber() {
        return municipalityNumber;
    }

    @Override
    public void setMunicipalityNumber(String municipalityNumber) {
        this.municipalityNumber = municipalityNumber;
    }

    @Override
    public String getCountyNumber() {
        return countyNumber;
    }

    @Override
    public void setCountyNumber(String countyNumber) {
        this.countyNumber = countyNumber;
    }

    @Override
    public Country getCountry() {
        if (null == countryCode)
            return null;
        return new Country(countryCode, countryCodeName);
    }

    @Override
    public void setCountry(Country country) {
        if (null != country) {
            this.countryCode = country.getCode();
            this.countryCodeName = country.getCodeName();
        } else {
            this.countryCode = null;
            this.countryCodeName = null;
        }
    }

    @Override
    public String getPlanIdentification() {
        return planIdentification;
    }

    @Override
    public void setPlanIdentification(String planIdentification) {
        this.planIdentification = planIdentification;
    }

    @Override
    public String getBaseTypeName() {
        return PLAN;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_PLAN;
    }

    @Override
    public String toString() {
        return "Plan{" + super.toString() + '\'' +
                "municipalityNumber='" + municipalityNumber + '\'' +
                ", countyNumber='" + countyNumber + '\'' +
                ", countryCode=" + countryCode +
                ", countryCodeName=" + countryCodeName +
                ", planIdentification='" + planIdentification + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Plan rhs = (Plan) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(municipalityNumber, rhs.municipalityNumber)
                .append(countyNumber, rhs.countyNumber)
                .append(countryCode, rhs.countryCode)
                .append(countryCodeName, rhs.countryCodeName)
                .append(planIdentification, rhs.planIdentification)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(municipalityNumber)
                .append(countyNumber)
                .append(countryCode)
                .append(countryCodeName)
                .append(planIdentification)
                .toHashCode();
    }
}

