package app.domain.noark5.nationalidentifier;

import app.domain.annotation.ANationalIdentifier;
import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.nationalidentifier.IDNumberEntity;
import app.webapp.payload.builder.noark5.nationalidentifier.DNumberLinksBuilder;
import app.webapp.payload.deserializers.noark5.nationalidentifier.DNumberDeserializer;
import app.webapp.payload.links.nationalidentifier.DNumberLinks;
import app.webapp.payload.serializers.noark5.nationalidentifier.DNumberSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_D_NUMBER;
import static app.utils.constants.Constants.TABLE_D_NUMBER;
import static app.utils.constants.N5ResourceMappings.*;

@Entity
@Table(name = TABLE_D_NUMBER)
@JsonSerialize(using = DNumberSerializer.class)
@JsonDeserialize(using = DNumberDeserializer.class)
@LinksPacker(using = DNumberLinksBuilder.class)
@LinksObject(using = DNumberLinks.class)
@ANationalIdentifier(name = D_NUMBER)
public class DNumber
        extends PersonIdentifier
        implements IDNumberEntity {

    /**
     * M??? - DNummer (xs:string)
     */
    @Column(name = D_NUMBER_FIELD_ENG, nullable = false)
    @JsonProperty(D_NUMBER_FIELD)
    private String dNumber;

    @Override
    public String getdNumber() {
        return dNumber;
    }

    @Override
    public void setdNumber(String dNumber) {
        this.dNumber = dNumber;
    }

    @Override
    public String getBaseTypeName() {
        return D_NUMBER;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_D_NUMBER;
    }

    @Override
    public String toString() {
        return "DNumber{" + super.toString() + '\'' +
                "dNumber='" + dNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        DNumber rhs = (DNumber) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(dNumber, rhs.dNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(dNumber)
                .toHashCode();
    }
}
