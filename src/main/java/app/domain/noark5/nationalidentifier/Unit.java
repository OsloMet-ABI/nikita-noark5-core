package app.domain.noark5.nationalidentifier;

import app.domain.annotation.ANationalIdentifier;
import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.nationalidentifier.IUnitEntity;
import app.webapp.payload.builder.noark5.nationalidentifier.UnitLinksBuilder;
import app.webapp.payload.deserializers.noark5.nationalidentifier.UnitDeserializer;
import app.webapp.payload.links.nationalidentifier.UnitLinks;
import app.webapp.payload.serializers.noark5.nationalidentifier.UnitSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_NI_UNIT;
import static app.utils.constants.Constants.TABLE_UNIT;
import static app.utils.constants.N5ResourceMappings.*;

@Entity
@Table(name = TABLE_UNIT)
@JsonSerialize(using = UnitSerializer.class)
@JsonDeserialize(using = UnitDeserializer.class)
@LinksPacker(using = UnitLinksBuilder.class)
@LinksObject(using = UnitLinks.class)
@ANationalIdentifier(name = UNIT_IDENTIFIER_ENG_OBJECT)
public class Unit
        extends NationalIdentifier
        implements IUnitEntity {

    /**
     * M??? - organisasjonsnummer (xs:string)
     */
    @Column(name = ORGANISATION_NUMBER_ENG, nullable = false)
    @JsonProperty(ORGANISATION_NUMBER)

    private String organisationNumber;

    @Override
    public String getUnitIdentifier() {
        return organisationNumber;
    }

    @Override
    public void setUnitIdentifier(String organisationNumber) {
        this.organisationNumber = organisationNumber;
    }

    @Override
    public String getBaseTypeName() {
        return NI_UNIT;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_NI_UNIT;
    }

    @Override
    public String toString() {
        return "Unit{" + super.toString() + '\'' +
                "organisationNumber='" + organisationNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Unit rhs = (Unit) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(organisationNumber, rhs.organisationNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(organisationNumber)
                .toHashCode();
    }
}
