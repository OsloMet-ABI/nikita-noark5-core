package app.domain.noark5.nationalidentifier;

import app.domain.annotation.ANationalIdentifier;
import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.nationalidentifier.IBuildingEntity;
import app.webapp.payload.builder.noark5.nationalidentifier.BuildingLinksBuilder;
import app.webapp.payload.deserializers.noark5.nationalidentifier.BuildingDeserializer;
import app.webapp.payload.links.nationalidentifier.BuildingLinks;
import app.webapp.payload.serializers.noark5.nationalidentifier.BuildingSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_BUILDING;
import static app.utils.constants.Constants.TABLE_BUILDING;
import static app.utils.constants.N5ResourceMappings.*;
@Entity
@Table(name = TABLE_BUILDING)
@JsonSerialize(using = BuildingSerializer.class)
@JsonDeserialize(using = BuildingDeserializer.class)
@LinksPacker(using = BuildingLinksBuilder.class)
@LinksObject(using = BuildingLinks.class)
@ANationalIdentifier(name = BUILDING)
public class Building
        extends NationalIdentifier
        implements IBuildingEntity {

    /**
     * M??? bygningsnummer - (xs:integer)
     */
    @Column(name = BUILDING_NUMBER_ENG, nullable = false)
    @JsonProperty(BUILDING_NUMBER)
    Integer buildingNumber;

    /**
     * M??? endringsloepenummer - (xs:integer)
     */
    @Column(name = BUILDING_CHANGE_NUMBER)
    @JsonProperty(BUILDING_CHANGE_NUMBER)
    Integer runningChangeNumber;

    @Override
    public Integer getBuildingNumber() {
        return buildingNumber;
    }

    @Override
    public void setBuildingNumber(Integer buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    @Override
    public Integer getRunningChangeNumber() {
        return runningChangeNumber;
    }

    @Override
    public void setRunningChangeNumber(Integer runningChangeNumber) {
        this.runningChangeNumber = runningChangeNumber;
    }

    @Override
    public String getBaseTypeName() {
        return BUILDING;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_BUILDING;
    }

    @Override
    public String toString() {
        return "Building{" +
                "buildingNumber=" + buildingNumber +
                ", runningChangeNumber=" +
                runningChangeNumber +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Building rhs = (Building) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(buildingNumber, rhs.buildingNumber)
                .append(runningChangeNumber,
                        rhs.runningChangeNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(buildingNumber)
                .append(runningChangeNumber)
                .toHashCode();
    }
}
