package app.domain.noark5;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IEventLogEntity;
import app.domain.noark5.admin.User;
import app.webapp.payload.builder.noark5.EventLogLinksBuilder;
import app.webapp.payload.deserializers.noark5.EventLogDeserializer;
import app.webapp.payload.links.EventLogLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_EVENT_LOG)
@JsonDeserialize(using = EventLogDeserializer.class)
@LinksPacker(using = EventLogLinksBuilder.class)
@LinksObject(using = EventLogLinks.class)
public class EventLog
        extends SystemIdEntity
    implements IEventLogEntity
{
    private static final long serialVersionUID = 1L;

    /**
     * M680 - referanseArkivenhet (xs:string/SystemID/UUID)
     */
    @Column(name = REFERENCE_ARCHIVE_UNIT_ENG)
    @JsonProperty(REFERENCE_ARCHIVE_UNIT)
    private UUID referenceArchiveUnitSystemId;

    /**
     * M??? - referanseEndretAv (xs:string/SystemID/UUID)
     */
    private UUID eventInitiatorSystemId;

    @ManyToOne
    private User referenceEventInitiator;

    private String relReferenceArchiveUnit;
    private String hrefReferenceArchiveUnit;


    // Link to Archive Unit (aka SystemIdEntity)
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = SYSTEM_ID_ENTITY_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private SystemIdEntity referenceSystemIdEntity;

    /**
     * WARNING: Calling this method will cause a massive join operation that fails
     * in MySQL/Mariadb. This is expected behavior as the SystemIdEntity uses
     * '@Inheritance(strategy = JOINED)'. We have tried discriminators etc., to see
     * if we can avoid the join across all tables, but alas it is not possible to
     * avoid it.
     *
     * @return referenceArchiveUnitSystemId as a UUID
     */
    @Override
    public UUID getReferenceArchiveUnitSystemId() {
        return referenceArchiveUnitSystemId;
    }

    @Override
    public void setReferenceArchiveUnitSystemId(UUID referenceArchiveUnitSystemId) {
        this.referenceArchiveUnitSystemId = referenceArchiveUnitSystemId;
    }

    @Override
    public OffsetDateTime getEventDate() {
        return getLastModifiedDate();
    }

    @Override
    public void setEventDate(OffsetDateTime changedDate) {
        setLastModifiedDate(changedDate);
    }

    @Override
    public String getEventInitiator() {
        return getLastModifiedBy();
    }

    @Override
    public void setEventInitiator(String eventInitiator) {
        setLastModifiedBy(eventInitiator);
    }

    @Override
    public UUID getEventInitiatorSystemId() {
        return eventInitiatorSystemId;
    }

    @Override
    public void setEventInitiatorSystemId(UUID eventInitiatorSystemId) {
        this.eventInitiatorSystemId = eventInitiatorSystemId;
    }

    @Override
    public User getReferenceEventInitiator() {
        return referenceEventInitiator;
    }

    @Override
    public void setReferenceEventInitiator(User referenceEventInitiator) {
        this.referenceEventInitiator = referenceEventInitiator;
    }

    public String getHrefReferenceArchiveUnit() {
        return hrefReferenceArchiveUnit;
    }

    public void setHrefReferenceArchiveUnit(String hrefReferenceArchiveUnit) {
        this.hrefReferenceArchiveUnit = hrefReferenceArchiveUnit;
    }

    public String getRelReferenceArchiveUnit() {
        return relReferenceArchiveUnit;
    }

    public void setRelReferenceArchiveUnit(String relReferenceArchiveUnit) {
        this.relReferenceArchiveUnit = relReferenceArchiveUnit;
    }

    @Override
    public SystemIdEntity getReferenceArchiveUnit() {
        return referenceSystemIdEntity;
    }

    @Override
    public void setReferenceArchiveUnit
            (SystemIdEntity referenceSystemIdEntity) {
        this.referenceSystemIdEntity = referenceSystemIdEntity;
    }

    @Override
    public String getBaseTypeName() {
        return EVENT_LOG;
    }

    @Override
    public String getBaseRel() {
        return REL_LOGGING_EVENT_LOG;
    }

    @Override
    public String toString() {
        return "EventLog{" + super.toString() +
                ", referenceArchiveUnitSystemId='" + referenceArchiveUnitSystemId + '\'' +
                ", referenceEventInitiator='" + referenceEventInitiator + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        EventLog rhs = (EventLog) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(referenceArchiveUnitSystemId, rhs.referenceArchiveUnitSystemId)
                .append(referenceEventInitiator, rhs.referenceEventInitiator)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(referenceArchiveUnitSystemId)
                .append(referenceEventInitiator)
                .toHashCode();
    }
}
