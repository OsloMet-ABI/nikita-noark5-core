package app.domain.noark5;

import app.domain.annotation.OrganisationId;
import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NoarkConcurrencyException;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serial;
import java.time.OffsetDateTime;

import static app.utils.constants.Constants.NOARK_FONDS_STRUCTURE_PATH;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@MappedSuperclass
public class NoarkEntity
        implements INoarkEntity {
    @Serial
    private static final long serialVersionUID = 1L;

    @Version
    @Column(name = "version")
    private Long version;

    /**
     * M600 - opprettetDato (xs:dateTime)
     */
    @CreatedDate
    @Column(name = CREATED_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    @JsonProperty(CREATED_DATE)
    private OffsetDateTime createdDate;

    /**
     * M601 - opprettetAv (xs:string)
     */
    @CreatedBy
    @Column(name = CREATED_BY_ENG)
    @JsonProperty(CREATED_BY)
    private String createdBy;

    /**
     * M682 - endretDato (xs:dateTime)
     */
    @LastModifiedDate
    @Column(name = LAST_MODIFIED_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime lastModifiedDate;

    /**
     * M683 - endretAv (xs:string)
     */
    @LastModifiedBy
    @Column(name = LAST_MODIFIED_BY_ENG)
    private String lastModifiedBy;

    @OrganisationId
    @Column
    private String organisation;

    @Override
    public String getOrganisation() {
        return organisation;
    }

    @Override
    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    @Override
    public Long getVersion() {
        return version;
    }

    @Override
    public void setVersion(Long version) {
        if (!this.version.equals(version)) {
            throw new NoarkConcurrencyException(
                    "Concurrency Exception. Old version [" + this.version +
                            "], new version [" + version + "]");
        }
    }

    /**
     * Utility method to set version of a default/template object e.g., a
     * Default File object. Such an object will not have a version value as
     * it is never persisted to the database. override must be set to true if
     * you want to manually assign a value. This is also for readability in
     * the code.
     *
     * @param version  the value to set, should be -1
     * @param override that it is OK to override the value
     */
    @Override
    public void setVersion(Long version, Boolean override) {
        if (override) {
            this.version = version;
        }
    }

    @Override
    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    @Override
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Override
    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    @Override
    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    // You should not use, me. All subclasses must implement this themselves
    public String getBaseTypeName() {
        return null;
    }

    @Override
    public String getBaseRel() {
        return null;
    }

    @Override
    public String getIdentifier() {
        return null;
    }

    // Most entities belong to arkivstruktur. These entities pick the value
    // up here
    @Override
    public String getFunctionalTypeName() {
        return NOARK_FONDS_STRUCTURE_PATH;
    }

    @Override
    public void createReference(
            @NotNull INoarkEntity entity,
            @NotNull String referenceType) {
        // I really should be overridden. Currently throwing an Exception if I
        // am not overriden as nikita is unable to process this
        throw new NikitaMalformedInputDataException("Error when trying to " +
                "create a reference between entities");
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        NoarkEntity rhs = (NoarkEntity) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(createdBy, rhs.getCreatedBy())
                .append(createdDate, rhs.getCreatedDate())
                .isEquals();
    }

    @Override
    public String toString() {
        return "NoarkEntity{" +
                ", version=" + version +
                ", lastModifiedDate=" + lastModifiedDate +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", createdDate=" + createdDate +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}
