package app.domain.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.IStorageLocationEntity;
import app.domain.noark5.*;
import app.webapp.payload.builder.noark5.secondary.StorageLocationLinksBuilder;
import app.webapp.payload.deserializers.noark5.secondary.StorageLocationDeserializer;
import app.webapp.payload.links.secondary.StorageLocationLinks;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashSet;
import java.util.Set;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_STORAGE_LOCATION;
import static app.utils.constants.Constants.TABLE_STORAGE_LOCATION;
import static app.utils.constants.N5ResourceMappings.*;

@Entity
@Table(name = TABLE_STORAGE_LOCATION)
@JsonDeserialize(using = StorageLocationDeserializer.class)
@LinksPacker(using = StorageLocationLinksBuilder.class)
@LinksObject(using = StorageLocationLinks.class)
public class StorageLocation
        extends SystemIdEntity
        implements IStorageLocationEntity {

    // Links to Fonds
    @ManyToMany(mappedBy = REFERENCE_STORAGE_LOCATION)
    private final Set<Fonds> referenceFonds = new HashSet<>();
    // Links to Series
    @ManyToMany(mappedBy = REFERENCE_STORAGE_LOCATION)
    private final Set<Series> referenceSeries = new HashSet<>();
    // Links to File
    @ManyToMany(mappedBy = REFERENCE_STORAGE_LOCATION)
    private final Set<File> referenceFile = new HashSet<>();
    // Links to Record
    @ManyToMany(mappedBy = REFERENCE_STORAGE_LOCATION)
    @JsonIgnore
    private final Set<RecordEntity> referenceRecordEntity = new HashSet<>();
    /**
     * M301 - oppbevaringssted (xs:string)
     */
    @Column(name = STORAGE_LOCATION_ENG)
    @JsonProperty(STORAGE_LOCATION)
    private String storageLocation;

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public Set<Fonds> getReferenceFonds() {
        return referenceFonds;
    }

    public void addReferenceFonds(Fonds fonds) {
        this.referenceFonds.add(fonds);
        fonds.getReferenceStorageLocation().add(this);
    }

    public void removeReferenceFonds(Fonds fonds) {
        this.referenceFonds.remove(fonds);
        fonds.getReferenceStorageLocation().remove(this);
    }

    public Set<Series> getReferenceSeries() {
        return referenceSeries;
    }

    public void addReferenceSeries(Series series) {
        this.referenceSeries.add(series);
        series.getReferenceStorageLocation().add(this);
    }

    public void removeReferenceSeries(Series series) {
        this.referenceSeries.remove(series);
        series.getReferenceStorageLocation().remove(this);
    }

    public Set<File> getReferenceFile() {
        return referenceFile;
    }

    public void addReferenceFile(File file) {
        this.referenceFile.add(file);
        file.getReferenceStorageLocation().add(this);
    }

    public void removeReferenceFile(File file) {
        this.referenceFile.remove(file);
        file.getReferenceStorageLocation().remove(this);
    }

    public Set<RecordEntity> getReferenceRecordEntity() {
        return referenceRecordEntity;
    }

    public void addReferenceRecord(RecordEntity record) {
        this.referenceRecordEntity.add(record);
        record.getReferenceStorageLocation().add(this);
    }

    public void removeReferenceRecord(RecordEntity record) {
        this.referenceRecordEntity.remove(record);
        record.getReferenceStorageLocation().remove(this);
    }

    @Override
    public String getBaseTypeName() {
        return STORAGE_LOCATION;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_STORAGE_LOCATION;
    }

    @Override
    public String toString() {
        return "StorageLocation{" + super.toString() +
                ", storageLocation='" + storageLocation + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        StorageLocation rhs = (StorageLocation) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(storageLocation, rhs.storageLocation)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(storageLocation)
                .toHashCode();
    }
}
