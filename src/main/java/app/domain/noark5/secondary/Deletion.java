package app.domain.noark5.secondary;

import app.domain.interfaces.entities.IDeletionEntity;
import app.domain.noark5.DocumentDescription;
import app.domain.noark5.Series;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.metadata.DeletionType;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static app.utils.constants.Constants.TABLE_DELETION;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;
@Entity
@Table(name = TABLE_DELETION)
public class Deletion
        extends SystemIdEntity
        implements IDeletionEntity {

    /**
     * M??? - slettingstype code (xs:string)
     */
    @Column(name = "deletion_type_code")
    private String deletionTypeCode;

    /**
     * M089 - slettingstype code name (xs:string)
     */
    @Column(name = "deletion_type_code_name")
    private String deletionTypeCodeName;

    /**
     * M614 - slettetAv (xs:string)
     */
    @Column(name = DELETION_BY_ENG)
    @JsonProperty(DELETION_BY)

    private String deletionBy;

    /**
     * M613 slettetDato (xs:dateTime)
     */
    @Column(name = DELETION_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    @JsonProperty(DELETION_DATE)
    private OffsetDateTime deletionDate;

    // TODO add 'referanseSlettetAv'

    // Links to Series
    @OneToMany(mappedBy = "referenceDeletion")
    private List<Series> referenceSeries = new ArrayList<>();

    // Links to DocumentDescription
    @OneToMany(mappedBy = "referenceDeletion")
    private List<DocumentDescription>
            referenceDocumentDescription = new ArrayList<>();

    public DeletionType getDeletionType() {
        if (null == deletionTypeCode)
            return null;
        return new DeletionType(deletionTypeCode, deletionTypeCodeName);
    }

    public void setDeletionType(DeletionType deletionType) {
        if (null != deletionType) {
            this.deletionTypeCode = deletionType.getCode();
            this.deletionTypeCodeName = deletionType.getCodeName();
        } else {
            this.deletionTypeCode = null;
            this.deletionTypeCodeName = null;
        }
    }

    public String getDeletionBy() {
        return deletionBy;
    }

    public void setDeletionBy(String deletionBy) {
        this.deletionBy = deletionBy;
    }

    public OffsetDateTime getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(OffsetDateTime deletionDate) {
        this.deletionDate = deletionDate;
    }

    @Override
    public String getBaseTypeName() {
        return DELETION;
    }

    @Override
    public String getBaseRel() {
        return DELETION; // TODO, should it have a relation key?
    }

    public List<Series> getReferenceSeries() {
        return referenceSeries;
    }

    public void setReferenceSeries(List<Series> referenceSeries) {
        this.referenceSeries = referenceSeries;
    }

    public void addSeries(Series series) {
        this.referenceSeries.add(series);
        series.setReferenceDeletion(this);
    }

    public void removeSeries(Series series) {
        this.referenceSeries.remove(series);
        series.setReferenceDeletion(null);
    }

    public List<DocumentDescription> getReferenceDocumentDescription() {
        return referenceDocumentDescription;
    }

    public void setReferenceDocumentDescription(
            List<DocumentDescription> referenceDocumentDescription) {
        this.referenceDocumentDescription = referenceDocumentDescription;
    }

    public void addDocumentDescription(
            DocumentDescription documentDescription) {
        this.referenceDocumentDescription.add(documentDescription);
        documentDescription.setReferenceDeletion(this);
    }

    public void removeDocumentDescription(
            DocumentDescription documentDescription) {
        this.referenceDocumentDescription.remove(documentDescription);
        documentDescription.setReferenceDeletion(null);
    }

    @Override
    public String toString() {
        return "Deletion{" + super.toString() +
                "deletionDate=" + deletionDate +
                ", deletionBy='" + deletionBy + '\'' +
                ", deletionTypeCode='" + deletionTypeCode + '\'' +
                ", deletionTypeCodeName='" + deletionTypeCodeName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Deletion rhs = (Deletion) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(deletionDate, rhs.deletionDate)
                .append(deletionBy, rhs.deletionBy)
                .append(deletionTypeCode, rhs.deletionTypeCode)
                .append(deletionTypeCodeName, rhs.deletionTypeCodeName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(deletionDate)
                .append(deletionBy)
                .append(deletionTypeCode)
                .append(deletionTypeCodeName)
                .toHashCode();
    }
}
