package app.domain.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.admin.IUserEntity;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.builder.noark5.admin.UserLinksBuilder;
import app.webapp.payload.deserializers.noark5.admin.UserDeserializer;
import app.webapp.payload.links.admin.UserLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
@Table(name = TABLE_USER)
@JsonDeserialize(using = UserDeserializer.class)
@LinksPacker(using = UserLinksBuilder.class)
@LinksObject(using = UserLinks.class)
public class User
        extends SystemIdEntity
        implements IUserEntity {

    @Column(unique = true, length = 100)
    @NotNull
    private String username;

    @Column(name = USER_FIRST_NAME, length = 50)
    @Size(min = 1, max = 50)
    private String firstname;

    @Column(name = USER_LAST_NAME, length = 50)
    @Size(min = 1, max = 50)
    private String lastname;

    /**
     * M602 - avsluttetDato (xs:dateTime)
     */
    @Column(name = FINALISED_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    @JsonProperty(FINALISED_DATE)
    private OffsetDateTime finalisedDate;

    /**
     * M603 - avsluttetAv (xs:string)
     */
    @Column(name = FINALISED_BY_ENG)
    @JsonProperty(FINALISED_BY)
    private String finalisedBy;

    @ManyToMany(mappedBy = "users")
    private Set<AdministrativeUnit> administrativeUnits = new HashSet<>();

    @Override
    public OffsetDateTime getFinalisedDate() {
        return finalisedDate;
    }

    @Override
    public void setFinalisedDate(OffsetDateTime finalisedDate) {
        this.finalisedDate = finalisedDate;
    }

    @Override
    public String getFinalisedBy() {
        return finalisedBy;
    }

    @Override
    public void setFinalisedBy(String finalisedBy) {
        this.finalisedBy = finalisedBy;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Set<AdministrativeUnit> getAdministrativeUnits() {
        return administrativeUnits;
    }

    @Override
    public User getUser() {
        return this;
    }

    @Override
    public String getFunctionalTypeName() {
        return NOARK_ADMINISTRATION_PATH;
    }

    @Override
    public String getBaseTypeName() {
        return USER;
    }

    @Override
    public String getBaseRel() {
        return REL_ADMIN_USER;
    }

    @Override
    public String toString() {
        return "User{" + super.toString() +
                "username='" + username + '\'' +
                ", finalisedDate=" + finalisedDate +
                ", finalisedBy='" + finalisedBy + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        User rhs = (User) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(finalisedBy, rhs.finalisedBy)
                .append(finalisedDate, rhs.finalisedDate)
                .append(username, rhs.username)
                .append(firstname, rhs.firstname)
                .append(lastname, rhs.lastname)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(finalisedBy)
                .append(finalisedDate)
                .append(username)
                .append(firstname)
                .append(lastname)
                .toHashCode();
    }
}
