package app.domain.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IChangeLogEntity;
import app.domain.noark5.EventLog;
import app.webapp.payload.builder.noark5.ChangeLogLinksBuilder;
import app.webapp.payload.deserializers.noark5.ChangeLogDeserializer;
import app.webapp.payload.links.ChangeLogLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.util.UUID;

import static app.utils.constants.Constants.REL_LOGGING_CHANGE_LOG;
import static app.utils.constants.Constants.TABLE_CHANGE_LOG;
import static app.utils.constants.N5ResourceMappings.*;

@Entity
@Table(name = TABLE_CHANGE_LOG)
@JsonDeserialize(using = ChangeLogDeserializer.class)
@LinksPacker(using = ChangeLogLinksBuilder.class)
@LinksObject(using = ChangeLogLinks.class)
public class ChangeLog
        extends EventLog
        implements IChangeLogEntity {

    /**
     * M681 - referanseMetadata (xs:string)
     */
    @Column(name = REFERENCE_METADATA_ENG)
    @JsonProperty(REFERENCE_METADATA)
    private String referenceMetadata;

    /**
     * M??? - referanseEndretAv (xs:string/SystemID/UUID)
     */
    private UUID changedBySystemId;

    @ManyToOne
    private User referenceChangedBy;

    /**
     * M684 - tidligereVerdi (xs:string)
     */
    @Column(name = OLD_VALUE_ENG)
    @JsonProperty(OLD_VALUE)
    private String oldValue;

    /**
     * M685 - nyVerdi (xs:string)
     */
    @Column(name = NEW_VALUE_ENG)
    @JsonProperty(NEW_VALUE)
    private String newValue;


    @Override
    public String getOldValue() {
        return oldValue;
    }

    @Override
    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    @Override
    public String getNewValue() {
        return newValue;
    }

    @Override
    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @Override
    public String getReferenceMetadata() {
        return referenceMetadata;
    }

    @Override
    public void setReferenceMetadata(String referenceMetadata) {
        this.referenceMetadata = referenceMetadata;
    }

    @Override
    public String getBaseTypeName() {
        return CHANGE_LOG;
    }

    @Override
    public String getBaseRel() {
        return REL_LOGGING_CHANGE_LOG;
    }
}
