package app.domain.noark5.meeting;

import app.domain.noark5.SystemIdEntity;
import jakarta.persistence.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.MEETING_PARTICIPANT;
import static jakarta.persistence.FetchType.LAZY;
@Entity
@Table(name = TABLE_MEETING_PARTICIPANT)
public class MeetingParticipant
        extends SystemIdEntity {

    /**
     * M372 - moetedeltakerNavn (xs:string)
     */
    @Column(name = "meeting_participant_name")
    private String meetingParticipantName;

    /**
     * M373 - moetedeltakerFunksjon (xs:string)
     */
    @Column(name = "meeting_participant_function")
    private String meetingParticipantFunction;

    // Link to MeetingFile
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = MEETING_PARTICIPANT_FILE_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private MeetingFile referenceMeetingFile;


    public String getMeetingParticipantName() {
        return meetingParticipantName;
    }

    public void setMeetingParticipantName(String meetingParticipantName) {
        this.meetingParticipantName = meetingParticipantName;
    }

    public String getMeetingParticipantFunction() {
        return meetingParticipantFunction;
    }

    public void setMeetingParticipantFunction(
            String meetingParticipantFunction) {
        this.meetingParticipantFunction = meetingParticipantFunction;
    }

    @Override
    public String getBaseTypeName() {
        return MEETING_PARTICIPANT;
    }

    public MeetingFile getReferenceMeetingFile() {
        return referenceMeetingFile;
    }

    public void setReferenceMeetingFile(MeetingFile referenceMeetingFile) {
        this.referenceMeetingFile = referenceMeetingFile;
    }

    @Override
    public String toString() {
        return "MeetingParticipant{" + super.toString() +
                "meetingParticipantFunction='" +
                meetingParticipantFunction + '\'' +
                ", meetingParticipantName='" + meetingParticipantName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        MeetingParticipant rhs = (MeetingParticipant) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(meetingParticipantFunction,
                        rhs.meetingParticipantFunction)
                .append(meetingParticipantName, rhs.meetingParticipantName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(meetingParticipantFunction)
                .append(meetingParticipantName)
                .toHashCode();
    }
}
