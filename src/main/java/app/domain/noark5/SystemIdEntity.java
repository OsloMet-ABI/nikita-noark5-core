package app.domain.noark5;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.ISystemId;
import app.domain.listeners.OrganisationListener;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static app.utils.constants.Constants.DISCRIMINATOR_COLUMN_NAME;
import static app.utils.constants.Constants.NOARK_FONDS_STRUCTURE_PATH;
import static app.utils.constants.N5ResourceMappings.SYSTEM_ID_ENG;
import static jakarta.persistence.CascadeType.REMOVE;
import static jakarta.persistence.DiscriminatorType.STRING;
import static jakarta.persistence.InheritanceType.JOINED;
import static org.hibernate.annotations.UuidGenerator.Style.TIME;


@Entity
@Inheritance(strategy = JOINED)
@DiscriminatorColumn(
        name = DISCRIMINATOR_COLUMN_NAME,
        discriminatorType = STRING
)
// Required for @CreatedDate / @CreatedBy / @LastModifiedDate / @LastModifiedBy
@EntityListeners({AuditingEntityListener.class, OrganisationListener.class})
public class SystemIdEntity
        extends NoarkEntity
        implements ISystemId, Comparable<SystemIdEntity> {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * M001 - systemID (xs:string)
     */
    @Id
    @GenericGenerator(name = "uuid-gen", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = SYSTEM_ID_ENG, updatable = false, nullable = false)
    @UuidGenerator(style = TIME)
    private UUID systemId;

    // Links to EventLog
    @OneToMany(mappedBy = "referenceSystemIdEntity",
            cascade = REMOVE, orphanRemoval = true)
    @JsonIgnore
    private List<EventLog> referenceEventLog = new ArrayList<>();

    @Override
    public UUID getSystemId() {
        return systemId;
    }

    @Override
    public void setSystemId(UUID systemId) {
        this.systemId = systemId;
    }

    @Override
    public String getSystemIdAsString() {
        if (null != systemId)
            return systemId.toString();
        else
            return null;
    }

    @Override
    public String getIdentifier() {
        return getSystemIdAsString();
    }

    public List<EventLog> getEventLog() {
        return referenceEventLog;
    }

    public void setEventLog(List<EventLog> referenceEventLog) {
        this.referenceEventLog = referenceEventLog;
    }

    // Used to disassociate the eventlog from a systemIDEntity without
    // deleting the referenced eventlog
    public void emptyEventLog() {
        this.referenceEventLog.clear();
    }

    // Most entities belong to arkivstruktur. These entities pick the value
    // up here
    @Override
    public String getFunctionalTypeName() {
        return NOARK_FONDS_STRUCTURE_PATH;
    }

    @Override
    public void createReference(
            @NotNull INoarkEntity entity,
            @NotNull String referenceType) {
        // I really should be overridden. Currently throwing an Exception if I
        // am not overriden as nikita is unable to process this
        throw new NikitaMalformedInputDataException("Error when trying to " +
                "create a reference between entities");
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(systemId)
                .toHashCode();
    }

    @Override
    public int compareTo(SystemIdEntity otherEntity) {
        if (null == otherEntity) {
            return -1;
        }
        return new CompareToBuilder()
                .append(this.systemId, otherEntity.systemId)
                .toComparison();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        SystemIdEntity rhs = (SystemIdEntity) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(systemId, rhs.getSystemId())
                .isEquals();
    }

    @Override
    public String toString() {
        return "NoarkEntity{" +
                "systemId=" + systemId +
                '}';
    }
}
