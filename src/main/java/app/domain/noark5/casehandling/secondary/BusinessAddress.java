package app.domain.noark5.casehandling.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.interfaces.entities.secondary.ISimpleAddress;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.secondary.PartUnit;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.TABLE_BUSINESS_ADDRESS;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_BUSINESS_ADDRESS)
public class BusinessAddress
        extends SystemIdEntity
        implements ISystemId, ISimpleAddress {

    @Embedded
    private SimpleAddress simpleAddress;

    @OneToOne(fetch = LAZY)
    private CorrespondencePartUnit referenceCorrespondencePartUnit;

    @OneToOne(fetch = LAZY)
    private PartUnit partUnit;

    public SimpleAddress getSimpleAddress() {
        return simpleAddress;
    }

    public void setSimpleAddress(SimpleAddress simpleAddress) {
        this.simpleAddress = simpleAddress;
    }

    public CorrespondencePartUnit getReferenceCorrespondencePartUnit() {
        return referenceCorrespondencePartUnit;
    }

    public void setReferenceCorrespondencePartUnit(
            CorrespondencePartUnit referenceCorrespondencePartUnit) {
        this.referenceCorrespondencePartUnit = referenceCorrespondencePartUnit;
    }

    public PartUnit getPartUnit() {
        return partUnit;
    }

    public void setPartUnit(PartUnit partUnit) {
        this.partUnit = partUnit;
    }
}
