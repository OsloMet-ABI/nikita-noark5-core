package app.domain.noark5;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.IConversion;
import app.domain.interfaces.IElectronicSignature;
import app.domain.interfaces.entities.ICreate;
import app.domain.noark5.metadata.Format;
import app.domain.noark5.metadata.VariantFormat;
import app.domain.noark5.secondary.Conversion;
import app.domain.noark5.secondary.ElectronicSignature;
import app.webapp.payload.builder.noark5.DocumentObjectLinksBuilder;
import app.webapp.payload.deserializers.noark5.DocumentObjectDeserializer;
import app.webapp.payload.links.DocumentObjectLinks;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.CascadeType.*;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_DOCUMENT_OBJECT,
        indexes = @Index(name = "index_filname",
                columnList = "original_filename"))
@JsonDeserialize(using = DocumentObjectDeserializer.class)
@LinksPacker(using = DocumentObjectLinksBuilder.class)
@LinksObject(using = DocumentObjectLinks.class)
public class DocumentObject
        extends SystemIdEntity
        implements ICreate, IElectronicSignature, IConversion {

    /**
     * M005 - versjonsnummer (xs:integer)
     **/
    @NotNull
    @Column(name = DOCUMENT_OBJECT_VERSION_NUMBER_ENG, nullable = false)
    private Integer versionNumber;

    /**
     * M??? - variantformat code (xs:string)
     */
    @NotNull
    @Column(name = "variant_format_code", nullable = false)
    private String variantFormatCode;

    /**
     * M700 - variantformat code name (xs:string)
     */
    @NotNull
    @Column(name = "variant_format_code_name", nullable = false)
    private String variantFormatCodeName;

    /**
     * M??? - format code (xs:string)
     */
    @Column(name = "format_code")
    private String formatCode;

    /**
     * M701 - format code name (xs:string)
     */
    @Column(name = "format_code_name")
    private String formatCodeName;

    /**
     * M702 - formatDetaljer (xs:string)
     */
    @Column(name = DOCUMENT_OBJECT_FORMAT_DETAILS_ENG)
    private String formatDetails;

    /**
     * M218 - referanseDokumentfil (xs:string)
     */
    @Column(name = DOCUMENT_OBJECT_REFERENCE_DOCUMENT_FILE_ENG)
    private String referenceDocumentFile;

    /**
     * M705 - sjekksum (xs:string)
     */
    @Column(name = DOCUMENT_OBJECT_CHECKSUM_ENG)

    private String checksum;

    /**
     * M706 - sjekksumAlgoritme (xs:string)
     */
    @Column(name = DOCUMENT_OBJECT_CHECKSUM_ALGORITHM_ENG)

    private String checksumAlgorithm;

    /**
     * M707 - filstoerrelse (xs:string)
     */
    @Column(name = DOCUMENT_OBJECT_FILE_SIZE_ENG)
    private Long fileSize;

    /**
     * M??? - filnavn (xs:string)
     */
    @Column(name = DOCUMENT_OBJECT_FILE_NAME_ENG, length = 100)

    private String originalFilename;

    /**
     * M??? - mimeType (xs:string)
     */
    @Column(name = DOCUMENT_OBJECT_MIME_TYPE_ENG)

    private String mimeType;

    @Column(columnDefinition = "TEXT")
    private String documentTokens;

    // Link to DocumentDescription
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = DOCUMENT_OBJECT_DOCUMENT_DESCRIPTION_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private DocumentDescription referenceDocumentDescription;

    // Links to Conversion
    @OneToMany(mappedBy = "referenceDocumentObject",
            cascade = {PERSIST, MERGE, REMOVE})
    private List<Conversion> referenceConversion = new ArrayList<>();

    // Link to ElectronicSignature
    //@OneToOne(mappedBy = REFERENCE_DOCUMENT_OBJECT_DB, fetch = LAZY,
    //        cascade = ALL)
    @OneToOne(mappedBy = REFERENCE_DOCUMENT_OBJECT_DB, fetch = LAZY, cascade = ALL)
    private ElectronicSignature referenceElectronicSignature;

    // Location in storage for the file
    @Column(name = "storage_path", nullable = true)
    private String storagePath;


    public Integer getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Integer versionNumber) {
        this.versionNumber = versionNumber;
    }

    public VariantFormat getVariantFormat() {
        if (null == variantFormatCode)
            return null;
        return new VariantFormat(variantFormatCode, variantFormatCodeName);
    }

    public void setVariantFormat(VariantFormat variantFormat) {
        if (null != variantFormat) {
            this.variantFormatCode = variantFormat.getCode();
            this.variantFormatCodeName = variantFormat.getCodeName();
        } else {
            this.variantFormatCode = null;
            this.variantFormatCodeName = null;
        }
    }

    public Format getFormat() {
        if (null == formatCode)
            return null;
        return new Format(formatCode, formatCodeName);
    }

    public void setFormat(Format format) {
        if (null != format) {
            this.formatCode = format.getCode();
            this.formatCodeName = format.getCodeName();
        } else {
            this.formatCode = null;
            this.formatCodeName = null;
        }
    }

    public String getFormatDetails() {
        return formatDetails;
    }

    public void setFormatDetails(String formatDetails) {
        this.formatDetails = formatDetails;
    }

    public String getReferenceDocumentFile() {
        return referenceDocumentFile;
    }

    public void setReferenceDocumentFile(String referenceDocumentFile) {
        this.referenceDocumentFile = referenceDocumentFile;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getChecksumAlgorithm() {
        return checksumAlgorithm;
    }

    public void setChecksumAlgorithm(String checksumAlgorithm) {
        this.checksumAlgorithm = checksumAlgorithm;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDocumentTokens() {
        return documentTokens;
    }

    public void setDocumentTokens(String documentTokens) {
        this.documentTokens = documentTokens;
    }

    @Override
    public String getBaseTypeName() {
        return DOCUMENT_OBJECT;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_DOCUMENT_OBJECT;
    }

    public DocumentDescription getReferenceDocumentDescription() {
        return referenceDocumentDescription;
    }

    public void setReferenceDocumentDescription(
            DocumentDescription referenceDocumentDescription) {
        this.referenceDocumentDescription = referenceDocumentDescription;
    }

    public List<Conversion> getReferenceConversion() {
        return referenceConversion;
    }

    public void setReferenceConversion(List<Conversion> referenceConversion) {
        this.referenceConversion = referenceConversion;
    }

    public void addReferenceConversion(Conversion conversion) {
        this.referenceConversion.add(conversion);
        conversion.setReferenceDocumentObject(this);
    }

    public void removeReferenceConversion(Conversion conversion) {
        this.referenceConversion.remove(conversion);
        conversion.setReferenceDocumentObject(null);
    }

    public ElectronicSignature getReferenceElectronicSignature() {
        return referenceElectronicSignature;
    }

    public void setReferenceElectronicSignature(
            ElectronicSignature referenceElectronicSignature) {
        this.referenceElectronicSignature = referenceElectronicSignature;
    }

    public String getStoragePath() {
        return this.storagePath;
    }

    public void setStoragePath(String storagepath) {
	this.storagePath = storagepath;
    }

    @Override
    public String toString() {
        return "DocumentObject{" + super.toString() +
                ", fileSize=" + fileSize +
                ", checksumAlgorithm='" + checksumAlgorithm + '\'' +
                ", checksum='" + checksum + '\'' +
                ", referenceDocumentFile='" + referenceDocumentFile + '\'' +
                ", formatDetails='" + formatDetails + '\'' +
                ", formatCode='" + formatCode + '\'' +
                ", formatCodeName='" + formatCodeName + '\'' +
                ", variantFormatCode='" + variantFormatCode + '\'' +
                ", variantFormatCodeName='" + variantFormatCodeName + '\'' +
                ", versionNumber=" + versionNumber +
                ", mimeType=" + mimeType +
                ", originalFilename=" + originalFilename +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        DocumentObject rhs = (DocumentObject) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(fileSize, rhs.fileSize)
                .append(checksumAlgorithm, rhs.checksumAlgorithm)
                .append(checksum, rhs.checksum)
                .append(referenceDocumentFile, rhs.referenceDocumentFile)
                .append(formatDetails, rhs.formatDetails)
                .append(formatCode, rhs.formatCode)
                .append(formatCodeName, rhs.formatCodeName)
                .append(variantFormatCode, rhs.variantFormatCode)
                .append(variantFormatCodeName, rhs.variantFormatCodeName)
                .append(versionNumber, rhs.versionNumber)
                .append(mimeType, rhs.mimeType)
                .append(originalFilename, rhs.originalFilename)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(fileSize)
                .append(checksumAlgorithm)
                .append(checksum)
                .append(referenceDocumentFile)
                .append(formatDetails)
                .append(formatCode)
                .append(formatCodeName)
                .append(variantFormatCode)
                .append(variantFormatCodeName)
                .append(versionNumber)
                .append(mimeType)
                .append(originalFilename)
                .toHashCode();
    }
}
