package app.domain.interfaces;

/**
 * Created by tsodring on 12/7/16.
 */
public interface ISingleStorageLocation {
    String getStorageLocation();

    void setStorageLocation(String storageLocation);
}
