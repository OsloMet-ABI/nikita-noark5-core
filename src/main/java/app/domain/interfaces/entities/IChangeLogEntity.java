package app.domain.interfaces.entities;

public interface IChangeLogEntity
        extends IEventLogEntity {

    String getReferenceMetadata();

    void setReferenceMetadata(String referenceMetadata);

    String getOldValue();

    void setOldValue(String oldValue);

    String getNewValue();

    void setNewValue(String newValue);

}
