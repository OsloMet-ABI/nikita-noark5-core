package app.domain.interfaces.entities.secondary;

import app.domain.interfaces.IBSM;

public interface IPartPersonEntity
        extends IGenericPersonEntity, IPartEntity, IBSM {
}
