package app.domain.interfaces.entities.secondary;

import app.domain.noark5.casehandling.secondary.ContactInformation;

/**
 * Created by tsodring on 5/22/17.
 */
public interface ICorrespondencePartPersonEntity
        extends IGenericPersonEntity, ICorrespondencePartEntity {

    ContactInformation getContactInformation();
}
