package app.domain.interfaces.entities;

import app.domain.interfaces.IPrecedence;
import app.domain.noark5.metadata.CaseStatus;

import java.time.OffsetDateTime;

public interface ICaseFileEntity
        extends IFileEntity, IPrecedence {
    Integer getCaseYear();

    void setCaseYear(Integer caseYear);

    Integer getCaseSequenceNumber();

    void setCaseSequenceNumber(Integer caseSequenceNumber);

    OffsetDateTime getCaseDate();

    void setCaseDate(OffsetDateTime caseDate);

    String getCaseResponsible();

    void setCaseResponsible(String caseResponsible);

    String getRecordsManagementUnit();

    void setRecordsManagementUnit(String recordsManagementUnit);

    CaseStatus getCaseStatus();

    void setCaseStatus(CaseStatus caseStatus);

    OffsetDateTime getLoanedDate();

    void setLoanedDate(OffsetDateTime loanedDate);

    String getLoaneLinks();

    void setLoaneLinks(String loaneLinks);
}
