package app.domain.interfaces.entities.nationalidentifier;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.metadata.CoordinateSystem;

public interface IPositionEntity
        extends ISystemId {
    CoordinateSystem getCoordinateSystem();

    void setCoordinateSystem(CoordinateSystem coordinateSystem);

    Double getX();

    void setX(Double x);

    Double getY();

    void setY(Double y);

    Double getZ();

    void setZ(Double z);
}
