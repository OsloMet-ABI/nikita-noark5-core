package app.domain.interfaces.entities.admin;

import app.domain.interfaces.entities.ICreate;
import app.domain.interfaces.entities.IFinalise;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.admin.User;

public interface IUserEntity
        extends INoarkEntity, ICreate, IFinalise {
    String getUsername();

    void setUsername(String username);

    String getFirstname();

    void setFirstname(String firstname);

    String getLastname();

    void setLastname(String lastname);

    User getUser();
}
