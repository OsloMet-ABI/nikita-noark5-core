package app.domain.interfaces.entities.admin;

import app.domain.interfaces.entities.IEventLogEntity;

public interface ICreateLogEntity
        extends IEventLogEntity {
}
