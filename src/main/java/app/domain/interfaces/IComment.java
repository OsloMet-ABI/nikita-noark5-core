package app.domain.interfaces;

import app.domain.noark5.secondary.Comment;

import java.util.Set;

public interface IComment {
    Set<Comment> getReferenceComment();

    void addComment(Comment comment);
}
