package app.domain.interfaces;

import app.domain.noark5.secondary.DocumentFlow;

import java.util.List;

public interface IDocumentFlow {
    List<DocumentFlow> getReferenceDocumentFlow();
    void addDocumentFlow(DocumentFlow documentFlow);
    void removeDocumentFlow(DocumentFlow documentFlow);
}
