package app.service;

import app.domain.noark5.admin.User;
import app.webapp.exceptions.UsernameExistsException;
import app.webapp.payload.links.admin.UserLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IUserService {

    UserLinks createNewUser(@NotNull final User user)
            throws UsernameExistsException;

    UserLinks findAll();

    UserLinks findBySystemID(@NotNull final UUID systemId);

    UserLinks handleUpdate(@NotNull final UUID systemId,
                           @NotNull final User incomingUser);

    User userGetByUsername(@NotNull final String username);

    User userGetBySystemId(@NotNull final UUID systemId);

    void deleteEntity(@NotNull final UUID systemId);

    long deleteAll();

    long deleteByUsername(@NotNull final String username);

    User validateUserReference(
            @NotNull final String type,
            @NotNull final User user,
            @NotNull final String username,
            @NotNull final UUID systemId);

    UserLinks getDefaultUser();
}
