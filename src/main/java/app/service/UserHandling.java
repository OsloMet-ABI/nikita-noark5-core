package app.service;

import app.webapp.exceptions.NikitaMisconfigurationException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Objects;

import static app.utils.constants.DatabaseConstants.DEFAULT_ORGANISATION;
import static app.utils.constants.ErrorMessagesConstants.*;
import static app.utils.constants.HATEOASConstants.CLAIM_ORGANISATION;
import static java.lang.String.format;

/**
 * The {@code UserHandling} class provides functionality to manage user authentication
 * and retrieve user details from JWT (JSON Web Token) claims in Nikita.
 *
 * <p>This class is responsible for extracting user information from the JWT, validating
 * user authentication, and handling claims related to the logged-in user. It interacts
 * with the User repository to fetch user details based on the username extracted from the
 * JWT claims.</p>
 *
 * <p>Key functionalities include:</p>
 * <ul>
 *     <li>Retrieving the currently logged-in user based on the username from the JWT.</li>
 *     <li>Validating user authentication status.</li>
 *     <li>Fetching specific claims from the JWT, with error handling for missing or invalid claims.</li>
 *     <li>Providing default values for certain claims, such as the organization.</li>
 *     <li>Handling exceptions related to misconfigurations in the authentication process.</li>
 * </ul>
 *
 * <p>Configuration for the username attribute is provided via the application properties,
 * allowing flexibility in how user information is extracted from the JWT.</p>
 *
 * @see NikitaMisconfigurationException
 */

@Component
public class UserHandling
        implements IUserHandling {

    @Value("${spring.security.oauth2.client.provider.keycloak.issuer-uri.user-name-attribute:preferred_username}")
    private String usernameAttribute;

    /**
     * Retrieves the username of the currently logged-in user from the JWT claims.
     *
     * @return the username as a String
     * @throws NikitaMisconfigurationException if the username is not found or is blank in the JWT claims
     */
    @Override
    public String getLoggedInUsername() {
        Object usernameObject = getJWTOrThrow().getClaims().get(usernameAttribute);
        if (usernameObject instanceof String && !((String) usernameObject).isBlank()) {
            return (String) usernameObject;
        } else {
            throw createNoUserInTokenException(usernameAttribute);
        }
    }

    /**
     * Retrieves the specified claim as a String if it is available.
     * If the claim is not of type String (e.g., a Map or List), a
     * {@link NikitaMisconfigurationException} is thrown. This method is useful
     * when the current request can continue without the claim value.
     * Note that the claim object can be blank or an empty string;
     * the method will return null to signify the difference between
     * a claim being present with a blank value and the absence of
     * the claim in the payload.
     *
     * @param claim the name of the claim to retrieve
     * @return the claim value as a String if it exists and is of type String, or null if the claim is absent or blank
     * @throws NikitaMisconfigurationException if the claim is not of type String
     */
    @Override
    public String getClaim(String claim) {
        Object claimObject = getJWTOrThrow().getClaims().get(claim);
        if (claimObject instanceof String) {
            return (String) claimObject;
        }
        if (Objects.isNull(claimObject)) {
            return null;
        }
        throw createMisconfigurationException();
    }

    /**
     * Retrieves the specified claim as a String and throws an exception if it is not available, null, or blank. This
     * is useful when we are unable to continue the current request without the claim value and need to throw an
     * exception as early as possible.
     *
     * @param claim the name of the claim to retrieve
     * @return the claim value as a String if it exists and is of type String
     * @throws NikitaMisconfigurationException if the claim is not found or is not a String
     */
    @Override
    public String getClaimOrThrow(String claim) {
        String claimedString = getClaim(claim);
        if (null != claimedString && !claimedString.isBlank()) {
            return claimedString;
        }
        throw createMisconfigurationException();
    }

    /**
     * Retrieves the current HttpServletRequest from the RequestContextHolder.
     *
     * @return the current HttpServletRequest
     * @throws NikitaMisconfigurationException if the request attributes are not available or not of the expected type
     */
    @Override
    public HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            throw createRequestException("Unable to get Request object as RequestAttributes is null");
        }
        if (!(requestAttributes instanceof ServletRequestAttributes)) {
            throw createRequestException("Request attributes are not of type ServletRequestAttributes as RequestAttributes are not of type ServletRequestAttributes");
        }
        return ((ServletRequestAttributes) requestAttributes).getRequest();
    }

    /**
     * Retrieves the organization claim from the JWT. If the claim is not present or is blank,
     * the default organization is returned.
     *
     * <p>This method checks if the organization claim is null or blank. If it is, the method
     * returns a predefined default organization value. The logic here is that it should be
     * possible to run nikita in a single organisation mode. There is a potential issue here
     * where a misconfiguration in the IAM/keycloak results in actual organisation names as well
     * default. </p>
     *
     * @return the organization as a String, or the default organization if the claim is not found or is blank
     */
    @Override
    public String getOrganisation() {
        String organisation = getClaim(CLAIM_ORGANISATION);
        if (organisation == null || organisation.isBlank()) {
            organisation = DEFAULT_ORGANISATION;
        }
        return organisation;
    }

    /**
     * Validates the user authentication by retrieving the current authentication object
     * and checking if the user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise
     * @throws NikitaMisconfigurationException if there is a misconfiguration in the authentication process
     */
    @Override
    public boolean validateUserAuthentication() throws NikitaMisconfigurationException {
        Authentication authentication = getAuthentication();
        return isUserAuthenticated(authentication);
    }

    /**
     * Retrieves the JWT from the current authentication object. If the authentication
     * object is not of type Jwt, a NikitaMisconfigurationException is thrown.
     *
     * @return the Jwt object representing the user's authentication token
     * @throws NikitaMisconfigurationException if the authentication object is null or not of type Jwt
     */
    protected Jwt getJWTOrThrow() {
        Authentication authentication = getAuthenticationOrThrow();
        Object userDetailsObject = authentication.getPrincipal();
        if (userDetailsObject instanceof Jwt jwt) {
            return jwt;
        } else {
            throw createMisconfigurationException();
        }
    }

    /**
     * Retrieves the current authentication object and throws an exception if it is null.
     *
     * @return the current Authentication object
     * @throws NikitaMisconfigurationException if the authentication object is null
     */
    protected Authentication getAuthenticationOrThrow() {
        Authentication authentication = getAuthentication();
        if (authentication == null) {
            throw createMisconfigurationException();
        }
        return authentication;
    }

    /**
     * Retrieves the current {@link Authentication} object from the security context.
     *
     * @return the current {@link Authentication} object, or null if no authentication is present.
     */
    protected Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * Checks if the provided {@link Authentication} object represents an authenticated user.
     * Assumes the authentication object is instantiated. Note: In springboot a call to
     * AnonymousAuthenticationToken::isAuthenticated will return true, even though the user
     * is not authenticated. Therefore, we check if the authentication is an instance of
     * AnonymousAuthenticationToken before we make the decision.
     *
     * @param authentication the {@link Authentication} object to check
     * @return true if the user is authenticated; false otherwise.
     * This method returns false if the authentication is an instance of
     * {@link AnonymousAuthenticationToken}.
     */
    protected boolean isUserAuthenticated(Authentication authentication) {
        if (authentication == null) {
            throw createMisconfigurationException();
        }
        return authentication.isAuthenticated() &&
                !(authentication instanceof AnonymousAuthenticationToken);
    }

    /**
     * Creates a NikitaMisconfigurationException with a standardized error message.
     *
     * @return a new instance of NikitaMisconfigurationException
     */
    protected NikitaMisconfigurationException createMisconfigurationException() {
        return new NikitaMisconfigurationException(format(AUTHENTICATION_CONFIGURATION_PROBLEM,
                getRequest().getServletPath()));
    }

    /**
     * Creates a NikitaMisconfigurationException with a standardized error message.
     *
     * @return a new instance of NikitaMisconfigurationException
     */
    protected NikitaMisconfigurationException createRequestException(String hint) {
        return new NikitaMisconfigurationException(format(AUTHENTICATION_REQUEST_PROBLEM, hint));
    }

    /**
     * Creates a NikitaMisconfigurationException with a standardized error message if it is not possible to find a user
     * in the JWT token that should be in the User table.
     *
     * @return a new instance of NikitaMisconfigurationException
     */
    protected NikitaMisconfigurationException createUserNotFoundException(String username) {
        return new NikitaMisconfigurationException(format(USER_NOT_FOUND, username));
    }

    /**
     * Creates a NikitaMisconfigurationException with a standardized error message if the current HTTP request has
     * a JWT token that it is not possible to retrieve the username from.
     *
     * @return a new instance of NikitaMisconfigurationException
     */
    protected NikitaMisconfigurationException createNoUserInTokenException(String claimName) {
        return new NikitaMisconfigurationException(format(AUTHENTICATION_USER_PROBLEM,
                getRequest().getServletPath(), claimName));
    }
}
