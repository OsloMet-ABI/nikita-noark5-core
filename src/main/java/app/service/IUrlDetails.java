package app.service;

public interface IUrlDetails {
    String getOutgoingAddress();

    String getOIDCConfigurationURL();

    String getAddress();

    String getContextPath();
}
