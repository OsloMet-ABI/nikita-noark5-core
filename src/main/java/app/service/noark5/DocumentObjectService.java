package app.service.noark5;

import app.domain.filehandling.FileChunkUpload;
import app.domain.filehandling.LargeFileUploadRegister;
import app.domain.noark5.DocumentDescription;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.metadata.Format;
import app.domain.noark5.metadata.VariantFormat;
import app.domain.noark5.secondary.Conversion;
import app.domain.repository.filehandling.IFileChunkUploadRepository;
import app.domain.repository.filehandling.ILargeFileUploadRepository;
import app.domain.repository.noark5.v5.IDocumentObjectRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.IDocumentObjectService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.IConversionService;
import app.utils.CommonUtils;
import app.webapp.exceptions.*;
import app.webapp.payload.builder.interfaces.IDocumentObjectLinksBuilder;
import app.webapp.payload.links.DocumentObjectLinks;
import app.webapp.payload.links.secondary.ConversionLinks;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.Detector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static app.utils.CommonUtils.FileUtils.mimeTypeIsConvertible;
import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.ErrorMessagesConstants.*;
import static app.utils.constants.ExceptionDetailsConstants.MISSING_DOCUMENT_DESCRIPTION_ERROR;
import static app.utils.constants.FileConstants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.ARCHIVE_VERSION_CODE;
import static app.utils.constants.N5ResourceMappings.PRODUCTION_VERSION_CODE;
import static java.io.File.separator;
import static java.lang.Long.parseLong;
import static java.lang.String.format;
import static java.security.MessageDigest.getInstance;
import static java.util.UUID.randomUUID;
import static java.util.regex.Pattern.compile;
import static org.springframework.http.HttpHeaders.ACCEPT;

/**
 * The following methods
 * - convertFileFromStorage
 * have been based on code from
 * - https://api.libreoffice.org/examples/java/DocumentHandling/
 * <p>
 * Note: document filenames are stored without reference to the root of the file
 * system. When retrieving a Path to a file associated with a documentObject
 * you have to add the diretoryStoreName in front of the filename
 * See: getDocumentFile(documentObject); how this is done
 */

@Service
public class DocumentObjectService
        extends NoarkService
        implements IDocumentObjectService {

    private final Logger logger =
            LoggerFactory.getLogger(DocumentObjectService.class);

    private final IDocumentObjectRepository documentObjectRepository;
    private final IConversionService conversionService;
    private final IMetadataService metadataService;
    private final IDocumentObjectLinksBuilder documentObjectLinksBuilder;
    private final ILargeFileUploadRepository largeFileUploadRepository;
    private final IFileChunkUploadRepository fileChunkUploadRepository;

    @Value("${nikita.startup.directory-store-name}")
    private String directoryStoreName;
    @Value("${nikita.startup.incoming-directory}")
    private String incomingDirectoryName;
    @Value("${nikita.application.checksum-algorithm}")
    private final String defaultChecksumAlgorithm = "SHA-256";

    public DocumentObjectService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IConversionService conversionService,
            IDocumentObjectRepository documentObjectRepository,
            IMetadataService metadataService,
            IDocumentObjectLinksBuilder documentObjectLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails,
            ILargeFileUploadRepository largeFileUploadRepository,
            IFileChunkUploadRepository fileChunkUploadRepository) {

        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.conversionService = conversionService;
        this.documentObjectRepository = documentObjectRepository;
        this.metadataService = metadataService;
        this.documentObjectLinksBuilder = documentObjectLinksBuilder;
        this.largeFileUploadRepository = largeFileUploadRepository;
        this.fileChunkUploadRepository = fileChunkUploadRepository;
    }

    // All CREATE operations

    @Override
    @Transactional
    public DocumentObjectLinks save(DocumentObject documentObject) {
        Long version =
                documentObjectRepository.
                        countByReferenceDocumentDescriptionAndVariantFormatCode(
                                documentObject.getReferenceDocumentDescription(),
                                documentObject.getVariantFormat().getCode());
        // + 1 because while arrays start at 0, document counts start at 1
        documentObject.setVersionNumber(version.intValue() + 1);
        setFormatIfNull(documentObject);
        validateFormat(documentObject);
        validateVariantFormat(documentObject);
        checkChecksumAlgorithmSetIfNull(documentObject);
        return packAsLinks(documentObjectRepository.save(documentObject));
    }

    @Override
    @Transactional
    public ConversionLinks
    createConversionAssociatedWithDocumentObject(
            @NotNull final UUID systemId,
            @NotNull final Conversion conversion) {
        DocumentObject documentObject = getDocumentObjectOrThrow(systemId);
        documentObject.addReferenceConversion(conversion);
        return conversionService.save(conversion);
    }

    @Override
    @Transactional
    public DocumentObjectLinks convertDocumentToPDF(
            @NotNull final UUID systemId) {
        return packAsLinks(convertDocumentToPDF(
                getDocumentObjectOrThrow(systemId)));
    }

    @Override
    @Transactional
    public DocumentObjectLinks
    handleIncomingFile(@NotNull final UUID systemId)
            throws IOException {
        HttpServletRequest request = getRequest();
        DocumentObject documentObject = getDocumentObjectOrThrow(systemId);
        // Following will be needed for uploading file in chunks
        // String headerContentRange = request.getHeader("content-range");
        // Content-Range:bytes 737280-819199/845769

        // Check that content-length is set, > 0 and in agreement with the
        // value set in documentObject
        if (request.getHeader("content-length") == null) {
            throw new StorageException("Attempt to upload a document without " +
                    "content-length set. The document was attempted to be " +
                    "associated with " + documentObject);
        }
        long contentLength = request.getIntHeader("content-length");
        if (contentLength < 1) {
            throw new StorageException("Attempt to upload a document with 0 " +
                    "or negative content-length set. Actual value was (" +
                    contentLength + "). The document was attempted to be " +
                    "associated with " + documentObject);
        }

        // Check that if the content-type is set it should be in agreement
        // with mimeType value in documentObject
            /*
            String headerContentType = request.getHeader("content-type");
            if (documentObject.getMimeType() != null && !headerContentType.equals(documentObject.getMimeType())) {
                throw new StorageException("Attempt to upload a document with a content-type set in the header ("
                        + contentLength + ") that is not the same as the mimeType in documentObject (" +
                        documentObject.getMimeType() + ").  The document was attempted to be associated with "
                        + documentObject);
            }
*/
        String originalFilename = request.getHeader("X-File-Name");

        if (null != originalFilename) {
            documentObject.setOriginalFilename(originalFilename);
        }

        storeAndCalculateChecksum(request.getInputStream(), documentObject);

        // We need to update the documentObject in the database as checksum
        // and checksum algorithm are set after the document has been uploaded
        return packAsLinks(documentObject);
    }

    @Override
    public DocumentObjectLinks handleIncomingFileChunk(UUID systemID) throws IOException {
        FileChunkUpload chunk = getChunkFromHeader();
        Path incoming = createIncomingFileChunk(systemID, chunk.getContentRangeFrom(),
                chunk.getContentRangeTo());
        InputStream inputStream = getRequest().getInputStream();
        String checksum = copyFileContentsToStorage(inputStream,
                incoming, defaultChecksumAlgorithm,
                chunk.getContentRangeTo() - chunk.getContentRangeFrom());
        if (chunk.isLastChunk()) {
            //finalise
        }
        fileChunkUploadRepository.save(chunk);
        return null;
    }

    private FileChunkUpload getChunkFromHeader() {
        HttpServletRequest request = getRequest();
        String contentRangeHeader = request.getHeader(CONTENT_RANGE);
        String contentType = request.getHeader(CONTENT_TYPE);
        String contentLengthStr = request.getHeader(CONTENT_LENGTH);

        if (null == contentType || contentType.isBlank()) {
            throw new NikitaMalformedInputDataException(CONTENT_TYPE_BLANK);
        }

        Long contentLength = -1L;
        if (null == contentLengthStr || contentLengthStr.isBlank()) {
            throw new NikitaMalformedInputDataException(CONTENT_RANGE_BLANK);
        } else {
            contentLength = parseLong(contentLengthStr);
            if (contentLength <= 0) {
                throw new NikitaMalformedInputDataException(
                        format(CONTENT_LENGTH_NEGATIVE, contentLengthStr));
            }
        }

        Matcher matcher = compile("bytes (\\d+)-(\\d+)/(\\d+)")
                .matcher(contentRangeHeader);

        if (matcher.find()) {
            Long from = parseLong(matcher.group(1));
            Long to = parseLong(matcher.group(2));
            Long total = parseLong(matcher.group(3));
            validateContentRange(from, to, total);
            return new FileChunkUpload.Builder()
                    .setContentLength(contentLength)
                    .setContentLengthTotal(total)
                    .setContentType(contentType)
                    .setContentRangeFrom(from)
                    .setContentRangeTo(to)
                    .build();
        } else {
            throw new NikitaMalformedInputDataException(CONTENT_RANGE_INVALID_PATTERN);
        }
    }

    /**
     * Validates the content range values.
     * <p>
     * Checks that the provided range values are valid for a content range.
     * It ensures that:
     * <ul>
     *     <li>All values (from, to, total) are positive.</li>
     *     <li>The 'from' value is less than the 'to' value.</li>
     * </ul>
     *
     * @param from  the starting position of the content range (inclusive).
     * @param to    the ending position of the content range (exclusive).
     * @param total the total size of the content.
     * @throws NikitaMalformedInputDataException if any of the following conditions are met:
     *                                           <ul>
     *                                               <li>Any of the values (from, to, total) are less than or equal to zero.</li>
     *                                               <li>The 'from' value is greater than or equal to the 'to' value.</li>
     *                                           </ul>
     */
    private void validateContentRange(Long from, Long to, Long total) {
        if (from <= 0 || to <= 0 || total <= 0) {
            throw new NikitaMalformedInputDataException(
                    format(CONTENT_RANGE_NEGATIVE_VALUE, from, to, total));
        }
        if (from >= to) {
            throw new NikitaMalformedInputDataException(
                    format(CONTENT_RANGE_INVALID_NUM_VALUE, from, to));
        }
    }

    @Override
    public void registerChunkedFileUpload(LargeFileUploadRegister LargeFileUploadRegister) {

    }

    // All READ operations

    @Override
    public DocumentObjectLinks findAll() {
        return (DocumentObjectLinks) processODataQueryGet();
    }

    @Override
    public ConversionLinks
    findAllConversionAssociatedWithDocumentObject(@NotNull final UUID systemId) {
        return (ConversionLinks) processODataQueryGet();
    }

    @Override
    public ConversionLinks
    findConversionAssociatedWithDocumentObject(@NotNull final UUID systemId,
                                               @NotNull final UUID subSystemId) {
        DocumentObject documentObject = getDocumentObjectOrThrow(systemId);
        Conversion conversion =
                conversionService.findConversionBySystemId(subSystemId);
        if (null == conversion.getReferenceDocumentObject()
                || conversion.getReferenceDocumentObject() != documentObject) {
            String error = INFO_CANNOT_FIND_OBJECT +
                    " Conversion " + subSystemId +
                    " below DocumentObject " + systemId + ".";
            logger.error(error);
            throw new NoarkEntityNotFoundException(error);
        }
        return conversionService.packAsLinks(conversion);
    }

    @Override
    public DocumentObjectLinks findBySystemId(
            @NotNull final UUID systemId) {
        return packAsLinks(getDocumentObjectOrThrow(systemId));
    }

    @Override
    public Resource loadAsResource(@NotNull final UUID systemId) {
        DocumentObject documentObject =
                getDocumentObjectOrThrow(systemId);
        Path file = Paths.get(directoryStoreName + separator
                + documentObject.getStoragePath());
        if (null == file) {
            throw new StorageFileNotFoundException(
                    "No file connected to " + systemId);
        }
        HttpServletRequest request = getRequest();
        HttpServletResponse response = getResponse();
        // First make sure the file exist
        try {
            Resource resource = new UrlResource(file.toUri());
            if (!resource.exists() && !resource.isReadable()) {
                throw new StorageFileNotFoundException(
                        "Could not read file " + file + " connected to " + systemId);
            }
            // When file exist, figure out how to return it.  Note
            // both format, file name, file size and mime type can be
            // unset until after a file is uploaded.
            String acceptType = request.getHeader(ACCEPT);
            String mimeType = documentObject.getMimeType();
            if (acceptType != null && mimeType != null
                    && !mimeTypeAccepted(mimeType, acceptType)) {
                throw new NoarkNotAcceptableException(
                        "The request [" +
                                request.getRequestURI() + "] is not acceptable. "
                                + "You have issued an Accept: " + acceptType +
                                ", while the mimeType you are trying to retrieve "
                                + "is [" + mimeType + "].");
            }
            if (null == mimeType) {
                mimeType = "application/octet-stream";
                logger.warn("Overriding unset mime-type during download for " +
                        "documentObject [" + documentObject + "]. " +
                        "Setting to [" + mimeType + "].");
            }
            response.setContentType(mimeType);
            response.addHeader("Content-Type", mimeType);
            response.setContentLength(documentObject.getFileSize().intValue());
            if (null != documentObject.getOriginalFilename()) {
                response.addHeader("Content-disposition", "inline; " +
                        "filename=" + documentObject.getOriginalFilename());
            }
            // Once file is uploaded, POST and PUT are no longer allowed
            response.addHeader("Allow", "GET");
            return resource;
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException(
                    "Could not read file " + file + " connected to " + systemId);
        }
    }

    @Transactional
    public DocumentObject convertDocumentToPDF(DocumentObject
                                                       originalDocumentObject) {
        DocumentObject archiveDocumentObject = new DocumentObject();

        try {
            OffsetDateTime now = OffsetDateTime.now();
            Path archiveFile = convertFileFromStorage(originalDocumentObject,
                    archiveDocumentObject);
            // Parent document description
            DocumentDescription documentDescription = originalDocumentObject
                    .getReferenceDocumentDescription();
            // If it's null, throw an exception. You can't create a
            // related documentObject unless it has a document description
            if (documentDescription == null) {
                throw new NoarkEntityNotFoundException(
                        MISSING_DOCUMENT_DESCRIPTION_ERROR);
            }
            // TODO: Double check this. Standard says it's only applicable to
            // archive version, but we increment every time a new document is
            // uploaded to a document description
            archiveDocumentObject.setVersionNumber(
                    originalDocumentObject.getVersionNumber());

            // Set creation details. Logged in user is responsible
            String username = getUser();
            archiveDocumentObject.setCreatedBy(username);
            archiveDocumentObject.setCreatedDate(now);

            // Handle the conversion details
            Conversion conversion = new Conversion();
            // perhaps here capture unoconv --version
            conversion.setConversionTool("LibreOffice via uconov (from Nikita)");
            conversion.setConvertedBy(username);
            conversion.setConvertedDate(now);
            conversion.setConvertedFromFormat(
                    originalDocumentObject.getFormat());
            conversion.setConverteLinksFormat(
                    archiveDocumentObject.getFormat());
            archiveDocumentObject.addReferenceConversion(conversion);

            // Tie the new document object and document description together
            archiveDocumentObject.setReferenceDocumentDescription
                    (documentDescription);
            documentDescription.addDocumentObject(archiveDocumentObject);

            archiveDocumentObject.setChecksum(
                    new DigestUtils(defaultChecksumAlgorithm).
                            digestAsHex(archiveFile.toFile()));
            archiveDocumentObject.setChecksumAlgorithm(
                    defaultChecksumAlgorithm);

            if (archiveDocumentObject.getFileSize() > 0) {
                moveIncomingToStorage(archiveFile, archiveDocumentObject);
                documentObjectRepository.save(archiveDocumentObject);
                return archiveDocumentObject;
            } else {
                logger.error("File size of archive version is not > 0. Not " +
                        "persisting this documentDescription to the database.");
            }
            return null;

        } catch (InterruptedException | IOException e) {
            logger.error("Problem when trying to convert to archive format"
                    + e);
        }
        return archiveDocumentObject;
    }

    protected Path extractTextFromImage(
            DocumentObject productionDocumentObject,
            DocumentObject textDocumentObject)
            throws IOException, InterruptedException {

        // TODO Do four OCR runs with different rotations and pick the
        // best one.

        Path productionVersion = getToFile(productionDocumentObject);

        // set the filename to ocrextract.txt.
        textDocumentObject.setFormat(new Format(FILE_EXTENSION_TEXT_CODE));
        validateFormat(textDocumentObject);

        textDocumentObject.setMimeType(MIME_TYPE_TEXT);
        textDocumentObject.setFormatDetails("OCR");

        textDocumentObject.setVariantFormat
                (new VariantFormat(PRODUCTION_VERSION_CODE));
        validateVariantFormat(textDocumentObject);

        setFilenameAndExtensionForArchiveDocument
                (productionDocumentObject, textDocumentObject);

        // Setting a UUID here as the filename on disk will use this UUID value
        textDocumentObject.setSystemId(randomUUID());
        Path textVersion = createIncomingFile(textDocumentObject);
        String[] cmdArray = new String[5];
        cmdArray[0] = "tesseract";
        cmdArray[1] = "-l";
        cmdArray[2] = "nor";
        cmdArray[3] = productionVersion.toAbsolutePath().toString();
        cmdArray[4] = textVersion.toAbsolutePath().toString();

        try {
            Process p = Runtime.getRuntime().exec(cmdArray);
            p.waitFor();
        } catch (RuntimeException e) {
            logger.error("Error converting document in " +
                    productionDocumentObject + " to text format");
            logger.error(e.toString());
        }

        textDocumentObject.setFileSize(Files.size(textVersion));
        setGeneratedDocumentFilename(textDocumentObject);
        textDocumentObject.setOrganisation(productionDocumentObject.getOrganisation());

        return textVersion;
    }

    @Transactional
    public DocumentObject extractOCRTextFromDocument(DocumentObject
                                                             originalDocumentObject) {
        DocumentObject textDocumentObject = new DocumentObject();

        try {
            OffsetDateTime now = OffsetDateTime.now();
            Path textFile = extractTextFromImage(originalDocumentObject, textDocumentObject);
            Tika tika = new Tika();
            textDocumentObject.setDocumentTokens(tika.parseToString(textFile));
            // Parent document description
            DocumentDescription documentDescription =
                    originalDocumentObject.getReferenceDocumentDescription();
            // If it's null, throw an exception. You can't create a
            // related documentObject unless it has a document description
            if (documentDescription == null) {
                throw new NoarkEntityNotFoundException(
                        MISSING_DOCUMENT_DESCRIPTION_ERROR);
            }
            // TODO: Double check this. Standard says it's only applicable to
            // archive version, but we increment every time a new document is
            // uploaded to a document description
            textDocumentObject.setVersionNumber(
                    originalDocumentObject.getVersionNumber());

            // Set creation details. Logged in user is responsible
            String username = getUser();
            textDocumentObject.setCreatedBy(username);
            textDocumentObject.setCreatedDate(now);

            // Handle the conversion details
            Conversion conversion = new Conversion();
            conversion.setConversionTool("Text extracted using tesseract (from Nikita)");
            conversion.setConvertedBy(username);
            conversion.setConvertedDate(now);
            conversion.setConvertedFromFormat(
                    originalDocumentObject.getFormat());
            conversion.setConverteLinksFormat(
                    textDocumentObject.getFormat());
            textDocumentObject.addReferenceConversion(conversion);

            // Tie the new document object and document description together
            textDocumentObject.setReferenceDocumentDescription
                    (documentDescription);
            documentDescription.addDocumentObject(textDocumentObject);

            textDocumentObject.setChecksum(
                    new DigestUtils(defaultChecksumAlgorithm).
                            digestAsHex(textFile.toFile()));
            textDocumentObject.setChecksumAlgorithm(
                    defaultChecksumAlgorithm);

            if (textDocumentObject.getFileSize() > 0) {
                moveIncomingToStorage(textFile, textDocumentObject);
                return textDocumentObject;
            } else {
                logger.error("File size of text version is not > 0. Not " +
                        "persisting this documentDescription to the database.");
            }
            return null;
        } catch (InterruptedException | IOException | TikaException e) {
            logger.error("Problem when trying to convert to text format"
                    + e);
        }
        return textDocumentObject;
    }

    // All DELETE operations

    /**
     * Delete a conversion object associated with the documentObject.
     *
     * @param systemId           UUID of the documentObject object
     * @param conversionSystemID UUID of the Conversion object
     */
    @Override
    @Transactional
    public void deleteConversion(
            UUID systemId, UUID conversionSystemID) {
        conversionService.deleteConversion(conversionSystemID);
    }

    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId)
            throws IOException {
        DocumentObject documentObject = getDocumentObjectOrThrow(systemId);
        deleteEntity(documentObject);
        deleteFile(documentObject);
    }

    protected void deleteFile(@NotNull DocumentObject documentObject) throws IOException {
        Path path = getToFile(documentObject);
        try {
            Files.deleteIfExists(path);
            String info = "Deleted " + documentObject.getSystemId()
                    + " connected file " + path + ".";
            logger.info(info);
        } catch (IOException e) {
            // Refuse to delete entity if file is not deleted
            logger.error(e.getMessage());
            throw e;
        }
    }

    /**
     * Deletes all {@link DocumentObject} entities in batches while attempting to delete associated files.
     *
     * <p>This method processes {@link DocumentObject} entities in batches of 20 to manage memory efficiently
     * and avoid performance issues when dealing with a large number of records. For each entity, it attempts
     * to delete the associated file first. If the file deletion is successful, the entity is then deleted from
     * the repository. If an error occurs during file deletion, the entity will not be deleted, and the error
     * will be logged.</p>
     *
     * <p>The method returns the total number of errors encountered during the deletion process, which can be
     * useful for monitoring and debugging purposes.</p>
     *
     * @return the total number of errors encountered while attempting to delete entities;
     * returns zero if no errors occurred during the deletion process.
     */
    @Override
    @Transactional
    public long deleteAll() {
        long numberOfErrors = 0L;
        Pageable pageable = PageRequest.of(0, 20);
        Page<DocumentObject> page;
        do {
            page = documentObjectRepository.findByOrganisation(getOrganisation(), pageable);
            List<DocumentObject> documentObjects = page.getContent();

            for (DocumentObject documentObject : documentObjects) {
                try {
                    // If file deletion experiences a problem the documentObject will not be deleted, as an
                    // exception should be thrown
                    deleteFile(documentObject);
                    deleteEntity(documentObject.getSystemId());
                } catch (IOException e) {
                    logger.error(format(BATCH_DELETE_DOCUMENT_OBJECT_FILE_ERROR,
                            documentObject.getSystemIdAsString(), e.getMessage()));
                    numberOfErrors++;
                }
            }
            pageable = pageable.next();
        } while (page.hasNext());
        return numberOfErrors;
    }

    // All UPDATE operations

    /**
     * Updates a DocumentDescription object in the database. First we try to
     * locate the DocumentDescription object. If the DocumentDescription object
     * does not exist a NoarkEntityNotFoundException exception is thrown that
     * the caller has to deal with.
     * <br>
     * After this the values you are allowed to update are copied from the
     * incomingDocumentDescription object to the existingDocumentDescription
     * object and the existingDocumentDescription object will be persisted to
     * the database when the transaction boundary is over.
     * <p>
     * Note, the version corresponds to the version number, when the object
     * was initially retrieved from the database. If this number is not the
     * same as the version number when re-retrieving the DocumentDescription
     * object from the database a NoarkConcurrencyException is thrown. Note.
     * This happens when the call to DocumentDescription.setVersion() occurs.
     * <p>
     * Note: variantFormat amd versionNumber are not nullable
     *
     * @param systemId               systemId of the incoming documentObject
     *                               object
     * @param incomingDocumentObject the incoming documentObject
     * @return the updated documentObject after it is persisted
     */
    @Override
    @Transactional
    public DocumentObjectLinks handleUpdate(
            @NotNull final UUID systemId, @NotNull final DocumentObject incomingDocumentObject) {

        DocumentObject existingDocumentObject =
                getDocumentObjectOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        existingDocumentObject.setFormat(
                incomingDocumentObject.getFormat());
        existingDocumentObject.setFormatDetails(
                incomingDocumentObject.getFormatDetails());
        existingDocumentObject.setOriginalFilename
                (incomingDocumentObject.getOriginalFilename());
        if (null != incomingDocumentObject.getVariantFormat()) {
            existingDocumentObject.setVariantFormat(
                    incomingDocumentObject.getVariantFormat());
        }
        if (null != incomingDocumentObject.getVersionNumber()) {
            existingDocumentObject.setVersionNumber(
                    incomingDocumentObject.getVersionNumber());
        }
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingDocumentObject.setVersion(getETag());
        return packAsLinks(existingDocumentObject);
    }

    @Override
    @Transactional
    public ConversionLinks handleUpdateConversionBySystemId(
            @NotNull final UUID systemId,
            @NotNull final UUID subSystemId,
            @NotNull final Conversion incomingConversion) {
        DocumentObject documentObject = getDocumentObjectOrThrow(systemId);
        Conversion existingConversion =
                conversionService.findConversionBySystemId(subSystemId);
        if (null == existingConversion.getReferenceDocumentObject()
                || existingConversion.getReferenceDocumentObject() != documentObject) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " Conversion " + subSystemId +
                    " below DocumentObject " + systemId + ".";
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        }
        existingConversion
                .setConvertedDate(incomingConversion.getConvertedDate());
        existingConversion
                .setConvertedBy(incomingConversion.getConvertedBy());
        existingConversion
                .setConvertedFromFormat(incomingConversion.getConvertedFromFormat());
        existingConversion
                .setConverteLinksFormat(incomingConversion.getConverteLinksFormat());
        existingConversion
                .setConversionTool(incomingConversion.getConversionTool());
        existingConversion
                .setConversionComment(incomingConversion.getConversionComment());
        return conversionService.packAsLinks(existingConversion);
    }

    // All template operations

    @Override
    public ConversionLinks
    generateDefaultConversion(@NotNull final UUID systemId) {
        return conversionService.generateDefaultConversion(systemId,
                getDocumentObjectOrThrow(systemId));
    }

    @Override
    public DocumentObjectLinks generateDefaultDocumentObject() {
        DocumentObject defaultDocumentObject = new DocumentObject();
        // TODO This is just temporary code as this will have to be
        // replaced if this ever goes into production
        defaultDocumentObject
                .setVariantFormat(new VariantFormat(PRODUCTION_VERSION_CODE));
        validateVariantFormat(defaultDocumentObject);
        defaultDocumentObject.setVersionNumber(1);
        defaultDocumentObject.setVersion(-1L, true);
        return packAsLinks(defaultDocumentObject);
    }

    /**
     * Retrieve mime-type of a file
     *
     * @param file A Path object pointing to the file
     * @return the actual mime-type
     */
    private String getMimeType(Path file) {

        MediaType mediaType;
        TikaInputStream stream = null;
        try {
            TikaConfig config = TikaConfig.getDefaultConfig();
            Detector detector = config.getDetector();

            stream = TikaInputStream.get(file);
            Metadata metadata = new Metadata();
            mediaType = detector.detect(stream, metadata);

            stream.close();
            return mediaType.toString();
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
        return "unknown";
    }

    /**
     * copy the contents of an input stream to an output stream.
     * <p>
     * Note we are currently using copyLarge as there may be large files being
     * uploaded. Perhaps we should consider only using copyLarge if the
     * length is known in advance.
     *
     * @param inputStream    inputstream of incoming document
     * @param incoming       path where the incoming document is to be placed
     * @param documentObject documentObject associated with this document
     */

    private void copyDocumentContentsToIncomingAndSetValues(
            InputStream inputStream, Path incoming,
            DocumentObject documentObject) throws IOException {
        String checksum = copyFileContentsToStorage(inputStream,
                incoming, documentObject.getChecksumAlgorithm(),
                documentObject.getFileSize() == null ? -1 : documentObject.getFileSize());
        checksumSetIfOK(checksum, incoming, documentObject);
    }

    private String copyFileContentsToStorage(
            InputStream inputStream, Path incoming,
            String checksumAlgorithm, Long expectedFilesize) {
        try {
            MessageDigest md = getInstance(checksumAlgorithm);
            // Create a DigestInputStream to be read with the identified
            // checksumAlgorithm. This is done to avoid having to create the
            // checksum after the document has been uploaded
            DigestInputStream digestInputStream = new DigestInputStream(inputStream, md);
            FileOutputStream outputStream = new FileOutputStream(incoming.toFile());

            long bytesTotal;
            try (digestInputStream; outputStream) {
                bytesTotal = IOUtils.copyLarge(digestInputStream, outputStream);
                outputStream.flush();
            }
            checkValues(bytesTotal, incoming, expectedFilesize);
            // Try close without exceptions if copy() threw an exception.
            // swallow any error to expose exceptions from IOUtil.copy()
            // same for outputStream
            // empty
            return getChecksum(digestInputStream);
        } catch (NoSuchAlgorithmException e) {
            throw new StorageException(
                    format(ERROR_CHECKSUM_UNKNOWN, checksumAlgorithm));
        } catch (IOException e) {
            throw new StorageException(
                    format(ERROR_STORAGE_UNKNOWN, incoming.getFileName()));
        }
    }


    private void checkValues(Long bytesTotal, Path incoming,
                             Long fileSize)
            throws IOException {

        // Check that  we actually copied in some data
        if (bytesTotal.equals(0L)) {
            Files.delete(incoming);
            String msg = "The file (" + incoming.getFileName() + ") " +
                    "has 0 length content. Rejecting upload!";
            throw new StorageException(msg);
        }

        // If the client has identified the filesize prior to document
        // upload, check that the number bytes uploaded matches the
        // number of bytes the client said they would upload
        // -1 indicates no value is expected to check
            if (fileSize != -1 && !fileSize.equals(bytesTotal)) {
            Files.delete(incoming);
                String msg = "The  file (" + incoming.getFileName()
                        + ") has length [" + bytesTotal + "] This does not" +
                    "match the value registered in advance [" + fileSize + "]";
            throw new StorageException(msg);
        }
    }

    private Long copyDocumentContentsToIncoming(
            DigestInputStream digestInputStream, OutputStream outputStream)
            throws IOException {

        long bytesTotal;
        try (digestInputStream; outputStream) {
            bytesTotal = IOUtils.copyLarge(digestInputStream, outputStream);
            outputStream.flush();
        }
        // Try close without exceptions if copy() threw an exception.
        // swallow any error to expose exceptions from IOUtil.copy()
        // same for outputStream
        // empty
        return bytesTotal;
    }

    private String getChecksum(DigestInputStream digestInputStream) {
        // Get the digest
        byte[] digest = digestInputStream.getMessageDigest().digest();

        // Convert digest to HEX
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    private void checksumSetIfOK(String newDigest,Path incoming,
                                 DocumentObject documentObject)
            throws IOException {

        String oldDigest = documentObject.getChecksum();

        // If the client had not specified a checksum for the incoming document
        // set the checksum to the value calculated
        if (null == oldDigest) {
            documentObject.setChecksum(newDigest);
        } else if (!oldDigest.equals(newDigest)) {
            Files.delete(incoming);
            String msg = "The file (" + incoming.getFileName() + ") " +
                    "has the following server-side calculated checksum [" +
                    newDigest + "]. This does not match the checksum " +
                    "specified by the client in the documentObject [" +
                    oldDigest + "]. Rejecting upload! This file was being " +
                    "associated with " + documentObject;
            logger.warn(msg);
            throw new StorageException(msg);
        }
    }

    // All HELPER operations

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid DocumentObject back. If there is no
     * valid DocumentObject, an exception is thrown
     *
     * @param systemId systemId of the documentObject to retrieve
     * @return the documentObject with the identified systemId
     */
    protected DocumentObject getDocumentObjectOrThrow(
            @NotNull final UUID systemId) {
        DocumentObject documentObject =
                documentObjectRepository.
                        findBySystemId(systemId);
        if (documentObject == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " DocumentObject, using systemId " +
                    systemId;
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        }
        return documentObject;
    }

    private void validateFormat(DocumentObject documentObject) {
        if (null != documentObject.getFormat()) {
            Format format = (Format) metadataService.findValidMetadata(
                    documentObject.getFormat());
            documentObject.setFormat(format);
        }
    }

    private void validateVariantFormat(DocumentObject documentObject) {
        // Assume value already set, as the deserializer will enforce it.
        VariantFormat variantFormat =
                (VariantFormat) metadataService.findValidMetadata(
                        documentObject.getVariantFormat());
        documentObject.setVariantFormat(variantFormat);
    }

    // Methods related to processing of Documents

    /**
     * Store an incoming file associated with a DocumentObject. When writing the
     * Incoming filestream, calculate the checksum at the same time and
     * update the DocumentObject with referenceToFile, size (bytes), checksum
     * and checksum algorithm
     * <p>
     * inputStream.read calculates the checksum while reading the input file
     * as it is a DigestInputStream
     * <p>
     * The document is first stored in rootLocation/incoming. Once additional
     * processing e.g. checksum generation is finished, the file is moved to
     * its proper location.
     * <p>
     * Note: You can't overwrite an existing documentObject. If you are updating
     * the document, you need to create a new version attached to the
     * documentDescription.
     */
    private void storeAndCalculateChecksum(InputStream inputStream,
                                           DocumentObject documentObject) {

        if (null != documentObject.getStoragePath()) {
            throw new StorageException(
                    "There is already a file associated with " +
                            documentObject);
        }
        try {

            Path incoming = createIncomingFile(documentObject);
            copyDocumentContentsToIncomingAndSetValues(inputStream, incoming,
                    documentObject);

            setGeneratedDocumentFilename(documentObject);
            Tika tika = new Tika();
            InputStream stream = TikaInputStream.get(incoming);
            String mimeType = tika.detect(stream);
            if (!mimeType.equals(documentObject.getMimeType())) {
                logger.warn("Overriding mime-type for documentObject [" +
                        documentObject + "]. Original was [" +
                        documentObject.getMimeType() + "]. Setting to [" +
                        mimeType + "].");
            }
            documentObject.setMimeType(mimeType);
            // Parse document for word tokens
            documentObject.setDocumentTokens(tika.parseToString(stream));
            // TODO find way to detect PRONOM code for a uploaded file.

            setFormatIfNull(documentObject);
            documentObject.setFileSize(Files.size(incoming));
            moveIncomingToStorage(incoming, documentObject);

            // Try to convert the file upon upload. Silently ignore
            // if there is a problem
            if (supportForDocumentConversion(documentObject)) {
                convertDocumentToPDF(documentObject);
            }

            // Run OCR on all images to look for text content
            if (0 == documentObject.getMimeType().indexOf("image/")) {
                extractOCRTextFromDocument(documentObject);
            }
            documentObjectRepository.save(documentObject);
        } catch (IOException | TikaException e) {
            String msg = "When associating an uploaded file with " +
                    documentObject + " the following Exception occurred " +
                    e;
            logger.error(msg);
            throw new StorageException(msg);
        }
    }

    /***
     * set Format values if the documentObject format is null
     *
     * According to API spec format is [0..1] [1..1] so it can be null on the
     * way in but has to have a value on the way out. This method can be
     * called to apply a default UNKNOWN value.
     *
     * @param documentObject The applicable documentobject
     */
    private void setFormatIfNull(DocumentObject documentObject) {
        Format format = documentObject.getFormat();
        if (null == format) {
            logger.warn("Setting format for documentObject [" +
                    documentObject +
                    "] to UNKNOWN after upload.");
            documentObject.setFormat(new Format("UNKNOWN"));
            validateFormat(documentObject);
        }
    }

    /**
     * Check if the checksum the client identifies is one that we support. If
     * it is null, set the checksun value to default. We only support SHA256,
     * however we leave it open that other checksum algorithms may be
     * supported in the future.
     *
     * @param documentObject The documentObject to check the checksum of
     */
    private void checkChecksumAlgorithmSetIfNull(
            DocumentObject documentObject) {
        String currentChecksumAlgorithm = documentObject.getChecksumAlgorithm();
        if (null != currentChecksumAlgorithm &&
                !defaultChecksumAlgorithm.equals(currentChecksumAlgorithm)) {
            throw new NikitaMalformedInputDataException(
                    "The checksum algorithm " +
                            documentObject.getChecksumAlgorithm() +
                            " is not supported");
        } else if (currentChecksumAlgorithm == null) {
            documentObject.setChecksumAlgorithm(defaultChecksumAlgorithm);
        }
    }

    private Path createIncomingFileChunk(UUID systemId, Long from, Long to)
            throws IOException {
        String chunkDirectory = incomingDirectoryName + separator +
                "chunks" + separator + systemId.toString();
        createChunkDirectory(chunkDirectory);
        return createIncomingFile(chunkDirectory + separator +
                from.toString() + "-" + to.toString());
    }

    private Path createIncomingFile(DocumentObject documentObject) throws IOException {
        // Set the filename to be the systemId of the documentObject,
        // effectively locking this in as one-to-one relationship
        return createIncomingFile(incomingDirectoryName +
                separator + documentObject.getSystemIdAsString());
    }

    private void createChunkDirectory(String chunkDirectory) {
        try {
            Files.createDirectories(Path.of(chunkDirectory));
        } catch (IOException e) {
            throw new StorageException(format(ERROR_CHUNK_DIRECTORY, chunkDirectory));
        }
    }
    private Path createIncomingFile(String pathname)
            throws IOException {
        Path incoming = Paths.get(pathname);
        Path path = Files.createFile(incoming);
        checkPathWritable(incoming);
        return path;
    }

    private void checkPathWritable(Path path) {
        // Check if we can write something to the file
        if (!Files.isWritable(path)) {
            throw new StorageException(
                    format(FILE_NOT_WRITABLE, path.getFileName()));
        }
    }

    private Path getToDirectory(DocumentObject documentObject) {
        return Paths.get(directoryStoreName + separator +
                calculateDirectoryStructure(documentObject));
    }

    private Path getToFile(DocumentObject documentObject) {
        return Paths.get(directoryStoreName + separator +
                calculateDirectoryStructure(documentObject) +
                documentObject.getReferenceDocumentFile());
    }

    private String calculateDirectoryStructure(DocumentObject documentObject) {
        String checksum = documentObject.getChecksum();
        if (checksum.length() > 6) {
            return format("%s%s%s%s%s%s",
                    checksum.substring(0, 2), separator,
                    checksum.substring(2, 4), separator,
                    checksum.substring(4, 6), separator);
        }
        return "";
    }

    private void moveIncomingToStorage(Path incoming,
                                       DocumentObject documentObject)
            throws IOException {
        Path toDirectory = getToDirectory(documentObject);
        Files.createDirectories(toDirectory);
        Path toFile = getToFile(documentObject);

        Files.move(incoming, toFile);
        String storagepath = toFile.toString()
                .replaceFirst(Pattern.quote(directoryStoreName), "");
        documentObject.setStoragePath(storagepath);
    }

    private void setGeneratedDocumentFilename(DocumentObject documentObject) {
        String extension = FilenameUtils.getExtension(
                documentObject.getOriginalFilename());
        String documentFilename = documentObject.getSystemIdAsString();
        if (extension != null) {
            documentFilename += "." + extension;
        }
        documentObject.setReferenceDocumentFile(documentFilename);
    }

    protected Path convertFileFromStorage(
            DocumentObject productionDocumentObject,
            DocumentObject archiveDocumentObject)
            throws IOException, InterruptedException {

        Path productionVersion = getToFile(productionDocumentObject);

        // set the filename to the same as the original filename minus
        // the extension. This needs to be a bit more advanced, an own
        // method. Consider the scenario where a file called .htaccess
        // is uploaded, or a file with no file extension

        archiveDocumentObject.setFormat(new Format(FILE_EXTENSION_PDF_CODE));
        validateFormat(archiveDocumentObject);

        archiveDocumentObject.setMimeType(MIME_TYPE_PDF);

        archiveDocumentObject
                .setVariantFormat(new VariantFormat(ARCHIVE_VERSION_CODE));
        validateVariantFormat(archiveDocumentObject);

        setFilenameAndExtensionForArchiveDocument(
                productionDocumentObject, archiveDocumentObject);

        // Setting a UUID here as the filename on disk will use this UUID value
        archiveDocumentObject.setSystemId(randomUUID());
        Path archiveVersion = createIncomingFile(archiveDocumentObject);
        String[] cmdArray = new String[8];
        cmdArray[0] = "unoconv";
        cmdArray[1] = "-f";
        cmdArray[2] = "pdf";

        // Include text formatting information / metadata in generated PDF
        cmdArray[3] = "-e";
        cmdArray[4] = "UseTaggedPDF=1";

        cmdArray[5] = "-o";
        cmdArray[6] = archiveVersion.toAbsolutePath().toString();
        cmdArray[7] = productionVersion.toAbsolutePath().toString();

        try {
            Process p = Runtime.getRuntime().exec(cmdArray);
            p.waitFor();
        } catch (RuntimeException e) {
            logger.error("Error converting document in " +
                    productionDocumentObject + " to archive format");
            logger.error(e.toString());
        }

        // Even though this was set earlier, we set it to the actual
        // detected mime-type
        archiveDocumentObject.setMimeType(getMimeType(archiveVersion));

        archiveDocumentObject.setFileSize(Files.size(archiveVersion));
        setGeneratedDocumentFilename(archiveDocumentObject);
        archiveDocumentObject.setOrganisation(productionDocumentObject.getOrganisation());

        return archiveVersion;
    }

    private void setFilenameAndExtensionForArchiveDocument(
            DocumentObject productionDocumentObject,
            DocumentObject archiveDocumentObject) {

        String originalFilename = FilenameUtils.
                removeExtension(productionDocumentObject.
                        getOriginalFilename());

        if (originalFilename == null) {
            originalFilename = archiveDocumentObject.getSystemIdAsString() + "." +
                    getArchiveFileExtension(archiveDocumentObject);
        } else {
            originalFilename += "." +
                    getArchiveFileExtension(archiveDocumentObject);
        }

        archiveDocumentObject.setOriginalFilename(originalFilename);
    }

    public DocumentObjectLinks packAsLinks(
            @NotNull final DocumentObject documentObject) {
        DocumentObjectLinks documentObjectLinks =
                new DocumentObjectLinks(documentObject);
        applyLinksAndHeader(documentObjectLinks,
                documentObjectLinksBuilder);
        return documentObjectLinks;
    }

    /**
     * Is this mimetype a format we can automatically convert to archive
     * format. If the mimeType is null, false should be returned. This method
     * will always return either true of false.
     *
     * @param documentObject the documentobject containg the mimetype
     * @return true if the mimetype is supported, false otherwise
     */

    private boolean supportForDocumentConversion(
            @NotNull DocumentObject documentObject) {
        return mimeTypeIsConvertible(documentObject.getMimeType());
    }

    /**
     * For a given HTTP Content-Type mime type value, check with the
     * type is compatible with the given HTTP Accept value.
     */
    private Boolean mimeTypeAccepted(String mimeType, String accept) {
        org.springframework.util.MimeType mime =
                org.springframework.http.MediaType.parseMediaType(mimeType);
        List<org.springframework.http.MediaType> acceptTypes =
                org.springframework.http.MediaType.parseMediaTypes(accept);
        boolean match = false;
        for (org.springframework.http.MediaType acceptType : acceptTypes) {
            if (acceptType.isCompatibleWith(mime)) {
                match = true;
            }
        }
        return match;
    }

    /**
     * @param documentObject the documentObject you want a archive version
     *                       mimeType for.
     * @return the documents mimeType
     */
    private String getArchiveFileExtension(DocumentObject documentObject) {
        return CommonUtils.FileUtils.getArchiveFileExtension(
                documentObject.getMimeType());
    }
}
