package app.service.noark5;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.bsm.BSMBase;
import app.domain.noark5.casehandling.secondary.CorrespondencePartInternal;
import app.domain.noark5.casehandling.secondary.CorrespondencePartPerson;
import app.domain.noark5.casehandling.secondary.CorrespondencePartUnit;
import app.domain.noark5.md_other.BSMMetadata;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.nationalidentifier.*;
import app.domain.noark5.secondary.*;
import app.domain.repository.noark5.v5.IDocumentDescriptionRepository;
import app.domain.repository.noark5.v5.IRecordRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.*;
import app.service.interfaces.casehandling.IRecordNoteService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.*;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.model.PatchMerge;
import app.webapp.model.PatchObjects;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.builder.interfaces.IRecordLinksBuilder;
import app.webapp.payload.links.*;
import app.webapp.payload.links.casehandling.*;
import app.webapp.payload.links.nationalidentifier.*;
import app.webapp.payload.links.secondary.*;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static app.utils.NoarkUtils.NoarkEntity.Create.validateDocumentMedium;
import static app.utils.NoarkUtils.NoarkEntity.Create.validateScreening;
import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.Constants.TEST_TITLE;
import static app.utils.constants.N5ResourceMappings.TITLE;
import static java.time.OffsetDateTime.now;
import static java.util.List.copyOf;

@Service
public class RecordService
        extends NoarkService
        implements IRecordService {

    private static final Logger logger =
            LoggerFactory.getLogger(RecordService.class);

    private final IDocumentDescriptionService documentDescriptionService;
    private final IRecordRepository recordRepository;
    private final IBSMService bsmService;
    private final IRecordLinksBuilder recordLinksBuilder;
    private final IDocumentDescriptionRepository documentDescriptionRepository;
    private final IAuthorService authorService;
    private final IKeywordService keywordService;
    private final ICrossReferenceService crossReferenceService;
    private final ICommentService commentService;
    private final ICorrespondencePartService correspondencePartService;
    private final IMetadataService metadataService;
    private final IRecordNoteService recordNoteService;
    private final IRegistryEntryService registryEntryService;
    private final INationalIdentifierService nationalIdentifierService;
    private final IPartService partService;
    private final IScreeningMetadataService screeningMetadataService;
    private final IStorageLocationService storageLocationService;

    public RecordService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IDocumentDescriptionService documentDescriptionService,
            IRecordRepository recordRepository,
            IBSMService bsmService,
            IRecordLinksBuilder recordLinksBuilder,
            IDocumentDescriptionRepository documentDescriptionRepository,
            IAuthorService authorService,
            IKeywordService keywordService,
            ICrossReferenceService crossReferenceService,
            ICommentService commentService,
            ICorrespondencePartService correspondencePartService,
            IMetadataService metadataService,
            IRecordNoteService recordNoteService, IRegistryEntryService registryEntryService, INationalIdentifierService nationalIdentifierService,
            IPartService partService,
            IScreeningMetadataService screeningMetadataService,
            IStorageLocationService storageLocationService,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.documentDescriptionService = documentDescriptionService;
        this.recordRepository = recordRepository;
        this.bsmService = bsmService;
        this.recordLinksBuilder = recordLinksBuilder;
        this.documentDescriptionRepository = documentDescriptionRepository;
        this.authorService = authorService;
        this.keywordService = keywordService;
        this.crossReferenceService = crossReferenceService;
        this.commentService = commentService;
        this.correspondencePartService = correspondencePartService;
        this.metadataService = metadataService;
        this.recordNoteService = recordNoteService;
        this.registryEntryService = registryEntryService;
        this.nationalIdentifierService = nationalIdentifierService;
        this.partService = partService;
        this.screeningMetadataService = screeningMetadataService;
        this.storageLocationService = storageLocationService;
    }

    // All CREATE operations

    @Override
    @Transactional
    public RecordLinks save(@NotNull final RecordEntity record) {
        validateDocumentMedium(metadataService, record);
        validateScreening(metadataService, record);
        bsmService.validateBSMList(record.getReferenceBSMBase());
        return packAsLinks(recordRepository.save(record));
    }

    @Override
    public RecordNoteLinks expanLinksRecordNote(
            @NotNull final UUID systemId,
            @NotNull final PatchMerge patchMerge) {
        return recordNoteService.expandRecorLinksRecordNote(
                getRecordOrThrow(systemId), patchMerge);
    }

    @Override
    public RegistryEntryLinks expanLinksRegistryEntry(
            @NotNull final UUID systemId,
            @NotNull final PatchMerge patchMerge) {
        return registryEntryService.expandRecorLinksRegistryEntry(
                getRecordOrThrow(systemId), patchMerge);
    }

    @Override
    @Transactional
    public DocumentDescriptionLinks
    createDocumentDescriptionAssociatedWithRecord(
            UUID systemId, DocumentDescription documentDescription) {
        RecordEntity record = getReferenceToRecordOrThrow(systemId);
        validateDocumentMedium(metadataService, documentDescription);
        validateScreening(metadataService, documentDescription);
        // Adding 1 as documentNumber starts at 1, not 0
        long documentNumber =
                documentDescriptionRepository.
                        countByReferenceRecordEntity(record) + 1;
        documentDescription.setDocumentNumber((int) documentNumber);
        record.addDocumentDescription(documentDescription);
        documentDescription.setDocumentNumber((int) documentNumber);
        documentDescription.setAssociationDate(now());
        documentDescription.setAssociatedBy(getUser());
        return documentDescriptionService.save(documentDescription);
    }

    /**
     * Create a CorrespondencePartPerson object and associate it with the
     * identified record
     *
     * @param systemId           The systemId of the record object you want to
     *                           create an associated correspondencePartPerson for
     * @param correspondencePart The incoming correspondencePartPerson
     * @return The persisted CorrespondencePartPerson object wrapped as a
     * CorrespondencePartPersonLinks object
     */
    @Override
    @Transactional
    public CorrespondencePartPersonLinks
    createCorrespondencePartPersonAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CorrespondencePartPerson correspondencePart) {
        return correspondencePartService.
                createNewCorrespondencePartPerson(correspondencePart,
                        getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public PartPersonLinks
    createPartPersonAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final PartPerson partPerson) {
        return partService.
                createNewPartPerson(partPerson,
                        getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public PartUnitLinks
    createPartUnitAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final PartUnit partUnit) {
        return partService.
                createNewPartUnit(partUnit, getReferenceToRecordOrThrow(systemId));
    }

    /**
     * Create a CorrespondencePartInternal object and associate it with the
     * identified record
     *
     * @param systemId           The systemId of the record object you want to
     *                           create an associated correspondencePartInternal for
     * @param correspondencePart The incoming correspondencePartInternal
     * @return The persisted CorrespondencePartInternal object wrapped as a
     * CorrespondencePartInternalLinks object
     */
    @Override
    @Transactional
    public CorrespondencePartInternalLinks
    createCorrespondencePartInternalAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CorrespondencePartInternal correspondencePart) {
        return correspondencePartService.
                createNewCorrespondencePartInternal(correspondencePart,
                        getReferenceToRecordOrThrow(systemId));
    }

    /**
     * Create a CorrespondencePartUnit object and associate it with the
     * identified record
     *
     * @param systemId           The systemId of the record object you want to
     *                           create an associated correspondencePartUnit for
     * @param correspondencePart The incoming correspondencePartUnit
     * @return The persisted CorrespondencePartUnit object wrapped as a
     * CorrespondencePartUnitLinks object
     */
    @Override
    @Transactional
    public CorrespondencePartUnitLinks
    createCorrespondencePartUnitAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CorrespondencePartUnit correspondencePart) {
        return correspondencePartService.
                createNewCorrespondencePartUnit(correspondencePart,
                        getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public BuildingLinks
    createBuildingAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final Building building) {
        return nationalIdentifierService.
                createNewBuilding(building, getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public CadastralUnitLinks
    createCadastralUnitAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CadastralUnit cadastralUnit) {
        return nationalIdentifierService.
                createNewCadastralUnit(cadastralUnit, getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public DNumberLinks
    createDNumberAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final DNumber dNumber) {
        return nationalIdentifierService.
                createNewDNumber(dNumber, getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public PlanLinks
    createPlanAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final Plan plan) {
        return nationalIdentifierService.
                createNewPlan(plan, getReferenceToRecordOrThrow(systemId));
    }

    @Override
    public PositionLinks
    createPositionAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final Position position) {
        return nationalIdentifierService.
                createNewPosition(position, getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public SocialSecurityNumberLinks
    createSocialSecurityNumberAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final SocialSecurityNumber socialSecurityNumber) {
        return nationalIdentifierService
                .createNewSocialSecurityNumber(socialSecurityNumber,
                        getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public UnitLinks
    createUnitAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final Unit unit) {
        return nationalIdentifierService.
                createNewUnit(unit, getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public CommentLinks createCommentAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final Comment comment) {
        return commentService.createNewComment
                (comment, getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public KeywordLinks createKeywordAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final Keyword keyword) {
        return keywordService
                .createKeywordAssociatedWithRecord(keyword,
                        getReferenceToRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public ScreeningMetadataLinks createScreeningMetadataAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final Metadata screeningMetadata) {
        RecordEntity record = getReferenceToRecordOrThrow(systemId);
        if (null == record.getReferenceScreening()) {
            throw new NoarkEntityNotFoundException(INFO_CANNOT_FIND_OBJECT +
                    " Screening, associated with Record with systemId " +
                    systemId);
        }
        return screeningMetadataService.createScreeningMetadata(
                record.getReferenceScreening(), screeningMetadata);
    }

    @Override
    @Transactional
    public StorageLocationLinks createStorageLocationAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final StorageLocation storageLocation) {
        RecordEntity record = getReferenceToRecordOrThrow(systemId);
        return storageLocationService
                .createStorageLocationAssociatedWithRecord(
                        storageLocation, record);
    }

    @Override
    @Transactional
    public CrossReferenceLinks createCrossReferenceAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CrossReference crossReference) {
        return crossReferenceService.createCrossReferenceAssociatedWithRecord(
                crossReference, getReferenceToRecordOrThrow(systemId));
    }

    /**
     * Create a RegistryEntryExpansionLinks that can be used when expanding a
     * File to a RegistryEntry. None of the File attributes should be present in
     * the returned payload. So we have a special approach that can be used to
     * achieve this.
     *
     * @return RegistryEntryExpansionLinks
     */
    @Override
    public RegistryEntryExpansionLinks
    generateDefaultValuesToExpanLinksRegistryEntry(@NotNull final UUID systemId) {
        return registryEntryService
                .generateDefaultExpandedRegistryEntry(systemId);
    }

    /**
     * Create a RecordNoteExpansionLinks that can be used when expanding a
     * File to a RecordNote. None of the File attributes should be present in
     * the returned payload. So we have a special approach that can be used to
     * achieve this.
     *
     * @return RecordNoteExpansionLinks
     */
    @Override
    public RecordNoteExpansionLinks
    generateDefaultValuesToExpanLinksRecordNote(@NotNull final UUID systemId) {
        return recordNoteService.generateDefaultExpandedRecordNote(systemId);
    }

    // All READ operations

    @Override
    public AuthorLinks findAllAuthorWithRecordBySystemId(
            @NotNull final UUID systemId) {
        getRecordOrThrow(systemId);
        return (AuthorLinks) processODataQueryGet();
    }

    /**
     * Retrieve all File associated with the record identified by
     * the records systemId.
     *
     * @param systemId systemId of the record
     * @return The parent File packed as a FileLinks
     */
    @Override
    public FileLinks
    findFileAssociatedWithRecord(@NotNull final UUID systemId) {
        return (FileLinks) processODataQueryGet();
    }

    /**
     * Retrieve all Class associated with the record identified by
     * the records systemId.
     *
     * @param systemId systemId of the record
     * @return The parent Class packed as a ClassLinks
     */
    @Override
    public ClassLinks
    findClassAssociatedWithRecord(@NotNull final UUID systemId) {
        return (ClassLinks) processODataQueryGet();
    }

    /**
     * Retrieve all Series associated with the record identified by
     * the records systemId.
     *
     * @param systemId systemId of the record
     * @return The parent Series packed as a SeriesLinks
     */
    @Override
    public SeriesLinks
    findSeriesAssociatedWithRecord(@NotNull final UUID systemId) {
        return (SeriesLinks) processODataQueryGet();
    }

    @Override
    public RecordLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getRecordOrThrow(systemId));
    }

    @Override
    public StorageLocationLinks findStorageLocationAssociatedWithRecord(
            @NotNull final UUID systemID) {
        return (StorageLocationLinks) processODataQueryGet();
    }

    @Override
    public RecordLinks findAll() {
        return (RecordLinks) processODataQueryGet();
    }

    @Override
    public KeywordLinks findKeywordAssociatedWithRecord(UUID systemId) {
        return (KeywordLinks) processODataQueryGet();
    }

    @Override
    public ScreeningMetadataLinks
    getScreeningMetadataAssociatedWithRecord(@NotNull final UUID systemId) {
        Screening screening = getRecordOrThrow(systemId)
                .getReferenceScreening();
        if (null == screening) {
            throw new NoarkEntityNotFoundException(
                    INFO_CANNOT_FIND_OBJECT + " Screening, using systemId " +
                            systemId);
        }
        SearchResultsPage page = new
                SearchResultsPage(copyOf(screening.getReferenceScreeningMetadata()));
        return new ScreeningMetadataLinks(page);
    }

    @Override
    public CommentLinks getCommentAssociatedWithRecord(
            @NotNull final UUID systemId) {
        // Make sure the record exists
        getRecordOrThrow(systemId);
        return (CommentLinks) processODataQueryGet();
    }

    @Override
    public DocumentDescriptionLinks
    getDocumentDescriptionAssociatedWithRecord(@NotNull final UUID systemId) {
        // Make sure the record exists
        getRecordOrThrow(systemId);
        return (DocumentDescriptionLinks) processODataQueryGet();
    }

    @Override
    public CorrespondencePartLinks
    getCorrespondencePartAssociatedWithRecord(
            @NotNull final UUID systemId) {
        // Make sure the record exists
        getRecordOrThrow(systemId);
        return (CorrespondencePartLinks) processODataQueryGet();
    }

    @Override
    public PartLinks
    getPartAssociatedWithRecord(@NotNull final UUID systemId) {
        // Make sure the record exists
        getRecordOrThrow(systemId);
        return (PartLinks) processODataQueryGet();
    }

    @Override
    public NationalIdentifierLinks getNationalIdentifierAssociatedWithRecord(
            @NotNull final UUID systemId) {
        // Make sure the record exists
        getRecordOrThrow(systemId);
        return (NationalIdentifierLinks) processODataQueryGet();
    }

    /**
     * Used to retrieve a BSMBase object so parent can can check that the
     * BSMMetadata object exists and is not outdated.
     * Done to simplify coding.
     *
     * @param name Name of the BSM parameter to check
     * @return BSMMetadata object corresponding to the name
     */
    @Override
    protected Optional<BSMMetadata> findBSMByName(@NotNull final String name) {
        return bsmService.findBSMByName(name);
    }

    // All UPDATE operations

    /**
     * Updates a Record object in the database. First we try to locate the
     * Record object. If the Record object does not exist a
     * NoarkEntityNotFoundException exception is thrown that the caller has
     * to deal with.
     * <p>
     * After this the values you are allowed to update are copied from the
     * incomingRecord object to the existingRecord object and the existingRecord
     * object will be persisted to the database when the transaction boundary
     * is over.
     * <p>
     * Note, the version corresponds to the version number, when the object
     * was initially retrieved from the database. If this number is not the
     * same as the version number when re-retrieving the Record object from
     * the database a NoarkConcurrencyException is thrown. Note. This happens
     * when the call to Record.setVersion() occurs.
     * <p>
     * It's a little unclear if it's possible to update this as it has no
     * fields that are updatable. It's also unclear how to set the archivedBy
     * value.
     *
     * @param systemId       The systemId of the record object to retrieve
     * @param incomingRecord The incoming record object
     * @return The updatedRecord after it is persisted
     */
    @Override
    @Transactional
    public RecordLinks handleUpdate(@NotNull final UUID systemId,
                                    @NotNull final RecordEntity incomingRecord) {
        bsmService.validateBSMList(incomingRecord.getReferenceBSMBase());
        RecordEntity existingRecord = getRecordOrThrow(systemId);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingRecord, incomingRecord);
        // Here copy all the values you are allowed to copy ....
        updateTitleAndDescription(incomingRecord, existingRecord);
        if (null != incomingRecord.getDocumentMedium()) {
            existingRecord.setDocumentMedium(
                    incomingRecord.getDocumentMedium());
        }
        existingRecord.setPublicTitle(existingRecord.getPublicTitle());
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingRecord.setVersion(getETag());
        publishChangeLogEvents(changeLogs);
        return packAsLinks(existingRecord);
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingRecord the existing RecordEntity from the database
     * @param newRecord      the incoming RecordEntity
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(RecordEntity existingRecord, RecordEntity newRecord) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // registrering	M020	tittel	Ved endring etter status Ekspedert/Avsluttet
        if (!existingRecord.getTitle().equals(newRecord.getTitle())) {
            changeLogs.add(createChangeLogObject(TITLE, existingRecord.getTitle(),
                    newRecord.getTitle(), existingRecord));
        }
        // registrering	M024	forfatter	Ved endring
        // TODO: Unclear how to handle change in a list
        return changeLogs;
    }

    @Override
    @Transactional
    public RecordLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final PatchObjects patchObjects) {
        return packAsLinks((RecordEntity) handlePatch(systemId, patchObjects));
    }

    /**
     * Persist and associate the incoming author object with the record
     * identified by systemId
     *
     * @param systemId The sytsemId of the record to associate with
     * @param author   The incoming author object
     * @return author object wrapped as a AuthorLinks
     */
    @Override
    @Transactional
    public AuthorLinks associateAuthorWithRecord(
            @NotNull final UUID systemId,
            @NotNull final Author author) {
        return authorService.associateAuthorWithRecord
                (author, getRecordOrThrow(systemId));
    }

    @Override
    @Transactional
    public Object associateBSM(@NotNull final UUID systemId,
                               @NotNull final List<BSMBase> bsm) {
        RecordEntity record = getRecordOrThrow(systemId);
        record.addReferenceBSMBase(bsm);
        return record;
    }

    // All DELETE operations

    @Override
    @Transactional
    public void deleteRecord(@NotNull final UUID systemId) {
        RecordEntity record = getRecordOrThrow(systemId);
        recordRepository.delete(record);
        handleDeletionForEventLog(record);
    }

    /**
     * Delete all objects belonging to the organisation identified by organisation
     */
    @Override
    @Transactional
    public void deleteAllByOrganisation() {
        recordRepository.deleteByOrganisation(getOrganisation());
    }

    // All template methods

    /**
     * Generate a Default CorrespondencePartUnit object that can be
     * associated with the identified Record.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @return the CorrespondencePartUnit object wrapped as a
     * CorrespondencePartUnitLinks object
     */
    @Override
    public RecordLinks generateDefaultRecord(@NotNull final UUID systemId) {
        RecordEntity defaultRecord = new RecordEntity();
        defaultRecord.setTitle(TEST_TITLE);
        defaultRecord.setVersion(-1L, true);
        return packAsLinks(defaultRecord);
    }

    @Override
    public ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId) {
        return screeningMetadataService.getDefaultScreeningMetadata(systemId);
    }

    @Override
    public KeywordTemplateLinks generateDefaultKeyword(@NotNull final UUID systemId) {
        return keywordService.generateDefaultKeyword(systemId);
    }

    @Override
    public CommentLinks generateDefaultComment(@NotNull final UUID systemId) {
        return commentService.generateDefaultComment(systemId);
    }

    /**
     * Generate a Default CorrespondencePartUnit object that can be
     * associated with the identified Record.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param systemId The systemId of the record object
     *                 you wish to create a templated object for
     * @return the CorrespondencePartUnit object wrapped as a
     * CorrespondencePartUnitLinks object
     */
    @Override
    public CorrespondencePartInternalLinks
    generateDefaultCorrespondencePartInternal(@NotNull final UUID systemId) {
        return correspondencePartService.
                generateDefaultCorrespondencePartInternal(systemId);
    }

    /**
     * Generate a Default CorrespondencePartUnit object that can be
     * associated with the identified Record.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param systemId The systemId of the record object
     *                 you wish to create a templated object for
     * @return the CorrespondencePartUnit object wrapped as a
     * CorrespondencePartUnitLinks object
     */
    @Override
    public CorrespondencePartPersonLinks
    generateDefaultCorrespondencePartPerson(@NotNull final UUID systemId) {
        return correspondencePartService.
                generateDefaultCorrespondencePartPerson(systemId);
    }

    /**
     * Generate a Default PartUnit object that can be
     * associated with the identified Record.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param systemId The systemId of the record object
     *                 you wish to create a templated object for
     * @return the PartUnit object wrapped as a
     * PartUnitLinks object
     */
    @Override
    public PartPersonLinks
    generateDefaultPartPerson(@NotNull final UUID systemId) {
        return partService.generateDefaultPartPerson(systemId);
    }

    /**
     * Generate a Default PartUnit object that can be
     * associated with the identified Record.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param systemId The systemId of the record object
     *                 you wish to create a templated object for
     * @return the PartUnit object wrapped as a
     * PartUnitLinks object
     */
    @Override
    public PartUnitLinks
    generateDefaultPartUnit(
            UUID systemId) {
        return partService.generateDefaultPartUnit(systemId);
    }

    /**
     * Generate a Default CorrespondencePartUnit object that can be
     * associated with the identified Record.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param systemId The systemId of the record object
     *                 you wish to create a templated object for
     * @return the CorrespondencePartUnit object wrapped as a
     * CorrespondencePartUnitLinks object
     */
    @Override
    public CorrespondencePartUnitLinks generateDefaultCorrespondencePartUnit(
            @NotNull final UUID systemId) {
        return correspondencePartService.
                generateDefaultCorrespondencePartUnit(systemId);
    }

    @Override
    public AuthorLinks generateDefaultAuthor(@NotNull final UUID systemId) {
        return authorService.generateDefaultAuthor(systemId);
    }

    @Override
    public BuildingLinks generateDefaultBuilding(
            @NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultBuilding(systemId);
    }

    @Override
    public CadastralUnitLinks generateDefaultCadastralUnit(
            @NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultCadastralUnit(systemId);
    }

    @Override
    public DNumberLinks generateDefaultDNumber(@NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultDNumber(systemId);
    }

    @Override
    public PlanLinks generateDefaultPlan(@NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultPlan(systemId);
    }

    @Override
    public PositionLinks generateDefaultPosition(
            @NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultPosition(systemId);
    }

    @Override
    public SocialSecurityNumberLinks generateDefaultSocialSecurityNumber(
            @NotNull final UUID systemId) {
        return nationalIdentifierService
                .generateDefaultSocialSecurityNumber(systemId);
    }

    @Override
    public UnitLinks generateDefaultUnit(@NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultUnit(systemId);
    }

    @Override
    public StorageLocationLinks getDefaultStorageLocation(
            @NotNull final UUID systemId) {
        return storageLocationService.getDefaultStorageLocation(systemId);
    }

    @Override
    public CrossReferenceLinks getDefaultCrossReference(
            @NotNull final UUID systemId) {
        return crossReferenceService.getDefaultCrossReference(systemId);
    }

    @Override
    public CrossReferenceLinks findCrossReferenceAssociatedWithRecord(
            @NotNull final UUID systemId) {
        return (CrossReferenceLinks) processODataQueryGet();
    }
    // All HELPER operations

    public RecordLinks packAsLinks(@NotNull final RecordEntity record) {
        RecordLinks recordLinks = new RecordLinks(record);
        applyLinksAndHeader(recordLinks, recordLinksBuilder);
        return recordLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid Record back. If there is no valid
     * Record, an exception is thrown
     *
     * @param systemId the systemId of the record you want to retrieve
     * @return the record
     */
    protected RecordEntity getRecordOrThrow(@NotNull final UUID systemId) {
        RecordEntity record =
                recordRepository.findBySystemId(systemId);
        if (record == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " Record, using systemId " + systemId;
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        }
        return record;
    }

    protected RecordEntity getReferenceToRecordOrThrow(@NotNull final UUID systemId) {
        RecordEntity record =
                recordRepository.getReferenceById(systemId);
        if (record == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " Record, using systemId " + systemId;
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        }
        return record;
    }
}
