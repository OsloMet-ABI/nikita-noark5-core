package app.service.noark5;

import app.domain.noark5.EventLog;
import app.domain.noark5.SystemIdEntity;
import app.domain.repository.noark5.v5.IEventLogRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.IEventLogService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.IEventLogLinksBuilder;
import app.webapp.payload.links.EventLogLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class EventLogService
        extends NoarkService
        implements IEventLogService {

    private static final Logger logger =
            LoggerFactory.getLogger(EventLogService.class);

    private final IEventLogRepository eventLogRepository;
    private final IEventLogLinksBuilder eventLogLinksBuilder;

    public EventLogService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IEventLogRepository eventLogRepository,
            IEventLogLinksBuilder eventLogLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.eventLogRepository = eventLogRepository;
        this.eventLogLinksBuilder = eventLogLinksBuilder;
    }

    // All CREATE operations

    @Override
    @Transactional
    public EventLogLinks createNewEventLog(
            @NotNull final EventLog eventLog,
            @NotNull final SystemIdEntity entity) {
        if (null == eventLog.getEventDate()) {
            eventLog.setEventDate(OffsetDateTime.now());
        }
        return packAsLinks(eventLogRepository.save(eventLog));
    }

    // All READ operations

    @Override
    public EventLogLinks findEventLogByOwner() {
        return (EventLogLinks) processODataQueryGet();
    }

    @Override
    public EventLogLinks findSingleEventLog(@NotNull final UUID systemId) {
        return packAsLinks(getEventLogOrThrow(systemId));
    }

    // All UPDATE operations

    @Override
    @Transactional
    public EventLogLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final EventLog incomingEventLog) {
        EventLog existingEventLog = getEventLogOrThrow(systemId);

        // Copy all the values you are allowed to copy ....

        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingEventLog.setVersion(getETag());
        return packAsLinks(existingEventLog);
    }

    // All DELETE operations

    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        //deleteEntity(getEventLogOrThrow(systemId));
    }

    // All template operations

    @Override
    public EventLogLinks generateDefaultEventLog(
            @NotNull final SystemIdEntity entity) {
        EventLog defaultEventLog = new EventLog();
        defaultEventLog.setEventDate(OffsetDateTime.now());
        defaultEventLog.setEventInitiator(getUser());
        defaultEventLog.setVersion(-1L, true);
        return packAsLinks(defaultEventLog);
    }

    // All helper operations


    public EventLogLinks packAsLinks(
            @NotNull final EventLog eventLog) {
        EventLogLinks eventLogLinks = new EventLogLinks(eventLog);
        applyLinksAndHeader(eventLogLinks, eventLogLinksBuilder);
        return eventLogLinks;
    }

    protected EventLog getEventLogOrThrow(@NotNull final UUID eventLogSystemId) {
        EventLog eventLog = eventLogRepository.findBySystemId(eventLogSystemId);
        if (eventLog == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " EventLog, using systemId " + eventLogSystemId;
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        }
        return eventLog;
    }
}
