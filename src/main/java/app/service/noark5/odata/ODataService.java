package app.service.noark5.odata;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.interfaces.odata.IODataService;
import app.service.noark5.ApplicationService;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.model.SearchResultsPage;
import app.webapp.odata.NikitaODataToHQL;
import app.webapp.odata.ODataToHQL;
import app.webapp.odata.QueryObject;
import app.webapp.odata.base.ODataLexer;
import app.webapp.odata.base.ODataParser;
import app.webapp.payload.builder.noark5.LinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.hibernate.Hibernate;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.rowset.serial.SerialClob;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.List;

import static app.utils.CommonUtils.FileUtils.getClassFromName;
import static app.utils.constants.Constants.DELETE_RESPONSE;
import static app.utils.constants.Constants.HATEOAS_API_PATH;
import static app.utils.constants.ODataConstants.DOLLAR_FILTER_EQ;
import static java.lang.Integer.MAX_VALUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.antlr.v4.runtime.CharStreams.fromString;
import static org.hibernate.ScrollMode.SCROLL_INSENSITIVE;


@Service
public class ODataService
        extends ApplicationService
        implements IODataService {

    private final Logger logger =
            LoggerFactory.getLogger(ODataService.class);

    /**
     * if a query does not have a page size set (e.g., $top=20) then use the
     * value set from yml file. If yml value missing fall back to 10
     */
    @Value("${nikita.application.pagination.max-page-size:10}")
    protected int maxResults;

    /**
     * To prevent resource starvation set an absolute maximum size for a
     * query. If e.g., a client calls a request with $top=90000 then use the
     * value set from yml file. If yml value missing fall back to 1000
     */
    @Value("${nikita.application.pagination.absolute-max-page-size:1000}")
    protected int absoluteMaxResults;

    public ODataService(EntityManager entityManager,
                        ILocalUserDetails userDetails,
                        IUrlDetails urlDetails) {
        super(entityManager, userDetails, urlDetails);
    }

    @Override
    @Transactional
    public String processODataQueryDelete() {
        QueryObject queryObject = convertODataToHQL("");
        Query<INoarkEntity> query = queryObject.getQuery();
        int result = query.executeUpdate();
        logger.info(result + " records deleted for OData Query [" +
                query.getQueryString() + "]");
        return DELETE_RESPONSE;
    }

    @Override
    public LinksNoarkObject processODataQueryGet() {
        return processODataQueryGet("");
    }

    /**
     * Note: This method has an obvious flaw:
     * .search(DocumentObject.class)
     * Currently we only allow $search to work on document contents
     * (documentTokens). This is because hibernate is generating indexes per
     * object type (Fonds.class, DocumentObject.class). When we make
     * hibernate work with a single index, we can search across all values.
     * The point of the code at the moment is just to get fulltext working.
     */
    @Override
    public LinksNoarkObject processODataSearchQuery(
            String search, int fetchCount, int from) {
        try {
            SerialClob clobQuery = new SerialClob(search.toCharArray());
            /*
            SearchResult<DocumentObject> results =
                    Search.session((Session) entityManager)
                    .search(DocumentObject.class)
                    .where(f -> f.match()
                            .fields("documentTokens")
                            .matching(clobQuery))
                    .fetch(from, fetchCount);
            long totalRows = results.total().hitCount();
            List<INoarkEntity> resultsDocObject = List.copyOf(results.hits());
            SearchResultsPage page = new SearchResultsPage(resultsDocObject, totalRows,
                    fetchCount, from);
            DocumentObjectLinksBuilder handler = new DocumentObjectLinksBuilder();
            DocumentObjectLinks documentObjectLinks =
                    new DocumentObjectLinks(page);
            handler.setPublicAddress(address.getAddress());
            handler.setContextPath(address.getContextPath());
            handler.addLinks(documentObjectLinks, new Authorisation());
            setOutgoingRequestHeaderList();
            return documentObjectLinks;
            */
            return null;
        } catch (SQLException e) {
            logger.error(e.toString());
        }
        return null;
    }

    @Override
    public LinksNoarkObject processODataQueryGet(String odataAppend) {
        try {
            QueryObject queryObject = convertODataToHQL(odataAppend);
            String fromEntity = queryObject.getFromEntity();
            Query<INoarkEntity> query = queryObject.getQuery();

            int totalRows = getCountFromQuery(query);
            checkMaxResultsChangeIfRequired(query);
            int top = query.getMaxResults();
            int skip = query.getFirstResult();
            // Execute the SELECT query and return the query results
            List<INoarkEntity> entityList = query.getResultList();
            // Unproxy objects in the list. It was noticed that (nikita) Class
            // objects had proxied values in some cases. It is unclear why as
            // the list is a direct query to pull out all Class objects. I
            // reason that it may be due to a class having a child and parent.
            // This is relevant then for File and Fonds. Basically make
            // sure the list only contains a list of unproxied objects
            for (int i = 0; i < entityList.size(); i++) {
                if (entityList.get(i) instanceof HibernateProxy) {
                    entityList.set(i, (INoarkEntity)
                            Hibernate.unproxy(entityList.get(i)));
                }
            }
            SearchResultsPage page = new SearchResultsPage(entityList, totalRows, top, skip);
            LinksNoarkObject noarkObject;
            LinksBuilder handler;

            //if (entityList.size() > 0) {
            Class<INoarkEntity> cls = getClassFromName(fromEntity);
            LinksPacker packer = cls.getAnnotation(LinksPacker.class);
            LinksObject linksObject = cls.getAnnotation(LinksObject.class);
            // TODO: add if null
            // class supports a Page rather than a list. But it is only
            // intended to be here temporarily. Once this works all Hateoas
            // classes should be forced to work with a Page rather than a List
            boolean classSupportsPage = true;
            try {
                linksObject.using()
                        .getDeclaredConstructor(SearchResultsPage.class);
            } catch (NoSuchMethodException exception) {
                classSupportsPage = false;
            }

            if (classSupportsPage) {
                noarkObject = linksObject.using()
                        .getDeclaredConstructor(SearchResultsPage.class)
                        .newInstance(page);
            } // TODO : Is it safe to remove this else ??
            else {
                noarkObject = linksObject.using()
                        .getDeclaredConstructor(List.class)
                        .newInstance(entityList);
            }
            handler = packer.using().getConstructor().newInstance();
            // } else {
            //     noarkObject = new LinksNoarkObject(new ArrayList<>(),
            //            "unknown");
            //    handler = new EmptyListLinksBuilder();
            // }
            handler.setAddress(urlDetails.getAddress());
            handler.setContextPath(urlDetails.getContextPath());
            handler.addLinks(noarkObject);
            setOutgoingRequestHeaderList();
            return noarkObject;
        } catch (Exception e) {
            throw new NikitaMalformedInputDataException(e.getMessage());
        }
    }

    private QueryObject convertODataToHQL(@NotNull final String additionalOdata) {
        StringBuffer requestURL = getRequest().getRequestURL();

        // Remove trailing / as it causes confusion for parsing. See also:
        // https://gitlab.com/OsloMet-ABI/nikita-noark5-core/-/issues/200
        if (requestURL.charAt(requestURL.length() - 1) == '/') {
            requestURL.setLength(requestURL.length() - 1);
        }
        //  remove everything up to api/packagename/
        int apiLength = (HATEOAS_API_PATH + "/").length();
        int startApi = requestURL.lastIndexOf(HATEOAS_API_PATH + "/");
        int endPackageName = requestURL.indexOf("/", startApi + apiLength);
        String odataQuery = requestURL.substring(endPackageName + 1);

        String queryString = getRequest().getQueryString();
        if (queryString != null) {
            queryString = urlDecodeValue(queryString);
        } else {
            queryString = "";
        }

        // Add owned by to the query as first parameter
        if (!queryString.isBlank()) {
            // TODO: Find out why decode is not replacing %20
            queryString = queryString.replaceAll("%20", " ");
            queryString = queryString.replaceAll("%2F", "/");
            queryString = queryString.replaceAll("%2B", "+");
            queryString = queryString.replaceAll("%24", "\\$");
            queryString = queryString.replaceAll("%27", "'");
            queryString = queryString.replaceAll("%40", "@");
            queryString = queryString.replaceAll("%28", "(");
            queryString = queryString.replaceAll("%2C", ",");
            queryString = queryString.replaceAll("%29", ")");
        } else {
            queryString = "";
        }


        //  addOrganisation(queryString);
        if (queryString.length() > 0) {
            odataQuery += "?" + queryString;
        }
        if (!additionalOdata.isBlank()) {
            odataQuery += " and " + additionalOdata;
        }
        String dmlStatementType = "";

        if (getRequest().getMethod().equalsIgnoreCase("delete")) {
            dmlStatementType = "delete";
        }
        return convertODataToHQL(odataQuery, dmlStatementType);
    }

    private String urlDecodeValue(String value) {
        String decoded = "";
        decoded = URLDecoder.decode(value, UTF_8);
        return decoded;
    }

    private void addOrganisation(StringBuilder originalRequest) {
        String andCondition = "' and ";
        int start = DOLLAR_FILTER_EQ.length();
        if (originalRequest.length() > 0) {
            if (originalRequest.lastIndexOf(DOLLAR_FILTER_EQ) >= 0) {
                start = originalRequest.lastIndexOf(DOLLAR_FILTER_EQ)
                        + DOLLAR_FILTER_EQ.length();
            }
            // Something is on the original request, but it is not a $filter
            else {
                originalRequest.insert(0, DOLLAR_FILTER_EQ);
                andCondition = "'&";
            }
        } else {
            // If originalRequest.length() > 0 then there is no $filter in
            // the request. This is typical if you  search for the root of en
            // object e.g. .../arkivstruktur/arkiv. A manual addition of
            // $filter is required
            originalRequest.insert(0, DOLLAR_FILTER_EQ);
        }

        //originalRequest.insert(start,
        //        "organisasjon/systemID eq '" + getOrganisation()
        //        .getSystemId().toString() + andCondition);
    }

    public QueryObject convertODataToHQL(String odataQuery,
                                         String dmlStatementType) {
        ODataLexer lexer = new ODataLexer(fromString(odataQuery));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ODataParser parser = new ODataParser(tokens);
        ParseTree tree = parser.odataRelativeUri();
        ParseTreeWalker walker = new ParseTreeWalker();
        // Make the HQL Statement
        ODataToHQL hqlWalker = new NikitaODataToHQL(dmlStatementType, getOrganisation());
        walker.walk(hqlWalker, tree);
        return new QueryObject(hqlWalker.getHqlStatement(
                entityManager.unwrap(Session.class)), hqlWalker.getEntity());
    }

    // All helper methods

    /**
     * To avoid resource starvation a client is prohibited from running
     * SQL queries that produce large result sets. Rather pagination should
     * be used. The upper value for absoluteMaxResults is configurable in yml
     * property files and by default is set to 1000. This number is arbitrary.
     *
     * @param query the query object
     */
    private void checkMaxResultsChangeIfRequired(Query<INoarkEntity> query) {
        // Integer.MAX_VALUE is used by hibernate to indicate no value has
        // been set
        if (query.getMaxResults() == MAX_VALUE) {
            query.setMaxResults(maxResults);
        }
        if (query.getMaxResults() > absoluteMaxResults) {
            query.setMaxResults(absoluteMaxResults);
        }
    }

    /**
     * Find the total number of rows that this query contains. This is a
     * subtle hack where we run to the end of the cursor list and check how
     * many results there are. We note how many rows, then close the cursor
     * list and  the query can continue using maxResults (count) and
     * firstResult (offset). Note the efficiency of this approach will vary
     * between DBMS.
     * Note: if we don't "reset" the maxResults and firstResult values before
     * using the cursor approach, we will not have correct values for count.
     * Seeing as the API is stateless then a query will have to be called
     * multiple times. Hopefully the hibernate cache will take care of this
     * so there aren't performance issues.
     *
     * @param query the query object
     * @return the number rows that make up the count field
     */
    private int getCountFromQuery(Query<INoarkEntity> query) {
        // "reset" the maxResult value to ensure we get a proper count value
        int maxResult = query.getMaxResults();
        query.setMaxResults(Integer.MAX_VALUE);
        // "reset" the firstResult value to ensure we get a proper count value
        int firstResult = query.getFirstResult();
        query.setFirstResult(0);
        ScrollableResults<INoarkEntity> cursor = query.scroll(SCROLL_INSENSITIVE);
        cursor.last();
        int totalRows = cursor.getRowNumber() == 0 ? 1 :
                cursor.getRowNumber() + 1;
        cursor.close();
        // Set the values for maxResult and offset back to what they were
        // initially so the results returned later will be correct
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        return totalRows;
    }
}
