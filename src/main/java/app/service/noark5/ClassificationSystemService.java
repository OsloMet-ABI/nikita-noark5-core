package app.service.noark5;

import app.domain.noark5.Class;
import app.domain.noark5.ClassificationSystem;
import app.domain.noark5.Series;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.metadata.ClassificationType;
import app.domain.repository.noark5.v5.IClassificationSystemRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.IClassService;
import app.service.interfaces.IClassificationSystemService;
import app.service.interfaces.metadata.IMetadataService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.IClassificationSystemLinksBuilder;
import app.webapp.payload.links.ClassLinks;
import app.webapp.payload.links.ClassificationSystemLinks;
import app.webapp.payload.links.SeriesLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static app.utils.NoarkUtils.NoarkEntity.Create.setFinaliseEntityValues;
import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.N5ResourceMappings.CLASSIFICATION_SYSTEM_TYPE;
import static app.utils.constants.N5ResourceMappings.TITLE;

/**
 * Service class for ClassificationSystem.
 * <p>
 * Provides basic CRUD functionality for ClassificationSystem using systemId.
 * <p>
 * Also supports CREATE for a (Noark Classification Class) Class.
 * <p>
 * All public methods return Hateoas objects
 */
@Service
public class ClassificationSystemService
        extends NoarkService
        implements IClassificationSystemService {

    private static final Logger logger = LoggerFactory.getLogger(
            ClassificationSystemService.class);

    private final IMetadataService metadataService;
    private final IClassService classService;
    private final IClassificationSystemRepository classificationSystemRepository;
    private final IClassificationSystemLinksBuilder
            classificationSystemLinksBuilder;

    public ClassificationSystemService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IMetadataService metadataService,
            IClassService classService,
            IClassificationSystemRepository classificationSystemRepository,
            IClassificationSystemLinksBuilder
                    classificationSystemLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.metadataService = metadataService;
        this.classService = classService;
        this.classificationSystemRepository = classificationSystemRepository;
        this.classificationSystemLinksBuilder =
                classificationSystemLinksBuilder;
    }
    // All CREATE operations

    /**
     * Persists a new classificationSystem object to the database. Some
     * values are set in the incoming payload (e.g. title) and some are set
     * by the core. owner, createdBy, createdDate are automatically set by
     * the core.
     *
     * @param classificationSystem classificationSystem object with some values
     *                             set
     * @return the newly persisted classificationSystem object wrapped as a
     * classificationSystemLinks object
     */
    @Override
    @Transactional
    public ClassificationSystemLinks save(
            final ClassificationSystem classificationSystem) {
        validateClassificationType(classificationSystem);
        setFinaliseEntityValues(classificationSystem);
        return packAsLinks(classificationSystemRepository
                .save(classificationSystem));
    }

    /**
     * Persists a new classificationSystem object to the database, that is
     * first associated with a parent classificationSystem object. Some
     * values are set in the incoming payload (e.g. title) and some are set
     * by the core. owner, createdBy, createdDate are automatically set by
     * the core.
     * <p>
     * First we try to locate the parent. If the parent does not exist a
     * NoarkEntityNotFoundException exception is thrown
     *
     * @param classificationSystemSystemId The systemId of the
     *                                     classificationSystem
     * @param klass                        incoming class object with some values set
     * @return the newly persisted class object wrapped as classSystemLinks
     * object
     */
    @Override
    @Transactional
    public ClassLinks createClassAssociatedWithClassificationSystem(
            @NotNull final UUID classificationSystemSystemId,
            @NotNull final Class klass) {
        ClassificationSystem classificationSystem =
                getClassificationSystemOrThrow(classificationSystemSystemId, false);
        klass.setReferenceClassificationSystem(classificationSystem);
        return classService.save(klass);
    }

    /**
     * Generate a Default Class object that can be associated with the
     * identified ClassificationSystem. Note this object has not been persisted
     * to the core.
     * <br>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param systemId The systemId of the
     *                 classificationSystem object you
     *                 wish to generate a default Class for
     * @return the Class object wrapped as a ClassLinks object
     */
    @Override
    public ClassLinks generateDefaultClass(
            @NotNull final UUID systemId) {
        return classService.generateDefaultClass(systemId);
    }
    // All READ operations

    /**
     * Retrieve a single ClassificationSystem objects from the database.
     *
     * @param systemId The systemId of the
     *                 ClassificationSystem  object you
     *                 wish to retrieve
     * @return the ClassificationSystem object wrapped as a
     * ClassificationSystemLinks object
     */
    @Override
    public ClassificationSystemLinks findSingleClassificationSystem(
            @NotNull final UUID systemId) {
        return packAsLinks(getClassificationSystemOrThrow(systemId));
    }

    @Override
    public ClassificationSystemLinks findAllClassificationSystem() {
        return (ClassificationSystemLinks) processODataQueryGet();
    }

    @Override
    public ClassLinks findAllClassAssociatedWithClassificationSystem(
            @NotNull final UUID systemId) {
        // Make sure the ClassificationSystem exists
        getClassificationSystemOrThrow(systemId, false);
        return (ClassLinks) processODataQueryGet();
    }

    @Override
    public SeriesLinks findSeriesAssociatedWithClassificationSystem(
            @NotNull final UUID systemId) {
        getClassificationSystemOrThrow(systemId, false);
        return (SeriesLinks) processODataQueryGet();
    }
    // All UPDATE operations

    /**
     * Updates a ClassificationSystem object in the database. First we try to
     * locate the ClassificationSystem object. If the ClassificationSystem
     * object does not exist a NoarkEntityNotFoundException exception is
     * thrown that the caller has to deal with.
     * <br>
     * After this the values you are allowed to update are copied from the
     * incomingClassificationSystem object to the
     * existingClassificationSystem object and the existingClassificationSystem
     * object will be persisted to the database when the transaction boundary
     * is over.
     * <p>
     * Note, the version corresponds to the version number, when the object
     * was initially retrieved from the database. If this number is not the
     * same as the version number when re-retrieving the ClassificationSystem
     * object from the database a NoarkConcurrencyException is thrown. Note.
     * <p>
     * This happens when the call to ClassificationSystem.setVersion() occurs.
     *
     * @param systemId                     The systemId of the
     *                                     classificationSystem object to
     *                                     retrieve
     * @param incomingClassificationSystem The incoming classificationSystem
     *                                     object
     * @return the ClassificationSystem object wrapped as a
     * ClassificationSystemLinks object
     */
    @Override
    @Transactional
    public ClassificationSystemLinks
    handleUpdate(@NotNull final UUID systemId, @NotNull final ClassificationSystem incomingClassificationSystem) {
        ClassificationSystem existingClassificationSystem =
                getClassificationSystemOrThrow(systemId, false);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingClassificationSystem, incomingClassificationSystem);
        // Copy all the values you are allowed to copy ....
        updateTitleAndDescription(incomingClassificationSystem,
                existingClassificationSystem);
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingClassificationSystem.setVersion(getETag());
        publishChangeLogEvents(changeLogs);
        return packAsLinks(existingClassificationSystem);
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingClassificationSystem the existing ClassificationSystem from the database
     * @param newClassificationSystem      the incoming ClassificationSystem
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(ClassificationSystem existingClassificationSystem,
                                                                        ClassificationSystem newClassificationSystem) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // klassifikasjonssystem	M086	klassifikasjonstype	Ved endring
        if (null != existingClassificationSystem.getClassificationType() &&
                !existingClassificationSystem.getClassificationType().equals(
                        newClassificationSystem.getClassificationType())) {
            changeLogs.add(createChangeLogObject(CLASSIFICATION_SYSTEM_TYPE,
                    existingClassificationSystem.getClassificationType().getCode() + " " +
                            existingClassificationSystem.getClassificationType().getCodeName(),
                    newClassificationSystem.getClassificationType().getCode() + " " +
                            newClassificationSystem.getClassificationType().getCodeName(),
                    existingClassificationSystem));
        }
        // klassifikasjonssystem	M020	tittel	Ved endring
        if (!existingClassificationSystem.getTitle().equals(newClassificationSystem.getTitle())) {
            changeLogs.add(createChangeLogObject(TITLE, existingClassificationSystem.getTitle(),
                    newClassificationSystem.getTitle(), existingClassificationSystem));
        }
        return changeLogs;
    }

    // All DELETE operations
    @Override
    @Transactional
    public void deleteClassificationSystem(@NotNull final UUID systemId) {
        ClassificationSystem classificationSystem =
                getClassificationSystemOrThrow(systemId, false);
        for (Series series : classificationSystem.getReferenceSeries()) {
            series.removeClassificationSystem(classificationSystem);
        }
    }

    /**
     * Delete all objects belonging to the organisation identified by organisation
     */
    @Override
    @Transactional
    public void deleteAllByOrganisation() {
        classificationSystemRepository.deleteByOrganisation(getOrganisation());
    }

    /**
     * Generate a Default ClassificationSystem object
     * <br>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @return the ClassificationSystem object wrapped as a
     * ClassificationSystemLinks object
     */
    @Override
    public ClassificationSystemLinks generateDefaultClassificationSystem() {
        ClassificationSystem defaultClassificationSystem =
                new ClassificationSystem();
        defaultClassificationSystem.setVersion(-1L, true);
        return packAsLinks(defaultClassificationSystem);
    }
    // All HELPER operations

    public ClassificationSystemLinks packAsLinks(
            ClassificationSystem classificationSystem) {
        ClassificationSystemLinks classificationSystemLinks =
                new ClassificationSystemLinks(classificationSystem);
        applyLinksAndHeader(classificationSystemLinks,
                classificationSystemLinksBuilder);
        return classificationSystemLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid ClassificationSystem back. If
     * there is no validClassificationSystem, an exception is thrown
     *
     * @param systemId systemId of the classificationSystem object to retrieve
     * @return the classificationSystem object
     */
    protected ClassificationSystem getClassificationSystemOrThrow(@NotNull final UUID systemId) {
        return getClassificationSystemOrThrow(systemId, true);
    }

    protected ClassificationSystem getClassificationSystemOrThrow(
            @NotNull final UUID systemId, @NotNull final boolean logRead) {
        ClassificationSystem classificationSystem =
                classificationSystemRepository.findBySystemId(systemId);
        if (classificationSystem == null) {
            String error = INFO_CANNOT_FIND_OBJECT +
                    "ClassificationSystem, using systemId " + systemId;
            logger.error(error);
            throw new NoarkEntityNotFoundException(error);
        }
        if (logRead) {
            handleReadForEventLog(classificationSystem);
        }
        return classificationSystem;
    }

    private void validateClassificationType(
            ClassificationSystem classificationSystem) {
        if (null != classificationSystem.getClassificationType()) {
            ClassificationType classificationType = (ClassificationType)
                    metadataService.findValidMetadata(
                            classificationSystem.getClassificationType());
            classificationSystem
                    .setClassificationType(classificationType);
        }
    }
}
