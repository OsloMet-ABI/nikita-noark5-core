package app.service.noark5.casehandling;

import app.domain.noark5.RecordEntity;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.casehandling.RecordNote;
import app.domain.noark5.secondary.DocumentFlow;
import app.domain.repository.noark5.v5.casehandling.IRecordNoteRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.casehandling.IRecordNoteService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.IDocumentFlowService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.model.PatchMerge;
import app.webapp.payload.builder.interfaces.IRecordNoteLinksBuilder;
import app.webapp.payload.links.casehandling.RecordNoteExpansionLinks;
import app.webapp.payload.links.casehandling.RecordNoteLinks;
import app.webapp.payload.links.secondary.DocumentFlowLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static app.utils.NoarkUtils.NoarkEntity.Create.validateDocumentMedium;
import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

@Service
public class RecordNoteService
        extends NoarkService
        implements IRecordNoteService {

    private static final Logger logger =
            LoggerFactory.getLogger(RecordNoteService.class);

    private final IRecordNoteRepository recordNoteRepository;
    private final IDocumentFlowService documentFlowService;
    private final IMetadataService metadataService;
    private final IRecordNoteLinksBuilder recordNoteLinksBuilder;

    public RecordNoteService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IRecordNoteRepository recordNoteRepository,
            IDocumentFlowService documentFlowService,
            IMetadataService metadataService,
            IRecordNoteLinksBuilder recordNoteLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.recordNoteRepository = recordNoteRepository;
        this.documentFlowService = documentFlowService;
        this.metadataService = metadataService;
        this.recordNoteLinksBuilder = recordNoteLinksBuilder;
    }

    // All CREATE methods

    @Override
    @Transactional
    public RecordNoteLinks save(@NotNull final RecordNote recordNote) {
        validateDocumentMedium(metadataService, recordNote);
        return packAsLinks(recordNoteRepository.save(recordNote));
    }

    @Override
    @Transactional
    public RecordNoteLinks expandRecorLinksRecordNote(
            @NotNull final RecordEntity record,
            @NotNull final PatchMerge patchMerge) {
        RecordNote recordNote = new RecordNote(record);
        record.setRecordId(recordNote.getRecordId());

        entityManager.createNativeQuery(
                        "INSERT INTO " + TABLE_RECORD_NOTE +
                                "(" +
                                SYSTEM_ID_ENG + ", " +
                                REGISTRY_ENTRY_NUMBER_ENG + ", " +
                                REGISTRY_ENTRY_STATUS_CODE_ENG + ", " +
                                REGISTRY_ENTRY_STATUS_CODE_NAME_ENG + "," +
                                REGISTRY_ENTRY_TYPE_CODE_ENG + ", " +
                                REGISTRY_ENTRY_TYPE_CODE_NAME_ENG + "," +
                                REGISTRY_ENTRY_DATE_ENG + ", " +
                                REGISTRY_ENTRY_YEAR_ENG + ", " +
                                REGISTRY_ENTRY_SEQUENCE_NUMBER_ENG + ", " +
                                ") VALUES (?,?,?,?,?,?,?,?,?)")
                .setParameter(1, record.getSystemIdAsString())
                .setParameter(2, recordNote.getDocumentDate())
                .setParameter(3, recordNote.getDueDate())
                .setParameter(4, recordNote.getFreedomAssessmentDate())
                .setParameter(5, recordNote.getLoanedDate())
                .setParameter(6, recordNote.getLoaneLinks())
                .setParameter(7, recordNote.getNumberOfAttachments())
                .setParameter(8, recordNote.getReceivedDate())
                .setParameter(9, recordNote.getSentDate())
                .executeUpdate();
        handleCreationForEventLog(recordNote);
        return packAsLinks(recordNote);
    }

    // All READ methods

    @Override
    public RecordNoteLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getRecordNoteOrThrow(systemId));
    }

    @Override
    public RecordNoteLinks findAll() {
        return (RecordNoteLinks) processODataQueryGet();
    }

    @Override
    public DocumentFlowLinks findAllDocumentFlowWithRecordNoteBySystemId(
            @NotNull final UUID systemId) {
        getRecordNoteOrThrow(systemId);
        return (DocumentFlowLinks) processODataQueryGet();
    }

    // All UPDATE operations

    @Override
    @Transactional
    public DocumentFlowLinks associateDocumentFlowWithRecordNote(
            UUID systemId, DocumentFlow documentFlow) {
        return documentFlowService.associateDocumentFlowWithRecordNote(
                documentFlow, getRecordNoteOrThrow(systemId));
    }

    /**
     * Updates a Record object in the database. First we try to locate the
     * Record object. If the Record object does not exist a
     * NoarkEntityNotFoundException exception is thrown that the caller has
     * to deal with.
     * <p>
     * After this the values you are allowed to update are copied from the
     * incomingRecord object to the existingRecord object and the existingRecord
     * object will be persisted to the database when the transaction boundary
     * is over.
     * <p>
     * Note, the version corresponds to the version number, when the object
     * was initially retrieved from the database. If this number is not the
     * same as the version number when re-retrieving the Record object from
     * the database a NoarkConcurrencyException is thrown. Note. This happens
     * <p>
     * Copies the values you are allowed to change, title, description, dueDate,
     * freedomAssessmentDate, loanedDate, loaneLinks
     * <p>
     * Note. The last known version number (derived from an ETAG in the request)
     * is retrieved and checked to make sure the user is updating the latest
     * copy.
     *
     * @param systemId           The systemId of the recordNote object
     *                           you wish to update
     * @param incomingRecordNote The updated recordNote object. Note
     *                           the values you are allowed to change are
     *                           copied from this object. This object is
     *                           not persisted.
     * @return the updated recordNote after being persisted to the database
     * wrapped as RecordNoteLinks object
     */
    @Override
    @Transactional
    public RecordNoteLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final RecordNote incomingRecordNote) {
        RecordNote existingRecordNote = getRecordNoteOrThrow(systemId);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingRecordNote, incomingRecordNote);
        // Copy all the values you are allowed to copy ....
        updateTitleAndDescription(incomingRecordNote, existingRecordNote);
        if (null != incomingRecordNote.getDocumentMedium()) {
            existingRecordNote.setDocumentMedium(
                    incomingRecordNote.getDocumentMedium());
        }
        if (null != incomingRecordNote.getDocumentDate()) {
            existingRecordNote.setDocumentDate(
                    incomingRecordNote.getDocumentDate());
        }
        if (null != incomingRecordNote.getDueDate()) {
            existingRecordNote.setDueDate(
                    incomingRecordNote.getDueDate());
        }
        if (null != incomingRecordNote.getFreedomAssessmentDate()) {
            existingRecordNote.setFreedomAssessmentDate(
                    incomingRecordNote.getFreedomAssessmentDate());
        }
        if (null != incomingRecordNote.getLoanedDate()) {
            existingRecordNote.setLoanedDate(
                    incomingRecordNote.getLoanedDate());
        }
        if (null != incomingRecordNote.getLoaneLinks()) {
            existingRecordNote.setLoaneLinks(
                    incomingRecordNote.getLoaneLinks());
        }
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingRecordNote.setVersion(getETag());
        publishChangeLogEvents(changeLogs);
        return packAsLinks(existingRecordNote);
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingRecordNote the existing RecordNote from the database
     * @param newRecordNote      the incoming RecordNote
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(RecordNote existingRecordNote, RecordNote newRecordNote) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // registrering	M020	tittel	Ved endring etter status Ekspedert/Avsluttet
        if (!existingRecordNote.getTitle().equals(newRecordNote.getTitle())) {
            changeLogs.add(createChangeLogObject(TITLE, existingRecordNote.getTitle(),
                    newRecordNote.getTitle(), existingRecordNote));
        }
        return changeLogs;
    }
    // All DELETE operations

    /**
     * Delete a RecordNote identified by the given systemId
     * <p>
     * Note. This assumes all children have also been deleted.
     *
     * @param recordNoteSystemId The systemId of the recordNote object
     *                           you wish to delete
     */
    @Override
    @Transactional
    public String deleteEntity(
            @NotNull final UUID recordNoteSystemId) {
        RecordNote recordNote = getRecordNoteOrThrow(recordNoteSystemId);
        for (DocumentFlow documentFlow : recordNote
                .getReferenceDocumentFlow()) {
            documentFlowService.deleteDocumentFlow(documentFlow);
        }
        recordNoteRepository.delete(recordNote);
        handleDeletionForEventLog(recordNote);
        // If the delete failed an exception should have been thrown. Getting
        // this far means it went OK
        return DELETE_RESPONSE;
    }

    /**
     * Delete all RecordNote objects belonging to the organisation identified by organisation
     */
    @Override
    @Transactional
    public String deleteAllByOrganisation() {
        //String user = getUser();
        String organisation = getOrganisation();
        long count = recordNoteRepository.countByOrganisation(organisation);
        recordNoteRepository.deleteByOrganisation(organisation);
        logger.info("Deleted [" + count + "] RecordNote objects belonging to " +
                "[" + organisation + "]");
        return DELETE_RESPONSE;
    }

    // All template methods

    @Override
    public RecordNoteLinks generateDefaultRecordNote(
            @NotNull final UUID caseFileSystemId) {
        return packAsLinks(generateDefaultRecordNote());
    }

    @Override
    public DocumentFlowLinks generateDefaultDocumentFlow(
            @NotNull final UUID systemId) {
        return documentFlowService.generateDefaultDocumentFlow();
    }

    @Override
    public RecordNoteExpansionLinks generateDefaultExpandedRecordNote(
            @NotNull final UUID systemId) {
        return packAsRecordNoteExpansionLinks(
                generateDefaultRecordNote());
    }

    // All helper methods

    private RecordNoteExpansionLinks packAsRecordNoteExpansionLinks(
            RecordNote recordNote) {
        RecordNoteExpansionLinks recordNoteLinks =
                new RecordNoteExpansionLinks(recordNote);
        applyLinksAndHeader(recordNoteLinks, recordNoteLinksBuilder);
        return recordNoteLinks;
    }

    private RecordNote generateDefaultRecordNote() {
        RecordNote defaultRecordNote = new RecordNote();
        defaultRecordNote.setTitle(DEFAULT_TITLE + "RecordNote");
        defaultRecordNote.setVersion(-1L, true);
        return defaultRecordNote;
    }

    public RecordNoteLinks packAsLinks(@NotNull final RecordNote recordNote) {
        RecordNoteLinks recordNoteLinks = new RecordNoteLinks(recordNote);
        applyLinksAndHeader(recordNoteLinks, recordNoteLinksBuilder);
        return recordNoteLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid RecordNote back. If there is no
     * valid RecordNote, an exception is thrown
     *
     * @param systemId systemId of the recordNote to find.
     * @return the recordNote
     */
    protected RecordNote getRecordNoteOrThrow(@NotNull final UUID systemId) {
        RecordNote recordNote =
                recordNoteRepository.findBySystemId(systemId);
        if (recordNote == null) {
            String error = INFO_CANNOT_FIND_OBJECT +
                    " RecordNote, using systemId " + systemId;
            logger.error(error);
            throw new NoarkEntityNotFoundException(error);
        }
        return recordNote;
    }
}
