package app.service.noark5.casehandling;

import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.secondary.SignOff;
import app.domain.repository.noark5.v5.secondary.ISignOffRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.casehandling.ISignOffService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.noark5.secondary.SignOffLinksBuilder;
import app.webapp.payload.links.secondary.SignOffLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class SignOffService
        extends NoarkService
        implements ISignOffService {

    private final ISignOffRepository signOffRepository;
    private final SignOffLinksBuilder signOffLinksBuilder;

    public SignOffService(EntityManager entityManager,
                          ApplicationEventPublisher applicationEventPublisher,
                          IPatchService patchService,
                          ISignOffRepository signOffRepository,
                          SignOffLinksBuilder signOffLinksBuilder,
                          ILocalUserDetails userDetails,
                          IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.signOffRepository = signOffRepository;
        this.signOffLinksBuilder = signOffLinksBuilder;
    }

    @Override
    public SignOffLinks save(SignOff signOff) {
        return packAsLinks(signOffRepository.save(signOff));
    }

    @Override
    public SignOffLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getSignOffOrThrow(systemId));
    }

    @Override
    public SignOff findSignOffBySystemId(@NotNull final UUID systemId) {
        return getSignOffOrThrow(systemId);
    }

    @Override
    @Transactional
    public SignOffLinks updateSignOff(
            @NotNull final UUID signOffSystemId,
            @NotNull final SignOff incomingSignOff) {
        // TODO: Revisit the logic behind this. Very unsure about the actual
        // approach we should use.
        SignOff signOff = getSignOffOrThrow(signOffSystemId);
        signOff.setSignOffBy(incomingSignOff.getSignOffBy());
        signOff.setSignOffMethod(incomingSignOff.getSignOffMethod());
        return packAsLinks(signOff);
    }

    // All DELETE methods

    /**
     * Delete a SignOff identified by the given systemId
     *
     * @param systemId The systemId of the SignOff object you wish to delete
     */
    @Override
    @Transactional
    public void deleteSignOff(@NotNull final UUID systemId) {
        SignOff signOff = getSignOffOrThrow(systemId);
        for (RegistryEntry registryEntry : signOff.getReferenceRegistryEntry()) {
            registryEntry.removeSignOff(signOff);
        }
        signOffRepository.delete(signOff);
    }

    // All template methods

    public SignOffLinks generateDefaultSignOff(
            @NotNull final UUID systemId) {
        SignOff defaultSignOff = new SignOff();
        defaultSignOff.setSignOffDate(OffsetDateTime.now());
        defaultSignOff.setSignOffBy(getUser());
        defaultSignOff.setVersion(-1L, true);
        return packAsLinks(defaultSignOff);
    }

    // All helper methods

    public SignOffLinks packAsLinks(SignOff signOff) {
        SignOffLinks signOffLinks =
                new SignOffLinks(signOff);
        applyLinksAndHeader(signOffLinks, signOffLinksBuilder);
        return signOffLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid SignOff back. If there is no
     * valid SignOff, an exception is thrown
     *
     * @param systemId systemId of the signOff to find.
     * @return the signOff
     */
    protected SignOff getSignOffOrThrow(@NotNull final UUID systemId) {
        SignOff signOff = signOffRepository.findBySystemId(systemId);
        if (signOff == null) {
            throw new NoarkEntityNotFoundException(INFO_CANNOT_FIND_OBJECT +
                    " SignOff, using systemId " + systemId);
        }
        return signOff;
    }
}
