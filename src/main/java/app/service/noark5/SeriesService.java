package app.service.noark5;

import app.domain.noark5.ClassificationSystem;
import app.domain.noark5.File;
import app.domain.noark5.Series;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.casehandling.CaseFile;
import app.domain.noark5.metadata.DocumentMedium;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.metadata.SeriesStatus;
import app.domain.noark5.secondary.Screening;
import app.domain.noark5.secondary.StorageLocation;
import app.domain.repository.noark5.v5.ISeriesRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.ICaseFileService;
import app.service.interfaces.IClassificationSystemService;
import app.service.interfaces.IFileService;
import app.service.interfaces.ISeriesService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.IScreeningMetadataService;
import app.service.interfaces.secondary.IStorageLocationService;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NoarkEntityEditWhenClosedException;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.exceptions.NoarkInvalidStructureException;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.builder.interfaces.ISeriesLinksBuilder;
import app.webapp.payload.builder.interfaces.secondary.IScreeningMetadataLinksBuilder;
import app.webapp.payload.links.*;
import app.webapp.payload.links.casehandling.CaseFileLinks;
import app.webapp.payload.links.secondary.ScreeningMetadataLinks;
import app.webapp.payload.links.secondary.StorageLocationLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static app.utils.NoarkUtils.NoarkEntity.Create.*;
import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static java.util.List.copyOf;

@Service
public class SeriesService
        extends NoarkService
        implements ISeriesService {

    private static final Logger logger =
            LoggerFactory.getLogger(SeriesService.class);

    private final IMetadataService metadataService;
    private final IFileService fileService;
    private final ICaseFileService caseFileService;
    private final IClassificationSystemService classificationSystemService;
    private final ISeriesRepository seriesRepository;
    private final ISeriesLinksBuilder seriesLinksBuilder;
    private final IScreeningMetadataService screeningMetadataService;
    private final IStorageLocationService storageLocationService;
    private final IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder;

    @Value("${nikita.import.allowed:false}")
    private boolean isImportAllowed;

    public SeriesService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IClassificationSystemService classificationSystemService,
            IFileService fileService,
            ICaseFileService caseFileService,
            IMetadataService metadataService,
            ISeriesRepository seriesRepository,
            ISeriesLinksBuilder seriesLinksBuilder,
            IScreeningMetadataService screeningMetadataService,
            IStorageLocationService storageLocationService,
            IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.metadataService = metadataService;
        this.fileService = fileService;
        this.caseFileService = caseFileService;
        this.seriesRepository = seriesRepository;
        this.classificationSystemService = classificationSystemService;
        this.seriesLinksBuilder = seriesLinksBuilder;
        this.screeningMetadataService = screeningMetadataService;
        this.storageLocationService = storageLocationService;
        this.screeningMetadataLinksBuilder = screeningMetadataLinksBuilder;
    }

    // All CREATE operations
    @Override
    @Transactional
    public CaseFileLinks createCaseFileAssociatedWithSeries(
            @NotNull final UUID systemId,
            @NotNull final CaseFile caseFile) {
        Series series = getSeriesOrThrow(systemId, DO_NOT_LOG);
        checkOpenOrThrow(series);
        /* reject if associated Record or ClassificationSystem already
         * exist, as only either Record, File or ClassificationSystem
         * is allowed on the same level according to Noark 5v5. */
        if (!series.getReferenceClassificationSystem().isEmpty()
                || !series.getReferenceRecordEntity().isEmpty()) {
            String info = INFO_INVALID_STRUCTURE +
                    ". Either " + RECORD + ", " + FILE + " or " + CLASSIFICATION_TYPE + " below " + SERIES + " with systemId " + series.getSystemIdAsString();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, SERIES, CASE_FILE);
        }
        caseFile.setReferenceSeries(series);
        return caseFileService.save(caseFile);
    }

    @Override
    @Transactional
    public StorageLocationLinks createStorageLocationAssociatedWithSeries(
            UUID systemId, StorageLocation storageLocation) {
        Series series = getSeriesOrThrow(systemId, DO_NOT_LOG);
        checkOpenOrThrow(series);
        return storageLocationService
                .createStorageLocationAssociatedWithSeries(
                        storageLocation, series);
    }

    @Override
    @Transactional
    public FileLinks createFileAssociatedWithSeries(
            @NotNull final UUID systemId,
            @NotNull final File file) {
        Series series = getSeriesOrThrow(systemId, DO_NOT_LOG);
        checkOpenOrThrow(series);
        /* reject if associated Record or ClassificationSystem already
         * exist, as only either Record, File or ClassificationSystem
         * is allowed on the same level according to Noark 5v5. */
        if (!series.getReferenceClassificationSystem().isEmpty()
                || !series.getReferenceRecordEntity().isEmpty()) {
            String info = INFO_INVALID_STRUCTURE +
                    ". Either " + RECORD + ", " + FILE + " or " + CLASSIFICATION_TYPE + " below " + SERIES + " with systemId " + series.getSystemIdAsString();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, SERIES, FILE);
        }
        file.setReferenceSeries(series);
        return fileService.createFile(file);
    }

    @Override
    @Transactional
    public SeriesLinks save(Series series) {
        validateDocumentMedium(metadataService, series);
        validateDeletion(series.getReferenceDeletion());
        validateScreening(metadataService, series);
        if (null == series.getSeriesStatus()) {
            checkSeriesStatusUponCreation(series);
        }
        return packAsLinks(seriesRepository.save(series));
    }

    @Override
    @Transactional
    public ClassificationSystemLinks createClassificationSystem(
            UUID systemId, ClassificationSystem classificationSystem) {
        Series series = getSeriesOrThrow(systemId, DO_NOT_LOG);
        /* reject if associated Record or File already exist, as only
         * either Record, File or ClassificationSystem is allowed on
         * the same level according to Noark 5v5. */
        if (!series.getReferenceFile().isEmpty()
                || !series.getReferenceRecordEntity().isEmpty()) {
            String info = INFO_INVALID_STRUCTURE +
                    ". Either " + RECORD + ", " + FILE + " or " + CLASSIFICATION_TYPE + " below " + SERIES + " with systemId " + series.getSystemIdAsString();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, SERIES, CLASSIFICATION_SYSTEM);
        }
        series.addClassificationSystem(classificationSystem);
        return classificationSystemService.save(classificationSystem);
    }

    @Override
    public ScreeningMetadataLinks createScreeningMetadataAssociatedWithSeries(
            UUID systemId, Metadata screeningMetadata) {
        Series series = getSeriesOrThrow(systemId, DO_NOT_LOG);
        if (null == series.getReferenceScreening()) {
            throw new NoarkEntityNotFoundException(INFO_CANNOT_FIND_OBJECT +
                    " Screening, associated with Series with systemId " +
                    systemId);
        }
        return screeningMetadataService.createScreeningMetadata(
                series.getReferenceScreening(), screeningMetadata);
    }

    // All READ operations
    @Override
    public SeriesLinks findAll() {
        return (SeriesLinks) processODataQueryGet();
    }

    @Override
    public CaseFileLinks findCaseFilesBySeries(
            @NotNull final UUID systemId) {
        // Make sure Series exists
        getSeriesOrThrow(systemId);
        return (CaseFileLinks) processODataQueryGet();
    }

    @Override
    public RecordLinks findAllRecordAssociatedWithSeries(
            @NotNull final UUID systemId) {
        // Make sure Series exists
        getSeriesOrThrow(systemId, DO_NOT_LOG);
        return (RecordLinks) processODataQueryGet();
    }

    @Override
    public FileLinks findAllFileAssociatedWithSeries(
            @NotNull final UUID systemId) {
        // Make sure Series exists
        getSeriesOrThrow(systemId, DO_NOT_LOG);
        return (FileLinks) processODataQueryGet();
    }

    /**
     * Retrieve the list of ClassificationSystemLinks object associated with
     * the Series object identified by systemId
     *
     * @param systemId The systemId of the Series object to retrieve the
     *                 associated ClassificationSystemLinks
     * @return A ClassificationSystemLinks list
     */
    @Override
    public ClassificationSystemLinks
    findClassificationSystemAssociatedWithSeries(
            @NotNull final UUID systemId) {
        // Make sure Series exists
        getSeriesOrThrow(systemId, DO_NOT_LOG);
        return (ClassificationSystemLinks) processODataQueryGet();
    }

    /**
     * Retrieve the list of FondsLinks object associated with
     * the Series object identified by systemId
     *
     * @param systemId The systemId of the Series object to retrieve the
     *                 associated FondsLinks
     * @return A FondsLinks list
     */
    @Override
    public FondsLinks findFondsAssociatedWithSeries(
            @NotNull final UUID systemId) {
        getSeriesOrThrow(systemId, DO_NOT_LOG);
        return (FondsLinks) processODataQueryGet();
    }

    @Override
    public StorageLocationLinks findStorageLocationAssociatedWithSeries(
            @NotNull final UUID systemID) {
        return (StorageLocationLinks) processODataQueryGet();
    }

    // systemId
    @Override
    public SeriesLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getSeriesOrThrow(systemId));
    }


    @Override
    public ScreeningMetadataLinks
    getScreeningMetadataAssociatedWithSeries(@NotNull final UUID systemId) {
        Screening screening = getSeriesOrThrow(systemId, DO_NOT_LOG)
                .getReferenceScreening();
        if (null == screening) {
            throw new NoarkEntityNotFoundException(
                    INFO_CANNOT_FIND_OBJECT + " Screening, using systemId " +
                            systemId);
        }
        return packAsLinks(new SearchResultsPage(copyOf(
                screening.getReferenceScreeningMetadata())));
    }

    // All UPDATE operations

    /**
     * Update an existing Series object.
     * <p>
     * Note: title is not nullable
     *
     * @param systemId       systemId of Series to update
     * @param incomingSeries the incoming series
     * @return the updated series object after it is persisted
     */
    @Override
    @Transactional
    public SeriesLinks handleUpdate(@NotNull final UUID systemId,
                                    @NotNull final Series incomingSeries) {
        Series existingSeries = getSeriesOrThrow(systemId, DO_NOT_LOG);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingSeries, incomingSeries);
        // Here copy all the values you are allowed to copy ....
        updateDeletion(incomingSeries, existingSeries);
        updateTitleAndDescription(incomingSeries, existingSeries);
        if (null != incomingSeries.getDocumentMedium()) {
            existingSeries.setDocumentMedium(
                    incomingSeries.getDocumentMedium());
        }
        existingSeries.setSeriesStatus(incomingSeries.getSeriesStatus());

        existingSeries.setReferencePrecursorSystemID
                (incomingSeries.getReferencePrecursorSystemID());
        existingSeries.setReferenceSuccessorSystemID
                (incomingSeries.getReferenceSuccessorSystemID());
        updateSeriesReferences(existingSeries);
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingSeries.setVersion(getETag());
        publishChangeLogEvents(changeLogs);
        return packAsLinks(existingSeries);
    }

    @Override
    @Transactional
    public void updateSeriesReferences(Series series) {
        if (null != series.getReferencePrecursorSystemID()) {
            Series referenceSeries = seriesRepository.
                    findBySystemId(series.getReferencePrecursorSystemID());
            if (null != referenceSeries) {
                if (null != referenceSeries.getReferenceSuccessorSystemID()) {
                    String info = "not allowed to set precursor to series with existing successor";
                    throw new NikitaMalformedInputDataException(info);
                }
                referenceSeries
                        .setReferenceSuccessorSystemID(series.getSystemId());
                referenceSeries.setReferenceSuccessor(series);
            }
            // Will set reference to null if series with SystemID not found
            series.setReferencePrecursor(referenceSeries);
        } else {
            series.setReferencePrecursor(null);
        }

        if (null != series.getReferenceSuccessorSystemID()) {
            Series referenceSeries = seriesRepository.
                    findBySystemId(series.getReferenceSuccessorSystemID());
            if (null != referenceSeries) {
                if (null != referenceSeries.getReferencePrecursorSystemID()) {
                    String info = "not allowed to set successor to series with existing precursor";
                    throw new NikitaMalformedInputDataException(info);
                }
                referenceSeries
                        .setReferencePrecursorSystemID(series.getSystemId());
                referenceSeries.setReferencePrecursor(series);
            }
            // Will set reference to null if series with SystemID not found
            series.setReferenceSuccessor(referenceSeries);
        } else {
            series.setReferenceSuccessor(null);
        }
    }

    // All DELETE operations
    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        Series series = getSeriesOrThrow(systemId, DO_NOT_LOG);
        deleteEntity(series);
    }

    /**
     * Delete all objects belonging to the organisation identified by organisation
     */
    @Override
    @Transactional
    public void deleteAllByOrganisation() {
        seriesRepository.deleteByOrganisation(getOrganisation());
    }

    public SeriesLinks generateDefaultSeries(@NotNull final UUID systemId) {
        Series defaultSeries = new Series();
        SeriesStatus seriesStatus = (SeriesStatus)
                metadataService.findValidMetadataByEntityTypeOrThrow
                        (SERIES_STATUS, SERIES_STATUS_ACTIVE_CODE, null);
        defaultSeries.setSeriesStatus(seriesStatus);
        DocumentMedium documentMedium = (DocumentMedium)
                metadataService.findValidMetadataByEntityTypeOrThrow
                        (DOCUMENT_MEDIUM, DOCUMENT_MEDIUM_ELECTRONIC_CODE, null);
        defaultSeries.setDocumentMedium(documentMedium);
        defaultSeries.setVersion(-1L, true);
        return packAsLinks(defaultSeries);
    }

    @Override
    public ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId) {
        return screeningMetadataService.getDefaultScreeningMetadata(systemId);
    }

    @Override
    public StorageLocationLinks getDefaultStorageLocation(
            @NotNull final UUID systemId) {
        return storageLocationService.getDefaultStorageLocation(systemId);
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingSeries the existing Series from the database
     * @param newSeries      the incoming Series
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(Series existingSeries, Series newSeries) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // arkivdel	M051	arkivdelstatus	Ved endring
        if (!existingSeries.getSeriesStatus().equals(newSeries.getSeriesStatus())) {
            changeLogs.add(createChangeLogObject(SERIES_STATUS,
                    existingSeries.getSeriesStatus().getCode() + " " +
                            existingSeries.getSeriesStatus().getCodeName(),
                    newSeries.getSeriesStatus().getCode() + " " +
                            newSeries.getSeriesStatus().getCodeName(),
                    existingSeries));
        }
        // arkivdel	M020	tittel	Ved endring
        if (!existingSeries.getTitle().equals(newSeries.getTitle())) {
            changeLogs.add(createChangeLogObject(TITLE, existingSeries.getTitle(),
                    newSeries.getTitle(), existingSeries));
        }
        // TODO: Missing  arkivdel	M204	referanseKlassifikasjonssystem	Ved endring
        // The variable is a FK link, not a UUID field. Must be implemented first
        return changeLogs;
    }

    // Helper methods

    public ScreeningMetadataLinks packAsLinks(SearchResultsPage page) {
        ScreeningMetadataLinks screeningMetadataLinks =
                new ScreeningMetadataLinks(page);
        applyLinksAndHeader(screeningMetadataLinks,
                screeningMetadataLinksBuilder);
        return screeningMetadataLinks;
    }

    public SeriesLinks packAsLinks(@NotNull final Series series) {
        SeriesLinks seriesLinks = new SeriesLinks(series);
        applyLinksAndHeader(seriesLinks, seriesLinksBuilder);
        return seriesLinks;
    }

    /**
     * Internal helper method. Check that the status of the CaseFile is set to
     * open or throw an exception.
     *
     * @param series The series object to check if it open
     */
    private void checkOpenOrThrow(@NotNull Series series) {
        if (!isImportAllowed &&
                null != series.getSeriesStatus() &&
                SERIES_STATUS_CLOSED_CODE.equals(series.getSeriesStatus().getCode())) {
            String info = INFO_CANNOT_ASSOCIATE_WITH_CLOSED_OBJECT +
                    ". Series with systemId " + series.getSystemIdAsString() +
                    " has status code " + SERIES_STATUS_CLOSED_CODE;
            logger.info(info);
            throw new NoarkEntityEditWhenClosedException(info);
        }
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid Series back. If there is no valid
     * Series, an exception is thrown
     *
     * @param systemId systemId of the series object to retrieve
     * @return the Series object
     */
    public Series getSeriesOrThrow(@NotNull final UUID systemId) {
        return getSeriesOrThrow(systemId, true);
    }

    private Series getSeriesOrThrow(@NotNull final UUID systemId, @NotNull final boolean logRead) {
        Series series = seriesRepository.
                findBySystemId(systemId);
        if (series == null) {
            throw new NoarkEntityNotFoundException(
                    INFO_CANNOT_FIND_OBJECT + " Series, using systemId "
                            + systemId);
        }
        if (logRead) {
            handleReadForEventLog(series);
        }
        return series;

    }

    /**
     * Internal helper method. Verify the SeriesStatus code provided is
     * in the metadata catalog, and copy the code name associated with
     * the code from the metadata catalog into the Series.
     * <p>
     * If the SeriesStatus code is unknown, a
     * NoarkEntityNotFoundException exception is thrown that the
     * caller has to deal with.  If the code and code name provided do
     * not match the entries in the metadata catalog, a
     * NoarkInvalidStructureException exception is thrown.
     *
     * @param series The series object
     */
    public void checkSeriesStatusUponCreation(Series series) {
        if (series.getSeriesStatus() != null) {
            SeriesStatus seriesStatus = (SeriesStatus) metadataService
                    .findValidMetadata(series.getSeriesStatus());
            series.setSeriesStatus(seriesStatus);
        }
    }
}
