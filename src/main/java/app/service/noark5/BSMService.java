package app.service.noark5;

import app.domain.noark5.bsm.BSMBase;
import app.domain.noark5.md_other.BSMMetadata;
import app.domain.repository.noark5.v5.other.IBSMMetadataRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.IBSMService;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static app.utils.constants.N5ResourceMappings.BSM_DEF;

/**
 * Service class for businessSpecificMetadata (virksomhetsspesifikkeMetadata).
 */
@Service
public class BSMService
        extends NoarkService
        implements IBSMService {

    private static final Logger logger =
            LoggerFactory.getLogger(BSMService.class);

    private final IBSMMetadataRepository metadataRepository;

    public BSMService(EntityManager entityManager,
                      ApplicationEventPublisher applicationEventPublisher,

                      IPatchService patchService,
                      IBSMMetadataRepository metadataRepository,
                      ILocalUserDetails userDetails,
                      IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.metadataRepository = metadataRepository;
    }

    @Override
    public Optional<BSMMetadata> findBSMByName(@NotNull String name) {
        return metadataRepository.findByName(name);
    }

    /**
     * Validate a list of BSMBase objects.Currently we are only checking that
     * the name is registered
     *
     * @param referenceBSMBase the list of BSMBase objects
     */
    @Override
    public void validateBSMList(@NotNull List<BSMBase> referenceBSMBase) {
        for (BSMBase bsmBase : referenceBSMBase) {
            checkBSMNameRegistered(bsmBase);
        }
    }

    /**
     * Check that the name of the BSMBase object is registered
     *
     * @param bsmBase The BSMBase object with a name to check
     */
    private void checkBSMNameRegistered(BSMBase bsmBase) {
        if (metadataRepository.findByName(
                bsmBase.getValueName()).isEmpty()) {
            String error = bsmBase.getValueName() + " is not a registered" +
                    BSM_DEF + " name";
            logger.error(error);
            throw new NikitaMalformedInputDataException(error);
        }
    }
}
