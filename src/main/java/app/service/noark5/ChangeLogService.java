package app.service.noark5;

import app.domain.noark5.admin.ChangeLog;
import app.domain.repository.noark5.v5.IChangeLogRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.IChangeLogService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.IChangeLogLinksBuilder;
import app.webapp.payload.links.ChangeLogLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class ChangeLogService
        extends NoarkService
        implements IChangeLogService {

    private final IChangeLogRepository changeLogRepository;
    private final IChangeLogLinksBuilder changeLogLinksBuilder;

    public ChangeLogService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IChangeLogRepository changeLogRepository,
            IChangeLogLinksBuilder changeLogLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.changeLogRepository = changeLogRepository;
        this.changeLogLinksBuilder = changeLogLinksBuilder;
    }

    @Override
    public ChangeLogLinks generateDefaultChangeLog() {
        ChangeLog defaultChangeLog = new ChangeLog();
        defaultChangeLog.setEventDate(OffsetDateTime.now());
        defaultChangeLog.setEventInitiator(getUser());
        defaultChangeLog.setVersion(-1L, true);
        return packAsLinks(defaultChangeLog);
    }

    @Override
    @Transactional
    public ChangeLogLinks createNewChangeLog(ChangeLog changeLog) {
        if (null == changeLog.getEventDate())
            changeLog.setEventDate(OffsetDateTime.now());
        return packAsLinks(changeLogRepository.save(changeLog));
    }

    @Override
    public ChangeLogLinks findChangeLogByOwner() {
        return (ChangeLogLinks) processODataQueryGet();
    }

    @Override
    public ChangeLogLinks findSingleChangeLog(@NotNull final UUID systemId) {
        return packAsLinks(getChangeLogOrThrow(systemId));
    }

    @Override
    @Transactional
    public ChangeLogLinks handleUpdate(@NotNull final UUID systemId,
                                       @NotNull ChangeLog incomingChangeLog) {
        ChangeLog existingChangeLog = getChangeLogOrThrow(systemId);
	/*
        // Copy all the values you are allowed to copy ....
        existingChangeLog.setChangeLogText(incomingChangeLog.getChangeLogText());
        existingChangeLog.setChangeLogDate(incomingChangeLog.getChangeLogDate());
        existingChangeLog.setChangeLogRegisteredBy
            (incomingChangeLog.getChangeLogRegisteredBy());
	*/
        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingChangeLog.setVersion(getETag());
        return packAsLinks(existingChangeLog);
    }

    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        deleteEntity(getChangeLogOrThrow(systemId));
    }


    public ChangeLogLinks packAsLinks(@NotNull final ChangeLog changeLog) {
        ChangeLogLinks changeLogLinks = new ChangeLogLinks(changeLog);
        applyLinksAndHeader(changeLogLinks, changeLogLinksBuilder);
        return changeLogLinks;
    }

    protected ChangeLog getChangeLogOrThrow(@NotNull final UUID systemId) {
        ChangeLog changeLog = changeLogRepository.
                findBySystemId(systemId);
        if (changeLog == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " ChangeLog, using systemId " + systemId;
            throw new NoarkEntityNotFoundException(info);
        }
        return changeLog;
    }
}
