package app.service.noark5;

import app.domain.noark5.Fonds;
import app.domain.noark5.FondsCreator;
import app.domain.noark5.Series;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.metadata.DocumentMedium;
import app.domain.noark5.metadata.FondsStatus;
import app.domain.noark5.secondary.StorageLocation;
import app.domain.repository.noark5.v5.IFondsRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.IFondsCreatorService;
import app.service.interfaces.IFondsService;
import app.service.interfaces.ISeriesService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.IStorageLocationService;
import app.webapp.exceptions.NoarkEntityEditWhenClosedException;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.exceptions.NoarkInvalidStructureException;
import app.webapp.payload.builder.interfaces.IFondsLinksBuilder;
import app.webapp.payload.links.FondsCreatorLinks;
import app.webapp.payload.links.FondsLinks;
import app.webapp.payload.links.SeriesLinks;
import app.webapp.payload.links.secondary.StorageLocationLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static app.utils.NoarkUtils.NoarkEntity.Create.setFinaliseEntityValues;
import static app.utils.NoarkUtils.NoarkEntity.Create.validateDocumentMedium;
import static app.utils.constants.Constants.*;
import static app.utils.constants.DatabaseConstants.DELETE_FONDS_STORAGE_LOCATION;
import static app.utils.constants.DatabaseConstants.DELETE_FROM_FONDS_FONDS_CREATOR;
import static app.utils.constants.N5ResourceMappings.*;

@Service
public class FondsService
        extends NoarkService
        implements IFondsService {

    private static final Logger logger =
            LoggerFactory.getLogger(FondsService.class);

    private final IFondsRepository fondsRepository;
    private final ISeriesService seriesService;
    private final IMetadataService metadataService;
    private final IFondsCreatorService fondsCreatorService;
    private final IFondsLinksBuilder fondsLinksBuilder;
    private final IStorageLocationService storageLocationService;

    @Value("${nikita.import.allowed:false}")
    private boolean isImportAllowed;

    public FondsService(EntityManager entityManager,
                        ApplicationEventPublisher applicationEventPublisher,
                        IPatchService patchService,
                        IFondsRepository fondsRepository,
                        ISeriesService seriesService,
                        IMetadataService metadataService,
                        IFondsCreatorService fondsCreatorService,
                        IFondsLinksBuilder fondsLinksBuilder,
                        IStorageLocationService storageLocationService,
                        ILocalUserDetails userDetails,
                        IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.fondsRepository = fondsRepository;
        this.seriesService = seriesService;
        this.metadataService = metadataService;
        this.fondsCreatorService = fondsCreatorService;
        this.fondsLinksBuilder = fondsLinksBuilder;
        this.storageLocationService = storageLocationService;
    }

    // All CREATE operations

    /**
     * Persists a new fonds object to the database. Some values are set in the
     * incoming payload (e.g. title) and some are set by the core.
     * owner, createdBy, createdDate are automatically set by the core.
     *
     * @param fonds fonds object with some values set
     * @return the newly persisted fonds object wrapped as a fondsLinks object
     */
    @Override
    @Transactional
    public FondsLinks createNewFonds(@NotNull Fonds fonds) {
        validateDocumentMedium(metadataService, fonds);
        if (null == fonds.getFondsStatus()) {
            FondsStatus fondsStatus = (FondsStatus)
                    metadataService.findValidMetadataByEntityTypeOrThrow
                            (FONDS_STATUS, FONDS_STATUS_OPEN_CODE, null);
            fonds.setFondsStatus(fondsStatus);
        }
        checkFondsStatusUponCreation(fonds);
        setFinaliseEntityValues(fonds);
        handleCreationForEventLog(fonds);
        return packAsLinks(fondsRepository.save(fonds));
    }

    /**
     * Persists a new fonds object to the database, that is first associated
     * with a parent fonds object. Some values are set in the incoming
     * payload (e.g. title) and some are set by the core. owner, createdBy,
     * createdDate are automatically set by the core.
     * <p>
     * First we try to locate the parent. If the parent does not exist a
     * NoarkEntityNotFoundException exception is thrown
     *
     * @param childFonds          incoming fonds object with some values set
     * @param parentFondsSystemId The systemId of the parent fonds
     * @return the newly persisted fonds object
     */
    @Override
    @Transactional
    public FondsLinks createFondsAssociatedWithFonds(
            @NotNull final UUID parentFondsSystemId,
            @NotNull final Fonds childFonds) {
        Fonds parentFonds = getFondsOrThrow(parentFondsSystemId, false);
        checkFondsDoesNotContainSeries(parentFonds);
        childFonds.setReferenceParentFonds(parentFonds);
        return createNewFonds(childFonds);
    }

    /**
     * Persists a new Series object to the database. Some values are set in
     * the incoming payload (e.g. title) and some are set by the core. owner,
     * createdBy, createdDate are automatically set by the core.
     * <p>
     * First we try to locate the fonds to associate the Series with. If the
     * fonds does not exist a NoarkEntityNotFoundException exception is
     * thrown. Then we check that the fonds does not have children fonds. If it
     * does an NoarkInvalidStructureException exception is thrown. After that
     * we check that the Fonds object is not already closed.
     *
     * @param systemId The systemId of the fonds object to associate a
     *                 Series with
     * @param series   The incoming Series object
     * @return the newly persisted series object wrapped as a SeriesLinks
     * object
     */
    @Override
    @Transactional
    public SeriesLinks createSeriesAssociatedWithFonds(
            @NotNull final UUID systemId,
            @NotNull final Series series) {
        Fonds fonds = getFondsOrThrow(systemId, false);
        seriesService.updateSeriesReferences(series);
        seriesService.checkSeriesStatusUponCreation(series);
        checkFondsNotClosed(fonds);
        checkFondsDoesNotContainSubFonds(fonds);
        series.setReferenceFonds(fonds);
        return seriesService.save(series);
    }

    /**
     * Persists a new FondsCreator object to the database. Some values are set
     * in the incoming payload (e.g. fonds_creator_name) and some are set by
     * the core. owner,createdBy, createdDate are automatically set by the core.
     * <p>
     * First we try to locate the fonds to associate the FondsCreator with. If
     * the fonds does not exist a NoarkEntityNotFoundException exception is
     * thrown. After that we check that the Fonds object is not already closed.
     *
     * @param systemId     The systemId of the fonds object you wish to
     *                     associate the fondsCreator object with
     * @param fondsCreator incoming fondsCreator object with some values set
     * @return the newly persisted fondsCreator object wrapped as a
     * FondsCreatorLinks object
     */
    @Override
    @Transactional
    public FondsCreatorLinks createFondsCreatorAssociatedWithFonds(
            @NotNull final UUID systemId,
            @NotNull final FondsCreator fondsCreator) {
        Fonds fonds = getFondsOrThrow(systemId, false);
        checkFondsNotClosed(fonds);
        fonds.addFondsCreator(fondsCreator);
        return fondsCreatorService.createNewFondsCreator(fondsCreator);
    }

    @Override
    @Transactional
    public StorageLocationLinks createStorageLocationAssociatedWithFonds(
            UUID systemId, StorageLocation storageLocation) {
        Fonds fonds = getFondsOrThrow(systemId, false);
        return storageLocationService
                .createStorageLocationAssociatedWithFonds(
                        storageLocation, fonds);
    }

    // All READ operations

    /**
     * Retrieve a list of Series objects associated with a given Fonds
     * from the database. First we try to locate the Fonds object. If the
     * Fonds object does not exist a NoarkEntityNotFoundException exception
     * is thrown that the caller has to deal with.
     * <p>
     * If any Series objects exist, they are wrapped in a SeriesLinks
     * object and returned to the caller.
     *
     * @param systemId The systemId of the Fonds object that you want to
     *                 retrieve associated Series objects
     * @return the list of Series objects wrapped as a SeriesLinks object
     */
    @Override
    public SeriesLinks findSeriesAssociatedWithFonds(
            @NotNull final UUID systemId) {
        return (SeriesLinks) processODataQueryGet();
    }

    /**
     * Retrieve a single Fonds objects from the database.
     *
     * @param systemId The systemId of the Fonds object you wish to
     *                 retrieve
     * @return the Fonds object wrapped as a FondsLinks object
     */
    @Override
    public FondsLinks findSingleFonds(@NotNull final UUID systemId) {
        return packAsLinks(getFondsOrThrow(systemId));
    }

    /**
     * Retrieve a list of paginated Fonds objects associated from the database.
     *
     * @return the list of Fonds object wrapped as a FondsLinks object
     */
    @Override
    public FondsLinks findAllFonds() {
        return (FondsLinks) processODataQueryGet();
    }

    /**
     * Retrieve a list of children fonds belonging to the fonds object
     * identified by systemId
     *
     * @param systemId The systemId of the Fonds object to retrieve its children
     * @return A FondsLinks object containing the children fonds's
     */
    @Override
    public FondsLinks findAllChildren(@NotNull final UUID systemId) {
        getFondsOrThrow(systemId, false);
        return (FondsLinks) processODataQueryGet();
    }

    /**
     * Retrieve a list of FondsCreator objects associated with a given Fonds
     * from the database. First we try to locate the Fonds object. If the
     * Fonds object does not exist a NoarkEntityNotFoundException exception
     * is thrown that the caller has to deal with.
     * <p>
     * If any FondsCreator objects exist, they are wrapped in a
     * FondsCreatorLinks object and returned to the caller.
     *
     * @param systemId The systemId of the Fonds object that you want to
     *                 retrieve associated FondsCreator objects
     * @return the fondsCreator objects wrapped as a FondsCreatorLinks object
     */
    @Override
    public FondsCreatorLinks findFondsCreatorAssociatedWithFonds(
            @NotNull final UUID systemId) {
        return (FondsCreatorLinks) processODataQueryGet();
    }

    @Override
    public StorageLocationLinks findStorageLocationAssociatedWithFonds(
            @NotNull final UUID systemID) {
        return (StorageLocationLinks) processODataQueryGet();
    }

    /**
     * Generate a Default Series object that can be associated with the
     * identified Fonds.
     * <br>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param systemId The systemId of the Fonds object you wish to
     *                 generate a default Series for
     * @return the Series object wrapped as a SeriesLinks object
     */
    @Override
    public SeriesLinks generateDefaultSeries(@NotNull final UUID systemId) {
        return seriesService.generateDefaultSeries(systemId);
    }

    /**
     * Generate a Default Fonds object that can be associated with the
     * identified Fonds. If systemId has a value, it is assumed you wish
     * to generate a sub-fonds.
     * <br>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param systemId The systemId of the Fonds object you wish to
     *                 generate a default sub-fonds. Null if the Fonds
     *                 is not a sub-fonds.
     * @return the Fonds object wrapped as a FondsLinks object
     */
    @Override
    public FondsLinks generateDefaultFonds(@NotNull final UUID systemId) {
        Fonds defaultFonds = new Fonds();
        DocumentMedium documentMedium = (DocumentMedium)
                metadataService.findValidMetadataByEntityTypeOrThrow
                        (DOCUMENT_MEDIUM, DOCUMENT_MEDIUM_ELECTRONIC_CODE, null);
        defaultFonds.setDocumentMedium(documentMedium);
        defaultFonds.setVersion(-1L, true);
        return packAsLinks(defaultFonds);
    }

    @Override
    public StorageLocationLinks getDefaultStorageLocation(
            @NotNull final UUID systemId) {
        return storageLocationService.getDefaultStorageLocation(systemId);
    }

    // All UPDATE operations

    /**
     * Updates a Fonds object in the database. First we try to locate the
     * Fonds object. If the Fonds object does not exist a
     * NoarkEntityNotFoundException exception is thrown that the caller has
     * to deal with.
     * <br>
     * After this the values you are allowed to update are copied from the
     * incomingFonds object to the existingFonds object and the existingFonds
     * object will be persisted to the database when the transaction boundary
     * is over.
     * <p>
     * Note, the version corresponds to the version number, when the object
     * was initially retrieved from the database. If this number is not the
     * same as the version number when re-retrieving the Fonds object from
     * the database a NoarkConcurrencyException is thrown. Note. This happens
     * when the call to Fonds.setVersion() occurs.
     *
     * @param systemId      The systemId of the fonds object to retrieve
     * @param incomingFonds The incoming fonds object
     */
    @Override
    @Transactional
    public FondsLinks handleUpdate(@NotNull final UUID systemId,
                                   @NotNull Fonds incomingFonds) {
        Fonds existingFonds = getFondsOrThrow(systemId, false);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingFonds, incomingFonds);
        // Copy all the values you are allowed to copy ....
        updateTitleAndDescription(incomingFonds, existingFonds);
        if (null != incomingFonds.getDocumentMedium()) {
            existingFonds.setDocumentMedium(
                    incomingFonds.getDocumentMedium());
        }
        existingFonds.setFondsStatus(incomingFonds.getFondsStatus());
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingFonds.setVersion(getETag());
        publishChangeLogEvents(changeLogs);
        return packAsLinks(existingFonds);
    }

    // All DELETE operations

    /**
     * Deletes a Fonds object from the database. First we try to locate the
     * Fonds object. If the Fonds object does not exist a
     * NoarkEntityNotFoundException exception is thrown that the caller has
     * to deal with. Note that as there is a @ManyToMany relationship between
     * Fonds and FondsCreator with a join table, we first have to
     * disassociate the link between Fonds and FondsCreator or we hit a
     * foreign key constraint issue. The same applies for Fonds and
     * StorageLocation.
     * <p>
     * In order to minimise problems that could be caused with table and
     * column names changing, constants are used to define relevant column
     * and table names.
     * <p>
     * The approach is is discussed in a nikita gitlab issue
     * https://gitlab.com/OsloMet-ABI/nikita-noark5-core/issues/82
     *
     * @param systemId The systemId of the fonds object to retrieve
     */
    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        Fonds fonds = getFondsOrThrow(systemId, false);
        // Disassociate any links between Fonds and FondsCreator
        disassociateForeignKeys(fonds, DELETE_FROM_FONDS_FONDS_CREATOR);
        // Disassociate any links between Fonds and StorageLocation
        disassociateForeignKeys(fonds, DELETE_FONDS_STORAGE_LOCATION);
        deleteEntity(fonds);
    }

    /**
     * Delete all objects belonging to the organisation identified by organisation
     */
    @Override
    @Transactional
    public void deleteAllByOrganisation() {
        fondsRepository.deleteByOrganisation(getOrganisation());
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingFonds the existing Fonds from the database
     * @param newFonds      the incoming Fonds
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(Fonds existingFonds, Fonds newFonds) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // arkiv	M050	arkivstatus	Ved endring
        if (!existingFonds.getFondsStatus().equals(newFonds.getFondsStatus())) {
            changeLogs.add(createChangeLogObject(FONDS_STATUS,
                    existingFonds.getFondsStatus().getCode() + " " +
                            existingFonds.getFondsStatus().getCodeName(),
                    newFonds.getFondsStatus().getCode() + " " +
                            newFonds.getFondsStatus().getCodeName(),
                    existingFonds));
        }
        // arkiv	M020	tittel	Ved endring
        if (!existingFonds.getTitle().equals(newFonds.getTitle())) {
            changeLogs.add(createChangeLogObject(TITLE, existingFonds.getTitle(),
                    newFonds.getTitle(), existingFonds));
        }
        return changeLogs;
    }

    // All HELPER operations

    public FondsLinks packAsLinks(Fonds fonds) {
        FondsLinks fondsLinks = new FondsLinks(fonds);
        applyLinksAndHeader(fondsLinks, fondsLinksBuilder);
        return fondsLinks;
    }

    private Fonds getFondsOrThrow(@NotNull final UUID systemId) {
        return getFondsOrThrow(systemId, true);
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid Fonds back. If there is no valid
     * Fonds, a NoarkEntityNotFoundException exception is thrown
     *
     * @param systemId The systemId of the fonds object to retrieve
     * @return the fonds object
     */
    private Fonds getFondsOrThrow(@NotNull final UUID systemId,
                                  @NotNull final boolean logRead) {
        Fonds fonds = fondsRepository.findBySystemId(systemId);
        if (fonds == null) {
            throw new NoarkEntityNotFoundException(INFO_CANNOT_FIND_OBJECT +
                    " Fonds, using systemId " + systemId);
        }
        if (logRead) {
            handleReadForEventLog(fonds);
        }
        return fonds;
    }

    /**
     * Internal helper method. Check to make sure the fonds object does not
     * have a status of 'finalised'. If the check fails, a
     * NoarkEntityEditWhenClosedException exception is thrown that the caller
     * has to deal with.
     *
     * @param fonds The fonds object
     */
    private void checkFondsNotClosed(@NotNull Fonds fonds) {
        if (!isImportAllowed &&
                null != fonds.getFondsStatus() &&
                FONDS_STATUS_CLOSED_CODE.equals(
                        fonds.getFondsStatus().getCode())) {
            String info = INFO_CANNOT_ASSOCIATE_WITH_CLOSED_OBJECT +
                    ". Fonds with systemId " + fonds.getSystemId() +
                    " has status code " + FONDS_STATUS_CLOSED_CODE;
            logger.info(info);
            throw new NoarkEntityEditWhenClosedException(info);
        }
    }

    /**
     * Internal helper method. Check to make sure the fonds object does not
     * have any sub-fonds. This is useful when e.g. adding a Series to a fonds
     * .You must make sure that the fonds object does not have any child
     * fonds objects.
     * <p>
     * If the check fails, a NoarkInvalidStructureException exception is
     * thrown that the caller has to deal with.
     *
     * @param fonds The fonds object
     */
    private void checkFondsDoesNotContainSubFonds(@NotNull Fonds fonds) {
        if (fonds.getReferenceChildFonds() != null &&
                fonds.getReferenceChildFonds().size() > 0) {
            String info = INFO_INVALID_STRUCTURE + " Cannot associate a " +
                    "Series with a Fonds that already has sub-fonds " +
                    fonds.getSystemId();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, "Fonds", "Series");
        }
    }

    /**
     * Internal helper method. Check to make sure the fonds object does not
     * have any Series associated with it. This is useful when e.g. adding a
     * sub-fonds to a fonds. You must make sure that the fonds object
     * does not have any Series associated with it.
     * <p>
     * If the check fails, a NoarkInvalidStructureException exception is
     * thrown that the caller has to deal with.
     *
     * @param fonds The fonds object
     */
    private void checkFondsDoesNotContainSeries(@NotNull Fonds fonds) {
        if (fonds.getReferenceSeries() != null &&
                fonds.getReferenceSeries().size() > 0) {
            String info = INFO_INVALID_STRUCTURE + " Cannot associate a " +
                    "(sub) Fonds with a Fonds that already has a Series " +
                    "associated with it!" + fonds.getSystemId();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, "Fonds", "Series");
        }
    }

    /**
     * Internal helper method. Verify the FondsStatus code provided is
     * in the metadata catalog, and copy the code name associated with
     * the code from the metadata catalog into the Fonds.
     * <p>
     * If the FondsStatus code is unknown, a
     * NoarkEntityNotFoundException exception is thrown that the
     * caller has to deal with.  If the code and code name provided do
     * not match the entries in the metadata catalog, a
     * NoarkInvalidStructureException exception is thrown.
     *
     * @param fonds The fonds object
     */
    private void checkFondsStatusUponCreation(Fonds fonds) {
        if (fonds.getFondsStatus() != null) {
            FondsStatus fondsStatus = (FondsStatus)
                    metadataService.findValidMetadata(fonds.getFondsStatus());
            fonds.setFondsStatus(fondsStatus);
        }
    }
}
