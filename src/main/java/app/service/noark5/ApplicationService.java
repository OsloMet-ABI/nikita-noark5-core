package app.service.noark5;

import app.domain.noark5.admin.User;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.webapp.exceptions.NikitaMisconfigurationException;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Objects;

import static app.utils.CommonUtils.WebUtils.getMethodsForRequestAsListOrThrow;
import static app.utils.constants.Constants.*;
import static org.springframework.http.HttpHeaders.ALLOW;
import static org.springframework.http.HttpHeaders.ETAG;

@Service
public class ApplicationService {

    protected final EntityManager entityManager;
    protected final ILocalUserDetails userDetails;
    protected final IUrlDetails urlDetails;

    public ApplicationService(EntityManager entityManager,
                              ILocalUserDetails userDetails,
                              IUrlDetails urlDetails) {
        this.entityManager = entityManager;
        this.userDetails = userDetails;
        this.urlDetails = urlDetails;
    }

    protected String getServletPath() {
        return ((ServletRequestAttributes)
                Objects.requireNonNull(
                        RequestContextHolder.getRequestAttributes()))
                .getRequest().getServletPath();
    }

    protected Long getETag() {
        return Long.parseLong(((ServletRequestAttributes)
                Objects.requireNonNull(
                        RequestContextHolder.getRequestAttributes()))
                .getRequest().getHeader(ETAG).replaceAll("^\"|\"$", ""));
    }

    protected HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (null == requestAttributes) {
            throw new NikitaMisconfigurationException("Unable to get Request object");
        }
        return ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
    }

    protected HttpServletResponse getResponse() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (null == requestAttributes) {
            throw new NikitaMisconfigurationException("Unable to get Response object");
        }
        return ((ServletRequestAttributes) requestAttributes).getResponse();
    }

    protected String getMethod() {
        HttpServletRequest request = getRequest();
        String method = request.getMethod().toLowerCase();
        if (method.equals("get")) {
            String url = request.getRequestURL().toString();
            if (url.contains(NEW + DASH) || url.contains(EXPAND_TO)) {
                return "get-template";
            }
        }
        return method;
    }


    protected String getOrganisation() {
        return userDetails.getOrganisation();
    }

    /**
     * Get the user identified with the current request
     *
     * @return The user object
     */
    public User getUserObject() {
        return userDetails.getLoggedInUser();
    }

    public String getUser() {
        return userDetails.getLoggedInUsername();
    }

    /**
     * Set the outgoing ALLOW header
     */
    protected void setOutgoingRequestHeaderList() {
        HttpServletResponse response = ((ServletRequestAttributes)
                Objects.requireNonNull(
                        RequestContextHolder.getRequestAttributes()))
                .getResponse();
        HttpServletRequest request =
                ((ServletRequestAttributes)
                        RequestContextHolder
                                .getRequestAttributes()).getRequest();
        if (response != null) {
            response.addHeader(ALLOW, getMethodsForRequestAsListOrThrow(
                    request.getServletPath()));
        } else {
            throw new NikitaMisconfigurationException("Could not get outgoing respone to set ALLOW headers");
        }
    }
}
