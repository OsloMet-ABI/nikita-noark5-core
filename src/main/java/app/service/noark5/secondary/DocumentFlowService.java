package app.service.noark5.secondary;

import app.domain.noark5.admin.User;
import app.domain.noark5.casehandling.RecordNote;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.metadata.FlowStatus;
import app.domain.noark5.secondary.DocumentFlow;
import app.domain.repository.noark5.v5.secondary.IDocumentFlowRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.IUserService;
import app.service.application.IPatchService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.IDocumentFlowService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.secondary.IDocumentFlowLinksBuilder;
import app.webapp.payload.links.secondary.DocumentFlowLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.Constants.TEMPLATE_DOCUMENT_FLOW_FLOW_STATUS_CODE;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_FLOW_FLOW_FROM;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_FLOW_FLOW_TO;

@Service
public class DocumentFlowService
        extends NoarkService
        implements IDocumentFlowService {

    private static final Logger logger =
            LoggerFactory.getLogger(DocumentFlowService.class);

    private final IUserService userService;
    private final IMetadataService metadataService;
    private final IDocumentFlowRepository documentFlowRepository;
    private final IDocumentFlowLinksBuilder documentFlowLinksBuilder;

    public DocumentFlowService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IUserService userService,
            IMetadataService metadataService,
            IDocumentFlowRepository documentFlowRepository,
            IDocumentFlowLinksBuilder documentFlowLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.userService = userService;
        this.metadataService = metadataService;
        this.documentFlowRepository = documentFlowRepository;
        this.documentFlowLinksBuilder = documentFlowLinksBuilder;
    }
    // All READ methods

    @Override
    public DocumentFlowLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getDocumentFlowOrThrow(systemId));
    }

    @Override
    public DocumentFlowLinks findAll() {
        return (DocumentFlowLinks) processODataQueryGet();
    }
    // All update methods

    @Override
    @Transactional
    public DocumentFlowLinks associateDocumentFlowWithRegistryEntry
            (DocumentFlow documentFlow, RegistryEntry registryEntry) {
        validateFlowStatus(documentFlow);
        checkFlowSentDate(documentFlow);
        createUserReferences(documentFlow);
        documentFlow.setReferenceRegistryEntry(registryEntry);
        documentFlow = documentFlowRepository.save(documentFlow);
        registryEntry.addDocumentFlow(documentFlow);
        return packAsLinks(documentFlow);
    }

    @Override
    @Transactional
    public DocumentFlowLinks associateDocumentFlowWithRecordNote(
            @NotNull final DocumentFlow documentFlow,
            @NotNull final RecordNote recordNote) {
        validateFlowStatus(documentFlow);
        checkFlowSentDate(documentFlow);
        createUserReferences(documentFlow);
        documentFlow.setReferenceRecordNote(recordNote);
        return packAsLinks(documentFlowRepository.save(documentFlow));
    }

    @Override
    @Transactional
    public DocumentFlowLinks updateDocumentFlowBySystemId(
            @NotNull final UUID systemId, @NotNull final DocumentFlow incoming) {
        DocumentFlow existing = getDocumentFlowOrThrow(systemId);
        existing.setFlowReceivedDate(incoming.getFlowReceivedDate());
        existing.setFlowStatus(incoming.getFlowStatus());
        existing.setFlowComment(incoming.getFlowComment());
        /* Only allow 'sent' date to be set during creation
	        existing.setFlowSentDate(incoming.getFlowSentDate());
	    */
        User to = userService.validateUserReference
                (DOCUMENT_FLOW_FLOW_TO, incoming.getReferenceFlowTo(),
                        incoming.getFlowTo(),
                        incoming.getReferenceFlowToSystemID());
        existing.setReferenceFlowTo(to);
        if (null != to) {
            existing.setFlowTo(to.getUsername());
            existing.setReferenceFlowToSystemID(to.getSystemId());
        } else {
            existing.setFlowTo(incoming.getFlowTo());
            existing.setReferenceFlowToSystemID(incoming.getReferenceFlowToSystemID());
        }

        /* Only allow 'from' to be set during creation
        User from = userService.validateUserReference
            (DOCUMENT_FLOW_FLOW_FROM, incoming.getReferenceFlowFrom(),
             incoming.getFlowFrom(),
             incoming.getReferenceFlowFromSystemID());
        existing.setReferenceFlowFrom(from);
        if (null != from) {
            existing.setFlowFrom(from.getUsername());
            existing.setReferenceFlowFromSystemID(from.getSystemId());
        } else {
            existing.setFlowFrom(incoming.getFlowFrom());
            existing.setReferenceFlowFromSystemID
                (incoming.getReferenceFlowFromSystemID());
        }
        */
        existing.setVersion(getETag());
        return packAsLinks(existing);
    }
    // All DELETE methods

    @Override
    public void deleteDocumentFlowBySystemId(@NotNull final UUID systemId) {
        documentFlowRepository.delete(getDocumentFlowOrThrow(systemId));
    }

    @Override
    public void deleteDocumentFlow(DocumentFlow documentFlow) {
        documentFlowRepository.delete(documentFlow);
    }
    // All template methods

    public DocumentFlowLinks generateDefaultDocumentFlow() {
        DocumentFlow template = new DocumentFlow();
        template.setFlowSentDate(OffsetDateTime.now());
        template.setFlowStatus(new FlowStatus(
                TEMPLATE_DOCUMENT_FLOW_FLOW_STATUS_CODE));
        template.setVersion(-1L, true);
        validateFlowStatus(template);
        // Propose current user as the sender / creator of the flow record
        User u = userService.userGetByUsername(getUser());
        if (null != u) {
            template.setFlowFrom(u.getUsername());
            template.setReferenceFlowFromSystemID(u.getSystemId());
            template.setReferenceFlowFrom(u);
        } else {
            String info = "Unable to find User object for current user when generating template!";
            logger.warn(info);
        }
        return packAsLinks(template);
    }
    // All helper methods

    /**
     * Internal helper method. Rather than having a find and try catch
     * in multiple methods, we have it here once. Note. If you call
     * this, you will only ever get a valid DocumentFlow back. If
     * there is no valid DocumentFlow, an exception is thrown
     *
     * @param systemId systemId of the DocumentFlow object to retrieve
     * @return the DocumentFlow object
     */
    protected DocumentFlow getDocumentFlowOrThrow(
            @NotNull final UUID systemId) {
        DocumentFlow documentFlow = documentFlowRepository.
                findBySystemId(systemId);
        if (documentFlow == null) {
            String error = INFO_CANNOT_FIND_OBJECT +
                    " DocumentFlow, using systemId " + systemId;
            logger.error(error);
            throw new NoarkEntityNotFoundException(error);
        }
        return documentFlow;
    }

    private DocumentFlowLinks packAsLinks(DocumentFlow documentFlow) {
        DocumentFlowLinks documentFlowLinks =
                new DocumentFlowLinks(documentFlow);
        applyLinksAndHeader(documentFlowLinks, documentFlowLinksBuilder);
        return documentFlowLinks;
    }

    private void validateFlowStatus(DocumentFlow incomingDocumentFlow) {
        // Assume value already set, as the deserializer will enforce it.
        FlowStatus flowStatus =
                (FlowStatus) metadataService.findValidMetadata(
                        incomingDocumentFlow.getFlowStatus());
        incomingDocumentFlow.setFlowStatus(flowStatus);
    }

    private void checkFlowSentDate(DocumentFlow documentFlow) {
        if (null == documentFlow.getFlowSentDate()) {
            documentFlow.setFlowSentDate(OffsetDateTime.now());
        }
    }

    private void createUserReferences(DocumentFlow documentFlow) {
        // Verify during creation: If systemid is set, and point to an
        // existing user, verify the username match the existing user
        // and set the reference to this user.  If systemid is not
        // set, make sure username is set and accept username and
        // systemid verbatim.
        User to = userService.validateUserReference
                (DOCUMENT_FLOW_FLOW_TO, documentFlow.getReferenceFlowTo(),
                        documentFlow.getFlowTo(),
                        documentFlow.getReferenceFlowToSystemID());
        User from = userService.validateUserReference
                (DOCUMENT_FLOW_FLOW_FROM, documentFlow.getReferenceFlowFrom(),
                        documentFlow.getFlowFrom(),
                        documentFlow.getReferenceFlowFromSystemID());
        if (null == from && null == documentFlow.getFlowFrom()) {
            String info = "Missing " + DOCUMENT_FLOW_FLOW_FROM +
                    " username value";
            throw new NikitaMalformedInputDataException(info);
        }
        if (null == to && null == documentFlow.getFlowTo()) {
            String info = "Missing " + DOCUMENT_FLOW_FLOW_TO +
                    " username value";
            throw new NikitaMalformedInputDataException(info);
        }
        if (null != to && to.equals(from)) {
            String info = DOCUMENT_FLOW_FLOW_FROM + " and " +
                    DOCUMENT_FLOW_FLOW_TO + " are the same";
            throw new NikitaMalformedInputDataException(info);
        }
        documentFlow.setReferenceFlowTo(to);
        documentFlow.setReferenceFlowFrom(from);
        if (null != to
                && null == documentFlow.getFlowTo()) {
            documentFlow.setFlowTo(to.getUsername());
        }
        if (null != to
                && null == documentFlow.getReferenceFlowToSystemID()) {
            documentFlow.setReferenceFlowToSystemID(to.getSystemId());
        }
        if (null != from
                && null == documentFlow.getFlowFrom()) {
            documentFlow.setFlowFrom(from.getUsername());
        }
        if (null != from
                && null == documentFlow.getReferenceFlowFromSystemID()) {
            documentFlow.setReferenceFlowFromSystemID(from.getSystemId());
        }
    }
}
