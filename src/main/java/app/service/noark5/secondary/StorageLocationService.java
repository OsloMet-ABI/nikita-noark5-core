package app.service.noark5.secondary;

import app.domain.noark5.File;
import app.domain.noark5.Fonds;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.Series;
import app.domain.noark5.secondary.StorageLocation;
import app.domain.repository.noark5.v5.secondary.IStorageLocationRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.secondary.IStorageLocationService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.secondary.IStorageLocationLinksBuilder;
import app.webapp.payload.links.secondary.StorageLocationLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class StorageLocationService
        extends NoarkService
        implements IStorageLocationService {

    private static final Logger logger =
            LoggerFactory.getLogger(StorageLocationService.class);

    private final IStorageLocationRepository storageLocationRepository;
    private final IStorageLocationLinksBuilder storageLocationLinksBuilder;

    public StorageLocationService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IStorageLocationRepository storageLocationRepository,
            IStorageLocationLinksBuilder storageLocationLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.storageLocationRepository = storageLocationRepository;
        this.storageLocationLinksBuilder = storageLocationLinksBuilder;
    }

    // All CREATE methods

    @Override
    @Transactional
    public StorageLocationLinks createStorageLocationAssociatedWithFile
            (StorageLocation storageLocation, File file) {
        storageLocation.addReferenceFile(file);
        return packAsLinks(storageLocationRepository.save(storageLocation));
    }

    @Override
    @Transactional
    public StorageLocationLinks createStorageLocationAssociatedWithFonds
            (StorageLocation storageLocation, Fonds fonds) {
        storageLocation.addReferenceFonds(fonds);
        return packAsLinks(storageLocationRepository.save(storageLocation));
    }

    @Override
    @Transactional
    public StorageLocationLinks createStorageLocationAssociatedWithSeries
            (StorageLocation storageLocation, Series series) {
        storageLocation.addReferenceSeries(series);
        return packAsLinks(storageLocationRepository.save(storageLocation));
    }

    @Override
    @Transactional
    public StorageLocationLinks createStorageLocationAssociatedWithRecord
            (StorageLocation storageLocation, RecordEntity record) {
        storageLocation.addReferenceRecord(record);
        return packAsLinks(storageLocationRepository.save(storageLocation));
    }

    // All READ methods

    @Override
    public StorageLocationLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getStorageLocationOrThrow(systemId));
    }

    @Override
    public StorageLocationLinks findAll() {
        return (StorageLocationLinks) processODataQueryGet();
    }

    // All UPDATE methods

    @Override
    @Transactional
    public StorageLocationLinks updateStorageLocationBySystemId(
            @NotNull final UUID systemId,
            @NotNull final StorageLocation incomingStorageLocation) {
        StorageLocation existingStorageLocation =
                getStorageLocationOrThrow(systemId);
        existingStorageLocation.setStorageLocation(
                incomingStorageLocation.getStorageLocation());
        existingStorageLocation.setVersion(getETag());
        return packAsLinks(existingStorageLocation);
    }

    // All DELETE methods

    @Override
    @Transactional
    public void deleteStorageLocationBySystemId(@NotNull final UUID systemId) {
        StorageLocation storageLocation = getStorageLocationOrThrow(systemId);
        // Remove any associations between a Record and the given StorageLocation
        for (Fonds fonds : storageLocation.getReferenceFonds()) {
            fonds.removeReferenceStorageLocation(storageLocation);
        }
        // Remove any associations between a Series and the given StorageLocation
        for (Series series : storageLocation.getReferenceSeries()) {
            series.removeReferenceStorageLocation(storageLocation);
        }
        // Remove any associations between a File and the given StorageLocation
        for (File file : storageLocation.getReferenceFile()) {
            file.removeReferenceStorageLocation(storageLocation);
        }
        // Remove any associations between a Record and the given StorageLocation
        for (RecordEntity record : storageLocation.getReferenceRecordEntity()) {
            record.removeReferenceStorageLocation(storageLocation);
        }
        storageLocationRepository.delete(storageLocation);
    }

    public boolean deleteStorageLocationIfNotEmpty(StorageLocation storageLocation) {
        if (storageLocation.getReferenceFonds().size() > 0) {
            return false;
        }
        if (storageLocation.getReferenceSeries().size() > 0) {
            return false;
        }
        if (storageLocation.getReferenceFile().size() > 0) {
            return false;
        }
        if (storageLocation.getReferenceRecordEntity().size() > 0) {
            return false;
        }
        storageLocationRepository.delete(storageLocation);
        return true;
    }

    // All template methods

    public StorageLocationLinks getDefaultStorageLocation(UUID systemId) {
        StorageLocation storageLocation = new StorageLocation();
        storageLocation.setVersion(-1L, true);
        return packAsLinks(storageLocation);
    }

    // All helper methods

    public StorageLocationLinks packAsLinks(
            @NotNull final StorageLocation storageLocation) {
        StorageLocationLinks storageLocationLinks =
                new StorageLocationLinks(storageLocation);
        applyLinksAndHeader(storageLocationLinks,
                storageLocationLinksBuilder);
        return storageLocationLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, you
     * will only ever get a valid StorageLocation back. If there is no valid
     * StorageLocation, an exception is thrown
     *
     * @param storageLocationSystemId systemId of the StorageLocation object to retrieve
     * @return the StorageLocation object
     */
    protected StorageLocation getStorageLocationOrThrow(
            @NotNull final UUID storageLocationSystemId) {
        StorageLocation storageLocation = storageLocationRepository.
                findBySystemId(storageLocationSystemId);
        if (storageLocation == null) {
            String error = INFO_CANNOT_FIND_OBJECT +
                    " StorageLocation, using systemId " + storageLocationSystemId;
            logger.error(error);
            throw new NoarkEntityNotFoundException(error);
        }
        return storageLocation;
    }
}
