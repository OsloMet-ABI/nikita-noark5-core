package app.service.noark5.secondary;

import app.domain.interfaces.entities.secondary.*;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.bsm.BSMBase;
import app.domain.noark5.casehandling.secondary.*;
import app.domain.noark5.md_other.BSMMetadata;
import app.domain.noark5.metadata.CorrespondencePartType;
import app.domain.repository.noark5.v5.secondary.ICorrespondencePartRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.IBSMService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.ICorrespondencePartService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.model.PatchObjects;
import app.webapp.payload.builder.interfaces.secondary.ICorrespondencePartLinksBuilder;
import app.webapp.payload.links.casehandling.CorrespondencePartInternalLinks;
import app.webapp.payload.links.casehandling.CorrespondencePartLinks;
import app.webapp.payload.links.casehandling.CorrespondencePartPersonLinks;
import app.webapp.payload.links.casehandling.CorrespondencePartUnitLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.MetadataConstants.CORRESPONDENCE_PART_CODE_EA;
import static app.utils.constants.N5ResourceMappings.*;

@Service
public class CorrespondencePartService
        extends NoarkService
        implements ICorrespondencePartService {

    private static final Logger logger =
            LoggerFactory.getLogger(CorrespondencePartService.class);

    private final ICorrespondencePartRepository correspondencePartRepository;
    private final ICorrespondencePartLinksBuilder
            correspondencePartLinksBuilder;
    private final IMetadataService metadataService;
    private final IBSMService bsmService;

    public CorrespondencePartService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            ICorrespondencePartRepository correspondencePartRepository,
            ICorrespondencePartLinksBuilder correspondencePartLinksBuilder,
            IMetadataService metadataService, IBSMService bsmService,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.correspondencePartRepository = correspondencePartRepository;
        this.correspondencePartLinksBuilder = correspondencePartLinksBuilder;
        this.metadataService = metadataService;
        this.bsmService = bsmService;
    }

    // All CREATE methods

    @Override
    @Transactional
    public CorrespondencePartPersonLinks
    createNewCorrespondencePartPerson(
            @NotNull final CorrespondencePartPerson correspondencePart,
            @NotNull final RecordEntity record) {

        validateCorrespondencePartType(correspondencePart);
        ContactInformation contactInformation
                = correspondencePart.getContactInformation();
        if (contactInformation != null) {
            contactInformation.setCorrespondencePartPerson(correspondencePart);
        }
        PostalAddress postalAddress = correspondencePart.getPostalAddress();
        if (null != postalAddress) {
            postalAddress.getSimpleAddress().setAddressType(POSTAL_ADDRESS);
            postalAddress.setReferenceCorrespondencePartPerson(
                    correspondencePart);
        }
        ResidingAddress residingAddress =
                correspondencePart.getResidingAddress();
        if (null != residingAddress) {
            residingAddress.getSimpleAddress().setAddressType(RESIDING_ADDRESS);
            residingAddress.setCorrespondencePartPerson(correspondencePart);
        }
        record.addCorrespondencePart(correspondencePart);
        return packAsCorrespondencePartPersonLinks(
                correspondencePartRepository.save(correspondencePart));
    }

    /**
     * The correspondencePart.setPostalAddress(postalAddress); should not be
     * necessary as the deserializer will already have done this. Check this
     * to see if we really need to do this.
     *
     * @param correspondencePart Incoming correspondencePartUni
     * @param record             existing record retrieved from database
     * @return correspondencePart wraped as a correspondencePartUnitLinks
     */
    @Override
    @Transactional
    public CorrespondencePartUnitLinks createNewCorrespondencePartUnit(
            @NotNull final CorrespondencePartUnit correspondencePart,
            @NotNull final RecordEntity record) {
        validateCorrespondencePartType(correspondencePart);
        // Set values for ContactInformation, PostalAddress,
        // BusinessAddress
        PostalAddress postalAddress = correspondencePart.getPostalAddress();
        if (null != postalAddress) {
            postalAddress.getSimpleAddress().setAddressType(POSTAL_ADDRESS);
            correspondencePart.setPostalAddress(postalAddress);
        }
        BusinessAddress businessAddress =
                correspondencePart.getBusinessAddress();
        if (null != businessAddress) {
            businessAddress.getSimpleAddress().setAddressType(BUSINESS_ADDRESS);
            correspondencePart.setBusinessAddress(businessAddress);
        }
        record.addCorrespondencePart(correspondencePart);
        return packAsCorrespondencePartUnitLinks(
                correspondencePartRepository.save(correspondencePart));
    }

    @Override
    @Transactional
    public CorrespondencePartInternalLinks
    createNewCorrespondencePartInternal(
            @NotNull final CorrespondencePartInternal correspondencePart,
            @NotNull final RecordEntity record) {
        validateCorrespondencePartType(correspondencePart);
        record.addCorrespondencePart(correspondencePart);
        return packAsCorrespondencePartInternalLinks(
                correspondencePartRepository.save(correspondencePart));
    }

    // All READ methods

    @Override
    public CorrespondencePart findBySystemId(@NotNull final UUID systemId) {
        return getCorrespondencePartOrThrow(systemId);
    }

    @Override
    public CorrespondencePartInternalLinks
    findCorrespondencePartInternalBySystemId(UUID systemId) {
        return packAsCorrespondencePartInternalLinks((CorrespondencePartInternal)
                getCorrespondencePartOrThrow(systemId));
    }

    @Override
    public CorrespondencePartPersonLinks
    findCorrespondencePartPersonBySystemId(UUID systemId) {
        return packAsCorrespondencePartPersonLinks((CorrespondencePartPerson)
                getCorrespondencePartOrThrow(systemId));
    }

    @Override
    public CorrespondencePartUnitLinks
    findCorrespondencePartUnitBySystemId(UUID systemId) {
        return packAsCorrespondencePartUnitLinks((CorrespondencePartUnit)
                getCorrespondencePartOrThrow(systemId));
    }

    // All UPDATE methods

    /**
     * Update the CorrespondencePartPerson identified by systemId. Retrieve a
     * copy of the CorrespondencePartPerson from the database. If it does not
     * exist throw a not found exception. If it exists, call sub methods that
     * copy the values. Save the updated object back to the database.
     *
     * @param systemId                   systemId of the
     *                                   CorrespondencePartPerson to update
     * @param incomingCorrespondencePart incoming CorrespondencePartPerson with
     *                                   values to copy from
     * @return the updated CorrespondencePartPerson
     */
    @Override
    @Transactional
    public CorrespondencePartPersonLinks
    updateCorrespondencePartPerson(
            @NotNull final UUID systemId, @NotNull final CorrespondencePartPerson incomingCorrespondencePart) {
        CorrespondencePartPerson existingCorrespondencePart =
                (CorrespondencePartPerson)
                        getCorrespondencePartOrThrow(systemId);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingCorrespondencePart, incomingCorrespondencePart);
        updateCorrespondencePartType(incomingCorrespondencePart,
                existingCorrespondencePart);
        // Copy all the values you are allowed to copy ....
        // First the values
        existingCorrespondencePart.setdNumber(
                incomingCorrespondencePart.getdNumber());
        existingCorrespondencePart.setName(
                incomingCorrespondencePart.getName());
        existingCorrespondencePart.setSocialSecurityNumber(
                incomingCorrespondencePart.getSocialSecurityNumber());

        // Then secondary objects
        updateCorrespondencePartContactInformationCreateIfNull(
                existingCorrespondencePart, incomingCorrespondencePart);
        // Residing address
        updateCorrespondencePartResidingAddressCreateIfNull
                (existingCorrespondencePart, incomingCorrespondencePart);
        // Postal address
        updateCorrespondencePartPostalAddressCreateIfNull(
                existingCorrespondencePart, incomingCorrespondencePart);

        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingCorrespondencePart.setVersion(getETag());
        publishChangeLogEvents(changeLogs);
        return packAsCorrespondencePartPersonLinks(
                existingCorrespondencePart);
    }

    private void updateCorrespondencePartType(
            @NotNull final CorrespondencePart incomingCorrespondencePart,
            @NotNull final CorrespondencePart existingCorrespondencePart) {
        // Only copy if changed, in case it has an historical value
        if (existingCorrespondencePart.getCorrespondencePartType()
                != incomingCorrespondencePart.getCorrespondencePartType()) {
            validateCorrespondencePartType(incomingCorrespondencePart);
            existingCorrespondencePart.setCorrespondencePartType
                    (incomingCorrespondencePart.getCorrespondencePartType());
        }
    }

    @Override
    @Transactional
    public CorrespondencePartInternalLinks updateCorrespondencePartInternal(
            @NotNull final UUID systemId, @NotNull final CorrespondencePartInternal incomingCorrespondencePart) {
        CorrespondencePartInternal existingCorrespondencePart =
                (CorrespondencePartInternal)
                        getCorrespondencePartOrThrow(systemId);
        // Copy all the values you are allowed to copy ....

        updateCorrespondencePartType(incomingCorrespondencePart,
                existingCorrespondencePart);

        existingCorrespondencePart.setAdministrativeUnit(
                incomingCorrespondencePart.getAdministrativeUnit());
        existingCorrespondencePart.setCaseHandler(
                incomingCorrespondencePart.getCaseHandler());
        //existingCorrespondencePart.setReferenceAdministrativeUnit
        //      (incomingCorrespondencePart.getReferenceAdministrativeUnit());
//        existingCorrespondencePart.setReferenceUser(incomingCorrespondencePart
//                .getReferenceUser());
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingCorrespondencePart.setVersion(getETag());
        return packAsCorrespondencePartInternalLinks(
                existingCorrespondencePart);
    }

    @Override
    @Transactional
    public CorrespondencePartUnitLinks updateCorrespondencePartUnit(
            @NotNull final UUID systemId,
            @NotNull final CorrespondencePartUnit incomingCorrespondencePart) {
        CorrespondencePartUnit existingCorrespondencePart =
                (CorrespondencePartUnit) getCorrespondencePartOrThrow(systemId);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingCorrespondencePart, incomingCorrespondencePart);
        updateCorrespondencePartType(incomingCorrespondencePart,
                existingCorrespondencePart);
        // Copy all the values you are allowed to copy ....
        // First the values
        existingCorrespondencePart.setName(
                incomingCorrespondencePart.getName());
        existingCorrespondencePart.setUnitIdentifier(
                incomingCorrespondencePart.getUnitIdentifier());
        existingCorrespondencePart.setContactPerson(
                incomingCorrespondencePart.getContactPerson());

        // Then secondary objects
        // Contact information
        updateCorrespondencePartContactInformationCreateIfNull(
                existingCorrespondencePart, incomingCorrespondencePart);
        // Business address
        updateCorrespondencePartUnitBusinessAddressCreateIfNull(
                existingCorrespondencePart, incomingCorrespondencePart);
        // Postal address
        updateCorrespondencePartPostalAddressCreateIfNull(
                existingCorrespondencePart, incomingCorrespondencePart);
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingCorrespondencePart.setVersion(getETag());
        publishChangeLogEvents(changeLogs);
        return packAsCorrespondencePartUnitLinks(existingCorrespondencePart);
    }


    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingCorrespondencePartUnit the existing CorrespondencePartUnit from the database
     * @param newCorrespondencePartUnit      the incoming CorrespondencePartUnit
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(CorrespondencePartUnit existingCorrespondencePartUnit,
                                                                        CorrespondencePartUnit newCorrespondencePartUnit) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // korespondansepart	M400	korrespondansepartNavn	Ved endring
        if (null != existingCorrespondencePartUnit.getName() &&
                !existingCorrespondencePartUnit.getName().equals(newCorrespondencePartUnit.getName())) {
            changeLogs.add(createChangeLogObject(CORRESPONDENCE_PART_NAME, existingCorrespondencePartUnit.getName(),
                    newCorrespondencePartUnit.getName(), existingCorrespondencePartUnit));
        }
        return changeLogs;
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingCorrespondencePartPerson the existing CorrespondencePartPerson from the database
     * @param newCorrespondencePartPerson      the incoming CorrespondencePartPerson
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(CorrespondencePartPerson existingCorrespondencePartPerson,
                                                                        CorrespondencePartPerson newCorrespondencePartPerson) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // korespondansepart	M400	korrespondansepartNavn	Ved endring
        if (null != existingCorrespondencePartPerson.getName() &&
                !existingCorrespondencePartPerson.getName().equals(newCorrespondencePartPerson.getName())) {
            changeLogs.add(createChangeLogObject(CORRESPONDENCE_PART_NAME, existingCorrespondencePartPerson.getName(),
                    newCorrespondencePartPerson.getName(), existingCorrespondencePartPerson));
        }
        return changeLogs;
    }

    @Override
    @Transactional
    public CorrespondencePartLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final PatchObjects patchObjects) {
        return packAsLinks((CorrespondencePart)
                handlePatch(systemId, patchObjects));
    }

    // All template methods

    private void createTemplateCorrespondencePartType(
            @NotNull final CorrespondencePart correspondencePart) {
        CorrespondencePartType correspondencePartType = (CorrespondencePartType)
                metadataService.findValidMetadataByEntityTypeOrThrow
                        (CORRESPONDENCE_PART_TYPE, CORRESPONDENCE_PART_CODE_EA, null);
        correspondencePart.setCorrespondencePartType(correspondencePartType);

    }

    // All UPDATE methods

    /**
     * Delete a CorrespondencePartUnit identified by the given systemId
     * <p>
     * Note. This assumes all children have also been deleted.
     *
     * @param systemId The systemId of the CorrespondencePart object you wish
     *                 to delete
     */
    @Override
    @Transactional
    public void deleteCorrespondencePartUnit(@NotNull final UUID systemId) {
        CorrespondencePartUnit correspondencePartUnit = (CorrespondencePartUnit)
                getCorrespondencePartOrThrow(systemId);
        RecordEntity record = correspondencePartUnit.getReferenceRecordEntity();
        record.removeCorrespondencePart(correspondencePartUnit);
        for (BSMBase bsmBase : correspondencePartUnit.getReferenceBSMBase()) {
            bsmBase.setReferenceCorrespondencePart(null);
        }
        correspondencePartRepository.delete(correspondencePartUnit);
    }

    /**
     * Delete a CorrespondencePartPerson identified by the given systemId
     * <p>
     * Note. This assumes all children have also been deleted.
     *
     * @param systemId The systemId of the CorrespondencePart object you wish
     *                 to delete
     */
    @Override
    @Transactional
    public void deleteCorrespondencePartPerson(@NotNull final UUID systemId) {
        CorrespondencePartPerson correspondencePartPerson = (CorrespondencePartPerson)
                getCorrespondencePartOrThrow(systemId);
        RecordEntity record = correspondencePartPerson.getReferenceRecordEntity();
        record.removeCorrespondencePart(correspondencePartPerson);
        for (BSMBase bsmBase : correspondencePartPerson.getReferenceBSMBase()) {
            bsmBase.setReferenceCorrespondencePart(null);
        }
        correspondencePartRepository.delete(correspondencePartPerson);
        handleDeletionForEventLog(correspondencePartPerson);
    }

    /**
     * Delete a CorrespondencePartInternal identified by the given systemId
     * <p>
     * Note. This assumes all children have also been deleted.
     *
     * @param systemId The systemId of the CorrespondencePart object you wish
     *                 to delete
     */
    @Override
    @Transactional
    public void deleteCorrespondencePartInternal(@NotNull final UUID systemId) {
        correspondencePartRepository.delete(
                getCorrespondencePartOrThrow(systemId));
    }

    /**
     * Delete all objects belonging to the organisation identified by organisation
     */
    @Override
    @Transactional
    public void deleteAllByOrganisation() {
        correspondencePartRepository.deleteByOrganisation(getOrganisation());
    }

    // Internal helper methods

    /**
     * Update BusinessAddress if it exists. If none exists, create a
     * new BusinessAddress and set the values.
     *
     * @param existingCorrespondencePart The existing CorrespondencePart
     * @param incomingCorrespondencePart The incoming CorrespondencePart
     */
    private void updateCorrespondencePartUnitBusinessAddressCreateIfNull(
            @NotNull final IBusinessAddress existingCorrespondencePart,
            @NotNull final IBusinessAddress incomingCorrespondencePart) {

        if (existingCorrespondencePart.getBusinessAddress() != null &&
                incomingCorrespondencePart.getBusinessAddress() != null) {
            updateAddress(existingCorrespondencePart.
                            getBusinessAddress().getSimpleAddress(),
                    incomingCorrespondencePart.
                            getBusinessAddress().getSimpleAddress());
        }
        // Create a new BusinessAddress object based on the incoming one
        else if (incomingCorrespondencePart.getBusinessAddress() != null) {
            BusinessAddress businessAddress = new BusinessAddress();
            businessAddress.setSimpleAddress(new SimpleAddress());
            updateAddress(businessAddress.getSimpleAddress(),
                    incomingCorrespondencePart.getBusinessAddress()
                            .getSimpleAddress());
            existingCorrespondencePart.setBusinessAddress(businessAddress);
        }
        // Make sure the addressType field is set
        if (existingCorrespondencePart.getBusinessAddress() != null &&
                existingCorrespondencePart.getBusinessAddress().
                        getSimpleAddress() != null) {
            existingCorrespondencePart.getBusinessAddress().getSimpleAddress().
                    setAddressType(BUSINESS_ADDRESS);
        }
    }

    /**
     * Update ResidingAddress if it exists. If none exists, create a
     * new ResidingAddress and set the values.
     *
     * @param existingCorrespondencePart The existing CorrespondencePart
     * @param incomingCorrespondencePart The incoming CorrespondencePart
     */
    private void updateCorrespondencePartResidingAddressCreateIfNull(
            @NotNull final IResidingAddress existingCorrespondencePart,
            @NotNull final IResidingAddress incomingCorrespondencePart) {

        if (existingCorrespondencePart.getResidingAddress() != null &&
                incomingCorrespondencePart.getResidingAddress() != null) {
            updateAddress(existingCorrespondencePart.
                            getResidingAddress().getSimpleAddress(),
                    incomingCorrespondencePart.
                            getResidingAddress().getSimpleAddress());
        }
        // Create a new ResidingAddress object based on the incoming one
        else if (incomingCorrespondencePart.getResidingAddress() != null) {
            ResidingAddress residingAddress = new ResidingAddress();
            residingAddress.setSimpleAddress(new SimpleAddress());
            updateAddress(residingAddress.getSimpleAddress(),
                    incomingCorrespondencePart.getResidingAddress()
                            .getSimpleAddress());
            existingCorrespondencePart.setResidingAddress(residingAddress);
        }
        if (existingCorrespondencePart.getResidingAddress() != null &&
                existingCorrespondencePart.getResidingAddress().
                        getSimpleAddress() != null) {
            existingCorrespondencePart.getResidingAddress().getSimpleAddress().
                    setAddressType(RESIDING_ADDRESS);
        }
    }

    /**
     * Update PostalAddress if it exists. If none exists, create a
     * new PostalAddress and set the values.
     *
     * @param existingPostalAddress The existing
     *                              CorrespondencePart
     * @param incomingPostalAddress The incoming CorrespondencePart
     */
    private void updateCorrespondencePartPostalAddressCreateIfNull(
            @NotNull final IPostalAddress existingPostalAddress,
            @NotNull final IPostalAddress incomingPostalAddress) {

        if (existingPostalAddress.getPostalAddress() != null
                && incomingPostalAddress.getPostalAddress() != null) {
            updateAddress(existingPostalAddress.
                            getPostalAddress().getSimpleAddress(),
                    incomingPostalAddress.
                            getPostalAddress().getSimpleAddress());
        }
        // Create a new PostalAddress object based on the incoming one
        else if (incomingPostalAddress.getPostalAddress() != null) {
            PostalAddress postalAddress = new PostalAddress();
            postalAddress.setSimpleAddress(new SimpleAddress());
            updateAddress(postalAddress.getSimpleAddress(),
                    incomingPostalAddress.getPostalAddress().
                            getSimpleAddress());
            existingPostalAddress.setPostalAddress(postalAddress);
        }
        // Make sure the addressType field is set
        if (existingPostalAddress.getPostalAddress() != null &&
                existingPostalAddress.getPostalAddress().
                        getSimpleAddress() != null) {
            existingPostalAddress.getPostalAddress().getSimpleAddress().
                    setAddressType(POSTAL_ADDRESS);
        }
    }

    /**
     * Update ContactInformation if it exists. If none exists, create a
     * new ContactInformation and set the values.
     *
     * @param existingCorrespondencePart The existing CorrespondencePart
     * @param incomingCorrespondencePart The incoming CorrespondencePart
     */
    private void updateCorrespondencePartContactInformationCreateIfNull(
            @NotNull final IContactInformation existingCorrespondencePart,
            @NotNull final IContactInformation incomingCorrespondencePart) {

        if (existingCorrespondencePart.getContactInformation() != null &&
                incomingCorrespondencePart.getContactInformation() != null) {
            updateContactInformation(existingCorrespondencePart.
                            getContactInformation(),
                    incomingCorrespondencePart.
                            getContactInformation());
        }
        // Create a new ContactInformation object based on the incoming one
        else if (incomingCorrespondencePart.getContactInformation() != null) {
            existingCorrespondencePart.setContactInformation(
                    updateContactInformation(new ContactInformation(),
                            incomingCorrespondencePart.getContactInformation()));
        }
    }

    /**
     * Generate a Default CorrespondencePartUnit object that can be
     * associated with the identified Record.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param recordSystemId The systemId of the record object
     *                       you wish to create a templated object for
     * @return the CorrespondencePartUnit object wrapped as a
     * CorrespondencePartUnitLinks object
     */
    @Override
    public CorrespondencePartUnitLinks generateDefaultCorrespondencePartUnit(
            @NotNull final UUID recordSystemId) {
        CorrespondencePartUnit suggestedCorrespondencePart =
                new CorrespondencePartUnit();
        suggestedCorrespondencePart.setVersion(-1L, true);
        createTemplateCorrespondencePartType(suggestedCorrespondencePart);
        return packAsCorrespondencePartUnitLinks(suggestedCorrespondencePart);
    }

    /**
     * Generate a Default CorrespondencePartPerson object that can be
     * associated with the identified Record.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param recordSystemId The systemId of the record object
     *                       you wish to create a templated object for
     * @return the CorrespondencePartPerson object wrapped as a
     * CorrespondencePartPersonLinks object
     */
    @Override
    public CorrespondencePartPersonLinks
    generateDefaultCorrespondencePartPerson(
            @NotNull final UUID recordSystemId) {
        CorrespondencePartPerson suggestedCorrespondencePart =
                new CorrespondencePartPerson();
        suggestedCorrespondencePart.setVersion(-1L, true);
        createTemplateCorrespondencePartType(suggestedCorrespondencePart);
        return packAsCorrespondencePartPersonLinks(suggestedCorrespondencePart);
    }

    /**
     * Generate a Default CorrespondencePartInternal object that can be
     * associated with the identified Record.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param recordSystemId The systemId of the record object
     *                       you wish to create a templated object for
     * @return the CorrespondencePartInternal object wrapped as a
     * CorrespondencePartInternalLinks object
     */
    @Override
    public CorrespondencePartInternalLinks
    generateDefaultCorrespondencePartInternal(
            @NotNull final UUID recordSystemId) {
        CorrespondencePartInternal suggestedCorrespondencePart =
                new CorrespondencePartInternal();

        createTemplateCorrespondencePartType(suggestedCorrespondencePart);
        suggestedCorrespondencePart.setVersion(-1L, true);
        return packAsCorrespondencePartInternalLinks(
                suggestedCorrespondencePart);
    }

    /**
     * Copy the values you are allowed to copy from the incoming
     * contactInformation object to the existing contactInformation object
     * retrieved from the database.
     *
     * @param existingContactInformation An existing contactInformation object
     *                                   retrieved from the database
     * @param incomingContactInformation Incoming contactInformation object
     * @return The existing ContactInformation object updated with values
     */
    private ContactInformation updateContactInformation(
            @NotNull final IContactInformationEntity existingContactInformation,
            @NotNull final IContactInformationEntity incomingContactInformation) {

        existingContactInformation.setEmailAddress(
                incomingContactInformation.getEmailAddress());
        existingContactInformation.setMobileTelephoneNumber(
                incomingContactInformation.getMobileTelephoneNumber());
        existingContactInformation.setTelephoneNumber(
                incomingContactInformation.getTelephoneNumber());
        return (ContactInformation) existingContactInformation;
    }

    /**
     * Copy the values you are allowed to copy from the incoming address object
     * to the existing address object retrieved from the database.
     *
     * @param existingAddress An existing address object retrieved from the
     *                        database
     * @param incomingAddress Incoming address object
     */
    private void updateAddress(@NotNull final SimpleAddress existingAddress,
                               @NotNull final SimpleAddress incomingAddress) {

        existingAddress.setAddressType(incomingAddress.getAddressType());
        existingAddress.setAddressLine1(incomingAddress.getAddressLine1());
        existingAddress.setAddressLine2(incomingAddress.getAddressLine2());
        existingAddress.setAddressLine3(incomingAddress.getAddressLine3());
        existingAddress.setPostalNumber(incomingAddress.getPostalNumber());
        existingAddress.setPostalTown(incomingAddress.getPostalTown());
        existingAddress.setCountryCode(incomingAddress.getCountryCode());
    }

    private void validateCorrespondencePartType(
            @NotNull final CorrespondencePart correspondencePart) {
        // Assume value already set, as the deserializer will enforce it.
        CorrespondencePartType correspondencePartType =
                (CorrespondencePartType) metadataService.findValidMetadata(
                        correspondencePart.getCorrespondencePartType());
        correspondencePart.setCorrespondencePartType
                (correspondencePartType);
    }

    @Override
    protected Optional<BSMMetadata> findBSMByName(@NotNull final String name) {
        return bsmService.findBSMByName(name);
    }

    @Override
    @Transactional
    public Object associateBSM(@NotNull final UUID systemId,
                               @NotNull final List<BSMBase> bsm) {
        CorrespondencePart correspondencePart =
                getCorrespondencePartOrThrow(systemId);
        correspondencePart.addReferenceBSMBase(bsm);
        return correspondencePart;
    }

    public CorrespondencePartLinks packAsLinks(
            @NotNull final CorrespondencePart correspondencePart) {
        CorrespondencePartLinks correspondencePartLinks =
                new CorrespondencePartLinks(correspondencePart);
        applyLinksAndHeader(correspondencePartLinks,
                correspondencePartLinksBuilder);
        return correspondencePartLinks;
    }

    public CorrespondencePartPersonLinks
    packAsCorrespondencePartPersonLinks(
            @NotNull final CorrespondencePartPerson correspondencePartPerson) {
        CorrespondencePartPersonLinks correspondencePartPersonLinks =
                new CorrespondencePartPersonLinks(correspondencePartPerson);
        applyLinksAndHeader(correspondencePartPersonLinks,
                correspondencePartLinksBuilder);
        return correspondencePartPersonLinks;
    }

    public CorrespondencePartInternalLinks
    packAsCorrespondencePartInternalLinks(
            @NotNull final CorrespondencePartInternal correspondencePartInternal) {
        CorrespondencePartInternalLinks correspondencePartInternalLinks =
                new CorrespondencePartInternalLinks(correspondencePartInternal);
        applyLinksAndHeader(correspondencePartInternalLinks,
                correspondencePartLinksBuilder);
        return correspondencePartInternalLinks;
    }

    public CorrespondencePartUnitLinks
    packAsCorrespondencePartUnitLinks(
            @NotNull final CorrespondencePartUnit correspondencePartUnit) {
        CorrespondencePartUnitLinks correspondencePartUnitLinks =
                new CorrespondencePartUnitLinks(correspondencePartUnit);
        applyLinksAndHeader(correspondencePartUnitLinks,
                correspondencePartLinksBuilder);
        return correspondencePartUnitLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid CorrespondencePart back. If there
     * is no valid CorrespondencePart, an exception is thrown
     *
     * @param systemId systemId of correspondencePart to
     *                 retrieve
     * @return the retrieved CorrespondencePart
     */
    private CorrespondencePart getCorrespondencePartOrThrow(
            @NotNull final UUID systemId) {
        CorrespondencePart correspondencePart =
                correspondencePartRepository
                        .findBySystemId(systemId);
        if (correspondencePart == null) {
            String info = INFO_CANNOT_FIND_OBJECT + " CorrespondencePart, " +
                    "using systemId " + systemId;
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        }
        return correspondencePart;
    }
}
