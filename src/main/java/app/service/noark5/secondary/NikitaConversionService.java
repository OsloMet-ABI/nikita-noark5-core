package app.service.noark5.secondary;

import app.domain.noark5.DocumentObject;
import app.domain.noark5.secondary.Conversion;
import app.domain.repository.noark5.v5.secondary.IConversionRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.secondary.IConversionService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.secondary.IConversionLinksBuilder;
import app.webapp.payload.links.secondary.ConversionLinks;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

/**
 * Note, ConversionService is already an object in Spring so I need to name
 * this class differently to avoid a name conflict and subsequent exceptions.
 */
@Service
public class NikitaConversionService
        extends NoarkService
        implements IConversionService {

    private final IConversionRepository conversionRepository;
    private final IConversionLinksBuilder conversionLinksBuilder;

    // All CREATE methods

    public NikitaConversionService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IConversionRepository conversionRepository,
            IConversionLinksBuilder conversionLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.conversionRepository = conversionRepository;
        this.conversionLinksBuilder = conversionLinksBuilder;
    }

    // All READ methods

    @Override
    public ConversionLinks save(Conversion conversion) {
        return packAsLinks(conversionRepository.save(conversion));
    }

    // All DELETE methods

    @Override
    public ConversionLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getConversionOrThrow(systemId));
    }

    // All template methods

    @Override
    public Conversion findConversionBySystemId(@NotNull final UUID systemId) {
        return getConversionOrThrow(systemId);
    }

    // All helper methods

    @Override
    @Transactional
    public void deleteConversion(@NotNull final UUID systemId) {
        conversionRepository.delete(getConversionOrThrow(systemId));
    }

    public ConversionLinks generateDefaultConversion(
            @NotNull final UUID systemId,
            @NotNull final DocumentObject documentObject) {
        Conversion defaultConversion = new Conversion();
        /* Propose conversion done now by logged in user */
        defaultConversion.setConvertedDate(OffsetDateTime.now());
        defaultConversion.setConvertedBy(getUser());
        defaultConversion.setConverteLinksFormat(documentObject.getFormat());
        defaultConversion.setVersion(-1L, true);
        return packAsLinks(defaultConversion);
    }

    public ConversionLinks packAsLinks(@NotNull final Conversion conversion) {
        ConversionLinks conversionLinks = new ConversionLinks(conversion);
        applyLinksAndHeader(conversionLinks, conversionLinksBuilder);
        return conversionLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, you
     * will only ever get a valid Conversion back. If there is no valid
     * Conversion, an exception is thrown
     *
     * @param systemId systemId of the Conversion object to retrieve
     * @return the Conversion object
     */
    protected Conversion getConversionOrThrow(@NotNull final UUID systemId) {
        Conversion conversion = conversionRepository.findBySystemId(systemId);
        if (conversion == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " Conversion, using systemId " + systemId;
            throw new NoarkEntityNotFoundException(info);
        }
        return conversion;
    }
}
