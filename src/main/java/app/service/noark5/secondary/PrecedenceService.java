package app.service.noark5.secondary;

import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.admin.User;
import app.domain.noark5.casehandling.CaseFile;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.metadata.PrecedenceStatus;
import app.domain.noark5.secondary.Precedence;
import app.domain.repository.noark5.v5.secondary.IPrecedenceRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.IUserService;
import app.service.application.IPatchService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.IPrecedenceService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.secondary.IPrecedenceLinksBuilder;
import app.webapp.payload.links.secondary.PrecedenceLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.N5ResourceMappings.PRECEDENCE_APPROVED_BY;
import static app.utils.constants.N5ResourceMappings.TITLE;

@Service
public class PrecedenceService
        extends NoarkService
        implements IPrecedenceService {

    private static final Logger logger =
            LoggerFactory.getLogger(PrecedenceService.class);

    private final IMetadataService metadataService;
    private final IPrecedenceRepository precedenceRepository;
    private final IPrecedenceLinksBuilder precedenceLinksBuilder;
    private final IUserService userService;

    public PrecedenceService
            (EntityManager entityManager,
             ApplicationEventPublisher applicationEventPublisher,
             IPatchService patchService,
             IMetadataService metadataService,
             IPrecedenceRepository precedenceRepository,
             IPrecedenceLinksBuilder precedenceLinksBuilder,
             IUserService userService,
             ILocalUserDetails userDetails,
             IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.precedenceRepository = precedenceRepository;
        this.precedenceLinksBuilder = precedenceLinksBuilder;
        this.userService = userService;
        this.metadataService = metadataService;
    }

    // All CREATE methods

    @Override
    @Transactional
    public PrecedenceLinks createNewPrecedence(Precedence entity) {
        validatePrecedenceStatus(entity);
        User approvedBy = userService.validateUserReference
                (PRECEDENCE_APPROVED_BY, entity.getReferencePrecedenceApprovedBy(),
                        entity.getPrecedenceApprovedBy(),
                        entity.getReferencePrecedenceApprovedBySystemID());

        if (null != approvedBy) {
            entity.setPrecedenceApprovedBy(approvedBy.getUsername());
            entity.setReferencePrecedenceApprovedBySystemID(approvedBy.getSystemId());
            entity.setReferencePrecedenceApprovedBy(approvedBy);
        } // otherwise, accept value

        /* TODO check finalisedBy when ReferencePrecedenceFinalisedBy
        // is available.
        User finalisedBy = userService.validateUserReference
            (FINALISED_BY, entity.getReferencePrecedenceFinalisedBy(),
             entity.getPrecedenceFinalisedBy(),
             entity.getReferencePrecedenceFinalisedBySystemID());
        */
        return packAsLinks(precedenceRepository.save(entity));
    }

    // All READ methods

    @Override
    public PrecedenceLinks findAll() {
        return (PrecedenceLinks) processODataQueryGet();
    }

    @Override
    public PrecedenceLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getPrecedenceOrThrow(systemId));
    }

    // All UPDATE methods

    @Override
    @Transactional
    public PrecedenceLinks updatePrecedenceBySystemId
            (@NotNull final UUID systemId, Precedence incoming) {
        Precedence existing = getPrecedenceOrThrow(systemId);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existing, incoming);
        validatePrecedenceStatus(incoming);
        User approvedBy = userService.validateUserReference
                (PRECEDENCE_APPROVED_BY, incoming.getReferencePrecedenceApprovedBy(),
                        incoming.getPrecedenceApprovedBy(),
                        incoming.getReferencePrecedenceApprovedBySystemID());
        /* TODO use when getReferencePrecedenceFinalisedBy and friends
         * is available

        User finalisedBy = userService.validateUserReference
            (FINALISED_BY, incoming.getReferencePrecedenceFinalisedBy(),
             incoming.getPrecedenceFinalisedBy(),
             incoming.getReferencePrecedenceFinalisedBySystemID());
        */
        // Copy all the values you are allowed to copy ....
        existing.setTitle(incoming.getTitle());
        existing.setDescription(incoming.getDescription());
        existing.setPrecedenceDate(incoming.getPrecedenceDate());
        existing.setPrecedenceApprovedDate(incoming.getPrecedenceApprovedDate());
        if (null != approvedBy) {
            existing.setPrecedenceApprovedBy(approvedBy.getUsername());
            existing.setReferencePrecedenceApprovedBySystemID(approvedBy.getSystemId());
            existing.setReferencePrecedenceApprovedBy(approvedBy);
        } else {
            existing.setPrecedenceApprovedBy
                    (incoming.getPrecedenceApprovedBy());
            existing.setReferencePrecedenceApprovedBySystemID
                    (incoming.getReferencePrecedenceApprovedBySystemID());
            existing.setReferencePrecedenceApprovedBy(null);
        }
        existing.setPrecedenceAuthority(incoming.getPrecedenceAuthority());
        existing.setSourceOfLaw(incoming.getSourceOfLaw());
        existing.setPrecedenceStatus(incoming.getPrecedenceStatus());

        existing.setFinalisedDate(incoming.getFinalisedDate());
        existing.setFinalisedBy(incoming.getFinalisedBy());
        // TODO fix user reference lookup when NoarkGeneralEntity have
        // ReferenceFinalisedBy and friends.
        //existing.setReferenceFinalisedBy(incoming.getReferenceFinalisedBy());
        existing.setVersion(getETag());
        publishChangeLogEvents(changeLogs);
        return packAsLinks(existing);
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingPrecedence the existing Precedence from the database
     * @param newPrecedence      the incoming Precedence
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(Precedence existingPrecedence,
                                                                        Precedence newPrecedence) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // presedens	M020	tittel	Ved endring
        if (!existingPrecedence.getTitle().equals(newPrecedence.getTitle())) {
            changeLogs.add(createChangeLogObject(TITLE, existingPrecedence.getTitle(),
                    newPrecedence.getTitle(), existingPrecedence));
        }
        // TODO: mappe	M208	referanseArkivdel	Ved endring
        return changeLogs;
    }

// All DELETE methods

    /**
     * Delete the Precedence object identified by systemId.
     * Before deleting, disassociate the foreign keys between Precedence and
     * any related objects.
     *
     * @param systemId systemId of the Precedence object to delete
     */
    @Override
    @Transactional
    public void deletePrecedenceBySystemId(@NotNull final UUID systemId) {
        Precedence precedence = getPrecedenceOrThrow(systemId);
        // Remove any associations between a CaseFile and the given Precedence
        for (CaseFile caseFile : precedence.getReferenceCaseFile()) {
            caseFile.removePrecedence(precedence);
        }
        // Remove any associations between a RegistryEntry and the given
        // Precedence
        for (RegistryEntry registryEntry :
                precedence.getReferenceRegistryEntry()) {
            registryEntry.removePrecedence(precedence);
        }
        precedenceRepository.delete(precedence);
    }

    @Override
    @Transactional
    public boolean deletePrecedenceIfNotEmpty(Precedence precedence) {
        if (!precedence.getReferenceRegistryEntry().isEmpty()) {
            return false;
        }
        if (!precedence.getReferenceCaseFile().isEmpty()) {
            return false;
        }
        precedenceRepository.delete(precedence);
        return true;
    }

    // All template methods

    public PrecedenceLinks generateDefaultPrecedence() {
        Precedence precedence = new Precedence();
        precedence.setPrecedenceDate(OffsetDateTime.now());
        precedence.setVersion(-1L, true);
        return packAsLinks(precedence);
    }

    // All helper methods

    public PrecedenceLinks packAsLinks(
            @NotNull final Precedence precedence) {
        PrecedenceLinks precedenceLinks = new PrecedenceLinks(precedence);
        applyLinksAndHeader(precedenceLinks, precedenceLinksBuilder);
        return precedenceLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, you
     * will only ever get a valid Precedence back. If there is no valid
     * Precedence, an exception is thrown
     *
     * @param systemId systemId of the Precedence object to retrieve
     * @return the Precedence object
     */
    protected Precedence getPrecedenceOrThrow(
            @NotNull final UUID systemId) {
        Precedence precedence = precedenceRepository.
                findBySystemId(systemId);
        if (precedence == null) {
            String error = INFO_CANNOT_FIND_OBJECT +
                    " Precedence, using systemId " + systemId;
            logger.error(error);
            throw new NoarkEntityNotFoundException(error);
        }
        return precedence;
    }

    private void validatePrecedenceStatus(Precedence incomingPrecedence) {
        if (null != incomingPrecedence.getPrecedenceStatus()) {
            PrecedenceStatus PrecedenceStatus =
                    (PrecedenceStatus) metadataService
                            .findValidMetadata(incomingPrecedence
                                    .getPrecedenceStatus());
            incomingPrecedence.setPrecedenceStatus(PrecedenceStatus);
        }
    }
}
