package app.service.noark5.secondary;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.secondary.Author;
import app.domain.repository.noark5.v5.secondary.IAuthorRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.secondary.IAuthorService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.secondary.IAuthorLinksBuilder;
import app.webapp.payload.links.secondary.AuthorLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class AuthorService
        extends NoarkService
        implements IAuthorService {

    private final IAuthorRepository authorRepository;
    private final IAuthorLinksBuilder authorLinksBuilder;

    public AuthorService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IAuthorRepository authorRepository,
            IAuthorLinksBuilder authorLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.authorRepository = authorRepository;
        this.authorLinksBuilder = authorLinksBuilder;
    }

    // All CREATE methods

    @Override
    @Transactional
    public AuthorLinks associateAuthorWithDocumentDescription(
            @NotNull final Author author,
            @NotNull final DocumentDescription documentDescription) {
        author.setReferenceDocumentDescription(documentDescription);
        return packAsLinks(authorRepository.save(author));
    }

    @Override
    @Transactional
    public AuthorLinks associateAuthorWithRecord(
            @NotNull final Author author,
            @NotNull final RecordEntity record) {
        author.setReferenceRecord(record);
        return packAsLinks(authorRepository.save(author));
    }

    // All READ methods

    @Override
    public AuthorLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getAuthorOrThrow(systemId));
    }

    // All UPDATE methods

    @Override
    @Transactional
    public AuthorLinks updateAuthorBySystemId(
            @NotNull final UUID systemId,
            @NotNull final Author incomingAuthor) {
        Author existingAuthor = getAuthorOrThrow(systemId);
        existingAuthor.setAuthor(incomingAuthor.getAuthor());
        existingAuthor.setVersion(getETag());
        return packAsLinks(existingAuthor);
    }

    // All DELETE methods

    @Override
    @Transactional
    public void deleteAuthorBySystemId(@NotNull final UUID systemId) {
        deleteEntity(getAuthorOrThrow(systemId));
    }

    // All template methods

    public AuthorLinks generateDefaultAuthor(@NotNull final UUID systemId) {
        Author author = new Author();
        author.setVersion(-1L, true);
        return packAsLinks(author);
    }

    // All helper methods

    public AuthorLinks packAsLinks(@NotNull final Author author) {
        AuthorLinks authorLinks = new AuthorLinks(author);
        applyLinksAndHeader(authorLinks, authorLinksBuilder);
        return authorLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, you
     * will only ever get a valid Author back. If there is no valid
     * Author, an exception is thrown
     *
     * @param systemId systemId of the Author object to retrieve
     * @return the Author object
     */
    protected Author getAuthorOrThrow(@NotNull final UUID systemId) {
        Author author = authorRepository.findBySystemId(systemId);
        if (author == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " Author, using systemId " + systemId;
            throw new NoarkEntityNotFoundException(info);
        }
        return author;
    }
}
