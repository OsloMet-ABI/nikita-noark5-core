package app.service.noark5.secondary;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.metadata.CommentType;
import app.domain.noark5.secondary.Comment;
import app.domain.repository.noark5.v5.secondary.ICommentRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.ICommentService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.secondary.ICommentLinksBuilder;
import app.webapp.payload.links.secondary.CommentLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class CommentService
        extends NoarkService
        implements ICommentService {

    private static final Logger logger =
            LoggerFactory.getLogger(CommentService.class);

    private final IMetadataService metadataService;
    private final ICommentRepository commentRepository;
    private final ICommentLinksBuilder commentLinksBuilder;

    public CommentService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IMetadataService metadataService,
            ICommentRepository commentRepository,
            ICommentLinksBuilder commentLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.metadataService = metadataService;
        this.commentRepository = commentRepository;
        this.commentLinksBuilder = commentLinksBuilder;
    }

    // All CREATE methods

    @Override
    @Transactional
    public CommentLinks createNewComment(@NotNull final Comment comment,
                                         @NotNull final File file) {
        checkCommentType(comment);
        if (null == comment.getCommentDate())
            comment.setCommentDate(OffsetDateTime.now());
        file.addComment(comment);
        return packAsLinks(commentRepository.save(comment));
    }

    @Override
    @Transactional
    public CommentLinks createNewComment(@NotNull final Comment comment,
                                         @NotNull final RecordEntity record) {
        checkCommentType(comment);
        if (null == comment.getCommentDate())
            comment.setCommentDate(OffsetDateTime.now());
        comment.addRecordEntity(record);
        return packAsLinks(commentRepository.save(comment));
    }

    @Override
    @Transactional
    public CommentLinks createNewComment(
            @NotNull final Comment comment,
            @NotNull final DocumentDescription documentDescription) {
        checkCommentType(comment);
        if (null == comment.getCommentDate())
            comment.setCommentDate(OffsetDateTime.now());
        comment.addDocumentDescription(documentDescription);
        return packAsLinks(commentRepository.save(comment));
    }

    // All READ methods

    @Override
    public CommentLinks findSingleComment(
            @NotNull final UUID commentSystemId) {
        return packAsLinks(getCommentOrThrow(commentSystemId));
    }

    // All UPDATE methods

    @Override
    @Transactional
    public CommentLinks handleUpdate(@NotNull final UUID commentSystemId,
                                     @NotNull final Comment incomingComment) {
        Comment existingComment = getCommentOrThrow(commentSystemId);

        /* Only check if it changed, in case it has a historical value */
        if (existingComment.getCommentType() !=
                incomingComment.getCommentType())
            checkCommentType(incomingComment);

        // Copy all the values you are allowed to copy ....
        existingComment.setCommentText(incomingComment.getCommentText());
        existingComment.setCommentType(incomingComment.getCommentType());
        existingComment.setCommentDate(incomingComment.getCommentDate());
        existingComment.setCommentRegisteredBy
                (incomingComment.getCommentRegisteredBy());

        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingComment.setVersion(getETag());
        return packAsLinks(existingComment);
    }

    // All DELETE methods

    @Override
    @Transactional
    public void deleteComment(@NotNull final UUID systemId) {
        Comment comment = getCommentOrThrow(systemId);
        for (DocumentDescription documentDescription :
                comment.getReferenceDocumentDescription()) {
            comment.removeDocumentDescription(documentDescription);
        }
        for (RecordEntity record : comment.getReferenceRecordEntity()) {
            comment.removeRecordEntity(record);
        }
        for (File file : comment.getReferenceFile()) {
            comment.removeFile(file);
        }
        commentRepository.delete(comment);
    }

    // All template methods

    @Override
    public CommentLinks generateDefaultComment(@NotNull final UUID systemId) {
        Comment defaultComment = new Comment();
        defaultComment.setCommentDate(OffsetDateTime.now());
        defaultComment.setCommentRegisteredBy(getUser());
        defaultComment.setVersion(-1L, true);
        return packAsLinks(defaultComment);
    }

    // All helper methods

    private CommentLinks packAsLinks(Comment comment) {
        CommentLinks commentLinks = new CommentLinks(comment);
        applyLinksAndHeader(commentLinks, commentLinksBuilder);
        return commentLinks;
    }

    protected Comment getCommentOrThrow(@NotNull final UUID commentSystemId) {
        Comment comment = commentRepository.findBySystemId(commentSystemId);
        if (comment == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " Comment, using systemId " + commentSystemId;
            logger.error(info);
            throw new NoarkEntityNotFoundException(info);
        }
        return comment;
    }

    private void checkCommentType(Comment comment) {
        if (comment.getCommentType() != null) {
            CommentType commentType = (CommentType)
                    metadataService.findValidMetadata(comment.getCommentType());
            comment.setCommentType(commentType);
        }
    }
}
