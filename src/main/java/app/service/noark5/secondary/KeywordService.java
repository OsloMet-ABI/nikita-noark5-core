package app.service.noark5.secondary;

import app.domain.noark5.Class;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.secondary.Keyword;
import app.domain.repository.noark5.v5.secondary.IKeywordRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.secondary.IKeywordService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.secondary.IKeywordLinksBuilder;
import app.webapp.payload.links.secondary.KeywordLinks;
import app.webapp.payload.links.secondary.KeywordTemplateLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class KeywordService
        extends NoarkService
        implements IKeywordService {

    private static final Logger logger =
            LoggerFactory.getLogger(KeywordService.class);

    private final IKeywordRepository keywordRepository;
    private final IKeywordLinksBuilder keywordLinksBuilder;

    public KeywordService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IKeywordRepository keywordRepository,
            IKeywordLinksBuilder keywordLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.keywordRepository = keywordRepository;
        this.keywordLinksBuilder = keywordLinksBuilder;
    }

    // All CREATE methods

    @Override
    @Transactional
    public KeywordLinks createKeywordAssociatedWithFile
            (Keyword keyword, File file) {
        keyword.addReferenceFile(file);
        return packAsLinks(keywordRepository.save(keyword));
    }

    @Override
    @Transactional
    public KeywordLinks createKeywordAssociatedWithClass
            (Keyword keyword, Class klass) {
        keyword.addReferenceClass(klass);
        return packAsLinks(keywordRepository.save(keyword));
    }

    @Override
    @Transactional
    public KeywordLinks createKeywordAssociatedWithRecord
            (Keyword keyword, RecordEntity record) {
        keyword.addReferenceRecord(record);
        return packAsLinks(keywordRepository.save(keyword));
    }

    // All READ methods

    @Override
    public KeywordLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getKeywordOrThrow(systemId));
    }

    @Override
    public KeywordLinks findAll() {
        return (KeywordLinks) processODataQueryGet();
    }

    // All UPDATE methods

    @Override
    @Transactional
    public KeywordLinks updateKeywordBySystemId(@NotNull final UUID systemId, Keyword incomingKeyword) {
        Keyword existingKeyword = getKeywordOrThrow(systemId);
        existingKeyword.setKeyword(incomingKeyword.getKeyword());
        existingKeyword.setVersion(getETag());
        return packAsLinks(existingKeyword);
    }

    // All DELETE methods

    @Override
    @Transactional
    public void deleteKeywordBySystemId(@NotNull final UUID systemId) {
        Keyword keyword = getKeywordOrThrow(systemId);
        // Remove any associations between a Class and the given Keyword
        for (Class klass : keyword.getReferenceClass()) {
            klass.removeKeyword(keyword);
        }
        // Remove any associations between a Record and the given Keyword
        for (RecordEntity record : keyword.getReferenceRecordEntity()) {
            record.removeKeyword(keyword);
        }
        // Remove any associations between a File and the given Keyword
        for (File file : keyword.getReferenceFile()) {
            file.removeKeyword(keyword);
        }
        keywordRepository.delete(keyword);
    }

    public boolean deleteKeywordIfNotEmpty(Keyword keyword) {
        if (keyword.getReferenceClass().size() > 0) {
            return false;
        }
        if (keyword.getReferenceFile().size() > 0) {
            return false;
        }
        if (keyword.getReferenceRecordEntity().size() > 0) {
            return false;
        }
        keywordRepository.delete(keyword);
        return true;
    }

    // All template methods

    @Override
    public KeywordTemplateLinks generateDefaultKeyword(@NotNull final UUID systemId) {
        Keyword suggestedKeyword = new Keyword();
        suggestedKeyword.setVersion(-1L, true);
        return packAsKeywordTemplateLinks(suggestedKeyword);
    }

    // All helper methods

    public KeywordLinks packAsLinks(@NotNull final Keyword keyword) {
        KeywordLinks keywordLinks = new KeywordLinks(keyword);
        applyLinksAndHeader(keywordLinks, keywordLinksBuilder);
        return keywordLinks;
    }

    public KeywordTemplateLinks packAsKeywordTemplateLinks(@NotNull final Keyword keyword) {
        KeywordTemplateLinks keywordLinks =
                new KeywordTemplateLinks(keyword);
        applyLinksAndHeader(keywordLinks, keywordLinksBuilder);
        return keywordLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, you
     * will only ever get a valid Keyword back. If there is no valid
     * Keyword, an exception is thrown
     *
     * @param keywordSystemId systemId of the Keyword object to retrieve
     * @return the Keyword object
     */
    protected Keyword getKeywordOrThrow(
            @NotNull final UUID keywordSystemId) {
        Keyword keyword = keywordRepository.
                findBySystemId(keywordSystemId);
        if (keyword == null) {
            String error = INFO_CANNOT_FIND_OBJECT +
                    " Keyword, using systemId " + keywordSystemId;
            logger.error(error);
            throw new NoarkEntityNotFoundException(error);
        }
        return keyword;
    }
}
