package app.service.noark5.secondary;

import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.metadata.ScreeningMetadata;
import app.domain.noark5.secondary.Screening;
import app.domain.noark5.secondary.ScreeningMetadataLocal;
import app.domain.repository.noark5.v5.secondary.IScreeningMetadataLocalRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.IScreeningMetadataService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.secondary.IScreeningMetadataLinksBuilder;
import app.webapp.payload.links.secondary.ScreeningMetadataLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static app.utils.NoarkUtils.NoarkEntity.Create.validateScreeningMetadata;

@Service
public class ScreeningMetadataService
        extends NoarkService
        implements IScreeningMetadataService {

    private final IMetadataService metadataService;
    private final IScreeningMetadataLocalRepository repository;
    private final IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder;

    public ScreeningMetadataService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IMetadataService metadataService,
            IScreeningMetadataLocalRepository repository,
            IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.metadataService = metadataService;
        this.repository = repository;
        this.screeningMetadataLinksBuilder = screeningMetadataLinksBuilder;
    }

    @Override
    @Transactional
    public ScreeningMetadataLinks createScreeningMetadata(
            Screening screening, Metadata metadata) {
        ScreeningMetadata screeningMetadata = new ScreeningMetadata(
                metadata.getCode(), metadata.getCodeName());
        validateScreeningMetadata(metadataService, screeningMetadata);

        ScreeningMetadataLocal screeningMetadataLocal =
                new ScreeningMetadataLocal(screeningMetadata);
        screening.addReferenceScreeningMetadata(screeningMetadataLocal);
        return packAsLinks(repository.save(screeningMetadataLocal));
    }

    @Override
    public ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId) {
        ScreeningMetadataLocal screeningMetadataLocal =
                new ScreeningMetadataLocal();
        screeningMetadataLocal.setVersion(-1L, true);
        return packAsLinks(screeningMetadataLocal);
    }

    @Override
    public ScreeningMetadataLinks findAll() {
        return (ScreeningMetadataLinks) processODataQueryGet();
    }

    @Override
    public ScreeningMetadataLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getScreeningMetadataLocalOrThrow(systemId));
    }

    @Override
    @Transactional
    public ScreeningMetadataLinks updateScreeningMetadataBySystemId(
            UUID systemId, Metadata incomingScreeningMetadata) {
        ScreeningMetadata screeningMetadata = new ScreeningMetadata(
                incomingScreeningMetadata.getCode(),
                incomingScreeningMetadata.getCodeName());
        validateScreeningMetadata(metadataService, screeningMetadata);

        ScreeningMetadataLocal screeningMetadataLocal =
                getScreeningMetadataLocalOrThrow(systemId);
        screeningMetadataLocal.setCode(incomingScreeningMetadata.getCode());
        screeningMetadataLocal.setCodeName(incomingScreeningMetadata.getCodeName());
        screeningMetadataLocal.setVersion(getETag());
        return packAsLinks(screeningMetadataLocal);
    }

    @Override
    @Transactional
    public void deleteScreeningMetadataBySystemId(@NotNull final UUID systemId) {
        ScreeningMetadataLocal screeningMetadataLocal =
                getScreeningMetadataLocalOrThrow(systemId);
        screeningMetadataLocal.getReferenceScreening()
                .getReferenceScreeningMetadata().remove(screeningMetadataLocal);
        screeningMetadataLocal.setReferenceScreening(null);
        repository.deleteById(systemId);
    }

    public ScreeningMetadataLinks packAsLinks(
            @NotNull final ScreeningMetadata screeningMetadata) {
        ScreeningMetadataLinks screeningMetadataLinks =
                new ScreeningMetadataLinks(screeningMetadata);
        applyLinksAndHeader(screeningMetadataLinks,
                screeningMetadataLinksBuilder);
        return screeningMetadataLinks;
    }

    public ScreeningMetadataLinks packAsLinks(
            @NotNull final ScreeningMetadataLocal screeningMetadata) {
        ScreeningMetadataLinks screeningMetadataLinks =
                new ScreeningMetadataLinks(screeningMetadata);
        applyLinksAndHeader(screeningMetadataLinks,
                screeningMetadataLinksBuilder);
        return screeningMetadataLinks;
    }

    /*
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, be aware
     * that you will only ever get a valid ScreeningMetadataLocal back. If
     * there is no valid ScreeningMetadataLocal, an exception is thrown
     *
     * @param systemId of the screeningMetadataLocal object you are looking for
     * @return the newly found screeningMetadataLocal object
     */
    private ScreeningMetadataLocal getScreeningMetadataLocalOrThrow(
            @NotNull final UUID systemId) {
        Optional<ScreeningMetadataLocal> screeningMetadata =
                repository.findBySystemId(systemId);
        if (screeningMetadata.isPresent()) {
            return screeningMetadata.get();
        } else {
            String error = "Could not find ScreeningMetadataLocal object with" +
                    " systemId " + systemId;
            throw new NoarkEntityNotFoundException(error);
        }
    }
}
