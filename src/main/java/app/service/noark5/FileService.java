package app.service.noark5;

import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.Series;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.bsm.BSMBase;
import app.domain.noark5.md_other.BSMMetadata;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.nationalidentifier.*;
import app.domain.noark5.secondary.*;
import app.domain.repository.noark5.v5.IFileRepository;
import app.domain.repository.noark5.v5.ISeriesRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.*;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.*;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.exceptions.NoarkInvalidStructureException;
import app.webapp.model.PatchMerge;
import app.webapp.model.PatchObject;
import app.webapp.model.PatchObjects;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.builder.interfaces.IFileLinksBuilder;
import app.webapp.payload.builder.interfaces.secondary.IScreeningMetadataLinksBuilder;
import app.webapp.payload.links.ClassLinks;
import app.webapp.payload.links.FileLinks;
import app.webapp.payload.links.RecordLinks;
import app.webapp.payload.links.SeriesLinks;
import app.webapp.payload.links.casehandling.CaseFileExpansionLinks;
import app.webapp.payload.links.casehandling.CaseFileLinks;
import app.webapp.payload.links.nationalidentifier.*;
import app.webapp.payload.links.secondary.*;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static app.utils.NoarkUtils.NoarkEntity.Create.*;
import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.Constants.INFO_INVALID_STRUCTURE;
import static app.utils.constants.N5ResourceMappings.*;
import static java.util.List.copyOf;
import static java.util.UUID.fromString;

@Service
public class FileService
        extends NoarkService
        implements IFileService {

    private static final Logger logger =
            LoggerFactory.getLogger(FileService.class);

    @Value("${nikita.application.allow-same-level-file-record}")
    private Boolean allow_same_level_file_record;

    private final IRecordService recordService;
    private final ICaseFileService caseFileService;
    private final IFileRepository fileRepository;
    private final IBSMService bsmService;
    private final IFileLinksBuilder fileLinksBuilder;
    private final ICommentService commentService;
    private final IKeywordService keywordService;
    private final ICrossReferenceService crossReferenceService;
    private final IMetadataService metadataService;
    private final INationalIdentifierService nationalIdentifierService;
    private final IPartService partService;
    private final IScreeningMetadataService screeningMetadataService;
    private final IStorageLocationService storageLocationService;
    private final IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder;
    private final ISeriesRepository seriesRepository;

    public FileService(EntityManager entityManager,
                       ApplicationEventPublisher applicationEventPublisher,
                       IPatchService patchService,
                       IRecordService recordService,
                       ICaseFileService caseFileService,
                       IFileRepository fileRepository,
                       IBSMService bsmService,
                       IFileLinksBuilder fileLinksBuilder,
                       ICommentService commentService,
                       IKeywordService keywordService,
                       ICrossReferenceService crossReferenceService,
                       IMetadataService metadataService,
                       INationalIdentifierService nationalIdentifierService,
                       IPartService partService,
                       IScreeningMetadataService screeningMetadataService,
                       IStorageLocationService storageLocationService,
                       IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder,
                       ISeriesRepository seriesRepository,
                       ILocalUserDetails userDetails,
                       IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.recordService = recordService;
        this.caseFileService = caseFileService;
        this.fileRepository = fileRepository;
        this.bsmService = bsmService;
        this.fileLinksBuilder = fileLinksBuilder;
        this.commentService = commentService;
        this.keywordService = keywordService;
        this.crossReferenceService = crossReferenceService;
        this.metadataService = metadataService;
        this.nationalIdentifierService = nationalIdentifierService;
        this.partService = partService;
        this.screeningMetadataService = screeningMetadataService;
        this.storageLocationService = storageLocationService;
        this.screeningMetadataLinksBuilder = screeningMetadataLinksBuilder;
        this.seriesRepository = seriesRepository;
    }

    // All CREATE operations

    @Override
    @Transactional
    public FileLinks save(File file) {
        validateDocumentMedium(metadataService, file);
        setFinaliseEntityValues(file);
        bsmService.validateBSMList(file.getReferenceBSMBase());
        validateScreening(metadataService, file);
        return packAsLinks(fileRepository.save(file));
    }

    @Override
    @Transactional
    public FileLinks createFile(File file) {
        validateDocumentMedium(metadataService, file);
        validateScreening(metadataService, file);
        bsmService.validateBSMList(file.getReferenceBSMBase());
        fileRepository.save(file);
        setFinaliseEntityValues(file);
        return packAsLinks(file);
    }

    /**
     * Persists a new file object to the database as a sub-file to an
     * existing file object. Some values are set in the incoming
     * payload (e.g. title) while some are set by the core.  owner,
     * createdBy, createdDate are automatically set by the core.
     *
     * @param systemId systemId of the parent object to connect this
     *                 file as a child to
     * @param newfile  The file object with some values set
     * @return the newly persisted file object wrapped as a fileLinks object
     */
    @Override
    @Transactional
    public FileLinks createFileAssociatedWithFile(
            UUID systemId, File newfile) {
        File file = getFileOrThrow(systemId);
        /* reject if associated Record already exist, as only either
         * Record or File is allowed on the same level below File
         * according to Noark 5v5. */
        /* This is a stupid limitation, its removal has been requested
         * in https://github.com/arkivverket/noark5-standard/pull/142
         */
        if (!allow_same_level_file_record &&
                !file.getReferenceRecordEntity().isEmpty()) {
            String info = INFO_INVALID_STRUCTURE +
                    ". Either " + RECORD + " or " + FILE + " below " + FILE + " with systemId " + file.getSystemIdAsString();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, FILE, FILE);
        }
        newfile.setReferenceParentFile(file);
        return save(newfile);
    }

    @Override
    @Transactional
    public CommentLinks createCommentAssociatedWithFile
            (@NotNull final UUID systemId, Comment comment) {
        return commentService.createNewComment(comment,
                getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public RecordLinks createRecordAssociatedWithFile(
            UUID systemId, RecordEntity record) {
        File file = getFileOrThrow(systemId);
        /* reject if associated File already exist, as only either
         * Record or File is allowed on the same level below File
         * according to Noark 5v5. */
        /* This is a stupid limitation, its removal has been requested
         * in https://github.com/arkivverket/noark5-standard/pull/142
         */
        if (!allow_same_level_file_record &&
                !file.getReferenceChildFile().isEmpty()) {
            String info = INFO_INVALID_STRUCTURE +
                    ". Either " + RECORD + " or " + FILE + " below " + FILE + " with systemId " + file.getSystemIdAsString();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, FILE, RECORD);
        }
        record.setReferenceFile(file);
        return recordService.save(record);
    }

    @Override
    @Transactional
    public PartPersonLinks
    createPartPersonAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull PartPerson partPerson) {
        return partService.
                createNewPartPerson(partPerson, getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public PartUnitLinks
    createPartUnitAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull PartUnit partUnit) {
        return partService.
                createNewPartUnit(partUnit, getFileOrThrow(systemId));
    }

    @Override
    public KeywordLinks createKeywordAssociatedWithFile(
            UUID systemId, Keyword keyword) {
        return keywordService.createKeywordAssociatedWithFile(keyword,
                getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public BuildingLinks
    createBuildingAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull Building building) {
        return nationalIdentifierService.
                createNewBuilding(building, getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public CadastralUnitLinks
    createCadastralUnitAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull CadastralUnit cadastralUnit) {
        return nationalIdentifierService.
                createNewCadastralUnit(cadastralUnit, getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public DNumberLinks
    createDNumberAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull DNumber dNumber) {
        return nationalIdentifierService.
                createNewDNumber(dNumber, getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public PlanLinks
    createPlanAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull Plan plan) {
        return nationalIdentifierService.
                createNewPlan(plan, getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public PositionLinks
    createPositionAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull Position position) {
        return nationalIdentifierService.
                createNewPosition(position, getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public SocialSecurityNumberLinks
    createSocialSecurityNumberAssociatedWithFile
            (@NotNull final UUID systemId,
             @NotNull SocialSecurityNumber socialSecurityNumber) {
        return nationalIdentifierService.
                createNewSocialSecurityNumber(socialSecurityNumber,
                        getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public UnitLinks
    createUnitAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull Unit unit) {
        return nationalIdentifierService.
                createNewUnit(unit, getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public CaseFileLinks expanLinksCaseFile(
            @NotNull final UUID systemId, @NotNull PatchMerge patchMerge) {
        return caseFileService.expandFileAsCaseFileLinks(
                getFileOrThrow(systemId), patchMerge);
    }

    @Override
    @Transactional
    public StorageLocationLinks createStorageLocationAssociatedWithFile(
            UUID systemId, StorageLocation storageLocation) {
        File file = getFileOrThrow(systemId);
        return storageLocationService
                .createStorageLocationAssociatedWithFile(
                        storageLocation, file);
    }

    /**
     * Retrieve a list of children file belonging to the file object
     * identified by systemId
     *
     * @param systemId The systemId of the File object to retrieve its children
     * @return A FileLinks object containing the children file's
     */
    @Override
    public FileLinks findAllChildren(@NotNull final UUID systemId) {
        // Make sure the file exists
        getFileOrThrow(systemId);
        return (FileLinks) processODataQueryGet();
    }

    /**
     * Retrieve a list of children file belonging to the file object
     * identified by systemId
     *
     * @param systemId The systemId of the File object to retrieve its children
     * @return A FileLinks object containing the children file's
     */
    @Override
    public RecordLinks findAllRecords(@NotNull final UUID systemId) {
        // Make sure the file exists
        getFileOrThrow(systemId);
        return (RecordLinks) processODataQueryGet();
    }

    @Override
    public PartPersonLinks generateDefaultPartPerson(@NotNull final UUID systemId) {
        return partService.generateDefaultPartPerson(systemId);
    }

    @Override
    public PartUnitLinks generateDefaultPartUnit(@NotNull final UUID systemId) {
        return partService.generateDefaultPartUnit(systemId);
    }

    @Override
    public CaseFileExpansionLinks generateDefaultValuesToExpanLinksCaseFile(
            @NotNull final UUID systemId) {
        return caseFileService.generateDefaultExpandedCaseFile();
    }

    @Override
    public FileLinks findAll() {
        return (FileLinks) processODataQueryGet();
    }

    @Override
    public CommentLinks getCommentAssociatedWithFile(
            @NotNull final UUID systemId) {
        // Make sure the file exists
        getFileOrThrow(systemId);
        return (CommentLinks) processODataQueryGet();
    }

    @Override
    public PartLinks getPartAssociatedWithFile(
            @NotNull final UUID systemId) {
        // Make sure the file exists
        getFileOrThrow(systemId);
        return (PartLinks) processODataQueryGet();
    }

    @Override
    public KeywordLinks findKeywordAssociatedWithFile(
            @NotNull final UUID systemId) {
        getFileOrThrow(systemId);
        return (KeywordLinks) processODataQueryGet();
    }

    @Override
    public CrossReferenceLinks findCrossReferenceAssociatedWithFile(
            @NotNull final UUID systemId) {
        return (CrossReferenceLinks) processODataQueryGet();
    }

    @Override
    public StorageLocationLinks getStorageLocationAssociatedWithFile(
            @NotNull final UUID systemId) {
        getFileOrThrow(systemId);
        return (StorageLocationLinks) processODataQueryGet();
    }

    @Override
    public NationalIdentifierLinks getNationalIdentifierAssociatedWithFile(
            @NotNull final UUID systemId) {
        return (NationalIdentifierLinks) processODataQueryGet();
    }

    @Override
    public ScreeningMetadataLinks
    getScreeningMetadataAssociatedWithFile(@NotNull final UUID systemId) {
        Screening screening = getFileOrThrow(systemId)
                .getReferenceScreening();
        if (null == screening) {
            throw new NoarkEntityNotFoundException(
                    INFO_CANNOT_FIND_OBJECT + " Screening, using systemId " +
                            systemId);
        }
        return packAsLinks(new SearchResultsPage(copyOf(
                screening.getReferenceScreeningMetadata())));
    }

    @Override
    public FileLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getFileOrThrow(systemId));
    }

    /**
     * Retrieve all Class associated with the file identified by
     * the files systemId.
     *
     * @param systemId systemId of the file
     * @return The parent Class packed as a ClassLinks
     */
    @Override
    public ClassLinks
    findClassAssociatedWithFile(@NotNull final UUID systemId) {
        return (ClassLinks) processODataQueryGet();
    }

    /**
     * Retrieve all Series associated with the file identified by
     * the files systemId.
     *
     * @param systemId systemId of the file
     * @return The parent Series packed as a SeriesLinks
     */
    @Override
    public SeriesLinks
    findSeriesAssociatedWithFile(@NotNull final UUID systemId) {
        return (SeriesLinks) processODataQueryGet();
    }

    // All UPDATE operations

    /**
     * Updates a DocumentDescription object in the database. First we try to
     * locate the DocumentDescription object. If the DocumentDescription object
     * does not exist a NoarkEntityNotFoundException exception is thrown that
     * the caller has to deal with.
     * <br>
     * After this the values you are allowed to update are copied from the
     * incomingDocumentDescription object to the existingDocumentDescription
     * object and the existingDocumentDescription object will be persisted to
     * the database when the transaction boundary is over.
     * <p>
     * Note, the version corresponds to the version number, when the object
     * was initially retrieved from the database. If this number is not the
     * same as the version number when re-retrieving the DocumentDescription
     * object from the database a NoarkConcurrencyException is thrown. Note.
     * This happens when the call to DocumentDescription.setVersion() occurs.
     * <p>
     * Note: title is not nullable
     *
     * @param systemId     systemId of File to update
     * @param incomingFile the incoming file
     * @return the updated File object after it is persisted
     */
    @Override
    @Transactional
    public FileLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull File incomingFile) {
        File existingFile = getFileOrThrow(systemId);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingFile, incomingFile);
        // Here copy all the values you are allowed to copy ....
        updateTitleAndDescription(incomingFile, existingFile);
        if (null != incomingFile.getDocumentMedium()) {
            existingFile.setDocumentMedium(
                    incomingFile.getDocumentMedium());
        }
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingFile.setVersion(getETag());
        publishChangeLogEvents(changeLogs);
        return packAsLinks(existingFile);
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingFile the existing File from the database
     * @param newFile      the incoming File
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(File existingFile, File newFile) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // mappe	M020	tittel	Ved endring
        if (!existingFile.getTitle().equals(newFile.getTitle())) {
            changeLogs.add(createChangeLogObject(TITLE, existingFile.getTitle(),
                    newFile.getTitle(), existingFile));
        }
        // TODO: mappe	M208	referanseArkivdel	Ved endring
        return changeLogs;
    }

    @Transactional
    public FileLinks update(File file) {
        bsmService.validateBSMList(file.getReferenceBSMBase());
        return packAsLinks(file);
    }

    @Override
    @Transactional
    public FileLinks handleUpdate(
            UUID systemId, PatchObjects patchObjects) {
        for (PatchObject patchObject : patchObjects.getPatchObjects()) {
            File file = getFileOrThrow(systemId);
            Series fromSeries = seriesRepository.findBySystemId(fromString(patchObject.getFrom()));
            Series toSeries = seriesRepository.findBySystemId(fromString(patchObject.getPath()));
            if ("move".equalsIgnoreCase(patchObject.getOp())) {
                fromSeries.removeFile(file);
                file.setReferenceSeries(toSeries);
                toSeries.addFile(file);
                return packAsLinks(file);
            }
        }
        return null;
    }

    @Override
    @Transactional
    public ScreeningMetadataLinks createScreeningMetadataAssociatedWithFile(
            UUID systemId, Metadata screeningMetadata) {
        File file = getFileOrThrow(systemId);
        if (null == file.getReferenceScreening()) {
            throw new NoarkEntityNotFoundException(INFO_CANNOT_FIND_OBJECT +
                    " Screening, associated with File with systemId " +
                    systemId);
        }
        return screeningMetadataService.createScreeningMetadata(
                file.getReferenceScreening(), screeningMetadata);
    }

    @Override
    @Transactional
    public CrossReferenceLinks createCrossReferenceAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final CrossReference crossReference) {
        return crossReferenceService.createCrossReferenceAssociatedWithFile(
                crossReference, getFileOrThrow(systemId));
    }

    @Override
    @Transactional
    public Object associateBSM(@NotNull final UUID systemId,
                               @NotNull List<BSMBase> bsm) {
        File file = getFileOrThrow(systemId);
        file.addReferenceBSMBase(bsm);
        return file;
    }

    // All DELETE operations

    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        File file = getFileOrThrow(systemId);
        fileRepository.delete(file);
        handleDeletionForEventLog(file);
    }

    /**
     * Delete all objects belonging to the organisation identified by organisation
     */
    @Override
    @Transactional
    public void deleteAllByOrganisation() {
        fileRepository.deleteByOrganisation(getOrganisation());
    }

    // All template operations

    @Override
    public StorageLocationLinks getDefaultStorageLocation(@NotNull final UUID systemId) {
        return storageLocationService.getDefaultStorageLocation(systemId);
    }

    @Override
    public CrossReferenceLinks getDefaultCrossReference(
            @NotNull final UUID systemId) {
        return crossReferenceService.getDefaultCrossReference(systemId);
    }

    /**
     * Generate a Default File object
     * <br>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @return the File object wrapped as a FileLinks object
     */
    @Override
    public FileLinks generateDefaultFile(@NotNull final UUID systemId) {
        File defaultFile = new File();
        defaultFile.setVersion(-1L, true);
        return packAsLinks(defaultFile);
    }

    @Override
    public ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId) {
        return screeningMetadataService.getDefaultScreeningMetadata(systemId);
    }

    @Override
    public CommentLinks generateDefaultComment(@NotNull final UUID systemId) {
        return commentService.generateDefaultComment(systemId);
    }

    @Override
    public KeywordTemplateLinks generateDefaultKeyword(@NotNull final UUID systemId) {
        return keywordService.generateDefaultKeyword(systemId);
    }

    @Override
    public BuildingLinks generateDefaultBuilding(
            @NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultBuilding(systemId);
    }

    @Override
    public CadastralUnitLinks generateDefaultCadastralUnit(
            @NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultCadastralUnit(systemId);
    }

    @Override
    public DNumberLinks generateDefaultDNumber(@NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultDNumber(systemId);
    }

    @Override
    public PlanLinks generateDefaultPlan(@NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultPlan(systemId);
    }

    @Override
    public PositionLinks generateDefaultPosition(
            @NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultPosition(systemId);
    }

    @Override
    public SocialSecurityNumberLinks generateDefaultSocialSecurityNumber(
            @NotNull final UUID systemId) {
        return nationalIdentifierService
                .generateDefaultSocialSecurityNumber(systemId);
    }

    @Override
    public UnitLinks generateDefaultUnit(@NotNull final UUID systemId) {
        return nationalIdentifierService.generateDefaultUnit(systemId);
    }

    // All HELPER operations

    public ScreeningMetadataLinks packAsLinks(SearchResultsPage page) {
        ScreeningMetadataLinks screeningMetadataLinks =
                new ScreeningMetadataLinks(page);
        applyLinksAndHeader(screeningMetadataLinks,
                screeningMetadataLinksBuilder);
        return screeningMetadataLinks;
    }

    public FileLinks packAsLinks(@NotNull final File file) {
        FileLinks fileLinks = new FileLinks(file);
        applyLinksAndHeader(fileLinks, fileLinksBuilder);
        return fileLinks;
    }

    /**
     * Used to retrieve a BSMBase object so parent can can check that the
     * BSMMetadata object exists and is not outdated.
     * Done to simplify coding.
     *
     * @param name Name of the BSM parameter to check
     * @return BSMMetadata object corresponding to the name
     */
    @Override
    protected Optional<BSMMetadata> findBSMByName(String name) {
        return bsmService.findBSMByName(name);
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, be aware
     * that you will only ever get a valid File back. If there is no valid
     * File, an exception is thrown
     *
     * @param systemId systemId of the file object you are looking for
     * @return the newly found file object or null if it does not exist
     */
    public File getFileOrThrow(@NotNull final UUID systemId) {
        File file = fileRepository.findBySystemId(systemId);
        if (file == null) {
            String info = INFO_CANNOT_FIND_OBJECT + " File, using systemId " +
                    systemId;
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        }
        return file;
    }
}
