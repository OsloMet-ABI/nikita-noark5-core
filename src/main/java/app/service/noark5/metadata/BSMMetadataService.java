package app.service.noark5.metadata;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.md_other.BSMMetadata;
import app.domain.repository.noark5.v5.other.IBSMMetadataRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.metadata.IBSMMetadataService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.model.PatchMerge;
import app.webapp.model.PatchObjects;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.builder.interfaces.metadata.IBSMMetadataLinksBuilder;
import app.webapp.payload.links.metadata.BSMMetadataLinks;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static app.utils.CommonUtils.WebUtils.getMethodsForRequestAsListOrThrow;
import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.N5ResourceMappings.BSM_DEF;
import static java.util.List.copyOf;
import static org.springframework.http.HttpHeaders.ALLOW;

/**
 * Service class for BSMMetadata. Note this is the actual class that is used to
 * handle the retrieval, creation, deletion and updating of BSM Metadata.
 * This class does not handle adding BSM metdata to entities e.g. File. See
 * FileService to see how to add BSM metadata to a file.
 */
@Service
public class BSMMetadataService
        extends NoarkService
        implements IBSMMetadataService {

    private static final Logger logger =
            LoggerFactory.getLogger(BSMMetadataService.class);
    private final IBSMMetadataLinksBuilder linksBuilder;
    private final IBSMMetadataRepository metadataRepository;

    public BSMMetadataService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IBSMMetadataLinksBuilder linksBuilder,
            IBSMMetadataRepository metadataRepository,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.linksBuilder = linksBuilder;
        this.metadataRepository = metadataRepository;
    }

    // All CREATE methods

    @Override
    @Transactional
    public BSMMetadataLinks save(@NotNull final BSMMetadata bsmMetadata) {
        return packAsLinks(metadataRepository.save(bsmMetadata));
    }

    // All READ methods

    @Override
    public BSMMetadataLinks find(@NotNull final UUID systemId) {
        return packAsLinks(getBSMMetadataOrThrow(systemId));
    }

    @Override
    @SuppressWarnings("unchecked")
    public BSMMetadataLinks findAll() {
        SearchResultsPage page = new
                SearchResultsPage(copyOf((List<INoarkEntity>) (List)
                metadataRepository.findAll()));
        setOutgoingRequestHeaderList();
        return new BSMMetadataLinks(page, BSM_DEF);
    }
    // All UPDATE methods


    @Override
    public BSMMetadataLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final BSMMetadata incomingBsmMetadata) {
        BSMMetadata bsmMetadata = getBSMMetadataOrThrow(systemId);
        bsmMetadata.setDescription(incomingBsmMetadata.getDescription());
        bsmMetadata.setName(incomingBsmMetadata.getName());
        bsmMetadata.setOutdated(incomingBsmMetadata.getOutdated());
        bsmMetadata.setSource(incomingBsmMetadata.getSource());
        bsmMetadata.setType(incomingBsmMetadata.getType());
        bsmMetadata.setVersion(getETag());
        return packAsLinks(bsmMetadata);
    }

    @Override
    @Transactional
    public BSMMetadataLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final PatchObjects patchObjects) {
        return packAsLinks((BSMMetadata) handlePatch(systemId, patchObjects));
    }

    @Override
    @Transactional
    public Object handleUpdateRfc7396(
            @NotNull final UUID systemId, @NotNull PatchMerge patchMerge) {
        BSMMetadata bsmMetadata = getBSMMetadataOrThrow(systemId);
        handlePatchMerge(bsmMetadata, patchMerge);
        return packAsLinks(bsmMetadata);
    }

    // All DELETE methods

    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        deleteEntity(getBSMMetadataOrThrow(systemId));
    }

    // All helper methods

    /**
     * Set the outgoing ALLOW header
     */
    protected void setOutgoingRequestHeaderList() {
        HttpServletResponse response = ((ServletRequestAttributes)
                Objects.requireNonNull(
                        RequestContextHolder.getRequestAttributes()))
                .getResponse();
        HttpServletRequest request =
                ((ServletRequestAttributes)
                        RequestContextHolder
                                .getRequestAttributes()).getRequest();
        response.addHeader(ALLOW, getMethodsForRequestAsListOrThrow(
                request.getServletPath()));
    }

    public BSMMetadataLinks packAsLinks(
            @NotNull final BSMMetadata bsmMetadata) {
        BSMMetadataLinks bsmMetadataLinks =
                new BSMMetadataLinks(bsmMetadata);
        applyLinksAndHeader(bsmMetadataLinks, linksBuilder);
        return bsmMetadataLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, be aware
     * that you will only ever get a valid BSMMetadata back. If there is no valid
     * BSMMetadata, an exception is thrown
     *
     * @param systemId systemId of the bsmMetadata object you are looking for
     * @return the newly found bsmMetadata object or null if it does not exist
     */
    public BSMMetadata getBSMMetadataOrThrow(@NotNull final UUID systemId) {
        Optional<BSMMetadata> bsmMetadata =
                metadataRepository.findById(systemId);
        if (bsmMetadata.isEmpty()) {
            String info = INFO_CANNOT_FIND_OBJECT + " BSMMetadata, using systemId " +
                    systemId;
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        } else {
            return bsmMetadata.get();
        }
    }
}
