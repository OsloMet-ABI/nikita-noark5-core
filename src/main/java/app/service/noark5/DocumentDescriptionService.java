package app.service.noark5;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.metadata.AssociatedWithRecordAs;
import app.domain.noark5.metadata.DocumentStatus;
import app.domain.noark5.metadata.DocumentType;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.secondary.*;
import app.domain.repository.noark5.v5.IDocumentDescriptionRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.IBSMService;
import app.service.interfaces.IDocumentDescriptionService;
import app.service.interfaces.IDocumentObjectService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.*;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.builder.interfaces.IDocumentDescriptionLinksBuilder;
import app.webapp.payload.builder.interfaces.secondary.IScreeningMetadataLinksBuilder;
import app.webapp.payload.links.DocumentDescriptionLinks;
import app.webapp.payload.links.DocumentObjectLinks;
import app.webapp.payload.links.RecordLinks;
import app.webapp.payload.links.secondary.*;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static app.utils.NoarkUtils.NoarkEntity.Create.*;
import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.DatabaseConstants.DELETE_FROM_RECORD_DOCUMENT_DESCRIPTION;
import static app.utils.constants.N5ResourceMappings.*;
import static java.util.List.copyOf;

@Service
public class DocumentDescriptionService
        extends NoarkService
        implements IDocumentDescriptionService {

    private final IDocumentObjectService documentObjectService;
    private final IDocumentDescriptionRepository documentDescriptionRepository;
    private final IDocumentDescriptionLinksBuilder documentDescriptionLinksBuilder;
    private final IAuthorService authorService;
    private final ICommentService commentService;
    private final IMetadataService metadataService;
    private final IPartService partService;
    private final IBSMService bsmService;
    private final IScreeningMetadataService screeningMetadataService;
    private final IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder;
    private final ISecondaryService secondaryService;

    public DocumentDescriptionService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IDocumentObjectService documentObjectService,
            IDocumentDescriptionRepository documentDescriptionRepository,
            IDocumentDescriptionLinksBuilder
                    documentDescriptionLinksBuilder,
            IAuthorService authorService,
            ICommentService commentService,
            IMetadataService metadataService,
            IPartService partService,
            IBSMService bsmService,
            IScreeningMetadataService screeningMetadataService,
            IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder,
            ISecondaryService secondaryService,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.documentObjectService = documentObjectService;
        this.documentDescriptionRepository = documentDescriptionRepository;
        this.documentDescriptionLinksBuilder =
                documentDescriptionLinksBuilder;
        this.authorService = authorService;
        this.commentService = commentService;
        this.metadataService = metadataService;
        this.partService = partService;
        this.bsmService = bsmService;
        this.screeningMetadataService = screeningMetadataService;
        this.screeningMetadataLinksBuilder = screeningMetadataLinksBuilder;
        this.secondaryService = secondaryService;
    }

    // All CREATE operations
    @Override
    @Transactional
    public DocumentObjectLinks
    createDocumentObjectAssociatedWithDocumentDescription(
            @NotNull final UUID systemId,
            @NotNull final DocumentObject documentObject) {
        DocumentDescription documentDescription =
                getDocumentDescriptionOrThrow(systemId);
        documentObject.setReferenceDocumentDescription(documentDescription);
        List<DocumentObject> documentObjects = documentDescription
                .getReferenceDocumentObject();
        documentObjects.add(documentObject);
        bsmService.validateBSMList(documentDescription.getReferenceBSMBase());
        return documentObjectService.save(documentObject);
    }

    @Override
    @Transactional
    public ScreeningMetadataLinks
    createScreeningMetadataAssociatedWithDocumentDescription(
            UUID systemId, Metadata screeningMetadata) {
        DocumentDescription documentDescription =
                getDocumentDescriptionOrThrow(systemId);
        if (null == documentDescription.getReferenceScreening()) {
            throw new NoarkEntityNotFoundException(INFO_CANNOT_FIND_OBJECT +
                    " Screening, associated with DocumentDescription with systemId " +
                    systemId);
        }
        return screeningMetadataService.createScreeningMetadata(
                documentDescription.getReferenceScreening(), screeningMetadata);
    }

    @Override
    @Transactional
    public CommentLinks createCommentAssociatedWithDocumentDescription
            (@NotNull final UUID systemId, Comment comment) {
        return commentService.createNewComment
                (comment, getDocumentDescriptionOrThrow(systemId));
    }

    @Override
    @Transactional
    public PartPersonLinks
    createPartPersonAssociatedWithDocumentDescription(
            UUID systemId, PartPerson partPerson) {
        return partService.
                createNewPartPerson(partPerson,
                        getDocumentDescriptionOrThrow(systemId));
    }

    @Override
    @Transactional
    public PartUnitLinks
    createPartUnitAssociatedWithDocumentDescription(
            UUID systemId, PartUnit partUnit) {
        return partService.
                createNewPartUnit(partUnit,
                        getDocumentDescriptionOrThrow(systemId));
    }

    @Override
    @Transactional
    public DocumentDescriptionLinks save(DocumentDescription documentDescription) {
        validateDocumentMedium(metadataService, documentDescription);
        validateDocumentStatus(documentDescription);
        validateDocumentType(documentDescription);
        validateDeletion(documentDescription.getReferenceDeletion());
        bsmService.validateBSMList(documentDescription.getReferenceBSMBase());
        documentDescription.setAssociationDate(OffsetDateTime.now());
        documentDescription.setAssociatedBy(getUser());
        return packAsLinks(documentDescriptionRepository
                .save(documentDescription));
    }

    /**
     * Persist and associate the incoming author object with the
     * documentDescription identified by systemId
     *
     * @param systemId The systemId of the documentDescription to associate
     *                 with
     * @param author   The incoming author object
     * @return author object wrapped as a AuthorLinks
     */
    @Override
    @Transactional
    public AuthorLinks associateAuthorWithDocumentDescription(
            UUID systemId, Author author) {
        return authorService.associateAuthorWithDocumentDescription
                (author, getDocumentDescriptionOrThrow(systemId));
    }

    /**
     * Generate a Default DocumentDescription object.
     * <p>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @return the DocumentDescription object wrapped as a
     * DocumentDescriptionLinks object
     */
    @Override
    public DocumentDescriptionLinks generateDefaultDocumentDescription(
            @NotNull final UUID systemId) {
        DocumentDescription defaultDocumentDescription =
                new DocumentDescription();

        AssociatedWithRecordAs associatedWithRecordAs = (AssociatedWithRecordAs)
                metadataService.findValidMetadataByEntityTypeOrThrow
                        (ASSOCIATED_WITH_RECORD_AS, MAIN_DOCUMENT_CODE, null);
        defaultDocumentDescription
                .setAssociatedWithRecordAs(associatedWithRecordAs);
        DocumentType documentType = (DocumentType)
                metadataService.findValidMetadataByEntityTypeOrThrow
                        (DOCUMENT_TYPE, LETTER_CODE, null);
        defaultDocumentDescription.setDocumentType(documentType);
        DocumentStatus documentStatus = (DocumentStatus)
                metadataService.findValidMetadataByEntityTypeOrThrow
                        (DOCUMENT_STATUS, DOCUMENT_STATUS_FINALISED_CODE, null);
        defaultDocumentDescription.setDocumentStatus(documentStatus);
        defaultDocumentDescription.setVersion(-1L, true);
        return packAsLinks(defaultDocumentDescription);
    }

    @Override
    public CommentLinks generateDefaultComment(@NotNull final UUID systemId) {
        return commentService.generateDefaultComment(systemId);
    }

    @Override
    public PartPersonLinks generateDefaultPartPerson(
            @NotNull final UUID systemId) {
        return partService.generateDefaultPartPerson(systemId);
    }

    @Override
    public PartUnitLinks generateDefaultPartUnit(@NotNull final UUID systemId) {
        return partService.generateDefaultPartUnit(systemId);
    }

    @Override
    public AuthorLinks generateDefaultAuthor(@NotNull final UUID systemId) {
        return authorService.generateDefaultAuthor(systemId);
    }

    @Override
    public ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId) {
        return screeningMetadataService.getDefaultScreeningMetadata(systemId);
    }

    @Override
    public DocumentDescriptionLinks findBySystemId(
            @NotNull final UUID systemId) {
        return packAsLinks(getDocumentDescriptionOrThrow(systemId));
    }

    @Override
    public AuthorLinks findAllAuthorWithDocumentDescriptionBySystemId(
            UUID systemId) {
        getDocumentDescriptionOrThrow(systemId);
        return (AuthorLinks) processODataQueryGet();
    }

    @Override
    public ScreeningMetadataLinks
    getScreeningMetadataAssociatedWithDocumentDescription(
            @NotNull final UUID systemId) {
        Screening screening = getDocumentDescriptionOrThrow(systemId)
                .getReferenceScreening();
        if (null == screening) {
            throw new NoarkEntityNotFoundException(
                    INFO_CANNOT_FIND_OBJECT + " Screening, using systemId " +
                            systemId);
        }
        return packAsLinks(new SearchResultsPage(copyOf(
                screening.getReferenceScreeningMetadata())));
    }

    @Override
    public DocumentDescriptionLinks findAll() {
        return (DocumentDescriptionLinks) processODataQueryGet();
    }

    @Override
    public DocumentObjectLinks
    findAllDocumentObjectWithDocumentDescriptionBySystemId(
            @NotNull final UUID systemId) {
        getDocumentDescriptionOrThrow(systemId);
        return (DocumentObjectLinks) processODataQueryGet();
    }

    @Override
    public RecordLinks
    findAllRecordWithDocumentDescriptionBySystemId(
            @NotNull final UUID systemId) {
        getDocumentDescriptionOrThrow(systemId);
        return (RecordLinks) processODataQueryGet();
    }

    @Override
    public CommentLinks getCommentAssociatedWithDocumentDescription(
            @NotNull final UUID systemId) {
        return (CommentLinks) processODataQueryGet();
    }

    @Override
    public PartLinks getPartAssociatedWithDocumentDescription(
            @NotNull final UUID systemId) {
        return (PartLinks) processODataQueryGet();
    }

    // -- All UPDATE operations

    /**
     * Updates a DocumentDescription object in the database. First we try to locate the
     * DocumentDescription object. If the DocumentDescription object does not exist a
     * NoarkEntityNotFoundException exception is thrown that the caller has
     * to deal with.
     * <br>
     * After this the values you are allowed to update are copied from the
     * incomingDocumentDescription object to the existingDocumentDescription object and the
     * existingDocumentDescription object will be persisted to the database when the
     * transaction boundary is over.
     * <p>
     * Note, the version corresponds to the version number, when the object
     * was initially retrieved from the database. If this number is not the
     * same as the version number when re-retrieving the DocumentDescription object from
     * the database a NoarkConcurrencyException is thrown. Note. This happens
     * when the call to DocumentDescription.setVersion() occurs.
     *
     * @param systemId                    systemId of the incoming documentDescription object
     * @param incomingDocumentDescription the incoming documentDescription
     * @return the updated documentDescription after it is persisted
     */
    @Override
    @Transactional
    public DocumentDescriptionLinks handleUpdate(
            @NotNull final UUID systemId, @NotNull final DocumentDescription incomingDocumentDescription) {
        DocumentDescription existingDocumentDescription =
                getDocumentDescriptionOrThrow(systemId);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingDocumentDescription, incomingDocumentDescription);
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingDocumentDescription.setVersion(getETag());
        updateDeletion(incomingDocumentDescription,
                existingDocumentDescription);
        updateTitleAndDescription(incomingDocumentDescription,
                existingDocumentDescription);
        updateDocumentDescription(incomingDocumentDescription,
                existingDocumentDescription);

        validateDocumentType(incomingDocumentDescription);
        existingDocumentDescription.setDocumentType(
                incomingDocumentDescription.getDocumentType());
        validateDocumentStatus(incomingDocumentDescription);
        existingDocumentDescription.setDocumentStatus(
                incomingDocumentDescription.getDocumentStatus());

        bsmService.validateBSMList(incomingDocumentDescription
                .getReferenceBSMBase());

        existingDocumentDescription.setStorageLocation(
                incomingDocumentDescription.getStorageLocation());
        publishChangeLogEvents(changeLogs);
        return packAsLinks(existingDocumentDescription);
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingDocumentDescription the existing DocumentDescription from the database
     * @param newDocumentDescription      the incoming DocumentDescription
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(DocumentDescription existingDocumentDescription, DocumentDescription newDocumentDescription) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // dokumentbeskrivelse	M020	tittel	Ved endring etter status E
        if (!existingDocumentDescription.getTitle().equals(newDocumentDescription.getTitle())) {
            changeLogs.add(createChangeLogObject(TITLE, existingDocumentDescription.getTitle(),
                    newDocumentDescription.getTitle(), existingDocumentDescription));
        }
        // dokumentbeskrivelse	M020	tittel	Ved endring etter status E
        if (!existingDocumentDescription.getDocumentStatus().equals(newDocumentDescription.getDocumentStatus())) {
            changeLogs.add(createChangeLogObject(DOCUMENT_DESCRIPTION_STATUS,
                    existingDocumentDescription.getDocumentStatus().getCode() + " " +
                            existingDocumentDescription.getDocumentStatus().getCodeName(),
                    newDocumentDescription.getDocumentStatus().getCode() + " " +
                            newDocumentDescription.getDocumentStatus().getCodeName(),
                    existingDocumentDescription));
        }
        return changeLogs;
    }

// All DELETE operations

    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        DocumentDescription documentDescription = getDocumentDescriptionOrThrow(systemId);
        // Disassociate any links between DocumentDescription and Record
        disassociateForeignKeys(documentDescription, DELETE_FROM_RECORD_DOCUMENT_DESCRIPTION);
        // Delete all :1 secondary entities
        secondaryService.deleteDeletionObject(documentDescription);
        secondaryService.deleteClassifiedObject(documentDescription);
        secondaryService.deleteDisposalObject(documentDescription);
        secondaryService.deleteDisposalUndertakenObject(documentDescription);
        secondaryService.deleteElectronicSignatureObject(documentDescription);
        secondaryService.deleteScreeningObject(documentDescription);
        // Delete the documentDescription
        documentDescriptionRepository.delete(documentDescription);
    }

    /**
     * Delete all objects belonging to the organisation identified by organisation
     */
    @Override
    @Transactional
    public void deleteAllByOrganisation() {
        documentDescriptionRepository.deleteByOrganisation(getOrganisation());
    }

    // All HELPER operations

    public ScreeningMetadataLinks packAsLinks(SearchResultsPage page) {
        ScreeningMetadataLinks screeningMetadataLinks =
                new ScreeningMetadataLinks(page);
        applyLinksAndHeader(screeningMetadataLinks,
                screeningMetadataLinksBuilder);
        return screeningMetadataLinks;
    }

    public DocumentDescriptionLinks packAsLinks(
            @NotNull final DocumentDescription documentDescription) {
        DocumentDescriptionLinks documentDescriptionLinks =
                new DocumentDescriptionLinks(documentDescription);
        applyLinksAndHeader(documentDescriptionLinks,
                documentDescriptionLinksBuilder);
        return documentDescriptionLinks;
    }

    private void updateDocumentDescription(
            @NotNull final DocumentDescription incomingDocumentDescription,
            @NotNull final DocumentDescription existingDocumentDescription) {
        if (null != incomingDocumentDescription.getDocumentMedium()) {
            existingDocumentDescription.setDocumentMedium(
                    incomingDocumentDescription.getDocumentMedium());
        }
        if (null != incomingDocumentDescription.getAssociatedWithRecordAs()) {
            existingDocumentDescription.setAssociatedWithRecordAs(
                    incomingDocumentDescription.getAssociatedWithRecordAs());
        }
        if (null != incomingDocumentDescription.getDocumentType()) {
            existingDocumentDescription.setDocumentType(
                    incomingDocumentDescription.getDocumentType());
        }
        if (null != incomingDocumentDescription.getDocumentStatus()) {
            existingDocumentDescription.setDocumentStatus(
                    incomingDocumentDescription.getDocumentStatus());
        }
        existingDocumentDescription.setDocumentNumber(
                incomingDocumentDescription.getDocumentNumber());
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid DocumentDescription back. If there
     * is no valid DocumentDescription, an exception is thrown
     *
     * @param systemId systemId of the documentDescription
     * @return The documentDescription to be returned
     */
    protected DocumentDescription getDocumentDescriptionOrThrow(
            @NotNull final UUID systemId) {
        DocumentDescription documentDescription =
                documentDescriptionRepository.findBySystemId(
                        systemId);
        if (documentDescription == null) {
            String error = INFO_CANNOT_FIND_OBJECT +
                    " DocumentDescription, using systemId " +
                    systemId;
            throw new NoarkEntityNotFoundException(error);
        }
        return documentDescription;
    }

    private void validateDocumentStatus(DocumentDescription documentDescription) {
        // Assume value already set, as the deserializer will enforce it.
        DocumentStatus documentStatus = (DocumentStatus)
                metadataService.findValidMetadata(
                        documentDescription.getDocumentStatus());
        documentDescription.setDocumentStatus(documentStatus);
    }

    private void validateDocumentType(DocumentDescription documentDescription) {
        // Assume value already set, as the deserializer will enforce it.
        DocumentType documentType = (DocumentType)
                metadataService.findValidMetadata(
                        documentDescription.getDocumentType());
        documentDescription.setDocumentType(documentType);
    }
}
