package app.service.noark5;

import app.domain.noark5.Class;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.ChangeLog;
import app.domain.noark5.casehandling.CaseFile;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.secondary.CrossReference;
import app.domain.noark5.secondary.Keyword;
import app.domain.noark5.secondary.Screening;
import app.domain.repository.noark5.v5.IClassRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.ICaseFileService;
import app.service.interfaces.IClassService;
import app.service.interfaces.IFileService;
import app.service.interfaces.IRecordService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.ICrossReferenceService;
import app.service.interfaces.secondary.IKeywordService;
import app.service.interfaces.secondary.IScreeningMetadataService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.exceptions.NoarkInvalidStructureException;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.builder.interfaces.IClassLinksBuilder;
import app.webapp.payload.builder.interfaces.secondary.IScreeningMetadataLinksBuilder;
import app.webapp.payload.links.ClassLinks;
import app.webapp.payload.links.ClassificationSystemLinks;
import app.webapp.payload.links.FileLinks;
import app.webapp.payload.links.RecordLinks;
import app.webapp.payload.links.casehandling.CaseFileLinks;
import app.webapp.payload.links.secondary.CrossReferenceLinks;
import app.webapp.payload.links.secondary.KeywordLinks;
import app.webapp.payload.links.secondary.KeywordTemplateLinks;
import app.webapp.payload.links.secondary.ScreeningMetadataLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static app.utils.NoarkUtils.NoarkEntity.Create.setFinaliseEntityValues;
import static app.utils.NoarkUtils.NoarkEntity.Create.validateScreening;
import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.Constants.INFO_INVALID_STRUCTURE;
import static app.utils.constants.N5ResourceMappings.*;
import static java.util.List.copyOf;

/**
 * Service class for Class
 * <p>
 * Provides basic CRUD functionality for Class using systemId.
 * <p>
 * Also supports CREATE for a (Noark Classification Class) Class.
 * <p>
 * All public methods return Hateoas objects
 */
@Service
public class ClassService
        extends NoarkService
        implements IClassService {

    private static final Logger logger =
            LoggerFactory.getLogger(ClassService.class);

    private final IClassRepository classRepository;
    private final IFileService fileService;
    private final ICaseFileService caseFileService;
    private final IRecordService recordService;
    private final ICrossReferenceService crossReferenceService;
    private final IKeywordService keywordService;
    private final IClassLinksBuilder classLinksBuilder;
    private final IMetadataService metadataService;
    private final IScreeningMetadataService screeningMetadataService;
    private final IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder;

    public ClassService(EntityManager entityManager,
                        ApplicationEventPublisher applicationEventPublisher,

                        IPatchService patchService,
                        IClassRepository classRepository,
                        IFileService fileService,
                        ICaseFileService caseFileService,
                        IRecordService recordService,
                        ICrossReferenceService crossReferenceService,
                        IKeywordService keywordService,
                        IClassLinksBuilder classLinksBuilder,
                        IMetadataService metadataService,
                        IScreeningMetadataService screeningMetadataService,
                        IScreeningMetadataLinksBuilder screeningMetadataLinksBuilder,
                        ILocalUserDetails userDetails,
                        IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.classRepository = classRepository;
        this.fileService = fileService;
        this.keywordService = keywordService;
        this.crossReferenceService = crossReferenceService;
        this.caseFileService = caseFileService;
        this.recordService = recordService;
        this.classLinksBuilder = classLinksBuilder;
        this.metadataService = metadataService;
        this.screeningMetadataService = screeningMetadataService;
        this.screeningMetadataLinksBuilder = screeningMetadataLinksBuilder;
    }

    // All CREATE operations

    /**
     * Persists a new class object to the database. Some values are set in the
     * incoming payload (e.g. title) while some are set by the core.
     * owner, createdBy, createdDate are automatically set by the core.
     *
     * @param klass The class object with some values set
     * @return the newly persisted class object wrapped as a classLinks object
     */
    @Override
    @Transactional
    public ClassLinks save(@NotNull final Class klass) {
        setFinaliseEntityValues(klass);
        validateScreening(metadataService, klass);
        return packAsLinks(classRepository.save(klass));
    }

    /**
     * Persists a new class object to the database as a sub-class to an
     * existing class object. Some values are set in the incoming  payload
     * (e.g. title) while some are set by the core.  owner, createdBy,
     * createdDate are automatically set by the core.
     *
     * @param systemId systemId of the parent object to connect this
     *                 class as a child to
     * @param newklass The class object with some values set
     * @return the newly persisted class object wrapped as a classLinks object
     */
    @Override
    @Transactional
    public ClassLinks createClassAssociatedWithClass(
            @NotNull final UUID systemId,
            @NotNull final Class newklass) {
        /* reject if associated File or Record already exist, as only
         * either Record, File or Class is allowed on the same level
         * according to Noark 5v5. */
        Class klass = getClassOrThrow(systemId);
        if (!klass.getReferenceFile().isEmpty()
                || !klass.getReferenceRecordEntity().isEmpty()) {
            String info = INFO_INVALID_STRUCTURE +
                    ". Either " + CLASS + ", " + RECORD + " or " + FILE + " below " + CLASS + " with systemId " + klass.getSystemIdAsString();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, CLASS, CLASS);
        }
        newklass.setReferenceParentClass(klass);
        return save(newklass);
    }

    /**
     * Persists a new File object that is associated with the identified
     * Class object to the database. Some values are set in the incoming
     * payload (e.g. title) while some are set by the core.  owner, createdBy,
     * createdDate are automatically set by the core.
     *
     * @param systemId systemId of the Class object to associate this
     *                 File object to
     * @param file     The File object with some values set
     * @return the newly persisted File object wrapped as a FileLinks object
     */
    @Override
    @Transactional
    public FileLinks createFileAssociatedWithClass(
            @NotNull final UUID systemId,
            @NotNull final File file) {
        /* reject if associated Class or Record already exist, as only
         * either Record, File or Class is allowed on the same level
         * according to Noark 5v5. */
        Class klass = getClassOrThrow(systemId);
        if (!klass.getReferenceChildClass().isEmpty()
                || !klass.getReferenceRecordEntity().isEmpty()) {
            String info = INFO_INVALID_STRUCTURE +
                    ". Either " + CLASS + ", " + RECORD + " or " + FILE + " below " + CLASS + " with systemId " + klass.getSystemIdAsString();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, CLASS, FILE);
        }
        file.setReferenceClass(klass);
        return fileService.save(file);
    }

    /**
     * Persists a new CaseFile object that is associated with the identified
     * Class object to the database. Some values are set in the incoming
     * payload (e.g. title) while some are set by the core.  owner, createdBy,
     * createdDate are automatically set by the core.
     *
     * @param systemId systemId of the Class object to associate this
     *                 CaseFile object to
     * @param caseFile The CaseFile object with some values set
     * @return the newly persisted CaseFile object wrapped as a CaseFileLinks
     * object
     */
    @Override
    @Transactional
    public CaseFileLinks createCaseFileAssociatedWithClass(
            @NotNull final UUID systemId,
            @NotNull final CaseFile caseFile) {
        /* reject if associated Class or Record already exist, as only
         * either Record, File or Class is allowed on the same level
         * according to Noark 5v5. */
        Class klass = getClassOrThrow(systemId);
        if (!klass.getReferenceChildClass().isEmpty()
                || !klass.getReferenceRecordEntity().isEmpty()) {
            String info = INFO_INVALID_STRUCTURE +
                    ". Either " + CLASS + ", " + RECORD + " or " + FILE + " below " + CLASS + " with systemId " + klass.getSystemIdAsString();
            logger.info(info);
            throw new NoarkInvalidStructureException(info, CLASS, CASE_FILE);
        }
        caseFile.setReferenceClass(klass);
        return caseFileService.save(caseFile);
    }

    @Override
    public ScreeningMetadataLinks createScreeningMetadataAssociatedWithClass(
            @NotNull final UUID systemId,
            @NotNull final Metadata screeningMetadata) {
        Class klass = getClassOrThrow(systemId);
        if (null == klass.getReferenceScreening()) {
            throw new NoarkEntityNotFoundException(INFO_CANNOT_FIND_OBJECT +
                    " Screening, associated with Class with systemId " +
                    systemId);
        }
        return screeningMetadataService.createScreeningMetadata(
                klass.getReferenceScreening(), screeningMetadata);
    }

    @Override
    public ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId) {
        return screeningMetadataService.getDefaultScreeningMetadata(systemId);
    }

    @Override
    public KeywordTemplateLinks generateDefaultKeyword(@NotNull final UUID systemId) {
        return keywordService.generateDefaultKeyword(systemId);
    }

    @Override
    public CrossReferenceLinks getDefaultCrossReference(
            @NotNull final UUID systemId) {
        return crossReferenceService.getDefaultCrossReference(systemId);
    }

    /**
     * Generate a Default Class object that can be associated with the
     * identified Class. Note this object has not been persisted to the core.
     * <br>
     * Note. Ideally this method would be configurable based on the logged in
     * user and the business area they are working with. A generic Noark core
     * like this does not have scope for that kind of functionality.
     *
     * @param systemId The systemId of the class object you wish to
     *                 generate a default class for
     * @return the Class object wrapped as a ClassLinks object
     */
    @Override
    public ClassLinks generateDefaultSubClass(@NotNull final UUID systemId) {
        Class klass = new Class();
        klass.setVersion(-1L, true);
        return packAsLinks(klass);
    }

    @Override
    public ClassLinks generateDefaultClass(@NotNull final UUID systemId) {
        Class klass = new Class();
        klass.setVersion(-1L, true);
        return packAsLinks(klass);
    }

    /**
     * Persists a new Record object that is associated with the identified
     * Class object to the database. Some values are set in the incoming
     * payload (e.g. title) while some are set by the core.  owner, createdBy,
     * createdDate are automatically set by the core.
     *
     * @param systemId systemId of the Class object to associate this
     *                 Record object to
     * @param record   The Record object with some values set
     * @return the newly persisted Record object wrapped as a RecordLinks
     * object
     */
    @Override
    @Transactional
    public RecordLinks createRecordAssociatedWithClass(
            @NotNull final UUID systemId,
            @NotNull final RecordEntity record) {
        record.setReferenceClass(getClassOrThrow(systemId));
        return recordService.save(record);
    }

    @Override
    public CrossReferenceLinks createCrossReferenceAssociatedWithClass(
            @NotNull final UUID systemId,
            @NotNull final CrossReference crossReference) {
        return crossReferenceService.createCrossReferenceAssociatedWithClass(
                crossReference, getClassOrThrow(systemId));
    }

    @Override
    public KeywordLinks createKeywordAssociatedWithClass(
            @NotNull final UUID systemId, @NotNull final Keyword keyword) {
        return keywordService.createKeywordAssociatedWithClass(
                keyword, getClassOrThrow(systemId));
    }

    // All READ operations

    /**
     * Retrieve all class objects the user owns.
     *
     * @return ClassLinks object containing a list of Class objects
     */
    @Override
    public ClassLinks findAll() {
        return (ClassLinks) processODataQueryGet();
    }

    @Override
    public KeywordLinks findKeywordAssociatedWithClass(
            @NotNull final UUID systemId) {
        return (KeywordLinks) processODataQueryGet();
    }

    @Override
    public CrossReferenceLinks findCrossReferenceAssociatedWithClass(
            @NotNull final UUID systemId) {
        return (CrossReferenceLinks) processODataQueryGet();
    }

    /**
     * Retrieve a single class object identified by systemId
     * <p>
     * Note: This method can never return a null value.
     *
     * @param systemId The systemId of the Class object to retrieve
     * @return A ClassLinks object containing the class
     */
    @Override
    public ClassLinks findSingleClass(@NotNull final UUID systemId) {
        return packAsLinks(getClassOrThrow(systemId));
    }

    @Override
    public ScreeningMetadataLinks
    getScreeningMetadataAssociatedWithClass(@NotNull final UUID systemId) {
        Screening screening = getClassOrThrow(systemId)
                .getReferenceScreening();
        if (null == screening) {
            throw new NoarkEntityNotFoundException(
                    INFO_CANNOT_FIND_OBJECT + " Screening, using systemId " +
                            systemId);
        }
        return packAsLinks(new SearchResultsPage(copyOf(
                screening.getReferenceScreeningMetadata())));
    }

    /**
     * Retrieve a list of children class belonging to the class object
     * identified by systemId
     *
     * @param systemId The systemId of the Class object to retrieve its
     *                 children
     * @return A ClassLinks object containing the children class's
     */
    @Override
    public ClassLinks findAllChildren(@NotNull final UUID systemId) {
        // Make sure class exists
        getClassOrThrow(systemId);
        return (ClassLinks) processODataQueryGet();
    }

    /**
     * Retrieve the parent Class object associated with the Class object
     * identified by systemId
     *
     * @param systemId The systemId of the Class object to retrieve its parent
     *                 Class
     * @return A ClassLinksList of Class objects
     */
    @Override
    public ClassLinks
    findClassAssociatedWithClass(@NotNull final UUID systemId) {
        // Make sure class exists
        getClassOrThrow(systemId);
        return (ClassLinks) processODataQueryGet();
    }


    /**
     * Retrieve the ClassificationSystemLinks object associated with the
     * Class object identified by systemId
     *
     * @param systemId The systemId of the Class object to retrieve the
     *                 associated ClassificationSystemLinks
     * @return A ClassificationSystemLinks
     */
    @Override
    public ClassificationSystemLinks
    findClassificationSystemAssociatedWithClass(@NotNull final UUID systemId) {
        // Make sure class exists
        getClassOrThrow(systemId);
        return (ClassificationSystemLinks) processODataQueryGet();
    }

    @Override
    public FileLinks findAllFileAssociatedWithClass(
            @NotNull final UUID systemId) {
        getClassOrThrow(systemId);
        return (FileLinks) processODataQueryGet();
    }

    @Override
    public RecordLinks findAllRecordAssociatedWithClass(
            @NotNull final UUID systemId) {
        // Make sure class exists
        getClassOrThrow(systemId);
        return (RecordLinks) processODataQueryGet();
    }

    // All UPDATE operations

    /**
     * Updates a Class object in the database. First we try to locate the
     * Class object. If the Class object does not exist a
     * NoarkEntityNotFoundException exception is thrown that the caller has
     * to deal with.
     * <br>
     * After this the values you are allowed to update are copied from the
     * incomingClass object to the existingClass object and the existingClass
     * object will be persisted to the database when the transaction boundary
     * is over.
     * <p>
     * Note, the version corresponds to the version number, when the object
     * was initially retrieved from the database. If this number is not the
     * same as the version number when re-retrieving the Class object from
     * the database a NoarkConcurrencyException is thrown. Note. This happens
     * when the call to Class.setVersion() occurs.
     * *
     *
     * @param systemId      systemId of the incoming class object
     * @param incomingClass the contents to update
     * @return the updated Class object as a ClassLinks object
     */
    @Override
    @Transactional
    public ClassLinks handleUpdate(
            @NotNull final UUID systemId, @NotNull final Class incomingClass) {
        Class existingClass = getClassOrThrow(systemId);
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = prepareChangeLogs(existingClass, incomingClass);
        // Copy all the values you are allowed to copy ....
        updateTitleAndDescription(incomingClass, existingClass);
        publishChangeLogEvents(changeLogs);
        return packAsLinks(existingClass);
    }

    /**
     * Checking values specified in
     * <a href="https://github.com/arkivverket/noark5-standard/blob/master/kapitler/130-vedlegg_3_logg_over_endringer.rst">endringlogg</a>
     *
     * @param existingClass the existing Class from the database
     * @param newClass      the incoming Class
     */
    public List<Map.Entry<ChangeLog, SystemIdEntity>> prepareChangeLogs(Class existingClass, Class newClass) {
        List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogs = new ArrayList<>();
        // klasse	M020	tittel	Ved endring
        if (!existingClass.getTitle().equals(newClass.getTitle())) {
            changeLogs.add(createChangeLogObject(TITLE, existingClass.getTitle(),
                    newClass.getTitle(), existingClass));
        }
        return changeLogs;
    }

    // All DELETE operations

    /**
     * delete a Class entity with given systemId
     * <p>
     * Note: When deleting, observe the following behaviour if there are
     * multiple parents. If a Class object has both a Class and
     * ClassificationSystem object as parent, return the object at the
     * closest level. In this case it is Class.
     * <p>
     * Note: This method can return null! But it is unlikely that it will
     *
     * @param systemId systemId of the Class object to delete
     */
    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        Class klass = getClassOrThrow(systemId);
        deleteEntity(klass);
    }

    /**
     * Delete all objects belonging to the organisation identified by organisation
     */
    @Override
    @Transactional
    public void deleteAllByOrganisation() {
        classRepository.deleteByOrganisation(getOrganisation());
    }

    // All HELPER operations

    public ScreeningMetadataLinks packAsLinks(SearchResultsPage page) {
        ScreeningMetadataLinks screeningMetadataLinks =
                new ScreeningMetadataLinks(page);
        applyLinksAndHeader(screeningMetadataLinks,
                screeningMetadataLinksBuilder);
        return screeningMetadataLinks;
    }

    public ClassLinks packAsLinks(@NotNull final Class klass) {
        ClassLinks classLinks = new ClassLinks(klass);
        applyLinksAndHeader(classLinks, classLinksBuilder);
        return classLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once.  If you call this, be aware
     * that you will only ever get a valid Class back. If there is no valid
     * Class, an exception is thrown
     *
     * @param systemId systemId of the class object you are looking for
     * @return the newly found class object or null if it does not exist
     */
    protected Class getClassOrThrow(@NotNull final UUID systemId) {
        Optional<Class> klass = classRepository.findBySystemId(systemId);
        if (klass.isEmpty()) {
            String error = INFO_CANNOT_FIND_OBJECT + " Class, using systemId " +
                    systemId;
            throw new NoarkEntityNotFoundException(error);
        }
        return klass.get();
    }
}
