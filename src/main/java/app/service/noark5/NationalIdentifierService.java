package app.service.noark5;

import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.metadata.CoordinateSystem;
import app.domain.noark5.nationalidentifier.*;
import app.domain.repository.noark5.v5.INationalIdentifierRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.INationalIdentifierService;
import app.service.interfaces.metadata.IMetadataService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.nationalidentifier.*;
import app.webapp.payload.links.nationalidentifier.*;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.N5ResourceMappings.COORDINATE_SYSTEM;

@Service
public class NationalIdentifierService
        extends NoarkService
        implements INationalIdentifierService {

    private final INationalIdentifierRepository nationalIdentifierRepository;
    private final IBuildingLinksBuilder buildingLinksBuilder;
    private final ICadastralUnitLinksBuilder cadastralUnitLinksBuilder;
    private final IDNumberLinksBuilder dNumberLinksBuilder;
    private final IPlanLinksBuilder planLinksBuilder;
    private final IPositionLinksBuilder positionLinksBuilder;
    private final ISocialSecurityNumberLinksBuilder
            socialSecurityNumberLinksBuilder;
    private final IUnitLinksBuilder unitLinksBuilder;
    private final IMetadataService metadataService;

    public NationalIdentifierService
            (EntityManager entityManager,
             ApplicationEventPublisher applicationEventPublisher,
             IPatchService patchService,
             INationalIdentifierRepository nationalIdentifierRepository,
             IBuildingLinksBuilder buildingLinksBuilder,
             ICadastralUnitLinksBuilder cadastralUnitLinksBuilder,
             IDNumberLinksBuilder dNumberLinksBuilder,
             IPlanLinksBuilder planLinksBuilder,
             IPositionLinksBuilder positionLinksBuilder,
             ISocialSecurityNumberLinksBuilder
                     socialSecurityNumberLinksBuilder,
             IUnitLinksBuilder unitLinksBuilder,
             IMetadataService metadataService,
             ILocalUserDetails userDetails,
             IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.nationalIdentifierRepository = nationalIdentifierRepository;
        this.buildingLinksBuilder = buildingLinksBuilder;
        this.cadastralUnitLinksBuilder = cadastralUnitLinksBuilder;
        this.dNumberLinksBuilder = dNumberLinksBuilder;
        this.planLinksBuilder = planLinksBuilder;
        this.positionLinksBuilder = positionLinksBuilder;
        this.socialSecurityNumberLinksBuilder = socialSecurityNumberLinksBuilder;
        this.unitLinksBuilder = unitLinksBuilder;
        this.metadataService = metadataService;
    }

    // All CREATE methods

    @Override
    @Transactional
    public BuildingLinks createNewBuilding(
            @NotNull final Building building, @NotNull final RecordEntity record) {
        record.addNationalIdentifier(building);
        building.setReferenceRecord(record);
        return packAsLinks(nationalIdentifierRepository.save(building));
    }

    @Override
    @Transactional
    public BuildingLinks createNewBuilding(
            @NotNull final Building building, @NotNull final File file) {
        file.addNationalIdentifier(building);
        building.setReferenceFile(file);
        return packAsLinks(nationalIdentifierRepository.save(building));
    }

    @Override
    @Transactional
    public CadastralUnitLinks createNewCadastralUnit
            (@NotNull final CadastralUnit cadastralUnit,
             @NotNull final RecordEntity record) {
        record.addNationalIdentifier(cadastralUnit);
        cadastralUnit.setReferenceRecord(record);
        return packAsLinks(nationalIdentifierRepository.save(cadastralUnit));
    }

    @Override
    @Transactional
    public CadastralUnitLinks createNewCadastralUnit(
            @NotNull final CadastralUnit cadastralUnit,
            @NotNull final File file) {
        file.addNationalIdentifier(cadastralUnit);
        cadastralUnit.setReferenceFile(file);
        return packAsLinks(nationalIdentifierRepository.save(cadastralUnit));
    }

    @Override
    @Transactional
    public DNumberLinks createNewDNumber(
            @NotNull final DNumber dNumber, @NotNull final RecordEntity record) {
        record.addNationalIdentifier(dNumber);
        dNumber.setReferenceRecord(record);
        return packAsLinks(nationalIdentifierRepository.save(dNumber));
    }

    @Override
    @Transactional
    public DNumberLinks createNewDNumber(
            @NotNull final DNumber dNumber, @NotNull final File file) {
        file.addNationalIdentifier(dNumber);
        dNumber.setReferenceFile(file);
        return packAsLinks(nationalIdentifierRepository.save(dNumber));
    }

    @Override
    @Transactional
    public PlanLinks createNewPlan(
            @NotNull final Plan plan, @NotNull final RecordEntity record) {
        record.addNationalIdentifier(plan);
        plan.setReferenceRecord(record);
        return packAsLinks(nationalIdentifierRepository.save(plan));
    }

    @Override
    @Transactional
    public PlanLinks createNewPlan(
            @NotNull final Plan plan, @NotNull final File file) {
        file.addNationalIdentifier(plan);
        plan.setReferenceFile(file);
        return packAsLinks(nationalIdentifierRepository.save(plan));
    }

    @Override
    @Transactional
    public PositionLinks createNewPosition(
            @NotNull Position position, @NotNull RecordEntity record) {
        record.addNationalIdentifier(position);
        return packAsLinks(nationalIdentifierRepository.save(position));
    }

    @Override
    @Transactional
    public PositionLinks createNewPosition(
            @NotNull Position position, @NotNull File file) {
        file.addNationalIdentifier(position);
        return packAsLinks(nationalIdentifierRepository.save(position));
    }

    @Override
    @Transactional
    public SocialSecurityNumberLinks createNewSocialSecurityNumber(
            @NotNull final SocialSecurityNumber socialSecurityNumber,
            @NotNull final RecordEntity record) {
        record.addNationalIdentifier(socialSecurityNumber);
        socialSecurityNumber.setReferenceRecord(record);
        return packAsLinks(
                nationalIdentifierRepository.save(socialSecurityNumber));
    }

    @Override
    @Transactional
    public SocialSecurityNumberLinks createNewSocialSecurityNumber(
            @NotNull final SocialSecurityNumber socialSecurityNumber,
            @NotNull final File file) {
        file.addNationalIdentifier(socialSecurityNumber);
        socialSecurityNumber.setReferenceFile(file);
        return packAsLinks(nationalIdentifierRepository
                .save(socialSecurityNumber));
    }

    @Override
    @Transactional
    public UnitLinks createNewUnit(
            @NotNull final Unit unit, @NotNull final RecordEntity record) {
        record.addNationalIdentifier(unit);
        return packAsLinks(nationalIdentifierRepository.save(unit));
    }

    @Override
    @Transactional
    public UnitLinks createNewUnit(
            @NotNull final Unit unit, @NotNull final File file) {
        file.addNationalIdentifier(unit);
        return packAsLinks(nationalIdentifierRepository.save(unit));
    }

    // All READ methods

    @Override
    public BuildingLinks findBuildingBySystemId(
            @NotNull final UUID systemId) {
        return packAsLinks((Building) getNationalIdentifierOrThrow(systemId));
    }

    @Override
    public UnitLinks findUnitBySystemId(
            @NotNull final UUID systemId) {
        return packAsLinks((Unit) getNationalIdentifierOrThrow(systemId));
    }

    @Override
    public CadastralUnitLinks findCadastralUnitBySystemId(
            @NotNull final UUID systemId) {
        return packAsLinks((CadastralUnit) getNationalIdentifierOrThrow(systemId));
    }

    @Override
    public PositionLinks findPositionBySystemId(
            @NotNull final UUID systemId) {
        return packAsLinks((Position) getNationalIdentifierOrThrow(systemId));
    }

    @Override
    public PlanLinks findPlanBySystemId(
            @NotNull final UUID systemId) {
        return packAsLinks((Plan) getNationalIdentifierOrThrow(systemId));
    }

    @Override
    public SocialSecurityNumberLinks findSocialSecurityNumberBySystemId(
            @NotNull final UUID systemId) {
        return packAsLinks((SocialSecurityNumber) getNationalIdentifierOrThrow(systemId));
    }

    @Override
    public DNumberLinks findDNumberBySystemId(
            @NotNull final UUID systemId) {
        return packAsLinks((DNumber) getNationalIdentifierOrThrow(systemId));
    }

    // All UPDATE methods

    @Override
    @Transactional
    public BuildingLinks updateBuilding(
            @NotNull final UUID systemId, @NotNull final Building incomingBuilding) {
        Building existingBuilding =
                (Building) getNationalIdentifierOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        // First the values
        existingBuilding.setBuildingNumber
                (incomingBuilding.getBuildingNumber());
        existingBuilding.setRunningChangeNumber
                (incomingBuilding.getRunningChangeNumber());

        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingBuilding.setVersion(getETag());
        return packAsLinks(existingBuilding);
    }

    @Override
    @Transactional
    public CadastralUnitLinks updateCadastralUnit(
            @NotNull final UUID systemId, @NotNull final CadastralUnit incomingCadastralUnit) {
        CadastralUnit existingCadastralUnit =
                (CadastralUnit) getNationalIdentifierOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        // First the values
        existingCadastralUnit.setMunicipalityNumber
                (incomingCadastralUnit.getMunicipalityNumber());
        existingCadastralUnit.setHoldingNumber
                (incomingCadastralUnit.getHoldingNumber());
        existingCadastralUnit.setSubHoldingNumber
                (incomingCadastralUnit.getSubHoldingNumber());
        existingCadastralUnit.setLeaseNumber
                (incomingCadastralUnit.getLeaseNumber());
        existingCadastralUnit.setSectionNumber
                (incomingCadastralUnit.getSectionNumber());

        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingCadastralUnit.setVersion(getETag());
        return packAsLinks(existingCadastralUnit);
    }

    @Override
    @Transactional
    public DNumberLinks updateDNumber(
            @NotNull final UUID systemId, @NotNull final DNumber incomingDNumber) {
        DNumber existingDNumber =
                (DNumber) getNationalIdentifierOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        // First the values
        existingDNumber.setdNumber
                (incomingDNumber.getdNumber());

        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingDNumber.setVersion(getETag());
        return packAsLinks(existingDNumber);
    }

    @Override
    @Transactional
    public PlanLinks updatePlan(
            @NotNull final UUID systemId, @NotNull final Plan incomingPlan) {
        Plan existingPlan =
                (Plan) getNationalIdentifierOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        // First the values
        existingPlan.setMunicipalityNumber
                (incomingPlan.getMunicipalityNumber());
        existingPlan.setCountyNumber
                (incomingPlan.getCountyNumber());
        existingPlan.setCountry
                (incomingPlan.getCountry());
        existingPlan.setPlanIdentification
                (incomingPlan.getPlanIdentification());

        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingPlan.setVersion(getETag());
        return packAsLinks(existingPlan);
    }

    @Override
    @Transactional
    public PositionLinks updatePosition(
            @NotNull final UUID systemId, @NotNull final Position incomingPosition) {
        Position existingPosition =
                (Position) getNationalIdentifierOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        // First the values
        existingPosition.setX(incomingPosition.getX());
        existingPosition.setY(incomingPosition.getY());
        existingPosition.setZ(incomingPosition.getZ());
        existingPosition.setCoordinateSystem
                (incomingPosition.getCoordinateSystem());

        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingPosition.setVersion(getETag());
        return packAsLinks(existingPosition);
    }

    @Override
    @Transactional
    public SocialSecurityNumberLinks updateSocialSecurityNumber(
            @NotNull final UUID systemId, @NotNull final SocialSecurityNumber incomingSocialSecurityNumber) {
        SocialSecurityNumber existingSocialSecurityNumber =
                (SocialSecurityNumber) getNationalIdentifierOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        // First the values
        existingSocialSecurityNumber.setSocialSecurityNumber
                (incomingSocialSecurityNumber.getSocialSecurityNumber());

        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingSocialSecurityNumber.setVersion(getETag());
        return packAsLinks(existingSocialSecurityNumber);
    }

    @Override
    @Transactional
    public UnitLinks updateUnit(
            @NotNull final UUID systemId, @NotNull final Unit incomingUnit) {
        Unit existingUnit =
                (Unit) getNationalIdentifierOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        // First the values
        existingUnit
                .setUnitIdentifier(incomingUnit.getUnitIdentifier());

        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingUnit.setVersion(getETag());
        return packAsLinks(existingUnit);
    }

    // All DELETE methods

    @Override
    @Transactional
    public void deleteBuilding(@NotNull final UUID systemId) {
        deleteEntity(getNationalIdentifierOrThrow(systemId));
    }

    @Override
    @Transactional
    public void deleteCadastralUnit(@NotNull final UUID systemId) {
        deleteEntity(getNationalIdentifierOrThrow(systemId));
    }

    @Override
    @Transactional
    public void deleteDNumber(@NotNull final UUID systemId) {
        deleteEntity(getNationalIdentifierOrThrow(systemId));
    }

    @Override
    @Transactional
    public void deletePlan(@NotNull final UUID systemId) {
        deleteEntity(getNationalIdentifierOrThrow(systemId));
    }

    @Override
    @Transactional
    public void deletePosition(@NotNull final UUID systemId) {
        deleteEntity(getNationalIdentifierOrThrow(systemId));
    }

    @Override
    @Transactional
    public void deleteSocialSecurityNumber(@NotNull final UUID systemId) {
        deleteEntity(getNationalIdentifierOrThrow(systemId));
    }

    @Override
    @Transactional
    public void deleteUnit(@NotNull final UUID systemId) {
        deleteEntity(getNationalIdentifierOrThrow(systemId));
    }

    // All template methods

    /**
     * Generate a Default Building object.
     *
     * @return the Building object wrapped as a BuildingLinks object
     */
    @Override
    public BuildingLinks generateDefaultBuilding(
            @NotNull final UUID systemId) {
        Building building = new Building();
        building.setVersion(-1L, true);
        return packAsLinks(building);
    }

    /**
     * Generate a Default CadastralUnit object.
     *
     * @return the CadastralUnit object wrapped as a CadastralUnitLinks object
     */
    @Override
    public CadastralUnitLinks generateDefaultCadastralUnit(
            @NotNull final UUID systemId) {
        CadastralUnit cadastralUnit = new CadastralUnit();
        cadastralUnit.setVersion(-1L, true);
        return packAsLinks(cadastralUnit);
    }

    /**
     * Generate a Default DNumber object.
     *
     * @return the DNumber object wrapped as a DNumberLinks object
     */
    @Override
    public DNumberLinks generateDefaultDNumber(@NotNull final UUID systemId) {
        DNumber dNumber = new DNumber();
        dNumber.setVersion(-1L, true);
        return packAsLinks(dNumber);
    }

    /**
     * Generate a Default Plan object.
     *
     * @return the Plan object wrapped as a PlanLinks object
     */
    @Override
    public PlanLinks generateDefaultPlan(@NotNull final UUID systemId) {
        Plan plan = new Plan();
        plan.setVersion(-1L, true);
        return packAsLinks(plan);
    }

    /**
     * Generate a Default Position object.
     *
     * @return the Position object wrapped as a PositionLinks object
     */
    @Override
    public PositionLinks generateDefaultPosition(
            @NotNull final UUID systemId) {
        Position position = new Position();
        CoordinateSystem coordinateSystem = (CoordinateSystem)
                metadataService.findValidMetadataByEntityTypeOrThrow
                        (COORDINATE_SYSTEM, "EPSG:4326", "WGS84");
        position.setCoordinateSystem(coordinateSystem);
        position.setVersion(-1L, true);
        return packAsLinks(position);
    }

    /**
     * Generate a Default SocialSecurityNumber object.
     *
     * @return the SocialSecurityNumber object wrapped as a SocialSecurityNumberLinks object
     */
    @Override
    public SocialSecurityNumberLinks generateDefaultSocialSecurityNumber(
            @NotNull final UUID systemId) {
        SocialSecurityNumber socialSecurityNumber = new SocialSecurityNumber();
        socialSecurityNumber.setVersion(-1L, true);
        return packAsLinks(socialSecurityNumber);
    }

    /**
     * Generate a Default Unit object.
     *
     * @return the Unit object wrapped as a UnitLinks object
     */
    @Override
    public UnitLinks generateDefaultUnit(@NotNull final UUID systemId) {
        Unit unit = new Unit();
        unit.setVersion(-1L, true);
        return packAsLinks(unit);
    }

    // All helper methods

    public SocialSecurityNumberLinks packAsLinks(
            @NotNull final SocialSecurityNumber socialSecurityNumber) {
        SocialSecurityNumberLinks socialSecurityNumberLinks =
                new SocialSecurityNumberLinks(socialSecurityNumber);
        applyLinksAndHeader(socialSecurityNumberLinks,
                socialSecurityNumberLinksBuilder);
        return socialSecurityNumberLinks;
    }

    public UnitLinks packAsLinks(@NotNull final Unit unit) {
        UnitLinks unitLinks = new UnitLinks(unit);
        applyLinksAndHeader(unitLinks, unitLinksBuilder);
        return unitLinks;
    }

    public PositionLinks packAsLinks(@NotNull final Position position) {
        PositionLinks positionLinks = new PositionLinks(position);
        applyLinksAndHeader(positionLinks, positionLinksBuilder);
        return positionLinks;
    }

    public PlanLinks packAsLinks(@NotNull final Plan plan) {
        PlanLinks planLinks = new PlanLinks(plan);
        applyLinksAndHeader(planLinks, planLinksBuilder);
        return planLinks;
    }

    public DNumberLinks packAsLinks(@NotNull final DNumber dNumber) {
        DNumberLinks dNumberLinks = new DNumberLinks(dNumber);
        applyLinksAndHeader(dNumberLinks, dNumberLinksBuilder);
        return dNumberLinks;
    }

    public CadastralUnitLinks packAsLinks(
            @NotNull final CadastralUnit cadastralUnit) {
        CadastralUnitLinks cadastralUnitLinks =
                new CadastralUnitLinks(cadastralUnit);
        applyLinksAndHeader(cadastralUnitLinks, cadastralUnitLinksBuilder);
        return cadastralUnitLinks;
    }

    public BuildingLinks packAsLinks(@NotNull final Building building) {
        BuildingLinks buildingLinks = new BuildingLinks(building);
        applyLinksAndHeader(buildingLinks, buildingLinksBuilder);
        return buildingLinks;
    }


    /**
     * Internal helper method. Rather than having a find and try catch
     * in multiple methods, we have it here once. If you call this, be
     * aware that you will only ever get a valid NationalIdentifier
     * back. If there is no valid NationalIdentifier, an exception is
     * thrown.
     *
     * @param systemId systemId of identifier to retrieve
     * @return the retrieved NationalIdentifier
     */
    private NationalIdentifier getNationalIdentifierOrThrow(
            @NotNull final UUID systemId) {
        NationalIdentifier id = nationalIdentifierRepository
                .findBySystemId(systemId);
        if (id == null) {
            String error = INFO_CANNOT_FIND_OBJECT + " NationalIdentifier, " +
                    "using systemId " + systemId;
            throw new NoarkEntityNotFoundException(error);
        }
        return id;
    }
}
