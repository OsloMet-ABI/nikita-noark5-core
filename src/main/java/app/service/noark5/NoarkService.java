package app.service.noark5;

import app.domain.annotation.Updatable;
import app.domain.interfaces.entities.ISystemId;
import app.domain.interfaces.entities.ITitleDescription;
import app.domain.noark5.NoarkEntity;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.*;
import app.domain.noark5.bsm.BSMBase;
import app.domain.noark5.md_other.BSMMetadata;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.INoarkService;
import app.service.noark5.odata.ODataService;
import app.webapp.events.AfterNoarkEntityCreatedEvent;
import app.webapp.events.AfterNoarkEntityDeletedEvent;
import app.webapp.events.AfterNoarkEntityReadEvent;
import app.webapp.events.AfterNoarkEntityUpdatedEvent;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NikitaMisconfigurationException;
import app.webapp.exceptions.NoarkConcurrencyException;
import app.webapp.exceptions.PatchMisconfigurationException;
import app.webapp.model.PatchMerge;
import app.webapp.model.PatchObject;
import app.webapp.model.PatchObjects;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.deserializers.noark5.interfaces.IParseDateTime;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.Version;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UrlPathHelper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static app.utils.CommonUtils.WebUtils.getEnglishNameObjectOrThrow;
import static app.utils.CommonUtils.WebUtils.getMethodsForRequestAsListOrThrow;
import static app.utils.constants.Constants.*;
import static app.utils.constants.DatabaseConstants.ID;
import static app.utils.constants.N5ResourceMappings.BSM_DEF;
import static app.utils.constants.N5ResourceMappings.PASSWORD_ENG;
import static java.time.OffsetDateTime.now;
import static org.springframework.http.HttpHeaders.ALLOW;
import static org.springframework.http.HttpHeaders.ETAG;

public class NoarkService
        extends ODataService
        implements INoarkService, IParseDateTime {

    private static final Logger logger =
            LoggerFactory.getLogger(NoarkService.class);

    protected final ApplicationEventPublisher applicationEventPublisher;
    protected final IPatchService patchService;

    public NoarkService(EntityManager entityManager,
                        ApplicationEventPublisher applicationEventPublisher,
                        IPatchService patchService,
                        ILocalUserDetails userDetails,
                        IUrlDetails urlDetails) {
        super(entityManager, userDetails, urlDetails);
        this.applicationEventPublisher = applicationEventPublisher;
        this.patchService = patchService;
    }

    protected void updateTitleAndDescription(
            @NotNull ITitleDescription incomingEntity,
            @NotNull ITitleDescription existingEntity) {
        if (null != incomingEntity.getTitle()) {
            existingEntity.setTitle(incomingEntity.getTitle());
        }
        existingEntity.setDescription(incomingEntity.getDescription());
    }

    protected UUID getFirstSystemIDFromRequest() {
        HttpServletRequest request = ((ServletRequestAttributes)
                Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))
                .getRequest();
        String path = new UrlPathHelper().getPathWithinApplication(request);
        Pattern pattern = Pattern.compile(
                "([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f" +
                        "]{12})");
        Matcher matcher = pattern.matcher(path);
        if (matcher.find()) {
            return UUID.fromString(matcher.group(0));
        }
        return null;
    }


    /**
     * Set the outgoing OPTIONS header as well as the ETAG header
     * <p>
     * Note: when implementing support for organisation, multiple approaches
     * were attempted. Generally they failed due to lazy initialisation
     * errors or code that I'd expect to run didn't run (interceptors). The
     * easiest way to associate an entity with its organisation was to do it
     * here. Ideally we should solve this with an interceptor that sets the
     * relationship before saving the object.
     *
     * @param noarkObject of type ILinksNoarkObject
     */
    protected void setOutgoingRequestHeader(ILinksNoarkObject noarkObject) {
        HttpServletResponse response = ((ServletRequestAttributes)
                Objects.requireNonNull(
                        RequestContextHolder.getRequestAttributes()))
                .getResponse();
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder
                        .getRequestAttributes()).getRequest();
        Objects.requireNonNull(response);

        // Associate the object with its organisation
        if (noarkObject.isSingleEntity()) {
            // if the object is part of a post request then associate the
            // object with its organisation
            NoarkEntity entity = (NoarkEntity) noarkObject.getList().get(0);
            if (null != entity && getMethod().equalsIgnoreCase("post")) {
                //associateEntityWithItsOrganisation(entity);
                //associateSecondaryEntitiesWithOrganisation(entity);
            }
        }
        // Default/template objects will not have an ETAG an will have a
        // value of -1
        if (-1L != noarkObject.getEntityVersion()) {
            response.addHeader(ETAG,
                    "\"" + noarkObject.getEntityVersion().toString() + "\"");
        }
        response.addHeader(ALLOW, getMethodsForRequestAsListOrThrow(
                request.getServletPath()));
    }

    /**
     * See issue for a description of why this code was written this way
     * https://gitlab.com/OsloMet-ABI/nikita-noark5-core/issues/82
     *
     * @param entity       The entity to disassociate foreign keys with
     * @param deleteString the 'delete' SQL STRING
     */
    protected void disassociateForeignKeys(
            SystemIdEntity entity, String deleteString) {
        Query query = entityManager.createNativeQuery(deleteString);
        query.setParameter(ID, entity.getSystemIdAsString());
        query.executeUpdate();
    }

    protected void deleteEntity(NoarkEntity entity) {
        handleDeletionForEventLog((SystemIdEntity) entity);
        entityManager.remove(entity);
        entityManager.flush();
        entityManager.clear();
    }

    /**
     * An implementation of part of the PATCH (RFC 6902) standard
     * <p>
     * [
     * { "op": "replace", "path": "/requirementText", "value": "hello"},
     * ]
     * <p>
     * Swallows and converts the following exceptions to a
     * PatchMisconfigurationException:
     * <p>
     * - SecurityException if denied
     * - NoSuchMethodException method does not exist
     * - IllegalArgumentException problem with argument
     * - IllegalAccessException problem with access
     * - InvocationTargetException if it can't handle the syntax for some reason
     *
     * @param object       The object to update
     * @param patchObjects Multiple pathObject commands in one object
     *                     (contains an array)
     * @return The object after it was persisted
     * @throws PatchMisconfigurationException |
     */
    protected Object handlePatch(Object object, PatchObjects patchObjects) {
        for (PatchObject patchObject : patchObjects.getPatchObjects()) {
            if ("replace".equalsIgnoreCase(patchObject.getOp())
                    // Make sure that path contains a column to change
                    && null != patchObject.getPath()
                    // and that there is at least one letter after the slash
                    && patchObject.getPath().length() > 1) {
                handlePatchReplace(object, patchObject);
            } else if ("add".equalsIgnoreCase(patchObject.getOp())) {
                return handlePatchAdd((UUID) object, patchObject);
            } else if ("move".equalsIgnoreCase(patchObject.getOp())) {
                return handlePatchMove((UUID) object, patchObject);
            } else {
                String error = "Cannot handle this PatchObject : " +
                        patchObject;
                logger.error(error);
                throw new PatchMisconfigurationException(error);
            }
        }
        return null;
    }

    /**
     * registrering/systemId (what)
     * {
     * "op": "move",
     * "from": "mappe/systemId", (fromObject)
     * "path": "mappe/systemId" (toObject)
     * }
     *
     * @param originalObjectId systemId of object to patch
     * @param patchObject      the patch object that has the change to apply
     */
    protected Object handlePatchMove(@NotNull final UUID originalObjectId,
                                     PatchObject patchObject) {
        return patchService.handlePatch(originalObjectId, patchObject);
    }

    /**
     * {
     * "op": "add",
     * "path": ".../mappe/systemId",
     * "value": {
     * "virksomhetsspesifikkeMetadata": {
     * "ppt-v1:sakferdig": true,
     * "ppt-v1:datohenvist": "2020-06-30+02:00",
     * "ppt-v1:datotidvedtakferdig": "2020-06-30T15:35:43.128158+02:00",
     * "ppt-v1:skolekontakt": "Test BSM String",
     * "ppt-v1:refSkole": "https://skole.eksempel.com",
     * "ppt-v1:snittKarakter": 1.2,
     * "ppt-v1:antallDagerVurdert": 1
     * }
     * }
     * }
     *
     * @param uuid        Identity of object to add BSM
     * @param patchObject A single instance of a patchObject to apply
     */
    protected Object handlePatchAdd(@NotNull final UUID uuid, PatchObject patchObject) {
        List<BSMBase> bsm = getBSM(patchObject.getValue());
        checkBSM(bsm);
        return associateBSM(uuid, bsm);
    }

    protected void handlePatchReplace(Object object, PatchObject patchObject) {
        String path = patchObject.getPath();
        if ("/".equals(path.substring(0, 1))) {
            path = path.substring(1);
        }
        updateObject(object, patchObject.getValue(), path);
    }

    protected void handlePatchMerge(Object object, PatchMerge patchMerge) {

        for (Map.Entry<String, Object> entry : patchMerge.getMap().entrySet()) {
            updateObject(object, entry.getValue(), entry.getKey());
        }
    }

    protected void updateObject(Object object, Object value,
                                String incomingColumnName) {
        String column = getEnglishNameObjectOrThrow(incomingColumnName);
        String baseMethodName = column.substring(0, 1)
                .toUpperCase() + column.substring(1);
        String setMethodName = "set" + baseMethodName;
        String getMethodName = "get" + baseMethodName;
        try {
            Method getMethod =
                    object.getClass().getMethod(getMethodName);
            Class<T> retType = (Class<T>) getMethod.getReturnType();
            Method setMethod =
                    object.getClass().getMethod(setMethodName, retType);
            Method versionMethod =
                    object.getClass().getMethod("setVersion",
                            Long.class);
            // If the variable (path) you are trying to update is a
            // password then you have to encode the new password
            if (PASSWORD_ENG.equalsIgnoreCase(column)) {
                // Not possible to set password to null
                if (null != value) {
                    // Need to call method implemented in subclass userservice
                    //setMethod.invoke(object,
                    //        encoder.encode(value.toString()));
                }
            } else {
                versionMethod.invoke(object, getETag());
                if (null != value) {
                    setMethod.invoke(object, value);
                } else {
                    // Invoke the method with a null value
                    setMethod.invoke(object, new Object[]{null});
                }
            }
        } catch (SecurityException | NoSuchMethodException |
                 IllegalArgumentException | IllegalAccessException |
                 InvocationTargetException e) {
            // Avoid concurrency exception from being swallowed as an
            // InvocationTargetException
            if (e.getCause() instanceof NoarkConcurrencyException) {
                throw (NoarkConcurrencyException) e.getCause();
            }
            String error = "Cannot find internal method from Patch : " +
                    object.getClass().getName() + " " + incomingColumnName +
                    " : " + e.getMessage();
            logger.error(error);
            throw new PatchMisconfigurationException(error);
        }
    }

    private List<BSMBase> getBSM(Object object) {
        List<BSMBase> bsmList = new ArrayList<>();
        if (object instanceof LinkedHashMap) {
            LinkedHashMap values = (LinkedHashMap) ((LinkedHashMap) object)
                    .get(BSM_DEF);
            if (null != values) {
                for (Map.Entry<String, Object> entry :
                        (Iterable<Map.Entry<String, Object>>) values.entrySet()) {
                    if (entry.getValue() instanceof Boolean) {
                        bsmList.add(new BSMBase(entry.getKey(),
                                (Boolean) entry.getValue()));
                    } else if (entry.getValue() instanceof Double) {
                        bsmList.add(new BSMBase(entry.getKey(),
                                (Double) entry.getValue()));
                    } else if (entry.getValue() instanceof Integer) {
                        bsmList.add(new BSMBase(entry.getKey(),
                                (Integer) entry.getValue()));
                    } else if (entry.getValue() instanceof String value) {
                        if (value.startsWith("http://") ||
                                value.startsWith("https://")) {
                            BSMBase bsmBase = new BSMBase(entry.getKey());
                            bsmBase.setUriValue(value);
                            bsmList.add(bsmBase);
                        } else if (DATE_TIME_PATTERN
                                .matcher(value).matches()) {
                            bsmList.add(new BSMBase(
                                    entry.getKey(),
                                    deserializeDateTime(value)));
                        } else if (DATE_PATTERN.matcher(value).matches()) {
                            bsmList.add(new BSMBase(entry.getKey(),
                                    deserializeDate(value), true));
                        } else {
                            bsmList.add(new BSMBase(entry.getKey(), value));
                        }
                    }
                }
            }
        }
        return bsmList;
    }

    protected Object associateBSM(@NotNull final UUID systemId, List<BSMBase> bsm) {
        String error = "Cannot find internal nikita processor of BSM for " +
                "object with systemId " + systemId.toString();
        logger.error(error);
        throw new PatchMisconfigurationException(error);
    }

    /**
     * Update a systemId object using a RFC 7396 approach
     *
     * @param systemId   The systemId of the object to update
     * @param patchMerge Values to change
     * @return The updated BSMMetadata object wrapped as a BSMMetadataLinks
     */
    // Note the sub-class has to implement this as the sub-class is aware of
    // its type
    public Object handleUpdateRfc7396(@NotNull final UUID systemId,
                                      @NotNull PatchMerge patchMerge) {
        return null;
    }

    /**
     * @param object The object you want to update
     * @param column The column you want to update
     * @return The field if it is updatable, throw exception otherwise
     */
    private Field getUpdatableFieldOrThrow(Object object, String column) {
        try {
            Field field = object.getClass().getDeclaredField(column);
            if (null != field.getAnnotation(Updatable.class)) {
                return field;
            }
            String error = column + " is not an updatable attribute " +
                    "of class " + object.getClass().getName();
            logger.error(error);
            throw new NikitaMalformedInputDataException(error);
        } catch (NoSuchFieldException e) {
            String error = column + " is not an attribute associated with " +
                    object.getClass().getName();
            logger.error(error);
            throw new NikitaMalformedInputDataException(error);
        }
    }

    private Field getVersionFieldOrThrow(Object object) {
        Field[] fields = FieldUtils.getFieldsWithAnnotation(
                object.getClass(), Version.class);
        if (fields != null && fields.length == 1) {
            return fields[0];
        } else {
            String error = "Missing version attribute associated with " +
                    object.getClass().getName();
            logger.error(error);
            throw new NikitaMalformedInputDataException(error);
        }
    }

    /**
     * Used to retrieve a BSMBase object by getting the sub-class to
     * implement the repo class and return a BSMMetadata object if such an
     * object exists and is not outdated. Done to simplify coding.
     *
     * @param name Name of the BSM parameter to check
     * @return BSMMetadata object corresponding to the name
     */
    protected Optional<BSMMetadata> findBSMByName(String name) {
        String error = name + " unable to find subclass to process";
        logger.error(error);
        throw new NikitaMisconfigurationException(error);
    }

    private void checkBSM(List<BSMBase> bsm) {
        for (BSMBase bsmBase : bsm) {
            Optional<BSMMetadata> bsmMetadataOpt =
                    findBSMByName(bsmBase.getValueName());
            if (bsmMetadataOpt.isEmpty()) {
                String error = bsmBase.getValueName() + " is not a registered" +
                        " BSM datatype";
                logger.error(error);
                throw new PatchMisconfigurationException(error);
            } else if (bsmMetadataOpt.get().getOutdated()) {
                String error = bsmBase.getValueName() + " is outdated and " +
                        "cannot be assigned to any objects anymore";
                logger.error(error);
                throw new PatchMisconfigurationException(error);
            }
        }
    }


    public void applyLinksAndHeader(LinksNoarkObject noarkObject,
                                    ILinksBuilder handler) {
        String method = getMethod();
        switch (method) {
            case "get":
                handler.addLinks(noarkObject);
                break;
            case "get-template":
                handler.addLinksOnTemplate(noarkObject);
                break;
            case "post":
                handler.addLinks(noarkObject);
                if (noarkObject instanceof ISystemId) {

                }
                break;
            case "put":
                handler.addLinks(noarkObject);
                break;
            case "patch":
                handler.addLinks(noarkObject);
                break;
        }
        setOutgoingRequestHeader(noarkObject);
    }

    public Map.Entry<ChangeLog, SystemIdEntity> createChangeLogObject(String field, String oldValue, String newValue,
                                                                      SystemIdEntity systemIdEntity) {
        ChangeLog changeLog = new ChangeLog();
        changeLog.setEventInitiator(getUser());
        changeLog.setEventDate(now());
        changeLog.setNewValue(newValue);
        changeLog.setOldValue(oldValue);
        changeLog.setReferenceMetadata(field);
        changeLog.setReferenceArchiveUnit(systemIdEntity);
        changeLog.setHrefReferenceArchiveUnit(HATEOAS_API_PATH + SLASH + systemIdEntity.getFunctionalTypeName() +
                SLASH + systemIdEntity.getBaseTypeName() + SLASH + systemIdEntity.getSystemIdAsString());
        changeLog.setRelReferenceArchiveUnit(systemIdEntity.getBaseRel());
        changeLog.setReferenceArchiveUnitSystemId(systemIdEntity.getSystemId());
        changeLog.setOrganisation(userDetails.getOrganisation());
        User user = getUserObject();
        changeLog.setReferenceEventInitiator(user);
        changeLog.setEventInitiatorSystemId(user.getSystemId());
        return Map.entry(changeLog, systemIdEntity);
    }

    public Map.Entry<DeleteLog, SystemIdEntity> createDeleteLogObject(SystemIdEntity systemIdEntity) {
        DeleteLog deleteLog = new DeleteLog();
        deleteLog.setEventInitiator(getUser());
        deleteLog.setEventDate(now());
        deleteLog.setHrefReferenceArchiveUnit(HATEOAS_API_PATH + SLASH + systemIdEntity.getFunctionalTypeName() +
                SLASH + systemIdEntity.getBaseTypeName() + SLASH + systemIdEntity.getSystemIdAsString());
        deleteLog.setRelReferenceArchiveUnit(systemIdEntity.getBaseRel());
        deleteLog.setReferenceArchiveUnitSystemId(systemIdEntity.getSystemId());
        deleteLog.setOrganisation(userDetails.getOrganisation());
        User user = getUserObject();
        deleteLog.setReferenceEventInitiator(user);
        deleteLog.setEventInitiatorSystemId(user.getSystemId());
        return Map.entry(deleteLog, systemIdEntity);
    }

    public Map.Entry<ReadLog, SystemIdEntity> createReadLogObject(SystemIdEntity systemIdEntity) {
        ReadLog readLog = new ReadLog();
        readLog.setEventInitiator(getUser());
        readLog.setEventDate(now());
        readLog.setReferenceArchiveUnit(systemIdEntity);
        readLog.setHrefReferenceArchiveUnit(HATEOAS_API_PATH + SLASH + systemIdEntity.getFunctionalTypeName() +
                SLASH + systemIdEntity.getBaseTypeName() + SLASH + systemIdEntity.getSystemIdAsString());
        readLog.setRelReferenceArchiveUnit(systemIdEntity.getBaseRel());
        readLog.setReferenceArchiveUnitSystemId(systemIdEntity.getSystemId());
        readLog.setOrganisation(userDetails.getOrganisation());
        User user = getUserObject();
        readLog.setReferenceEventInitiator(user);
        readLog.setEventInitiatorSystemId(user.getSystemId());
        return Map.entry(readLog, systemIdEntity);
    }

    public Map.Entry<CreateLog, SystemIdEntity> createCreateLogObject(SystemIdEntity systemIdEntity) {
        CreateLog createLog = new CreateLog();
        createLog.setEventInitiator(getUser());
        createLog.setEventDate(now());
        createLog.setReferenceArchiveUnit(systemIdEntity);
        User user = getUserObject();
        createLog.setOrganisation(userDetails.getOrganisation());
        createLog.setReferenceEventInitiator(user);
        createLog.setEventInitiatorSystemId(user.getSystemId());
        return Map.entry(createLog, systemIdEntity);
    }

    public void handleDeletionForEventLog(SystemIdEntity systemIdEntity) {
        // Remove any links to existing event logs, leaving the eventlog objects intact
        Map.Entry<DeleteLog, SystemIdEntity> deleteLogEntry = createDeleteLogObject(systemIdEntity);
        applicationEventPublisher.publishEvent(
                new AfterNoarkEntityDeletedEvent(deleteLogEntry.getKey(), deleteLogEntry.getValue()));
    }

    public void handleReadForEventLog(SystemIdEntity systemIdEntity) {
        // Remove any links to existing event logs, leaving the eventlog objects intact
        Map.Entry<ReadLog, SystemIdEntity> readLogEntry = createReadLogObject(systemIdEntity);
        applicationEventPublisher.publishEvent(
                new AfterNoarkEntityReadEvent(readLogEntry.getKey(), readLogEntry.getValue()));
    }

    /**
     * Publish a CREATE events that have occurred
     *
     * @param systemIdEntity The create event to process
     */

    public void handleCreationForEventLog(SystemIdEntity systemIdEntity) {
        // Remove any links to existing event logs, leaving the eventlog objects intact
        Map.Entry<CreateLog, SystemIdEntity> createLogEntry = createCreateLogObject(systemIdEntity);
        applicationEventPublisher.publishEvent(
                new AfterNoarkEntityCreatedEvent(createLogEntry.getKey(), createLogEntry.getValue()));

    }

    /**
     * Publish all READ events that have occurred
     *
     * @param changeLogsMap Map containing read events to process
     */
    public void publishChangeLogEvents(List<Map.Entry<ChangeLog, SystemIdEntity>> changeLogsMap) {
        for (var changeLogEntry : changeLogsMap) {
            applicationEventPublisher.publishEvent(
                    new AfterNoarkEntityUpdatedEvent(changeLogEntry.getKey(), changeLogEntry.getValue()));
        }
    }
}
