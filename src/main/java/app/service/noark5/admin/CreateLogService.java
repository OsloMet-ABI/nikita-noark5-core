package app.service.noark5.admin;

import app.domain.noark5.admin.CreateLog;
import app.domain.repository.noark5.v5.admin.ICreateLogRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.admin.ICreateLogService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.admin.ICreateLogLinksBuilder;
import app.webapp.payload.links.admin.CreateLogLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class CreateLogService
        extends NoarkService
        implements ICreateLogService {

    private final ICreateLogRepository createLogRepository;
    private final ICreateLogLinksBuilder createLogLinksBuilder;

    public CreateLogService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            ICreateLogRepository createLogRepository,
            ICreateLogLinksBuilder createLogLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.createLogRepository = createLogRepository;
        this.createLogLinksBuilder = createLogLinksBuilder;
    }

    @Override
    public CreateLogLinks generateDefaultCreateLog() {
        CreateLog defaultCreateLog = new CreateLog();
        defaultCreateLog.setEventDate(OffsetDateTime.now());
        defaultCreateLog.setEventInitiator(getUser());
        defaultCreateLog.setVersion(-1L, true);
        return packAsLinks(defaultCreateLog);
    }

    @Override
    @Transactional
    public CreateLogLinks createNewCreateLog(CreateLog createLog) {
        if (null == createLog.getEventDate())
            createLog.setEventDate(OffsetDateTime.now());
        return packAsLinks(createLogRepository.save(createLog));
    }

    @Override
    public CreateLogLinks findCreateLogByOwner() {
        return (CreateLogLinks) processODataQueryGet();
    }

    @Override
    public CreateLogLinks findSingleCreateLog(@NotNull final UUID systemId) {
        return packAsLinks(getCreateLogOrThrow(systemId));
    }

    @Override
    @Transactional
    public CreateLogLinks handleUpdate(@NotNull final UUID systemId,
                                       @NotNull CreateLog incomingCreateLog) {
        CreateLog existingCreateLog = getCreateLogOrThrow(systemId);
	/*
        // Copy all the values you are allowed to copy ....
        existingCreateLog.setCreateLogText(incomingCreateLog.getCreateLogText());
        existingCreateLog.setCreateLogDate(incomingCreateLog.getCreateLogDate());
        existingCreateLog.setCreateLogRegisteredBy
            (incomingCreateLog.getCreateLogRegisteredBy());
	*/
        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingCreateLog.setVersion(getETag());
        return packAsLinks(existingCreateLog);
    }

    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        deleteEntity(getCreateLogOrThrow(systemId));
    }


    public CreateLogLinks packAsLinks(@NotNull final CreateLog createLog) {
        CreateLogLinks createLogLinks = new CreateLogLinks(createLog);
        applyLinksAndHeader(createLogLinks, createLogLinksBuilder);
        return createLogLinks;
    }

    protected CreateLog getCreateLogOrThrow(@NotNull final UUID systemId) {
        CreateLog createLog = createLogRepository.
                findBySystemId(systemId);
        if (createLog == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " CreateLog, using systemId " + systemId;
            throw new NoarkEntityNotFoundException(info);
        }
        return createLog;
    }
}
