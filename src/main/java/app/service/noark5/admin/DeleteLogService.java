package app.service.noark5.admin;

import app.domain.noark5.admin.DeleteLog;
import app.domain.repository.noark5.v5.admin.IDeleteLogRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.admin.IDeleteLogService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.admin.IDeleteLogLinksBuilder;
import app.webapp.payload.links.admin.DeleteLogLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class DeleteLogService
        extends NoarkService
        implements IDeleteLogService {

    private final IDeleteLogRepository deleteLogRepository;
    private final IDeleteLogLinksBuilder deleteLogLinksBuilder;

    public DeleteLogService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IDeleteLogRepository deleteLogRepository,
            IDeleteLogLinksBuilder deleteLogLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.deleteLogRepository = deleteLogRepository;
        this.deleteLogLinksBuilder = deleteLogLinksBuilder;
    }

    @Override
    public DeleteLogLinks generateDefaultDeleteLog() {
        DeleteLog defaultDeleteLog = new DeleteLog();
        defaultDeleteLog.setEventDate(OffsetDateTime.now());
        defaultDeleteLog.setEventInitiator(getUser());
        defaultDeleteLog.setVersion(-1L, true);
        return packAsLinks(defaultDeleteLog);
    }

    @Override
    @Transactional
    public DeleteLogLinks createNewDeleteLog(DeleteLog deleteLog) {
        if (null == deleteLog.getEventDate())
            deleteLog.setEventDate(OffsetDateTime.now());
        return packAsLinks(deleteLogRepository.save(deleteLog));
    }

    @Override
    public DeleteLogLinks findDeleteLogByOwner() {
        return (DeleteLogLinks) processODataQueryGet();
    }

    @Override
    public DeleteLogLinks findSingleDeleteLog(@NotNull final UUID systemId) {
        return packAsLinks(getDeleteLogOrThrow(systemId));
    }

    @Override
    @Transactional
    public DeleteLogLinks handleUpdate(@NotNull final UUID systemId,
                                       @NotNull DeleteLog incomingDeleteLog) {
        DeleteLog existingDeleteLog = getDeleteLogOrThrow(systemId);
	/*
        // Copy all the values you are allowed to copy ....
        existingDeleteLog.setDeleteLogText(incomingDeleteLog.getDeleteLogText());
        existingDeleteLog.setDeleteLogDate(incomingDeleteLog.getDeleteLogDate());
        existingDeleteLog.setDeleteLogRegisteredBy
            (incomingDeleteLog.getDeleteLogRegisteredBy());
	*/
        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingDeleteLog.setVersion(getETag());
        return packAsLinks(existingDeleteLog);
    }

    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        deleteEntity(getDeleteLogOrThrow(systemId));
    }


    public DeleteLogLinks packAsLinks(@NotNull final DeleteLog deleteLog) {
        DeleteLogLinks deleteLogLinks = new DeleteLogLinks(deleteLog);
        applyLinksAndHeader(deleteLogLinks, deleteLogLinksBuilder);
        return deleteLogLinks;
    }

    protected DeleteLog getDeleteLogOrThrow(@NotNull final UUID systemId) {
        DeleteLog deleteLog = deleteLogRepository.
                findBySystemId(systemId);
        if (deleteLog == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " DeleteLog, using systemId " + systemId;
            throw new NoarkEntityNotFoundException(info);
        }
        return deleteLog;
    }
}
