package app.service.noark5.admin;

import app.domain.noark5.admin.AdministrativeUnit;
import app.domain.noark5.admin.User;
import app.domain.repository.admin.IUserRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.IUserService;
import app.service.application.IPatchService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.exceptions.UsernameExistsException;
import app.webapp.payload.builder.interfaces.admin.IUserLinksBuilder;
import app.webapp.payload.links.admin.UserLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.Constants.SYSTEM;
import static app.utils.constants.ErrorMessagesConstants.USER_ALREADY_EXISTS;
import static java.lang.String.format;

@Service
public class UserService
        extends NoarkService
        implements IUserService {

    private static final Logger logger =
            LoggerFactory.getLogger(UserService.class);

    private final IUserRepository userRepository;
    private final IUserLinksBuilder userLinksBuilder;
    private final AdministrativeUnitService administrativeUnitService;

    public UserService(EntityManager entityManager,
                       ApplicationEventPublisher applicationEventPublisher,
                       IPatchService patchService,
                       IUserRepository userRepository,
                       IUserLinksBuilder userLinksBuilder,
                       AdministrativeUnitService administrativeUnitService,
                       ILocalUserDetails userDetails,
                       IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.userRepository = userRepository;
        this.userLinksBuilder = userLinksBuilder;
        this.administrativeUnitService = administrativeUnitService;
    }

    // All CREATE operations

    @Override
    @Transactional
    /**
     * Create a new user
     *
     * The person issuing the new user request has to be an admin for the
     * organisation. Get the logged in user and check that they have an ADMIN
     * authority.
     *
     * If the user is not logged in then this is the creation of a user and
     * an organisation.
     * Create a user associated with the administrativeUnit associated with the user
     * that created the new user.
     *
     * @param Incoming user to create
     * @return the newly persisted User object
     */
    public UserLinks createNewUser(final User user)
            throws UsernameExistsException {
        Optional<User> existingUser = userRepository
                .findByUsername(user.getUsername());
        if (existingUser.isPresent()) {
            throw new UsernameExistsException(
                    format(USER_ALREADY_EXISTS, user.getUsername()));
        }
        AdministrativeUnit administrativeUnit = administrativeUnitService
                .getAdministrativeUnitOrThrow(userDetails.getLoggedInUser());
        administrativeUnit.addUser(user);
        return packAsLinks(createAndSaveUser(user));
    }

// All READ operations

    @Override
    public UserLinks findBySystemID(@NotNull final UUID systemId) {
        return packAsLinks(getUserOrThrow(systemId));
    }

    @Override
    public UserLinks findAll() {
        return (UserLinks) processODataQueryGet();
    }

    /**
     * Look up username and return the equivalent User instance if it
     * exist, or null if not.
     *
     * @param username The username/emailaddress to check
     * @return User if the username is registered, null otherwise
     */
    @Override
    public User userGetByUsername(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        return userOptional.orElse(null);
    }

    /**
     * Look up systemId/UUID and return the equivalent User instance
     * if it exist, or null if not.
     *
     * @param systemId UUID for the username to check
     * @return User if the UUID is registered, null otherwise
     */
    @Override
    public User userGetBySystemId(@NotNull final UUID systemId) {
        Optional<User> userOptional = userRepository.findBySystemId(systemId);
        return userOptional.orElse(null);
    }

    @Override
    @Transactional
    public UserLinks handleUpdate(@NotNull final UUID systemId,
                                  @NotNull User incomingUser) {

        User existingUser = getUserOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        if (null != incomingUser.getFirstname()) {
            existingUser.setFirstname(incomingUser.getFirstname());
        }
        if (null != incomingUser.getLastname()) {
            existingUser.setLastname(incomingUser.getLastname());
        }

        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingUser.setVersion(getETag());
        return packAsLinks(existingUser);
    }

// All DELETE operations

    /**
     * Delete a user identified by the given systemId from the database.
     *
     * @param systemId systemId of the user to delete
     */
    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        deleteEntity(getUserOrThrow(systemId));
    }

    /**
     * Delete all user objects
     */
    @Override
    @Transactional
    public long deleteAll() {
        return userRepository.deleteByUsername(getUser());
    }

    @Override
    public UserLinks getDefaultUser() {
        User user = new User();
        user.setUsername("example@example.com");
        user.setFirstname("Hans");
        user.setLastname("Hansen");
        user.setVersion(-1L, true);
        return packAsLinks(user);
    }

    /**
     * Delete all objects belonging to the user identified by username
     */
    @Override
    @Transactional
    public long deleteByUsername(String username) {
        return userRepository.deleteByUsername(username);
    }

// All helper methods

    @Override
    public User validateUserReference
            (String type, User user, String username, UUID systemId) {
        if (null == user && null != systemId) {
            user = userGetBySystemId(systemId);
        }
        if (null != user &&
                (!user.getUsername().equals(username)
                        || !user.getSystemId().equals(systemId))) {
            String info = "Inconsistent " + type + " values rejected. ";
            throw new NikitaMalformedInputDataException(info);
        }
        // The values are consistent, return existing user
        return user;
    }

    private User createAndSaveUser(@NotNull final User user) {
        user.setCreatedBy(SYSTEM);
        user.setCreatedDate(OffsetDateTime.now());
        return userRepository.save(user);
    }

    public UserLinks packAsLinks(@NotNull final User user) {
        UserLinks userLinks = new UserLinks(user);
        applyLinksAndHeader(userLinks, userLinksBuilder);
        return userLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid User back. If there is no valid
     * User, a NoarkEntityNotFoundException exception is thrown
     *
     * @param systemId The systemId of the user object to retrieve
     * @return the user object
     */
    private User getUserOrThrow(@NotNull final UUID systemId) {
        Optional<User> userOptional =
                userRepository.findBySystemId(systemId);
        if (userOptional.isEmpty()) {
            String error = INFO_CANNOT_FIND_OBJECT + " User, using systemId " +
                    systemId;
            logger.error(error);
            throw new NoarkEntityNotFoundException(error);
        }
        return userOptional.get();
    }
}
