package app.service.noark5.admin;

import app.domain.noark5.admin.ReadLog;
import app.domain.repository.noark5.v5.admin.IReadLogRepository;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.admin.IReadLogService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.admin.IReadLogLinksBuilder;
import app.webapp.payload.links.admin.ReadLogLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

@Service
public class ReadLogService
        extends NoarkService
        implements IReadLogService {

    private final IReadLogRepository readLogRepository;
    private final IReadLogLinksBuilder readLogLinksBuilder;

    public ReadLogService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IReadLogRepository readLogRepository,
            IReadLogLinksBuilder readLogLinksBuilder,
            ILocalUserDetails userDetails,
            IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, userDetails, urlDetails);
        this.readLogRepository = readLogRepository;
        this.readLogLinksBuilder = readLogLinksBuilder;
    }

    @Override
    public ReadLogLinks generateDefaultReadLog() {
        ReadLog defaultReadLog = new ReadLog();
        defaultReadLog.setEventDate(OffsetDateTime.now());
        defaultReadLog.setEventInitiator(getUser());
        defaultReadLog.setVersion(-1L, true);
        return packAsLinks(defaultReadLog);
    }

    @Override
    @Transactional
    public ReadLogLinks createNewReadLog(ReadLog readLog) {
        if (null == readLog.getEventDate())
            readLog.setEventDate(OffsetDateTime.now());
        return packAsLinks(readLogRepository.save(readLog));
    }

    @Override
    public ReadLogLinks findReadLogByOwner() {
        return (ReadLogLinks) processODataQueryGet();
    }

    @Override
    public ReadLogLinks findSingleReadLog(@NotNull final UUID systemId) {
        return packAsLinks(getReadLogOrThrow(systemId));
    }

    @Override
    @Transactional
    public ReadLogLinks handleUpdate(@NotNull final UUID systemId,
                                     @NotNull ReadLog incomingReadLog) {
        ReadLog existingReadLog = getReadLogOrThrow(systemId);
	/*
        // Copy all the values you are allowed to copy ....
        existingReadLog.setReadLogText(incomingReadLog.getReadLogText());
        existingReadLog.setReadLogDate(incomingReadLog.getReadLogDate());
        existingReadLog.setReadLogRegisteredBy
            (incomingReadLog.getReadLogRegisteredBy());
	*/
        // Note setVersion can potentially result in a
        // NoarkConcurrencyException exception as it checks the ETAG
        // value
        existingReadLog.setVersion(getETag());
        return packAsLinks(existingReadLog);
    }

    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        deleteEntity(getReadLogOrThrow(systemId));
    }


    public ReadLogLinks packAsLinks(@NotNull final ReadLog readLog) {
        ReadLogLinks readLogLinks = new ReadLogLinks(readLog);
        applyLinksAndHeader(readLogLinks, readLogLinksBuilder);
        return readLogLinks;
    }

    protected ReadLog getReadLogOrThrow(@NotNull final UUID systemId) {
        ReadLog readLog = readLogRepository.
                findBySystemId(systemId);
        if (readLog == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " ReadLog, using systemId " + systemId;
            throw new NoarkEntityNotFoundException(info);
        }
        return readLog;
    }
}
