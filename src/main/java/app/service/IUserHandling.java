package app.service;

import app.webapp.exceptions.NikitaMisconfigurationException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.NotNull;

public interface IUserHandling {

    String getLoggedInUsername();

    HttpServletRequest getRequest();

    String getOrganisation();

    /**
     * Checks if the user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise.
     * @throws NikitaMisconfigurationException if the authentication object is null.
     */
    boolean validateUserAuthentication() throws NikitaMisconfigurationException;

    /* Note this assumes the claim can be returned as a string object . It can return a null value*/
    String getClaim(@NotNull String claim);

    /* Note this assumes the claim can be returned as a string object . */
    String getClaimOrThrow(@NotNull String claim);
}
