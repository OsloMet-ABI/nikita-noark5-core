package app.service;

import app.webapp.exceptions.NikitaMisconfigurationException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static app.utils.constants.Constants.SLASH;

@Component
public class UrlDetails
        implements IUrlDetails {

    @Value("${nikita.server.links.publicAddress}")
    private String publicAddress;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    private String issuerUri;

    /**
     * Get the outgoing address to use when generating links.
     * If we are not running behind a front facing server incoming requests
     * will not have X-Forward-* values set. In this case use the hardcoded
     * value from the properties file.
     * <p>
     * If X-Forward-*  values are set, then use them. At a minimum Host and
     * Proto must be set. If Port is also set use this to.
     *
     * @return the outgoing address
     */
    public String getOutgoingAddress() {
        HttpServletRequest request = getRequest();
        String address = request.getHeader("X-Forwarded-Host");
        String protocol = request.getHeader("X-Forwarded-Proto");
        String port = request.getHeader("X-Forwarded-Port");

        if (address != null && protocol != null) {
            if (port != null) {
                return protocol + "://" + address + ":" + port + contextPath +
                        SLASH;
            } else {
                return protocol + "://" + address + contextPath + SLASH;
            }
        } else {
            return publicAddress + contextPath + SLASH;
        }
    }

    @Override
    public String getOIDCConfigurationURL() {
        return issuerUri + "/.well-known/openid-configuration";
    }

    public String getAddress() {
        HttpServletRequest request = getRequest();
        String address = request.getHeader("X-Forwarded-Host");
        String scheme = request.getHeader("X-Forwarded-Proto");
        String port = request.getHeader("X-Forwarded-Port");

        if (address == null && scheme == null) {
            scheme = request.getScheme();
            address = request.getServerName();
            port = Integer.toString(request.getServerPort());
        }

        if (port != null) {
            return scheme + "://" + address + ":" + port;
        } else {
            return scheme + "://" + address;
        }
    }

    @Override
    public String getContextPath() {
        return contextPath;
    }

    protected HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (null == requestAttributes) {
            throw new NikitaMisconfigurationException("Unable to get Request object");
        }
        return ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
    }


}
