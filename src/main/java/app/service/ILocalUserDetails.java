package app.service;

import app.domain.noark5.admin.User;

public interface ILocalUserDetails
        extends IUserHandling {
    User getLoggedInUser();
}
