package app.service.application;

import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.webapp.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static app.utils.constants.Constants.*;

@Service
public class VendorService {

    @Value("${spring.application.name}")
    private String product;

    @Value("${nikita.system.vendor.version}")
    private String productVersion;

    @Value("${nikita.system.vendor.name}")
    private String vendor;

    @Value("${nikita.system.protocol.version}")
    private String protocolVersion;

    @Value("${nikita.system.build}")
    private String versionDate;

    private final ILocalUserDetails userDetails;
    private final IUrlDetails urlDetails;

    public VendorService(ILocalUserDetails userDetails, IUrlDetails urlDetails) {
        this.userDetails = userDetails;
        this.urlDetails = urlDetails;
    }

    /**
     * Add OIDC as an option
     * https://rel.arkivverket.no/noark5/v5/api/login/oidc/
     */
    public void addOpenIdConfiguration(List<ConformityLevel> conformityLevels) {
        ConformityLevel openId = new ConformityLevel();
        openId.setHref(urlDetails.getOIDCConfigurationURL());
        openId.setRel(REL_LOGIN_OIDC);
        conformityLevels.add(openId);
    }

    /**
     * Creates a list of the officially supported resource links.
     * These are: arkivstruktur, casehandling, metadata, admin and
     * loggingogsporing
     *
     */
    public void addConformityLevels(List<ConformityLevel> conformityLevels) {
        // arkivstruktur
        addConformityLevel(conformityLevels, HREF_BASE_FONDS_STRUCTURE, REL_FONDS_STRUCTURE);
        // sakarkiv
        addConformityLevel(conformityLevels, HREF_BASE_CASE_HANDLING, REL_CASE_HANDLING);
        // metadata
        addConformityLevel(conformityLevels, HREF_BASE_METADATA, REL_METADATA);
        // administrasjon
        addConformityLevel(conformityLevels, HREF_BASE_ADMIN, REL_ADMINISTRATION);
        // loggingogsporing
        addConformityLevel(conformityLevels, HREF_BASE_LOGGING, REL_LOGGING);
    }

    /**
     * Retrieves the application details based on the current user's authentication status.
     *
     * <p>The application details object provides the client with information about the available URLs
     * for interacting with the application. This method returns data that corresponds to the root level
     * of the application.</p>
     *
     * <p>If the user is not logged in, only a minimal amount of information is presented:</p>
     * <pre>
     * {
     *   "_links": {
     *     "https://rel.arkivverket.no/noark5/v5/api/login/oidc/": {
     *       "href": "https://example.com/keycloak/realms/recordkeeping/.well-known/openid-configuration"
     *     },
     *     "self": {
     *       "href": "https://example.com/noark5v5/"
     *     }
     *   }
     * }
     * </pre>
     * <p>If the user is logged in, further information is presented:</p>
     * <pre>
     * {
     *   "_links": {
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkiv/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/arkiv{?$filter&$orderby&$top&$skip}",
     *       "templated": true
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkiv/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/ny-arkiv"
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivskaper/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/arkivskaper{?$filter&$orderby&$top&$skip}",
     *       "templated": true
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkivskaper/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/ny-arkivskaper"
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivdel/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/arkivdel{?$filter&$orderby&$top&$skip}",
     *       "templated": true
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/klassifikasjonssystem/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/klassifikasjonssystem{?$filter&$orderby&$top&$skip}",
     *       "templated": true
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/klasse/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/klasse{?$filter&$orderby&$top&$skip}",
     *       "templated": true
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/mappe/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/mappe{?$filter&$orderby&$top&$skip}",
     *       "templated": true
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/registrering/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/registrering{?$filter&$orderby&$top&$skip}",
     *       "templated": true
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/dokumentbeskrivelse/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/dokumentbeskrivelse{?$filter&$orderby&$top&$skip}",
     *       "templated": true
     *     },
     *     "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/dokumentobjekt/": {
     *       "href": "https://example.com/noark5v5/api/arkivstruktur/dokumentobjekt{?$filter&$orderby&$top&$skip}",
     *       "templated": true
     *     }
     *   }
     * }
     *
     * </pre>
     *
     * @return a JSON object containing the application details, including available URLs.
     * If the user is not authenticated, a minimal amount of information is returned.
     */
    public ApplicationDetails getApplicationDetails() {
        List<ConformityLevel> conformityLevelList = new ArrayList<>();

        if (userDetails.validateUserAuthentication()) {
            addConformityLevels(conformityLevelList);
        }
        addOpenIdConfiguration(conformityLevelList);
        ApplicationDetails applicationDetails = new ApplicationDetails(conformityLevelList);
        applicationDetails.setSelfHref(urlDetails.getOutgoingAddress());
        return applicationDetails;
    }


    public FondsStructureDetails getFondsStructureDetails() {
        return new FondsStructureDetails(urlDetails.getOutgoingAddress());
    }

    public AdministrationDetails getAdministrationDetails() {
        return new AdministrationDetails(urlDetails.getOutgoingAddress());
    }

    public MetadataDetails getMetadataDetails() {
        return new MetadataDetails(urlDetails.getOutgoingAddress());
    }

    public CaseHandlingDetails getCaseHandlingDetails() {
        return new CaseHandlingDetails(urlDetails.getOutgoingAddress());
    }

    public LoggingDetails getLoggingDetails() {
        return new LoggingDetails(urlDetails.getOutgoingAddress());
    }

    public SystemInformation getSystemInformation() {
        SystemInformation systemInformation = new SystemInformation();
        systemInformation.setProduct(product);
        systemInformation.setProtocolVersion(protocolVersion);
        systemInformation.setVendor(vendor);
        systemInformation.setVersion(productVersion);
        systemInformation.setVersionDate(versionDate);
        return systemInformation;
    }

    /**
     * Add a conformity level
     *
     * @param conformityLevels ArrayList of conformityLevels
     * @param href             http address of endpoint
     * @param rel              rel associated with endpoint
     */
    private void addConformityLevel(List<ConformityLevel> conformityLevels,
                                    String href, String rel) {
        ConformityLevel conformityLevel = new ConformityLevel();
        conformityLevel.setHref(urlDetails.getOutgoingAddress() + href);
        conformityLevel.setRel(rel);
        conformityLevels.add(conformityLevel);
    }
}
