package app.service;

import app.domain.noark5.admin.User;
import app.domain.repository.admin.IUserRepository;
import app.webapp.exceptions.NikitaMisconfigurationException;
import org.springframework.stereotype.Component;

/**
 * The {@code LocalUserDetails} class provides functionality related to users registered in the
 * Nikita User table. It interacts with the User repository to fetch user details based on the username
 * extracted from the JWT claims.</p>
 * <p>The reason it extends IUserHandling is to  prevent a circular dependency during startup because of
 * the IUserRepository, and the OrganisationListener so the OrganisationListener only interacts with
 * a UserHandling component</p>
 *
 * <p>Dependencies:</p>
 * <ul>
 *     <li>{@link IUserRepository} - Repository interface for user data access.</li>
 * </ul>
 *
 * @see IUserHandling
 * @see IUserRepository
 * @see NikitaMisconfigurationException
 */
@Component
public class LocalUserDetails
        extends UserHandling
        implements ILocalUserDetails {

    private final IUserRepository userRepository;

    public LocalUserDetails(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Retrieves the currently logged-in user based on the username extracted from the JWT.
     *
     * @return the User object representing the logged-in user
     * @throws NikitaMisconfigurationException if the user is not found in the repository
     */
    @Override
    public User getLoggedInUser() {
        String username = getLoggedInUsername();
        return userRepository.findByUsername(username)
                .orElseThrow(() -> createUserNotFoundException(username));
    }
}
