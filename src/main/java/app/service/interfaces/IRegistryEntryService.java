package app.service.interfaces;

import app.domain.noark5.RecordEntity;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.secondary.DocumentFlow;
import app.domain.noark5.secondary.Precedence;
import app.domain.noark5.secondary.SignOff;
import app.webapp.model.PatchMerge;
import app.webapp.payload.links.casehandling.RegistryEntryExpansionLinks;
import app.webapp.payload.links.casehandling.RegistryEntryLinks;
import app.webapp.payload.links.secondary.DocumentFlowLinks;
import app.webapp.payload.links.secondary.PrecedenceLinks;
import app.webapp.payload.links.secondary.SignOffLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IRegistryEntryService {

    // All save methods
    RegistryEntryLinks save(@NotNull final RegistryEntry registryEntry);

    RegistryEntryLinks expandRecorLinksRegistryEntry(
            @NotNull final RecordEntity record,
            @NotNull final PatchMerge patchMerge);

    PrecedenceLinks createPrecedenceAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final Precedence precedence);

    SignOffLinks
    createSignOffAssociatedWithRegistryEntry(
            @NotNull final UUID systemId,
            @NotNull final SignOff signOff);

    SignOffLinks findAllSignOffAssociatedWithRegistryEntry(
            @NotNull final UUID systemId);

    SignOffLinks findSignOffAssociatedWithRegistryEntry(
            @NotNull final UUID systemId,
            @NotNull final UUID signOffSystemId);

    DocumentFlowLinks associateDocumentFlowWithRegistryEntry(
            @NotNull final UUID systemId,
            @NotNull final DocumentFlow documentFlow);

    // All find methods

    RegistryEntryLinks findBySystemId(@NotNull final UUID systemId);

    /**
     * @return Page of RegistryEntry objects the user owns
     */
    RegistryEntryLinks findAllRegistryEntry();

    DocumentFlowLinks findAllDocumentFlowWithRegistryEntryBySystemId(
            @NotNull final UUID systemId);

    PrecedenceLinks findAllPrecedenceForRegistryEntry(
            @NotNull final UUID systemId);

    // All UPDATE operations
    RegistryEntryLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final RegistryEntry incomingRegistryEntry);

    SignOffLinks
    handleUpdateSignOff(
            @NotNull final UUID systemId,
            @NotNull final UUID signOffSystemID,
            @NotNull final SignOff incomingSignOff);

    // All DELETE operations
    void deleteEntity(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    void deleteSignOff(@NotNull final UUID systemId,
                       @NotNull final UUID signOffSystemId);

    PrecedenceLinks generateDefaultPrecedence(
            @NotNull final UUID systemId);

    DocumentFlowLinks generateDefaultDocumentFlow(
            @NotNull final UUID systemId);

    RegistryEntryLinks generateDefaultRegistryEntry(
            @NotNull final UUID systemId);

    SignOffLinks generateDefaultSignOff(
            @NotNull final UUID systemId);

    RegistryEntryExpansionLinks generateDefaultExpandedRegistryEntry(
            @NotNull final UUID systemId);
}
