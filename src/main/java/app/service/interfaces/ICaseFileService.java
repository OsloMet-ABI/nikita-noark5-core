package app.service.interfaces;

import app.domain.noark5.File;
import app.domain.noark5.casehandling.CaseFile;
import app.domain.noark5.casehandling.RecordNote;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.secondary.Precedence;
import app.webapp.model.PatchMerge;
import app.webapp.payload.links.casehandling.CaseFileExpansionLinks;
import app.webapp.payload.links.casehandling.CaseFileLinks;
import app.webapp.payload.links.casehandling.RecordNoteLinks;
import app.webapp.payload.links.casehandling.RegistryEntryLinks;
import app.webapp.payload.links.secondary.PrecedenceLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;


public interface ICaseFileService {

    CaseFileLinks save(@NotNull final CaseFile caseFile);

    CaseFileLinks expandFileAsCaseFileLinks(
            @NotNull final File file,
            @NotNull final PatchMerge patchMerge);

    PrecedenceLinks createPrecedenceAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final Precedence precedence);

    RegistryEntryLinks createRegistryEntryAssociatedWithCaseFile(
            @NotNull final UUID systemId,
            @NotNull final RegistryEntry registryEntry);

    RecordNoteLinks createRecordNoteToCaseFile(
            @NotNull final UUID systemId,
            @NotNull final RecordNote RecordNote);

    CaseFileLinks findBySystemId(@NotNull final UUID systemId);

    CaseFileLinks findAll();

    PrecedenceLinks findAllPrecedenceForCaseFile(
            @NotNull final UUID systemId);

    RecordNoteLinks findAllRecordNoteToCaseFile(
            @NotNull final UUID systemId);

    RegistryEntryLinks findAllRegistryEntryToCaseFile(
            @NotNull final UUID systemId);

    // All UPDATE operations
    CaseFileLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final CaseFile incomingCaseFile);

    CaseFileLinks generateDefaultCaseFile();

    RecordNoteLinks generateDefaultRecordNote(
            @NotNull final UUID systemId);

    RegistryEntryLinks generateDefaultRegistryEntry(
            @NotNull final UUID caseFileSystemId);

    PrecedenceLinks generateDefaultPrecedence(
            @NotNull final UUID systemId);

    CaseFileExpansionLinks generateDefaultExpandedCaseFile();

    // All DELETE operations

    void deleteEntity(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    CaseFileLinks createCaseFileToCaseFile(
            @NotNull final UUID systemId,
            @NotNull final CaseFile caseFile);
}
