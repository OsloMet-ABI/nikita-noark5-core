package app.service.interfaces.admin;

import app.domain.noark5.admin.DeleteLog;
import app.webapp.payload.links.admin.DeleteLogLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IDeleteLogService {

    DeleteLogLinks generateDefaultDeleteLog();

    DeleteLogLinks createNewDeleteLog(@NotNull final DeleteLog deleteLog);

    DeleteLogLinks findDeleteLogByOwner();

    DeleteLogLinks findSingleDeleteLog(@NotNull final UUID systemId);

    DeleteLogLinks handleUpdate(@NotNull final UUID systemId,
                                @NotNull DeleteLog incomingDeleteLog);

    void deleteEntity(@NotNull final UUID systemId);
}
