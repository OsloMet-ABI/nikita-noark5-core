package app.service.interfaces.admin;


import app.domain.noark5.admin.AdministrativeUnit;
import app.domain.noark5.admin.User;
import app.webapp.payload.links.admin.AdministrativeUnitLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IAdministrativeUnitService {

    AdministrativeUnitLinks update(
            @NotNull final UUID systemId,
            @NotNull final AdministrativeUnit incomingAdministrativeUnit);

    AdministrativeUnitLinks createNewAdministrativeUnitByUser(
            @NotNull final AdministrativeUnit administrativeUnit,
            @NotNull final User user);

    AdministrativeUnitLinks createNewAdministrativeUnitBySystem(
            @NotNull final AdministrativeUnit administrativeUnit);

    void createNewAdministrativeUnitBySystemNoDuplicate(
            @NotNull final AdministrativeUnit administrativeUnit);

    AdministrativeUnitLinks findBySystemId(
            @NotNull final UUID administrativeUnitSystemId);

    AdministrativeUnitLinks findAll();

    AdministrativeUnit findFirst();

    AdministrativeUnit getAdministrativeUnitOrThrow(
            @NotNull final User user);

    // All DELETE operations
    void deleteEntity(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    AdministrativeUnitLinks generateDefaultAdministrativeUnit();
}
