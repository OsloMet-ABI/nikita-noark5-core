package app.service.interfaces.casehandling;

import app.domain.noark5.RecordEntity;
import app.domain.noark5.casehandling.RecordNote;
import app.domain.noark5.secondary.DocumentFlow;
import app.webapp.model.PatchMerge;
import app.webapp.payload.links.casehandling.RecordNoteExpansionLinks;
import app.webapp.payload.links.casehandling.RecordNoteLinks;
import app.webapp.payload.links.secondary.DocumentFlowLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IRecordNoteService {

    // All save methods
    RecordNoteLinks save(@NotNull final RecordNote recordNote);

    RecordNoteLinks expandRecorLinksRecordNote(
            @NotNull final RecordEntity record,
            @NotNull final PatchMerge patchMerge);

    // All find methods
    RecordNoteLinks findBySystemId(@NotNull final UUID systemId);

    RecordNoteLinks findAll();

    DocumentFlowLinks findAllDocumentFlowWithRecordNoteBySystemId(
            @NotNull final UUID systemId);

    DocumentFlowLinks associateDocumentFlowWithRecordNote(
            @NotNull final UUID systemId,
            @NotNull final DocumentFlow documentFlow);

    // All UPDATE operations
    RecordNoteLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final RecordNote incomingRecordNote);

    // All DELETE operations
    String deleteEntity(@NotNull final UUID systemId);

    String deleteAllByOrganisation();

    RecordNoteLinks generateDefaultRecordNote(
            @NotNull final UUID systemId);

    DocumentFlowLinks generateDefaultDocumentFlow(
            @NotNull final UUID systemId);

    RecordNoteExpansionLinks generateDefaultExpandedRecordNote(
            @NotNull final UUID systemId);
}
