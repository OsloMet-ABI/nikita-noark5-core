package app.service.interfaces;


import app.domain.noark5.ClassificationSystem;
import app.domain.noark5.File;
import app.domain.noark5.Series;
import app.domain.noark5.casehandling.CaseFile;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.secondary.StorageLocation;
import app.webapp.payload.links.*;
import app.webapp.payload.links.casehandling.CaseFileLinks;
import app.webapp.payload.links.secondary.ScreeningMetadataLinks;
import app.webapp.payload.links.secondary.StorageLocationLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface ISeriesService {

    // -- All CREATE operations
    SeriesLinks save(@NotNull final Series series);

    StorageLocationLinks createStorageLocationAssociatedWithSeries(
            @NotNull final UUID systemId,
            @NotNull final StorageLocation storageLocation);

    FileLinks createFileAssociatedWithSeries(
            @NotNull final UUID systemId,
            @NotNull final File file);

    CaseFileLinks createCaseFileAssociatedWithSeries(
            @NotNull final UUID systemId,
            @NotNull final CaseFile caseFile);

    ClassificationSystemLinks createClassificationSystem(
            @NotNull final UUID systemId,
            @NotNull final ClassificationSystem classificationSystem);

    void checkSeriesStatusUponCreation(@NotNull final Series series);

    // -- All READ operations
    SeriesLinks findAll();

    CaseFileLinks
    findCaseFilesBySeries(
            @NotNull final UUID systemId);

    // systemId
    SeriesLinks findBySystemId(
            @NotNull final UUID systemId);

    RecordLinks findAllRecordAssociatedWithSeries(
            @NotNull final UUID systemId);

    FileLinks findAllFileAssociatedWithSeries(
            @NotNull final UUID systemId);

    ClassificationSystemLinks
    findClassificationSystemAssociatedWithSeries(
            @NotNull final UUID systemId);

    FondsLinks findFondsAssociatedWithSeries(
            @NotNull final UUID systemId);

    StorageLocationLinks findStorageLocationAssociatedWithSeries(
            @NotNull final UUID systemId);

    // All UPDATE operations
    SeriesLinks handleUpdate(@NotNull final UUID systemId,
                             @NotNull final Series incomingSeries);

    void updateSeriesReferences(@NotNull final Series series);

    // All DELETE operations
    void deleteEntity(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    ScreeningMetadataLinks getScreeningMetadataAssociatedWithSeries(
            @NotNull final UUID systemId);

    ScreeningMetadataLinks createScreeningMetadataAssociatedWithSeries(
            @NotNull final UUID systemId,
            @NotNull final Metadata screeningMetadata);

    ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId);

    StorageLocationLinks getDefaultStorageLocation(
            @NotNull final UUID systemId);

    SeriesLinks generateDefaultSeries(@NotNull final UUID systemId);

    Series getSeriesOrThrow(UUID uuid);
}
