package app.service.interfaces;


import app.domain.noark5.Class;
import app.domain.noark5.ClassificationSystem;
import app.webapp.payload.links.ClassLinks;
import app.webapp.payload.links.ClassificationSystemLinks;
import app.webapp.payload.links.SeriesLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IClassificationSystemService {

    // -- All CREATE operations
    ClassificationSystemLinks save(
            @NotNull final ClassificationSystem classificationSystem);

    ClassLinks createClassAssociatedWithClassificationSystem(
            @NotNull final UUID systemId, @NotNull final Class klass);

    ClassLinks generateDefaultClass(@NotNull final UUID systemId);

    // -- All READ operations
    ClassificationSystemLinks findSingleClassificationSystem(
            @NotNull final UUID systemId);

    ClassificationSystemLinks findAllClassificationSystem();

    ClassLinks findAllClassAssociatedWithClassificationSystem(
            @NotNull final UUID systemId);

    SeriesLinks findSeriesAssociatedWithClassificationSystem(
            @NotNull final UUID systemId);

    // All UPDATE operations
    ClassificationSystemLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final ClassificationSystem incomingClassificationSystem);

    // All DELETE operations
    void deleteClassificationSystem(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    ClassificationSystemLinks generateDefaultClassificationSystem();
}
