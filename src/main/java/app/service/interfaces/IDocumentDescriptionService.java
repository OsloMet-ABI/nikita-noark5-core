package app.service.interfaces;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.secondary.Author;
import app.domain.noark5.secondary.Comment;
import app.domain.noark5.secondary.PartPerson;
import app.domain.noark5.secondary.PartUnit;
import app.webapp.payload.links.DocumentDescriptionLinks;
import app.webapp.payload.links.DocumentObjectLinks;
import app.webapp.payload.links.RecordLinks;
import app.webapp.payload.links.secondary.*;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IDocumentDescriptionService {

    // -- All CREATE operations
    DocumentDescriptionLinks save(
            @NotNull final DocumentDescription documentDescription);

    DocumentObjectLinks
    createDocumentObjectAssociatedWithDocumentDescription(
            @NotNull final UUID systemId,
            @NotNull final DocumentObject documentObject);

    CommentLinks createCommentAssociatedWithDocumentDescription(
            @NotNull final UUID systemId,
            @NotNull final Comment comment);

    PartPersonLinks createPartPersonAssociatedWithDocumentDescription(
            @NotNull final UUID systemId,
            @NotNull final PartPerson partPerson);

    PartUnitLinks createPartUnitAssociatedWithDocumentDescription(
            @NotNull final UUID systemId,
            @NotNull final PartUnit partUnit);

    // -- All READ operations

    AuthorLinks associateAuthorWithDocumentDescription(
            @NotNull final UUID systemId,
            @NotNull final Author author);

    DocumentDescriptionLinks generateDefaultDocumentDescription(
            @NotNull final UUID systemId);

    CommentLinks generateDefaultComment(@NotNull final UUID systemId);

    PartPersonLinks generateDefaultPartPerson(@NotNull final UUID systemId);

    PartUnitLinks generateDefaultPartUnit(@NotNull final UUID systemId);

    DocumentDescriptionLinks findAll();

    DocumentDescriptionLinks findBySystemId(@NotNull final UUID systemId);

    RecordLinks
    findAllRecordWithDocumentDescriptionBySystemId(
            @NotNull final UUID systemId);

    DocumentObjectLinks
    findAllDocumentObjectWithDocumentDescriptionBySystemId(
            @NotNull final UUID systemId);

    CommentLinks getCommentAssociatedWithDocumentDescription
            (@NotNull final UUID systemId);

    PartLinks getPartAssociatedWithDocumentDescription(
            @NotNull final UUID systemId);
    // -- All UPDATE operations

    DocumentDescriptionLinks handleUpdate
            (@NotNull final UUID systemId, @NotNull final DocumentDescription documentDescription);

    // -- All DELETE operations
    void deleteEntity(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    AuthorLinks
    findAllAuthorWithDocumentDescriptionBySystemId(
            @NotNull final UUID systemId);

    AuthorLinks generateDefaultAuthor(@NotNull final UUID systemId);

    ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId);

    ScreeningMetadataLinks
    createScreeningMetadataAssociatedWithDocumentDescription(
            @NotNull final UUID systemId,
            @NotNull final Metadata screeningMetadata);

    ScreeningMetadataLinks
    getScreeningMetadataAssociatedWithDocumentDescription(
            @NotNull final UUID systemId);
}
