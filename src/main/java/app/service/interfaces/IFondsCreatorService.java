package app.service.interfaces;

import app.domain.noark5.Fonds;
import app.domain.noark5.FondsCreator;
import app.webapp.payload.links.FondsCreatorLinks;
import app.webapp.payload.links.FondsLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IFondsCreatorService {

    FondsCreatorLinks createNewFondsCreator(
            @NotNull final FondsCreator fondsCreator);

    FondsLinks createFondsAssociatedWithFondsCreator(
            @NotNull final UUID systemId,
            @NotNull final Fonds fonds);

    // -- All READ operations

    FondsCreatorLinks findAll();

    FondsCreatorLinks findBySystemId(
            @NotNull final UUID systemId);

    FondsLinks findFondsAssociatedWithFondsCreator(
            @NotNull final UUID systemId);

    // -- All UPDATE operations
    FondsCreatorLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final FondsCreator fondsCreator);

    // -- All DELETE operations
    void deleteEntity(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    FondsCreatorLinks generateDefaultFondsCreator();
}
