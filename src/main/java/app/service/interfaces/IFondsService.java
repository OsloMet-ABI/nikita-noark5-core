package app.service.interfaces;


import app.domain.noark5.Fonds;
import app.domain.noark5.FondsCreator;
import app.domain.noark5.Series;
import app.domain.noark5.secondary.StorageLocation;
import app.webapp.payload.links.FondsCreatorLinks;
import app.webapp.payload.links.FondsLinks;
import app.webapp.payload.links.SeriesLinks;
import app.webapp.payload.links.secondary.StorageLocationLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IFondsService {

    // -- All CREATE operations
    FondsLinks createNewFonds(@NotNull final Fonds fonds);

    SeriesLinks createSeriesAssociatedWithFonds(
            @NotNull final UUID systemId,
            @NotNull final Series series);

    FondsLinks createFondsAssociatedWithFonds(
            @NotNull final UUID systemId,
            @NotNull final Fonds childFonds);

    FondsCreatorLinks createFondsCreatorAssociatedWithFonds(
            @NotNull final UUID systemId,
            @NotNull final FondsCreator fondsCreator);

    StorageLocationLinks createStorageLocationAssociatedWithFonds(
            @NotNull final UUID systemId,
            @NotNull final StorageLocation storageLocation);

    // -- All READ operations

    FondsLinks findSingleFonds(@NotNull final UUID systemId);

    FondsLinks findAllFonds();

    FondsLinks findAllChildren(@NotNull final UUID systemId);

    SeriesLinks findSeriesAssociatedWithFonds(
            @NotNull final UUID systemId);

    FondsCreatorLinks findFondsCreatorAssociatedWithFonds(
            @NotNull final UUID systemId);

    StorageLocationLinks findStorageLocationAssociatedWithFonds(
            @NotNull final UUID systemID);

    SeriesLinks generateDefaultSeries(@NotNull final UUID systemId);

    FondsLinks generateDefaultFonds(@NotNull final UUID systemId);

    // All UPDATE operations
    FondsLinks handleUpdate(@NotNull final UUID systemId,
                            @NotNull final Fonds incomingFonds);

    // All DELETE operations
    void deleteEntity(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    StorageLocationLinks getDefaultStorageLocation(
            @NotNull final UUID systemId);
}
