package app.service.interfaces;


import app.domain.filehandling.LargeFileUploadRegister;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.secondary.Conversion;
import app.webapp.payload.links.DocumentObjectLinks;
import app.webapp.payload.links.secondary.ConversionLinks;
import jakarta.validation.constraints.NotNull;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.UUID;

public interface IDocumentObjectService {

    // -- All CREATE operations
    DocumentObjectLinks save(@NotNull final DocumentObject documentObject);

    // -- All READ operations

    DocumentObjectLinks generateDefaultDocumentObject();

    DocumentObjectLinks findAll();

    ConversionLinks generateDefaultConversion(@NotNull final UUID systemId);

    ConversionLinks
    createConversionAssociatedWithDocumentObject(
            @NotNull final UUID systemId,
            @NotNull final Conversion conversion);

    ConversionLinks
    findAllConversionAssociatedWithDocumentObject(@NotNull final UUID systemId);

    ConversionLinks
    findConversionAssociatedWithDocumentObject(
            @NotNull final UUID systemId,
            @NotNull final UUID subSystemId);

    ConversionLinks handleUpdateConversionBySystemId(
            @NotNull final UUID systemId,
            @NotNull final UUID subSystemId,
            @NotNull final Conversion conversion);

    // systemId
    DocumentObjectLinks findBySystemId(@NotNull final UUID systemId);

    // All UPDATE operations
    DocumentObjectLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final DocumentObject documentObject);

    // All DELETE operations
    void deleteEntity(@NotNull final UUID systemId) throws IOException;

    long deleteAll() throws IOException;

    // -- File handling operations

    /**
     * Given an systemId for a documentObject, find the documentObject and
     * create a new documentObject with an archive version of the
     * original documentObject
     **/
    DocumentObjectLinks
    convertDocumentToPDF(@NotNull final UUID systemId);

    DocumentObjectLinks
    handleIncomingFile(@NotNull final UUID systemId) throws IOException;

    void registerChunkedFileUpload(@NotNull LargeFileUploadRegister LargeFileUploadRegister);

    Resource loadAsResource(@NotNull final UUID systemId);

    /**
     * Delete a conversion object associated with the documentObject.
     *
     * @param systemId           UUID of the documentObject object
     * @param conversionSystemID UUID of the Conversion object
     */
    void deleteConversion(@NotNull final UUID systemId,
                          @NotNull final UUID conversionSystemID);

    DocumentObjectLinks handleIncomingFileChunk(@NotNull final UUID systemID) throws IOException;
}
