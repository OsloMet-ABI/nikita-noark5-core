package app.service.interfaces;

import app.domain.noark5.Class;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.casehandling.CaseFile;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.secondary.CrossReference;
import app.domain.noark5.secondary.Keyword;
import app.webapp.payload.links.ClassLinks;
import app.webapp.payload.links.ClassificationSystemLinks;
import app.webapp.payload.links.FileLinks;
import app.webapp.payload.links.RecordLinks;
import app.webapp.payload.links.casehandling.CaseFileLinks;
import app.webapp.payload.links.secondary.CrossReferenceLinks;
import app.webapp.payload.links.secondary.KeywordLinks;
import app.webapp.payload.links.secondary.KeywordTemplateLinks;
import app.webapp.payload.links.secondary.ScreeningMetadataLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IClassService {

    // -- All CREATE operations
    ClassLinks save(@NotNull final Class klass);

    ClassLinks createClassAssociatedWithClass(
            @NotNull final UUID systemId, @NotNull final Class klass);

    FileLinks createFileAssociatedWithClass(
            @NotNull final UUID systemId, @NotNull final File file);

    CaseFileLinks createCaseFileAssociatedWithClass(
            @NotNull final UUID systemId, @NotNull final CaseFile caseFile);

    ScreeningMetadataLinks createScreeningMetadataAssociatedWithClass(
            @NotNull final UUID systemId,
            @NotNull final Metadata screeningMetadata);

    RecordLinks createRecordAssociatedWithClass(
            @NotNull final UUID systemId, @NotNull final RecordEntity record);

    CrossReferenceLinks createCrossReferenceAssociatedWithClass(
            @NotNull final UUID systemId,
            @NotNull final CrossReference crossReference);

    KeywordLinks createKeywordAssociatedWithClass(
            @NotNull final UUID systemId, @NotNull final Keyword keyword);

    ClassLinks generateDefaultSubClass(@NotNull final UUID systemId);

    ClassLinks generateDefaultClass(@NotNull final UUID systemId);

    // -- All READ operations
    ClassLinks findAll();

    ClassLinks findSingleClass(@NotNull final UUID systemId);

    ClassLinks findAllChildren(@NotNull final UUID systemId);

    ScreeningMetadataLinks getScreeningMetadataAssociatedWithClass(
            @NotNull final UUID systemId);

    ClassLinks findClassAssociatedWithClass(@NotNull final UUID systemId);

    ClassificationSystemLinks findClassificationSystemAssociatedWithClass(
            @NotNull final UUID systemId);

    FileLinks findAllFileAssociatedWithClass(
            @NotNull final UUID systemId);

    RecordLinks findAllRecordAssociatedWithClass(
            @NotNull final UUID systemId);

    KeywordLinks findKeywordAssociatedWithClass(@NotNull final UUID systemId);

    CrossReferenceLinks findCrossReferenceAssociatedWithClass(
            @NotNull final UUID systemId);

    // All UPDATE operations
    ClassLinks handleUpdate(@NotNull final UUID systemId,
                            @NotNull final Class klass);

    // All DELETE operations

    void deleteEntity(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    // All TEMPLATE operations

    ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId);

    KeywordTemplateLinks generateDefaultKeyword(@NotNull final UUID systemId);

    CrossReferenceLinks getDefaultCrossReference(@NotNull final UUID systemId);
}
