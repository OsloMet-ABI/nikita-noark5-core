package app.service.interfaces.secondary;

import app.domain.noark5.Class;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.secondary.Keyword;
import app.webapp.payload.links.secondary.KeywordLinks;
import app.webapp.payload.links.secondary.KeywordTemplateLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IKeywordService {

    KeywordLinks createKeywordAssociatedWithFile(
            @NotNull final Keyword keyword, @NotNull final File file);

    KeywordLinks createKeywordAssociatedWithRecord(
            @NotNull final Keyword keyword, @NotNull final RecordEntity record);

    KeywordLinks createKeywordAssociatedWithClass(
            @NotNull final Keyword keyword, @NotNull final Class klass);

    KeywordLinks findBySystemId(@NotNull final UUID systemId);

    KeywordLinks findAll();

    KeywordLinks updateKeywordBySystemId(
            @NotNull final UUID systemId,
            @NotNull final Keyword incomingKeyword);

    void deleteKeywordBySystemId(@NotNull final UUID systemId);

    KeywordTemplateLinks generateDefaultKeyword(@NotNull final UUID systemId);
}
