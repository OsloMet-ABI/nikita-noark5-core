package app.service.interfaces.secondary;

import app.domain.noark5.RecordEntity;
import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.domain.noark5.casehandling.secondary.CorrespondencePartInternal;
import app.domain.noark5.casehandling.secondary.CorrespondencePartPerson;
import app.domain.noark5.casehandling.secondary.CorrespondencePartUnit;
import app.webapp.model.PatchObjects;
import app.webapp.payload.links.casehandling.CorrespondencePartInternalLinks;
import app.webapp.payload.links.casehandling.CorrespondencePartLinks;
import app.webapp.payload.links.casehandling.CorrespondencePartPersonLinks;
import app.webapp.payload.links.casehandling.CorrespondencePartUnitLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface ICorrespondencePartService {

    CorrespondencePartPersonLinks updateCorrespondencePartPerson(
            @NotNull final UUID systemId, @NotNull final CorrespondencePartPerson incomingCorrespondencePart);

    CorrespondencePartUnitLinks updateCorrespondencePartUnit(
            @NotNull final UUID systemId, @NotNull final CorrespondencePartUnit incomingCorrespondencePart);

    CorrespondencePartInternalLinks updateCorrespondencePartInternal(
            @NotNull final UUID systemId, @NotNull final CorrespondencePartInternal incomingCorrespondencePart);

    CorrespondencePartUnitLinks createNewCorrespondencePartUnit(
            @NotNull final CorrespondencePartUnit correspondencePartUnit,
            @NotNull final RecordEntity record);

    CorrespondencePartInternalLinks createNewCorrespondencePartInternal(
            @NotNull final CorrespondencePartInternal correspondencePartUnit,
            @NotNull final RecordEntity record);

    CorrespondencePartPersonLinks createNewCorrespondencePartPerson(
            @NotNull final CorrespondencePartPerson correspondencePartPerson,
            @NotNull final RecordEntity record);

    CorrespondencePart findBySystemId(@NotNull final UUID systemId);

    CorrespondencePartInternalLinks
    findCorrespondencePartInternalBySystemId(@NotNull final UUID systemId);

    CorrespondencePartPersonLinks
    findCorrespondencePartPersonBySystemId(@NotNull final UUID systemId);

    CorrespondencePartUnitLinks
    findCorrespondencePartUnitBySystemId(@NotNull final UUID systemId);

    // All DELETE operations
    void deleteAllByOrganisation();

    void deleteCorrespondencePartUnit(@NotNull final UUID systemId);

    void deleteCorrespondencePartPerson(@NotNull final UUID systemId);

    void deleteCorrespondencePartInternal(@NotNull final UUID systemId);

    // All template operations
    CorrespondencePartUnitLinks generateDefaultCorrespondencePartUnit(
            @NotNull final UUID recordSystemId);

    CorrespondencePartPersonLinks generateDefaultCorrespondencePartPerson(
            @NotNull final UUID recordSystemId);

    CorrespondencePartInternalLinks generateDefaultCorrespondencePartInternal(
            @NotNull final UUID recordSystemId);

    CorrespondencePartLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final PatchObjects patchObjects);
}
