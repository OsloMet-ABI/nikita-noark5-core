package app.service.interfaces;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.casehandling.secondary.CorrespondencePartInternal;
import app.domain.noark5.casehandling.secondary.CorrespondencePartPerson;
import app.domain.noark5.casehandling.secondary.CorrespondencePartUnit;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.nationalidentifier.*;
import app.domain.noark5.secondary.*;
import app.webapp.model.PatchMerge;
import app.webapp.model.PatchObjects;
import app.webapp.payload.links.*;
import app.webapp.payload.links.casehandling.*;
import app.webapp.payload.links.nationalidentifier.*;
import app.webapp.payload.links.secondary.*;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IRecordService {

    // -- All CREATE operations
    RecordLinks save(@NotNull final RecordEntity record);

    DocumentDescriptionLinks createDocumentDescriptionAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final DocumentDescription documentDescription);

    CommentLinks createCommentAssociatedWithRecord
            (@NotNull final UUID systemId, Comment comment);

    StorageLocationLinks createStorageLocationAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final StorageLocation storageLocation);

    CrossReferenceLinks createCrossReferenceAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CrossReference crossReference);

    RecordLinks generateDefaultRecord(@NotNull final UUID systemId);

    CommentLinks generateDefaultComment(@NotNull final UUID systemId);

    CorrespondencePartInternalLinks generateDefaultCorrespondencePartInternal(
            @NotNull final UUID systemId);

    CorrespondencePartPersonLinks generateDefaultCorrespondencePartPerson(
            @NotNull final UUID systemId);

    CorrespondencePartUnitLinks generateDefaultCorrespondencePartUnit(
            @NotNull final UUID systemId);

    CommentLinks getCommentAssociatedWithRecord(@NotNull final UUID systemId);

    DocumentDescriptionLinks getDocumentDescriptionAssociatedWithRecord(
            @NotNull final UUID systemId);

    CorrespondencePartLinks getCorrespondencePartAssociatedWithRecord(
            @NotNull final UUID systemId);

    PartLinks getPartAssociatedWithRecord(@NotNull final UUID systemId);

    NationalIdentifierLinks getNationalIdentifierAssociatedWithRecord(
            @NotNull final UUID systemId);

    PartPersonLinks generateDefaultPartPerson(@NotNull final UUID systemId);

    PartUnitLinks generateDefaultPartUnit(@NotNull final UUID systemId);

    CorrespondencePartPersonLinks
    createCorrespondencePartPersonAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CorrespondencePartPerson correspondencePart);

    CorrespondencePartUnitLinks
    createCorrespondencePartUnitAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CorrespondencePartUnit correspondencePart);

    CorrespondencePartInternalLinks
    createCorrespondencePartInternalAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CorrespondencePartInternal correspondencePart);

    PartPersonLinks createPartPersonAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final PartPerson partPerson);

    PartUnitLinks createPartUnitAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final PartUnit partUnit);

    BuildingLinks createBuildingAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final Building building);

    CadastralUnitLinks createCadastralUnitAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final CadastralUnit cadastralUnit);

    DNumberLinks createDNumberAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final DNumber dNumber);

    PlanLinks createPlanAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final Plan plan);

    PositionLinks createPositionAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final Position position);

    SocialSecurityNumberLinks createSocialSecurityNumberAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final SocialSecurityNumber socialSecurityNumber);

    UnitLinks createUnitAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final Unit unit);

    KeywordLinks createKeywordAssociatedWithRecord(
            @NotNull final UUID systemId, @NotNull final Keyword keyword);

    // -- All READ operations

    FileLinks
    findFileAssociatedWithRecord(@NotNull final UUID systemId);

    ClassLinks
    findClassAssociatedWithRecord(@NotNull final UUID systemId);

    SeriesLinks
    findSeriesAssociatedWithRecord(@NotNull final UUID systemId);

    RecordLinks findBySystemId(@NotNull final UUID systemId);

    StorageLocationLinks findStorageLocationAssociatedWithRecord(
            @NotNull final UUID systemId);

    RecordLinks findAll();

    KeywordLinks findKeywordAssociatedWithRecord(
            @NotNull final UUID systemId);

    AuthorLinks associateAuthorWithRecord(
            @NotNull final UUID systemId,
            @NotNull final Author author);

    // -- All UPDATE operations
    RecordLinks handleUpdate(@NotNull final UUID systemId,
                             @NotNull final RecordEntity record);

    // -- All DELETE operations
    void deleteRecord(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    AuthorLinks findAllAuthorWithRecordBySystemId(
            @NotNull final UUID systemId);

    AuthorLinks generateDefaultAuthor(@NotNull final UUID systemId);

    BuildingLinks generateDefaultBuilding(@NotNull final UUID systemId);

    CadastralUnitLinks generateDefaultCadastralUnit(
            @NotNull final UUID systemId);

    DNumberLinks generateDefaultDNumber(@NotNull final UUID systemId);

    PlanLinks generateDefaultPlan(@NotNull final UUID systemId);

    PositionLinks generateDefaultPosition(@NotNull final UUID systemId);

    SocialSecurityNumberLinks generateDefaultSocialSecurityNumber(
            @NotNull final UUID systemId);

    UnitLinks generateDefaultUnit(@NotNull final UUID systemId);

    RecordLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final PatchObjects patchObjects);

    ScreeningMetadataLinks createScreeningMetadataAssociatedWithRecord(
            @NotNull final UUID systemId,
            @NotNull final Metadata screeningMetadata);

    ScreeningMetadataLinks getScreeningMetadataAssociatedWithRecord(
            @NotNull final UUID systemId);

    ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId);

    KeywordTemplateLinks generateDefaultKeyword(@NotNull final UUID systemId);

    StorageLocationLinks getDefaultStorageLocation(
            @NotNull final UUID systemId);

    CrossReferenceLinks getDefaultCrossReference(@NotNull final UUID systemId);

    RegistryEntryExpansionLinks generateDefaultValuesToExpanLinksRegistryEntry(
            @NotNull final UUID systemId);

    RecordNoteExpansionLinks generateDefaultValuesToExpanLinksRecordNote(
            @NotNull final UUID systemId);

    RecordNoteLinks expanLinksRecordNote(
            @NotNull final UUID systemId,
            @NotNull final PatchMerge patchMerge);

    RegistryEntryLinks expanLinksRegistryEntry(
            @NotNull final UUID systemId,
            @NotNull final PatchMerge patchMerge);

    CrossReferenceLinks findCrossReferenceAssociatedWithRecord(
            @NotNull UUID systemId);
}
