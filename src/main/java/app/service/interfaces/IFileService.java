package app.service.interfaces;


import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.bsm.BSMBase;
import app.domain.noark5.metadata.Metadata;
import app.domain.noark5.nationalidentifier.*;
import app.domain.noark5.secondary.*;
import app.webapp.model.PatchMerge;
import app.webapp.model.PatchObjects;
import app.webapp.payload.links.ClassLinks;
import app.webapp.payload.links.FileLinks;
import app.webapp.payload.links.RecordLinks;
import app.webapp.payload.links.SeriesLinks;
import app.webapp.payload.links.casehandling.CaseFileExpansionLinks;
import app.webapp.payload.links.casehandling.CaseFileLinks;
import app.webapp.payload.links.nationalidentifier.*;
import app.webapp.payload.links.secondary.*;
import jakarta.validation.constraints.NotNull;

import java.util.List;
import java.util.UUID;

public interface IFileService {

    // -- All CREATE operations
    FileLinks createFile(@NotNull final File file);

    CaseFileLinks expanLinksCaseFile(
            @NotNull final UUID systemId,
            @NotNull final PatchMerge patchMerge);

    FileLinks createFileAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final File file);

    CommentLinks createCommentAssociatedWithFile
            (@NotNull final UUID systemId,
             @NotNull final Comment comment);

    PartPersonLinks createPartPersonAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final PartPerson partPerson);

    PartUnitLinks createPartUnitAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final PartUnit partUnit);

    KeywordLinks createKeywordAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final Keyword keyword);

    FileLinks save(@NotNull final File file);

    RecordLinks createRecordAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final RecordEntity record);

    StorageLocationLinks createStorageLocationAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final StorageLocation storageLocation);

    // -- All READ operations

    PartPersonLinks generateDefaultPartPerson(@NotNull final UUID systemId);

    PartUnitLinks generateDefaultPartUnit(@NotNull final UUID systemId);

    CaseFileExpansionLinks generateDefaultValuesToExpanLinksCaseFile(
            @NotNull final UUID systemId);

    BuildingLinks
    createBuildingAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull Building building);

    CadastralUnitLinks
    createCadastralUnitAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final CadastralUnit cadastralUnit);

    DNumberLinks
    createDNumberAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final DNumber dNumber);

    PlanLinks
    createPlanAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final Plan plan);

    PositionLinks
    createPositionAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final Position position);

    SocialSecurityNumberLinks
    createSocialSecurityNumberAssociatedWithFile
            (@NotNull final UUID systemId,
             @NotNull final SocialSecurityNumber socialSecurityNumber);

    UnitLinks
    createUnitAssociatedWithFile(
            @NotNull final UUID systemId, @NotNull final Unit unit);

    FileLinks findAllChildren(@NotNull final UUID systemId);

    RecordLinks findAllRecords(@NotNull final UUID systemId);

    FileLinks findBySystemId(@NotNull final UUID systemId);

    FileLinks findAll();

    ClassLinks
    findClassAssociatedWithFile(@NotNull final UUID systemId);

    SeriesLinks
    findSeriesAssociatedWithFile(@NotNull final UUID systemId);

    CommentLinks getCommentAssociatedWithFile(
            @NotNull final UUID systemId);

    PartLinks getPartAssociatedWithFile(@NotNull final UUID systemId);

    KeywordLinks findKeywordAssociatedWithFile(@NotNull final UUID systemId);

    CrossReferenceLinks findCrossReferenceAssociatedWithFile(
            @NotNull final UUID systemID);

    StorageLocationLinks getStorageLocationAssociatedWithFile(
            @NotNull final UUID systemId);

    NationalIdentifierLinks getNationalIdentifierAssociatedWithFile(
            @NotNull final UUID systemId);

    ScreeningMetadataLinks getScreeningMetadataAssociatedWithFile(
            @NotNull final UUID systemId);

    CommentLinks generateDefaultComment(@NotNull final UUID systemId);

    BuildingLinks generateDefaultBuilding(@NotNull final UUID systemId);

    CadastralUnitLinks generateDefaultCadastralUnit(@NotNull final UUID systemId);

    DNumberLinks generateDefaultDNumber(@NotNull final UUID systemId);

    PlanLinks generateDefaultPlan(@NotNull final UUID systemId);

    PositionLinks generateDefaultPosition(@NotNull final UUID systemId);

    Object associateBSM(@NotNull final UUID systemId,
                        @NotNull final List<BSMBase> bsm);

    SocialSecurityNumberLinks generateDefaultSocialSecurityNumber(
            @NotNull final UUID systemId);

    UnitLinks generateDefaultUnit(@NotNull final UUID systemId);

    // -- All UPDATE operations
    FileLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final File file);

    // -- All DELETE operations
    void deleteEntity(@NotNull final UUID systemId);

    FileLinks generateDefaultFile(@NotNull final UUID systemId);

    void deleteAllByOrganisation();

    FileLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final PatchObjects patchObjects);

    ScreeningMetadataLinks createScreeningMetadataAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final Metadata screeningMetadata);

    CrossReferenceLinks createCrossReferenceAssociatedWithFile(
            @NotNull final UUID systemId,
            @NotNull final CrossReference crossReference);

    ScreeningMetadataLinks getDefaultScreeningMetadata(
            @NotNull final UUID systemId);

    KeywordTemplateLinks generateDefaultKeyword(@NotNull final UUID systemId);

    StorageLocationLinks getDefaultStorageLocation(
            @NotNull final UUID systemId);

    CrossReferenceLinks getDefaultCrossReference(@NotNull final UUID systemId);

}
