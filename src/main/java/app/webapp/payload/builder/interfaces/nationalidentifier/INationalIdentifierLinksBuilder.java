package app.webapp.payload.builder.interfaces.nationalidentifier;

import app.webapp.payload.builder.interfaces.ILinksBuilder;

public interface INationalIdentifierLinksBuilder
        extends ILinksBuilder {
}
