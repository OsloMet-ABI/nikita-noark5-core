package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.secondary.IAuthorEntity;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

public interface IAuthorLinksBuilder
        extends ILinksBuilder {

    void addRecordEntity(IAuthorEntity entity,
                         ILinksNoarkObject linksNoarkObject);

    void addDocumentDescription(IAuthorEntity entity,
                                ILinksNoarkObject linksNoarkObject);
}
