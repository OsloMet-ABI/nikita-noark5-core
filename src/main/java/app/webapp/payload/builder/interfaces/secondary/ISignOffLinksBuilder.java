package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.secondary.ISignOffEntity;
import app.webapp.payload.builder.interfaces.ISystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

public interface ISignOffLinksBuilder
        extends ISystemIdLinksBuilder {

    void addRegistryEntry(ISignOffEntity signOff,
                          ILinksNoarkObject linksNoarkObject);

    void addReferenceSignedOffRegistryEntry
            (ISignOffEntity entity, ILinksNoarkObject linksNoarkObject);

    void addReferenceSignedOffCorrespondenceParty
            (ISignOffEntity entity, ILinksNoarkObject linksNoarkObject);

    void addSignOffMethod(ISignOffEntity signOff,
                          ILinksNoarkObject linksNoarkObject);
}
