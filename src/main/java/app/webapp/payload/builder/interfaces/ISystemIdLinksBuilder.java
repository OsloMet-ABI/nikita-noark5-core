package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler
 */
public interface ISystemIdLinksBuilder
        extends ILinksBuilder {

    void addSelfLink(ISystemId entity,
                     ILinksNoarkObject linksNoarkObject);

    void addEntityLinks(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addEntityLinksOnCreate(ISystemId entity,
                                ILinksNoarkObject linksNoarkObject);

    void addEntityLinksOnTemplate(ISystemId entity,
                                  ILinksNoarkObject linksNoarkObject);

    void addEntityLinksOnRead(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addDocumentMedium(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject);

    void addEventLogLinks(ISystemId entity, ILinksNoarkObject linksNoarkObject);
}
