package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Describe Hateoas links handler for Series
 */
public interface ISeriesLinksBuilder
        extends ILinksBuilder {

    void addNewRegistration(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addNewFile(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewCaseFile(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addNewClassificationSystem(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject);

    void addSeriesSuccessor(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addSeriesPrecursor(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addRegistration(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addFile(ISystemId entity,
                 ILinksNoarkObject linksNoarkObject);

    void addCaseFile(ISystemId entity,
                     ILinksNoarkObject linksNoarkObject);

    void addClassificationSystem(ISystemId entity,
                                 ILinksNoarkObject linksNoarkObject);

    void addDeletionType(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addClassifiedCodeMetadata(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addFonds(ISystemId entity,
                  ILinksNoarkObject linksNoarkObject);

    void addSeriesStatus(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addNewStorageLocation(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);

    void addStorageLocation(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addAccessRestriction(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningDocument(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadata(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadataLocal(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addNewScreeningMetadataLocal(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject);
}
