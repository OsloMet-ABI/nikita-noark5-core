package app.webapp.payload.builder.interfaces.admin;

import app.webapp.payload.builder.interfaces.ILinksBuilder;

public interface IReadLogLinksBuilder
        extends ILinksBuilder {
}
