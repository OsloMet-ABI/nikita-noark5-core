package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler for FondsCreator
 */
public interface IFondsCreatorLinksBuilder
        extends ILinksBuilder {

    void addFonds(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addNewFonds(ISystemId entity, ILinksNoarkObject linksNoarkObject);

}
