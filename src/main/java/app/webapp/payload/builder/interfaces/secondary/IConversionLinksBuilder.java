package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.secondary.IConversionEntity;
import app.webapp.payload.builder.interfaces.ISystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

public interface IConversionLinksBuilder
        extends ISystemIdLinksBuilder {

    void addDocumentObject(IConversionEntity conversion,
                           ILinksNoarkObject linksNoarkObject);

    void addFormat(IConversionEntity conversion,
                   ILinksNoarkObject linksNoarkObject);
}
