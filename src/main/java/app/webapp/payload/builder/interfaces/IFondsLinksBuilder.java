package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Describe Hateoas links handler
 */
public interface IFondsLinksBuilder
        extends ILinksBuilder {

    void addFondsCreator(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addSeries(ISystemId entity,
                   ILinksNoarkObject linksNoarkObject);

    void addFonds(ISystemId entity,
                  ILinksNoarkObject linksNoarkObject);

    void addParentFonds(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addNewSubFonds(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addNewFondsCreator(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addSubFonds(ISystemId entity,
                     ILinksNoarkObject linksNoarkObject);

    void addFondsStatus(ISystemId entity,
                        ILinksNoarkObject linksNoarkObject);

    void addNewSeries(ISystemId entity,
                      ILinksNoarkObject linksNoarkObject);

    void addStorageLocation(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);

    void addNewStorageLocation(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);
}
