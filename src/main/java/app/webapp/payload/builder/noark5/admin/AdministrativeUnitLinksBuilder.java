package app.webapp.payload.builder.noark5.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.admin.IAdministrativeUnitLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Used to add AdministrativeUnitLinks links with AdministrativeUnit
 * specific information
 * <p>
 * Not sure if there is a difference in what should be returned of links for
 * various CRUD operations so keeping them
 * separate calls at the moment.
 */
@Component("administrativeUnitLinksBuilder")
public class AdministrativeUnitLinksBuilder
        extends SystemIdLinksBuilder
        implements IAdministrativeUnitLinksBuilder {

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        addUser(entity, linksNoarkObject);
        addAdministrativeUnit(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnCreate(ISystemId entity,
                                       ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnRead(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject) {
        super.addEntityLinksOnTemplate(entity, linksNoarkObject);
        addEntityLinks(entity, linksNoarkObject);
    }

    public void addChildAdministrativeUnit(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_ADMIN + SLASH + ADMINISTRATIVE_UNIT + SLASH + entity.getSystemIdAsString() + SLASH + NEW_ADMINISTRATIVE_UNIT,
                REL_ADMIN_ADMINISTRATIVE_UNIT, false));
    }

    public void addAdministrativeUnit(INoarkEntity entity,
                                      ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_ADMIN + SLASH + ADMINISTRATIVE_UNIT,
                REL_ADMIN_USER, true));
    }

    public void addUser(INoarkEntity entity,
                        ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_ADMIN + SLASH + USER,
                REL_ADMIN_USER, true));
    }
}
