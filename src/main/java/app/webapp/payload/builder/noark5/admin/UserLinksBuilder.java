package app.webapp.payload.builder.noark5.admin;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.admin.IUserLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Created by tsodring on 06/08/17.
 * <p>
 * Used to add UserLinks links with User specific information
 * <p>
 * Not sure if there is a difference in what should be returned of links for
 * various CRUD operations so keeping them separate calls at the moment.
 */
@Component("userLinksBuilder")
public class UserLinksBuilder
        extends SystemIdLinksBuilder
        implements IUserLinksBuilder {

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        addAdministrativeUnit(entity, linksNoarkObject);
        addNewAdministrativeUnit(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnCreate(ISystemId entity,
                                       ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnRead(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    public void addNewAdministrativeUnit(ISystemId entity,
                                         ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_ADMIN + SLASH + USER + SLASH + entity.getSystemIdAsString() + SLASH + NEW_ADMINISTRATIVE_UNIT,
                REL_ADMIN_NEW_ADMINISTRATIVE_UNIT, false));
    }

    public void addAdministrativeUnit(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_ADMIN + SLASH + USER + SLASH + entity.getSystemIdAsString() + SLASH + ADMINISTRATIVE_UNIT,
                REL_ADMIN_ADMINISTRATIVE_UNIT, false));
    }
}
