package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.DocumentObject;
import app.webapp.payload.builder.interfaces.IDocumentObjectLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Used to add DocumentObjectLinks links with DocumentObject specific
 * information
 */
@Component("documentObjectLinksBuilder")
public class DocumentObjectLinksBuilder
        extends SystemIdLinksBuilder
        implements IDocumentObjectLinksBuilder {

    public DocumentObjectLinksBuilder() {
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {

        // links for primary entities
        addDocumentDescription(entity, linksNoarkObject);
        // links for secondary entities
        addConversion(entity, linksNoarkObject);
        addNewConversion(entity, linksNoarkObject);
        addReferenceDocumentFile(entity, linksNoarkObject);
        // links for metadata entities
        addVariantFormat(entity, linksNoarkObject);
        addFormat(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(ISystemId entity,
                                         ILinksNoarkObject linksNoarkObject) {
        addVariantFormat(entity, linksNoarkObject);
        addFormat(entity, linksNoarkObject);
    }

    @Override
    public void addConversion(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_OBJECT + SLASH + entity.getSystemIdAsString() + SLASH + CONVERSION,
                REL_FONDS_STRUCTURE_CONVERSION, false));
    }

    @Override
    public void addNewConversion(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_OBJECT + SLASH + entity.getSystemIdAsString() + SLASH + NEW_CONVERSION,
                REL_FONDS_STRUCTURE_NEW_CONVERSION, false));
    }

    /**
     * Create a REL/HREF pair for the parent documentDescription associated
     * with the given DocumentObject
     * <p>
     * "../api/arkivstruktur/dokumentbeskrivelse/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/registrering/"
     *
     * @param entity           documentObject
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addDocumentDescription(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_DESCRIPTION + SLASH + getDocumentDescriptionSystemId(entity),
                REL_FONDS_STRUCTURE_DOCUMENT_DESCRIPTION));
    }

    @Override
    public void addReferenceDocumentFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_DOCUMENT_OBJECT + SLASH + entity.getSystemIdAsString() + SLASH + REFERENCE_FILE,
                REL_FONDS_STRUCTURE_DOCUMENT_FILE, false));
    }

    @Override
    public void addVariantFormat(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + VARIANT_FORMAT,
                REL_METADATA_VARIANT_FORMAT, false));
    }

    @Override
    public void addFormat(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + FORMAT,
                REL_METADATA_FORMAT, false));
    }

    // Internal helper methods

    /**
     * Cast the ISystemId entity to a DocumentObject and retrieve the
     * systemId of the associated DocumentDescription
     *
     * @param entity the DocumentObject
     * @return systemId of the associated DocumentDescription
     */
    private String getDocumentDescriptionSystemId(ISystemId entity) {
        if (((DocumentObject) entity).getReferenceDocumentDescription()
                != null) {
            return ((DocumentObject) entity).getReferenceDocumentDescription()
                    .getSystemIdAsString();
        }
        return null;
    }
}
