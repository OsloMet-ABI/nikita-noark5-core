package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.File;
import app.webapp.payload.builder.interfaces.IFileLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Used to add FileLinks links with File specific information
 */
@Component("fileLinksBuilder")
public class FileLinksBuilder
        extends SystemIdLinksBuilder
        implements IFileLinksBuilder {

    public FileLinksBuilder() {
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        addFileLinks(entity, linksNoarkObject);

        addExpanLinksCaseFile(entity, linksNoarkObject);
        //addExpanLinksMeetingFile(entity, linksNoarkObject);
        addNewSubFile(entity, linksNoarkObject);
    }

    protected void addFileLinks(ISystemId entity,
                                ILinksNoarkObject linksNoarkObject) {
        addEndFile(entity, linksNoarkObject);
        addSeries(entity, linksNoarkObject);
        addSubFile(entity, linksNoarkObject);
        addRecordEntity(entity, linksNoarkObject);
        addNewRecord(entity, linksNoarkObject);
        addComment(entity, linksNoarkObject);
        addNewComment(entity, linksNoarkObject);
        addCrossReference(entity, linksNoarkObject);
        addNewCrossReference(entity, linksNoarkObject);
        addClass(entity, linksNoarkObject);
        addNewClass(entity, linksNoarkObject);
        addReferenceSeries(entity, linksNoarkObject);
        addNewReferenceSeries(entity, linksNoarkObject);
        addReferenceSecondaryClassification(entity, linksNoarkObject);
        addNewReferenceSecondaryClassification(entity, linksNoarkObject);

        addPart(entity, linksNoarkObject);
        addNewPartPerson(entity, linksNoarkObject);
        addNewPartUnit(entity, linksNoarkObject);

        addNewKeyword(entity, linksNoarkObject);
        addKeyword(entity, linksNoarkObject);
        addClassifiedCodeMetadata(entity, linksNoarkObject);
        addNewStorageLocation(entity, linksNoarkObject);
        addStorageLocation(entity, linksNoarkObject);
        addAccessRestriction(entity, linksNoarkObject);
        addScreeningDocument(entity, linksNoarkObject);
        addScreeningMetadata(entity, linksNoarkObject);
        addScreeningMetadataLocal(entity, linksNoarkObject);
        addNewScreeningMetadataLocal(entity, linksNoarkObject);
        addNewBuilding(entity, linksNoarkObject);
        addNewCadastralUnit(entity, linksNoarkObject);
        addNewDNumber(entity, linksNoarkObject);
        addNewPlan(entity, linksNoarkObject);
        addNewPosition(entity, linksNoarkObject);
        addNewSocialSecurityNumber(entity, linksNoarkObject);
        addNewUnit(entity, linksNoarkObject);
        addNationalIdentifier(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject) {
        super.addEntityLinksOnTemplate(entity, linksNoarkObject);
        addDocumentMedium(entity, linksNoarkObject);
        addMetadataFileType(entity, linksNoarkObject);
        addClassifiedCodeMetadata(entity, linksNoarkObject);
        addAccessRestriction(entity, linksNoarkObject);
        addScreeningDocument(entity, linksNoarkObject);
        addScreeningMetadata(entity, linksNoarkObject);
    }

    /**
     * Create a REL/HREF pair for the parent Series associated with the given
     * File. Checks if the File is actually associated with a Series. Note every
     * File should actually be associated with a Series, but we are not doing
     * that check here.
     * <p>
     * "../api/arkivstruktur/arkivdel/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivdel/"
     *
     * @param entity           file
     * @param linksNoarkObject linksFile
     */
    @Override
    public void addSeries(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject) {
        File file = getFile(entity);
        if (file.getReferenceSeries() != null) {
            linksNoarkObject.addLink(entity,
                    new Link(getOutgoingAddress() + HREF_BASE_SERIES + SLASH +
                            file.getReferenceSeries().getSystemId(),
                            REL_FONDS_STRUCTURE_SERIES));
        }
    }

    /**
     * Create a REL/HREF pair for the parent Class associated with the given
     * File. Checks if the File is actually associated with a Class.
     * <p>
     * "../api/arkivstruktur/klasse/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/klasse/"
     *
     * @param entity           file
     * @param linksNoarkObject linksFile
     */
    @Override
    public void addClass(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject) {
        File file = getFile(entity);
        if (file.getReferenceClass() != null) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CLASS + SLASH + file.getReferenceClass().getSystemId(),
                    REL_FONDS_STRUCTURE_CLASS));
        }
    }

    @Override
    public void addEndFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + FILE_END,
                REL_FONDS_STRUCTURE_END_FILE,
                false));
    }

    @Override
    public void addExpanLinksCaseFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + FILE_EXPAND_TO_CASE_FILE,
                REL_FONDS_STRUCTURE_EXPAND_TO_CASE_FILE, false));
    }

    @Override
    public void addExpanLinksMeetingFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + FILE_EXPAND_TO_MEETING_FILE,
                REL_FONDS_STRUCTURE_EXPAND_TO_MEETING_FILE, false));
    }

    @Override
    public void addRecordEntity(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + RECORD,
                REL_FONDS_STRUCTURE_RECORD, false));
    }

    @Override
    public void addNewRecord(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_RECORD,
                REL_FONDS_STRUCTURE_NEW_RECORD, false));
    }

    @Override
    public void addComment(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + COMMENT,
                REL_FONDS_STRUCTURE_COMMENT, false));
    }

    @Override
    public void addNewComment(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_COMMENT,
                REL_FONDS_STRUCTURE_NEW_COMMENT, false));
    }

    @Override
    public void addParentFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        File file = getFile(entity).getReferenceParentFile();
        if (file != null) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FILE + SLASH + file.getSystemId(),
                    REL_FONDS_STRUCTURE_PARENT_FILE));
        }
    }

    @Override
    public void addSubFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + SUB_FILE,
                REL_FONDS_STRUCTURE_SUB_FILE, false));
    }

    @Override
    public void addNewSubFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_FILE,
                REL_FONDS_STRUCTURE_NEW_FILE, false));
    }

    /**
     * Create a REL/HREF pair for the list of Part objects associated with the
     * given File.
     * <p>
     * "../api/arkivstruktur/mappe/1234/part"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/part/"
     *
     * @param entity             file
     * @param linksNoarkObject linksFile
     */
    @Override
    public void addPart(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity,
                new Link(getOutgoingAddress() + HREF_BASE_FILE + SLASH +
                        entity.getSystemId() + SLASH + PART,
                        REL_FONDS_STRUCTURE_PART, true));
    }

    @Override
    public void addNewPartPerson(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity,
                new Link(getOutgoingAddress() + HREF_BASE_FILE + SLASH +
                        entity.getSystemId() + SLASH + NEW_PART_PERSON,
                        REL_FONDS_STRUCTURE_NEW_PART_PERSON));
    }

    @Override
    public void addNewPartUnit(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity,
                new Link(getOutgoingAddress() + HREF_BASE_FILE + SLASH +
                        entity.getSystemId() + SLASH + NEW_PART_UNIT,
                        REL_FONDS_STRUCTURE_NEW_PART_UNIT));
    }

    @Override
    public void addCrossReference(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + CROSS_REFERENCE,
                REL_FONDS_STRUCTURE_CROSS_REFERENCE, false));
    }

    @Override
    public void addNewCrossReference(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_CROSS_REFERENCE,
                REL_FONDS_STRUCTURE_NEW_CROSS_REFERENCE, false));
    }

    @Override
    public void addNewClass(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_CLASS,
                REL_FONDS_STRUCTURE_NEW_CLASS, false));
    }

    @Override
    public void addReferenceSeries(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + SERIES,
                REL_FONDS_STRUCTURE_REFERENCE_SERIES, false));
    }

    @Override
    public void addNewReferenceSeries(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + SERIES,
                REL_FONDS_STRUCTURE_NEW_REFERENCE_SERIES, false));
    }

    @Override
    public void addReferenceSecondaryClassification(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + SECONDARY_CLASSIFICATION,
                REL_CASE_HANDLING_SECONDARY_CLASSIFICATION, false));
    }

    @Override
    public void addNewReferenceSecondaryClassification(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + NEW_SECONDARY_CLASSIFICATION_SYSTEM,
                REL_CASE_HANDLING_NEW_SECONDARY_CLASSIFICATION, false));
    }

    public void addMetadataFileType(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + FILE_TYPE,
                REL_METADATA_FILE_TYPE, false));
    }

    @Override
    public void addKeyword(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        if (((File) entity).getReferenceKeyword().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FILE + SLASH + entity.getSystemIdAsString() +
                    SLASH + KEYWORD, REL_FONDS_STRUCTURE_KEYWORD, true));
        }
    }

    @Override
    public void addNewKeyword(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemIdAsString() + SLASH + NEW_KEYWORD,
                REL_FONDS_STRUCTURE_NEW_KEYWORD, false));
    }

    @Override

    public void addClassifiedCodeMetadata(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + CLASSIFIED_CODE,
                REL_METADATA_CLASSIFIED_CODE));
    }

    public void addStorageLocation(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        if (((File) entity).getReferenceStorageLocation().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FILE + SLASH + entity.getSystemIdAsString() +
                    SLASH + STORAGE_LOCATION,
                    REL_FONDS_STRUCTURE_STORAGE_LOCATION, true));
        }
    }

    @Override
    public void addNewStorageLocation(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemIdAsString() +
                SLASH + NEW_STORAGE_LOCATION,
                REL_FONDS_STRUCTURE_NEW_STORAGE_LOCATION, false));

    }

    @Override
    public void addAccessRestriction(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + ACCESS_RESTRICTION,
                REL_METADATA_ACCESS_RESTRICTION, false));
    }

    @Override
    public void addScreeningDocument(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SCREENING_DOCUMENT,
                REL_METADATA_SCREENING_DOCUMENT, false));
    }

    @Override
    public void addScreeningMetadata(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SCREENING_METADATA,
                REL_METADATA_SCREENING_METADATA));
    }

    @Override
    public void addScreeningMetadataLocal(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject) {
        if (null != ((File) entity).getReferenceScreening()) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FONDS_STRUCTURE + SLASH + FILE + SLASH +
                    entity.getSystemId() + SLASH + SCREENING_METADATA,
                    REL_FONDS_STRUCTURE_SCREENING_METADATA));
        }
    }

    @Override
    public void addNewScreeningMetadataLocal(ISystemId entity,
                                             ILinksNoarkObject linksNoarkObject) {
        if (null != ((File) entity).getReferenceScreening()) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FONDS_STRUCTURE + SLASH + FILE + SLASH +
                    entity.getSystemId() + SLASH + NEW_SCREENING_METADATA,
                    REL_FONDS_STRUCTURE_NEW_SCREENING_METADATA));
        }
    }

    @Override
    public void addNewBuilding(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH +
                NEW_BUILDING, REL_FONDS_STRUCTURE_NEW_BUILDING));
    }

    @Override
    public void addNewCadastralUnit(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH +
                NEW_CADASTRAL_UNIT, REL_FONDS_STRUCTURE_NEW_CADASTRAL_UNIT));
    }

    @Override
    public void addNewDNumber(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH +
                NEW_D_NUMBER, REL_FONDS_STRUCTURE_NEW_D_NUMBER));
    }

    @Override
    public void addNewPlan(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH +
                NEW_PLAN, REL_FONDS_STRUCTURE_NEW_PLAN));
    }

    @Override
    public void addNewPosition(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH +
                NEW_POSITION, REL_FONDS_STRUCTURE_NEW_POSITION));
    }

    @Override
    public void addNewSocialSecurityNumber(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH +
                NEW_SOCIAL_SECURITY_NUMBER,
                REL_FONDS_STRUCTURE_NEW_SOCIAL_SECURITY_NUMBER));
    }

    @Override
    public void addNewUnit(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH +
                NEW_NI_UNIT, REL_FONDS_STRUCTURE_NEW_NI_UNIT));
    }

    @Override
    public void addNationalIdentifier(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FILE + SLASH + entity.getSystemId() + SLASH + NATIONAL_IDENTIFIER,
                REL_FONDS_STRUCTURE_NATIONAL_IDENTIFIER));
    }

    /**
     * Cast the ISystemId entity to a File
     *
     * @param entity the File
     * @return a File object
     */
    private File getFile(@NotNull ISystemId entity) {
        return (File) entity;
    }
}
