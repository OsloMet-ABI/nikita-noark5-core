package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;
import org.springframework.stereotype.Component;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Used to add PartLinks links with Part
 * specific information
 */
@Component("partPersonLinksBuilder")
public class PartPersonLinksBuilder
        extends PartLinksBuilder {

    @Override
    public void addEntityLinksOnTemplate(ISystemId entity,
                                         ILinksNoarkObject
                                                 linksNoarkObject) {
        super.addEntityLinksOnTemplate(entity, linksNoarkObject);
    }


}
