package app.webapp.payload.deserializers.noark5.admin;

import app.domain.noark5.admin.User;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IFinalisedDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming User JSON object.
 */
public class UserDeserializer
        extends SystemIdEntityDeserializer<User>
        implements IFinalisedDeserializer {
    @Override
    public User deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
               User user = new User();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        JsonNode currentNode = objectNode.get(SYSTEM_ID);
        if (null != currentNode) {
            user.setSystemId(UUID.fromString(currentNode.textValue()));
            objectNode.remove(SYSTEM_ID);
        }
        // Deserialize username
        currentNode = objectNode.get(USER_NAME);
        if (null != currentNode) {
            user.setUsername(currentNode.textValue());
            objectNode.remove(USER_NAME);
        }
        // Deserialize firstname
        currentNode = objectNode.get(FIRST_NAME);
        if (null != currentNode) {
            user.setFirstname(currentNode.textValue());
            objectNode.remove(FIRST_NAME);
        }
        // Deserialize lastname
        currentNode = objectNode.get(SECOND_NAME);
        if (null != currentNode) {
            user.setLastname(currentNode.textValue());
            objectNode.remove(SECOND_NAME);
        }
        deserializeNoarkCreateEntity(user, objectNode, errors);
        deserializeNoarkFinaliseEntity(user, objectNode, errors);

        check_payload_at_end(errors, objectNode);
        return user;
    }

    @Override
    protected String getType() {
        return USER;
    }
}