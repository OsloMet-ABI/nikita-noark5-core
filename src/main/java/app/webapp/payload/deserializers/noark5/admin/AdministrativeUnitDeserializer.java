package app.webapp.payload.deserializers.noark5.admin;

import app.domain.noark5.admin.AdministrativeUnit;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IFinalisedDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming AdministrativeUnit JSON object.
 * <p>
 */
public class AdministrativeUnitDeserializer
        extends SystemIdEntityDeserializer<AdministrativeUnit>
        implements IFinalisedDeserializer {
    @Override
    public AdministrativeUnit deserialize(JsonParser jsonParser,
                                          DeserializationContext dc)
            throws IOException {

        AdministrativeUnit administrativeUnit = new AdministrativeUnit();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        // Deserialize general properties
        deserializeNoarkSystemIdEntity(administrativeUnit, objectNode);
        deserializeNoarkCreateEntity(administrativeUnit, objectNode, errors);
        deserializeNoarkFinaliseEntity(administrativeUnit, objectNode, errors);

        // Deserialize administrativeUnitStatus
        JsonNode currentNode = objectNode.get(ADMINISTRATIVE_UNIT_STATUS);
        if (currentNode != null) {
            administrativeUnit.setAdministrativeUnitStatus(currentNode.textValue());
            objectNode.remove(ADMINISTRATIVE_UNIT_STATUS);
        }

        // Deserialize administrativeUnitName
        currentNode = objectNode.get(ADMINISTRATIVE_UNIT_NAME);
        if (currentNode != null) {
            administrativeUnit.setAdministrativeUnitName(currentNode.textValue());
            objectNode.remove(ADMINISTRATIVE_UNIT_NAME);
        }

        // Deserialize shortName
        currentNode = objectNode.get(SHORT_NAME);
        if (currentNode != null) {
            administrativeUnit.setShortName(currentNode.textValue());
            objectNode.remove(SHORT_NAME);
        }

        // Deserialize referenceToParent
        currentNode = objectNode.get(ADMINISTRATIVE_UNIT_PARENT_REFERENCE);
        if (currentNode != null) {
            AdministrativeUnit parentAdministrativeUnit = administrativeUnit.
                    getParentAdministrativeUnit();
            // Will it not always be null??
            if (parentAdministrativeUnit == null) {
                parentAdministrativeUnit = new AdministrativeUnit();
                parentAdministrativeUnit.setSystemId(UUID.fromString(
                        currentNode.textValue()));
                parentAdministrativeUnit.getReferenceChildAdministrativeUnit().
                        add(administrativeUnit);
            }
            objectNode.remove(ADMINISTRATIVE_UNIT_PARENT_REFERENCE);
        }


        check_payload_at_end(errors, objectNode);

        return administrativeUnit;
    }

    @Override
    protected String getType() {
        return ADMINISTRATIVE_UNIT;
    }
}
