package app.webapp.payload.deserializers.noark5.casehandling;

import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.domain.noark5.casehandling.secondary.CorrespondencePartUnit;
import app.webapp.payload.deserializers.noark5.secondary.CorrespondencePartDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_UNIT;

/**
 * Deserialize an incoming CorrespondencePart JSON object.
 */
public class CorrespondencePartUnitDeserializer<P extends CorrespondencePart>
        extends CorrespondencePartDeserializer<CorrespondencePart> {

    @Override
    public CorrespondencePartUnit deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               CorrespondencePartUnit correspondencePartUnit = new CorrespondencePartUnit();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeNoarkSystemIdEntity(correspondencePartUnit, objectNode);
        deserializeCorrespondencePartUnitEntity(correspondencePartUnit, objectNode, errors);
        deserializeBSM(objectNode, correspondencePartUnit);
        check_payload_at_end(errors, objectNode);
        return correspondencePartUnit;
    }


    @Override
    protected String getType() {
        return CORRESPONDENCE_PART_UNIT;
    }
}