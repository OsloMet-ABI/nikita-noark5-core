package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.metadata.AssociatedWithRecordAs;
import app.domain.noark5.metadata.DocumentStatus;
import app.domain.noark5.metadata.DocumentType;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming DocumentDescription JSON object.
 */
public class DocumentDescriptionDeserializer
        extends SystemIdEntityDeserializer<DocumentDescription>
        implements IClassifiedDeserializer, IDeletionDeserializer, IDisposalDeserializer,
        IDisposalUndertakenDeserializer, IDocumentMediumDeserializer, IElectronicSignatureDeserializer,
        IScreeningDeserializer, ITitleAndDescriptionDeserializer {
    @Override
    public DocumentDescription deserialize(
            JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
               DocumentDescription documentDescription = new DocumentDescription();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general record properties
        deserializeSystemIdEntity(documentDescription, objectNode, errors);
        deserializeNoarkTitleDescriptionEntity(documentDescription,
                objectNode, errors);
        // Deserialize documentType
        DocumentType documentType = (DocumentType)
                deserializeMetadataValue(objectNode,
                        DOCUMENT_DESCRIPTION_DOCUMENT_TYPE,
                        new DocumentType(),
                        errors, true);
        documentDescription.setDocumentType(documentType);
        // Deserialize documentStatus
        DocumentStatus documentStatus = (DocumentStatus)
                deserializeMetadataValue(objectNode,
                        DOCUMENT_DESCRIPTION_STATUS,
                        new DocumentStatus(),
                        errors, true);
        documentDescription.setDocumentStatus(documentStatus);
        // Deserialize associatedWithRecordAs
        AssociatedWithRecordAs associatedWithRecordAs = (AssociatedWithRecordAs)
                deserializeMetadataValue(objectNode,
                        DOCUMENT_DESCRIPTION_ASSOCIATED_WITH_RECORD_AS,
                        new AssociatedWithRecordAs(),
                        errors, true);
        documentDescription.setAssociatedWithRecordAs(associatedWithRecordAs);
        // Deserialize documentNumber
        documentDescription.setDocumentNumber
                (deserializeInteger(DOCUMENT_DESCRIPTION_DOCUMENT_NUMBER,
                        objectNode, errors, false));
        // Deserialize associationDate
        documentDescription.setAssociationDate(
                deserializeDateTime(DOCUMENT_DESCRIPTION_ASSOCIATED_DATE,
                        objectNode, errors));
        // Deserialize associatedBy
        JsonNode currentNode =
                objectNode.get(DOCUMENT_DESCRIPTION_ASSOCIATED_BY);
        if (null != currentNode) {
            documentDescription.setAssociatedBy(currentNode.textValue());
            objectNode.remove(DOCUMENT_DESCRIPTION_ASSOCIATED_BY);
        }
        // Deserialize storageLocation, not using
        // DeserializeStorageLocation from CommonUtils, as it is a
        // single string here, while deserializeStorageLocation()
        // expect a list of strings.
        currentNode = objectNode.get(STORAGE_LOCATION);
        if (null != currentNode) {
            documentDescription.setStorageLocation(currentNode.textValue());
            objectNode.remove(STORAGE_LOCATION);
        }
        // Deserialize general documentDescription properties
        deserializeDocumentMedium(documentDescription, objectNode, errors);
        currentNode = objectNode.get(DOCUMENT_DESCRIPTION_EXTERNAL_REFERENCE);
        if (null != currentNode) {
            documentDescription.setExternalReference(currentNode.textValue());
            objectNode.remove(DOCUMENT_DESCRIPTION_EXTERNAL_REFERENCE);
        }
        documentDescription.setDisposalUndertaken(deserializeDisposalUndertaken(objectNode, errors));
        documentDescription.setReferenceDisposal(deserializeDisposal(objectNode, errors));
        documentDescription.setReferenceDeletion(deserializeDeletion(objectNode, errors));
        documentDescription.setReferenceScreening(deserializeScreening(objectNode, errors));
        documentDescription.setReferenceClassified(deserializeClassified(objectNode, errors));
        documentDescription.setReferenceElectronicSignature(deserializeElectronicSignature(objectNode, errors));
        deserializeBSM(objectNode, documentDescription);
        return documentDescription;
    }

    @Override
    protected String getType() {
        return DOCUMENT_DESCRIPTION;
    }
}