package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.secondary.Part;
import app.domain.noark5.secondary.PartUnit;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.PART_UNIT;

/**
 * Deserialize an incoming PartUnit JSON object.
 */
public class PartUnitDeserializer<P extends Part>
        extends PartDeserializer<Part> {
    @Override
    public PartUnit deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               PartUnit part = new PartUnit();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeNoarkSystemIdEntity(part, objectNode);
        deserializePartUnitEntity(part, objectNode, errors);
        check_payload_at_end(errors, objectNode);
        return part;
    }

    @Override
    protected String getType() {
        return PART_UNIT;
    }
}