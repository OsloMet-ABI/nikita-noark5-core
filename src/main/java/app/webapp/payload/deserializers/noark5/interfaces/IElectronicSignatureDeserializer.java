package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.noark5.metadata.ElectronicSignatureSecurityLevel;
import app.domain.noark5.metadata.ElectronicSignatureVerified;
import app.domain.noark5.secondary.ElectronicSignature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.*;

public interface IElectronicSignatureDeserializer
        extends IDeserializer {

    default ElectronicSignature deserializeElectronicSignatureEntity(
            ElectronicSignature electronicSignature,
            ObjectNode objectNode, StringBuilder errors) {
        // Deserialize elektroniskSignaturSikkerhetsnivaa
        ElectronicSignatureSecurityLevel essLevel =
                (ElectronicSignatureSecurityLevel)
                        deserializeMetadataValue(objectNode,
                                ELECTRONIC_SIGNATURE_SECURITY_LEVEL_FIELD,
                                new ElectronicSignatureSecurityLevel(),
                                errors, true);
        electronicSignature
                .setElectronicSignatureSecurityLevel(essLevel);
        // Deserialize elektroniskSignaturVerifisert
        ElectronicSignatureVerified esVerified =
                (ElectronicSignatureVerified)
                        deserializeMetadataValue(objectNode,
                                ELECTRONIC_SIGNATURE_VERIFIED_FIELD,
                                new ElectronicSignatureVerified(),
                                errors, true);
        electronicSignature.setElectronicSignatureVerified(esVerified);
        // Deserialize verifisertDato
        JsonNode currentNode =
                objectNode.get(ELECTRONIC_SIGNATURE_VERIFIED_DATE);
        if (null != currentNode) {
            electronicSignature.setVerifiedDate(
                    deserializeDateTime(currentNode.textValue(),
                            objectNode, errors));
            objectNode.remove(ELECTRONIC_SIGNATURE_VERIFIED_DATE);
        } else {
            errors.append(ELECTRONIC_SIGNATURE
                    + "." + ELECTRONIC_SIGNATURE_VERIFIED_DATE
                    + " is missing. ");
        }
        // Deserialize verifisertAv
        currentNode = objectNode.get(ELECTRONIC_SIGNATURE_VERIFIED_BY);
        if (null != currentNode) {
            electronicSignature.setVerifiedBy(currentNode.textValue());
            objectNode.remove(ELECTRONIC_SIGNATURE_VERIFIED_BY);
        } else {
            errors.append(ELECTRONIC_SIGNATURE
                    + "." + ELECTRONIC_SIGNATURE_VERIFIED_BY
                    + " is missing. ");
        }
        return electronicSignature;
    }

    default ElectronicSignature deserializeElectronicSignature(
            ObjectNode objectNode, StringBuilder errors) {
        ElectronicSignature es = null;
        JsonNode esNode = objectNode.get(ELECTRONIC_SIGNATURE);
        if (null != esNode
                && !esNode.equals(NullNode.getInstance())) {
            es = new ElectronicSignature();
            ObjectNode esObjectNode = esNode.deepCopy();
            deserializeElectronicSignatureEntity(es, esObjectNode,
                    errors);
            if (0 == esObjectNode.size()) {
                objectNode.remove(ELECTRONIC_SIGNATURE);
            }
        } else if (null != esNode) { // Remove NullNode
            objectNode.remove(ELECTRONIC_SIGNATURE);
        }
        return es;
    }
}
