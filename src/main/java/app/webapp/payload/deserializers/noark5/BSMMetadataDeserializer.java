package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.md_other.BSMMetadata;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class BSMMetadataDeserializer
        extends SystemIdEntityDeserializer<BSMMetadata> {

    @Override
    public BSMMetadata deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext)
            throws IOException {

               ObjectNode objectNode = mapper.readTree(jsonParser);

        BSMMetadata bsmMetadata = new BSMMetadata();
        deserializeSystemIdEntity(bsmMetadata, objectNode, errors);
        JsonNode currentNode = objectNode.get(NAME);
        if (null != currentNode) {
            bsmMetadata.setName(currentNode.textValue());
            objectNode.remove(NAME);
        } else {
            errors.append(NAME + " is missing. ");
        }
        currentNode = objectNode.get(TYPE);
        if (null != currentNode) {
            bsmMetadata.setType(currentNode.textValue());
            objectNode.remove(TYPE);
        } else {
            errors.append(TYPE + " is missing. ");
        }
        currentNode = objectNode.get(OUTDATED);
        if (null != currentNode) {
            bsmMetadata.setOutdated(currentNode.booleanValue());
            objectNode.remove(OUTDATED);
        }
        currentNode = objectNode.get(DESCRIPTION);
        if (null != currentNode) {
            bsmMetadata.setDescription(currentNode.textValue());
            objectNode.remove(DESCRIPTION);
        }
        currentNode = objectNode.get(SOURCE);
        if (null != currentNode) {
            bsmMetadata.setSource(currentNode.textValue());
            objectNode.remove(SOURCE);
        }
        check_payload_at_end(errors, objectNode);
        return bsmMetadata;
    }
}
