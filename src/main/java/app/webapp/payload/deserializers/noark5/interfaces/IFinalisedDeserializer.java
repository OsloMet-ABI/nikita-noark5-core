package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.IFinalise;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.FINALISED_BY;
import static app.utils.constants.N5ResourceMappings.FINALISED_DATE;

public interface IFinalisedDeserializer
        extends IDeserializer {
    default void deserializeNoarkFinaliseEntity(IFinalise finaliseEntity, ObjectNode objectNode, StringBuilder errors) {
        // Deserialize finalisedDate
        finaliseEntity.setFinalisedDate(deserializeDateTime(FINALISED_DATE, objectNode, errors));
        // Deserialize finalisedBy
        JsonNode currentNode = objectNode.get(FINALISED_BY);
        if (null != currentNode) {
            finaliseEntity.setFinalisedBy(currentNode.textValue());
            objectNode.remove(FINALISED_BY);
        }
    }
}
