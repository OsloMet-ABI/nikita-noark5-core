package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.IStorageLocation;
import app.domain.noark5.secondary.StorageLocation;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.STORAGE_LOCATION;

public interface IStorageLocationDeserializer
        extends IDeserializer {

    default void deserializeStorageLocation(
            IStorageLocation storageLocationEntity,
            ObjectNode objectNode) {
        // Deserialize storageLocation
        JsonNode currentNode = objectNode.get(STORAGE_LOCATION);
        if (null != currentNode) {
            if (currentNode.isArray()) {
                currentNode.iterator();
                for (JsonNode node : currentNode) {
                    String location = node.textValue();
                    StorageLocation storageLocation =
                            new StorageLocation();
                    storageLocation.setStorageLocation(location);
                    storageLocationEntity.addReferenceStorageLocation(
                            storageLocation);
                }
            }
            objectNode.remove(STORAGE_LOCATION);
        }
    }

}
