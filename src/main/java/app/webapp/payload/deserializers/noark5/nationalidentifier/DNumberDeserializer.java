package app.webapp.payload.deserializers.noark5.nationalidentifier;

import app.domain.noark5.nationalidentifier.DNumber;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.D_NUMBER;
import static app.utils.constants.N5ResourceMappings.D_NUMBER_FIELD;

public class DNumberDeserializer
        extends SystemIdEntityDeserializer<DNumber> {

    @Override
    public DNumber deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               DNumber dNumber = new DNumber();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(dNumber, objectNode);
        // Deserialize dNummer
        JsonNode currentNode = objectNode.get(D_NUMBER_FIELD);
        if (null != currentNode) {
            dNumber.setdNumber(currentNode.textValue());
            objectNode.remove(D_NUMBER_FIELD);
        }
        check_payload_at_end(errors, objectNode);
        return dNumber;
    }

    @Override
    protected String getType() {
        return D_NUMBER;
    }
}