package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.secondary.Keyword;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.KEYWORD;

public class KeywordDeserializer
        extends SystemIdEntityDeserializer<Keyword> {
    @Override
    public Keyword deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {

               Keyword keyword = new Keyword();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(keyword, objectNode);
        deserializeNoarkCreateEntity(keyword, objectNode, errors);
        // Deserialize forfatter
        JsonNode currentNode = objectNode.get(KEYWORD);
        if (null != currentNode) {
            keyword.setKeyword(currentNode.textValue());
            objectNode.remove(KEYWORD);
        }
        check_payload_at_end(errors, objectNode);
        return keyword;
    }

    @Override
    protected String getType() {
        return KEYWORD;
    }
}