package app.webapp.payload.deserializers.noark5.admin;

import app.domain.noark5.admin.CreateLog;
import app.webapp.payload.deserializers.noark5.EventLogDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.CREATE_LOG;

public class CreateLogDeserializer<C extends CreateLog>
        extends EventLogDeserializer<CreateLog> {
    @Override
    public CreateLog deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
        CreateLog createLog = new CreateLog();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        check_payload_at_end(errors, objectNode);
        return createLog;
    }

    @Override
    protected String getType() {
        return CREATE_LOG;
    }
}