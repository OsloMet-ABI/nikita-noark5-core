package app.webapp.payload.deserializers.noark5.nationalidentifier;

import app.domain.noark5.nationalidentifier.Building;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;


public class BuildingDeserializer
        extends SystemIdEntityDeserializer<Building> {
    @Override
    public Building deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               Building building = new Building();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(building, objectNode);
        // Deserialize bygningsnummer
        building.setBuildingNumber(deserializeInteger(BUILDING_NUMBER, objectNode, errors, true));
        // Deserialize endringsloepenummer
        building.setRunningChangeNumber(deserializeInteger(BUILDING_CHANGE_NUMBER, objectNode, errors, false));
        check_payload_at_end(errors, objectNode);
        return building;
    }

    @Override
    protected String getType() {
        return BUILDING;
    }
}