package app.webapp.payload.links.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.RegistryEntrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.REGISTRY_ENTRY;


@JsonSerialize(using = RegistryEntrySerializer.class)
public class RegistryEntryLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public RegistryEntryLinks(INoarkEntity entity) {
        super(entity);
    }

    public RegistryEntryLinks(SearchResultsPage page) {
        super(page, REGISTRY_ENTRY);
    }

}
