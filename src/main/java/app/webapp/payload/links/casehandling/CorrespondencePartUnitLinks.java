package app.webapp.payload.links.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.CorrespondencePartUnitSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_UNIT;

@JsonSerialize(using = CorrespondencePartUnitSerializer.class)
public class CorrespondencePartUnitLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public CorrespondencePartUnitLinks(INoarkEntity entity) {
        super(entity);
    }

    public CorrespondencePartUnitLinks(SearchResultsPage page) {
        super(page, CORRESPONDENCE_PART_UNIT);
    }
}
