package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.AuthorSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.AUTHOR;

@JsonSerialize(using = AuthorSerializer.class)
public class AuthorLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public AuthorLinks(INoarkEntity entity) {
        super(entity);
    }

    public AuthorLinks(SearchResultsPage page) {
        super(page, AUTHOR);
    }
}
