package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.DocumentFlowSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.DOCUMENT_FLOW;

@JsonSerialize(using = DocumentFlowSerializer.class)
public class DocumentFlowLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public DocumentFlowLinks() {
    }

    public DocumentFlowLinks(INoarkEntity entity) {
        super(entity);
    }

    public DocumentFlowLinks(SearchResultsPage page) {
        super(page, DOCUMENT_FLOW);
    }
}
