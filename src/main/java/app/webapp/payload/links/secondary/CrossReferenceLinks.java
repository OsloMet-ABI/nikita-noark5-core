package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.CrossReferenceSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CROSS_REFERENCE;

@JsonSerialize(using = CrossReferenceSerializer.class)
public class CrossReferenceLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public CrossReferenceLinks() {
    }

    public CrossReferenceLinks(INoarkEntity entity) {
        super(entity);
    }

    public CrossReferenceLinks(SearchResultsPage page) {
        super(page, CROSS_REFERENCE);
    }
}
