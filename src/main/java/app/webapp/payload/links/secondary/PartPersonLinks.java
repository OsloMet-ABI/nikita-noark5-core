package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.PartPersonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.PART_PERSON;

@JsonSerialize(using = PartPersonSerializer.class)
public class PartPersonLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public PartPersonLinks(INoarkEntity entity) {
        super(entity);
    }

    public PartPersonLinks(SearchResultsPage page) {
        super(page, PART_PERSON);
    }
}
