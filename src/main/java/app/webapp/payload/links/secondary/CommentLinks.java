package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.CommentSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.COMMENT;

@JsonSerialize(using = CommentSerializer.class)
public class CommentLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public CommentLinks() {
    }

    public CommentLinks(INoarkEntity entity) {
        super(entity);
    }

    public CommentLinks(SearchResultsPage page) {
        super(page, COMMENT);
    }
}
