package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;

import java.util.List;

public interface ILinksNoarkObject {
    List<Link> getLinks(INoarkEntity entity);

    List<INoarkEntity> getList();

    void addLink(INoarkEntity entity, Link link);

    void addSelfLink(Link selfLink);

    void addNextLink(Link selfLink);

    void addLink(Link selfLink);

    List<Link> getSelfLinks();

    boolean isSingleEntity();

    Long getEntityVersion();

    SearchResultsPage getPage();

    long getCount();
}
