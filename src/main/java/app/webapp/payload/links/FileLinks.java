package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.FileSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.FILE;

@JsonSerialize(using = FileSerializer.class)
public class FileLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public FileLinks(INoarkEntity entity) {
        super(entity);
    }

    public FileLinks(SearchResultsPage page) {
        super(page, FILE);
    }
}
