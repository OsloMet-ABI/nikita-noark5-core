package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.DocumentObjectSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.DOCUMENT_OBJECT;

@JsonSerialize(using = DocumentObjectSerializer.class)
public class DocumentObjectLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public DocumentObjectLinks(INoarkEntity entity) {
        super(entity);
    }

    public DocumentObjectLinks(SearchResultsPage page) {
        super(page, DOCUMENT_OBJECT);
    }
}
