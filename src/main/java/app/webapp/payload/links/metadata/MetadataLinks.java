package app.webapp.payload.links.metadata;

import app.domain.interfaces.entities.IMetadataEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.metadata.MetadataSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(using = MetadataSerializer.class)
public class MetadataLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public MetadataLinks(IMetadataEntity entity) {
        super(entity);
    }

    public MetadataLinks(List<IMetadataEntity> entityList, String entityType) {
        super((List<INoarkEntity>) (List) entityList, entityType);
    }
}

