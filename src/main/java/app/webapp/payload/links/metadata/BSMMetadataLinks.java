package app.webapp.payload.links.metadata;

import app.domain.noark5.md_other.BSMMetadata;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.metadata.BSMMetadataSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = BSMMetadataSerializer.class)
public class BSMMetadataLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public BSMMetadataLinks(BSMMetadata entity) {
        super(entity);
    }

    public BSMMetadataLinks(SearchResultsPage page, String entityType) {
        super(page, entityType);
    }
}

