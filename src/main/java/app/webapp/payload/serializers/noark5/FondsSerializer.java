package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.Fonds;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.IDocumentMediumPrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.FONDS_STATUS;

/**
 * Serialize an outgoing Fonds object as JSON.
 */
public class FondsSerializer
        extends NoarkGeneralEntitySerializer
        implements IDocumentMediumPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject fondsLinks, JsonGenerator jgen)
            throws IOException {
        Fonds fonds = (Fonds) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, fonds);
        printTitleAndDescription(jgen, fonds);
        printNullableMetadata(jgen, FONDS_STATUS, fonds.getFondsStatus());
        printDocumentMedium(jgen, fonds);
        printFinaliseEntity(jgen, fonds);
        printCreateEntity(jgen, fonds);
        printModifiedEntity(jgen, fonds);
        printLinks(jgen, fondsLinks.getLinks(fonds));
        jgen.writeEndObject();
    }
}
