package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.IDocumentMedium;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.DOCUMENT_MEDIUM;

public interface IDocumentMediumPrint
        extends IPrint {
    default void printDocumentMedium(JsonGenerator jgen, IDocumentMedium documentMedium) throws IOException {
        printNullableMetadata(jgen, DOCUMENT_MEDIUM, documentMedium.getDocumentMedium());
    }
}
