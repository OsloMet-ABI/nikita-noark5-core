package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.IElectronicSignature;
import app.domain.noark5.secondary.ElectronicSignature;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface IElectronicSignaturePrint
        extends IPrint {
    default void printElectronicSignature(JsonGenerator jgen, IElectronicSignature esEntity) throws IOException {
        ElectronicSignature es = esEntity.getReferenceElectronicSignature();
        if (es != null) {
            jgen.writeObjectFieldStart(ELECTRONIC_SIGNATURE);
            printNullableMetadata(jgen, ELECTRONIC_SIGNATURE_SECURITY_LEVEL_FIELD,
                    es.getElectronicSignatureSecurityLevel());
            printNullableMetadata(jgen, ELECTRONIC_SIGNATURE_VERIFIED_FIELD,
                    es.getElectronicSignatureVerified());
            printNullableDateTime(jgen, ELECTRONIC_SIGNATURE_VERIFIED_DATE, es.getVerifiedDate());
            printNullable(jgen, ELECTRONIC_SIGNATURE_VERIFIED_BY, es.getVerifiedBy());
            jgen.writeEndObject();
        }
    }
}
