package app.webapp.payload.serializers.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.admin.ICreateLogEntity;
import app.webapp.payload.builder.noark5.admin.CreateLogLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.admin.CreateLogLinks;
import app.webapp.payload.serializers.noark5.EventLogSerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

@LinksPacker(using = CreateLogLinksBuilder.class)
@LinksObject(using = CreateLogLinks.class)
public class CreateLogSerializer
        extends EventLogSerializer {
    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject createLogLinks, JsonGenerator jgen)
            throws IOException {
        ICreateLogEntity createLog = (ICreateLogEntity) noarkEntity;
        jgen.writeStartObject();
        if (createLog != null) {
            serializeEventLog(createLog, jgen);
        }
        printLinks(jgen, createLogLinks.getLinks(createLog));
        jgen.writeEndObject();
    }

}
