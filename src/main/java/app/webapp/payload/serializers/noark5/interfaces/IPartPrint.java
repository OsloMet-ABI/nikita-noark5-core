package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.secondary.IContactInformationEntity;
import app.domain.interfaces.entities.secondary.IGenericPersonEntity;
import app.domain.interfaces.entities.secondary.IGenericUnitEntity;
import app.domain.interfaces.entities.secondary.ISimpleAddressEntity;
import app.domain.noark5.casehandling.secondary.PostalNumber;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface IPartPrint
        extends IPrint {

    default void printGenericPerson(JsonGenerator jgen, IGenericPersonEntity partPerson) throws IOException {
        if (null != partPerson) {
            String ssn = partPerson.getSocialSecurityNumber();
            String dnumber = partPerson.getdNumber();
            if (null != ssn || null != dnumber) {
                jgen.writeObjectFieldStart(PERSON_IDENTIFIER);
                printNullable(jgen, SOCIAL_SECURITY_NUMBER, ssn);
                printNullable(jgen, D_NUMBER_FIELD, dnumber);
                jgen.writeEndObject();
            }
            printNullable(jgen, NAME, partPerson.getName());
            if (null != partPerson.getPostalAddress()) {
                printAddress(jgen, partPerson.getPostalAddress().getSimpleAddress());
            }
            if (null != partPerson.getResidingAddress()) {
                printAddress(jgen, partPerson.getResidingAddress().getSimpleAddress());
            }
            if (null != partPerson.getContactInformation()) {
                printContactInformation(jgen, partPerson.getContactInformation());
            }
        }
    }

    default void printContactInformation(JsonGenerator jgen, IContactInformationEntity contactInformation)
            throws IOException {
        if (null != contactInformation) {
            jgen.writeFieldName(CONTACT_INFORMATION);
            jgen.writeStartObject();
            printNullable(jgen, EMAIL_ADDRESS, contactInformation.getEmailAddress());
            printNullable(jgen, MOBILE_TELEPHONE_NUMBER, contactInformation.getMobileTelephoneNumber());
            printNullable(jgen, TELEPHONE_NUMBER, contactInformation.getTelephoneNumber());
            jgen.writeEndObject();
        }
    }

    default void printAddress(JsonGenerator jgen, ISimpleAddressEntity address) throws IOException {
        if (null != address) {
            jgen.writeFieldName(address.getAddressType());
            jgen.writeStartObject();
            printNullable(jgen, ADDRESS_LINE_1, address.getAddressLine1());
            printNullable(jgen, ADDRESS_LINE_2, address.getAddressLine2());
            printNullable(jgen, ADDRESS_LINE_3, address.getAddressLine3());
            if (null != address.getPostalNumber()) {
                PostalNumber postalNumber = address.getPostalNumber();
                if (null != postalNumber && null != postalNumber.getPostalNumber()) {
                    jgen.writeStringField(POSTAL_NUMBER, postalNumber.getPostalNumber());
                }
            }
            printNullable(jgen, POSTAL_TOWN, address.getPostalTown());
            printNullable(jgen, COUNTRY_CODE, address.getCountryCode());
            jgen.writeEndObject();
        }
    }

    default void printGenericUnit(JsonGenerator jgen, IGenericUnitEntity unit) throws IOException {
        if (null != unit) {
            if (null != unit.getUnitIdentifier()) {
                jgen.writeObjectFieldStart(UNIT_IDENTIFIER);
                jgen.writeStringField(ORGANISATION_NUMBER, unit.getUnitIdentifier());
                jgen.writeEndObject();
            }
            printNullable(jgen, NAME, unit.getName());
            if (null != unit.getBusinessAddress()) {
                printAddress(jgen, unit.getBusinessAddress().getSimpleAddress());
            }
            if (null != unit.getPostalAddress()) {
                printAddress(jgen, unit.getPostalAddress().getSimpleAddress());
            }
            if (null != unit.getContactInformation()) {
                printContactInformation(jgen, unit.getContactInformation());
            }
            printNullable(jgen, CONTACT_PERSON, unit.getContactPerson());
        }
    }
}
