package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.IDisposalUndertaken;
import app.domain.noark5.secondary.DisposalUndertaken;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface IDisposalUndertakenPrint
        extends IPrint {
    default void printDisposalUndertaken(JsonGenerator jgen, IDisposalUndertaken disposalUndertakenEntity)
            throws IOException {
        if (disposalUndertakenEntity != null) {
            DisposalUndertaken disposalUndertaken = disposalUndertakenEntity.getReferenceDisposalUndertaken();
            if (disposalUndertaken != null) {
                jgen.writeObjectFieldStart(DISPOSAL_UNDERTAKEN);
                printNullable(jgen, DISPOSAL_UNDERTAKEN_BY, disposalUndertaken.getDisposalBy());
                printNullableDate(jgen, DISPOSAL_UNDERTAKEN_DATE, disposalUndertaken.getDisposalDate());
                jgen.writeEndObject();
            }
        }
    }
}
