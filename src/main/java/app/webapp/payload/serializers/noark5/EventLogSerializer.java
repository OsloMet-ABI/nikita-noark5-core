package app.webapp.payload.serializers.noark5;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IChangeLogEntity;
import app.domain.interfaces.entities.IEventLogEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.builder.noark5.EventLogLinksBuilder;
import app.webapp.payload.links.EventLogLinks;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

@LinksPacker(using = EventLogLinksBuilder.class)
@LinksObject(using = EventLogLinks.class)
public class EventLogSerializer
        extends SystemIdEntitySerializer {
    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject eventLogLinks, JsonGenerator jgen)
            throws IOException {
        IEventLogEntity eventLog = (IEventLogEntity) noarkEntity;
        jgen.writeStartObject();
        serializeEventLog(eventLog, jgen);
        if (eventLog instanceof IChangeLogEntity) {
            serializeChangeLog((IChangeLogEntity) eventLog, jgen);
        }
        printLinks(jgen, eventLogLinks.getLinks(eventLog));
        jgen.writeEndObject();
    }

}
