package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.IRegistryEntryEntity;
import app.domain.noark5.casehandling.RegistryEntry;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.IRegistryEntryPrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;


/**
 * Serialize an outgoing RegistryEntry object as JSON.
 */

public class RegistryEntrySerializer
        extends RecordNoteSerializer
        implements IRegistryEntryPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject registryEntryLinks,
                                     JsonGenerator jgen) throws IOException {
        RegistryEntry registryEntry = (RegistryEntry) noarkEntity;
        jgen.writeStartObject();
        printRecordEntity(jgen, registryEntry);
        printRecordNoteEntity(jgen, registryEntry);
        printRegistryEntryEntity(jgen, registryEntry);
        printElectronicSignature(jgen, registryEntry);
        printLinks(jgen, registryEntryLinks.getLinks(registryEntry));
        jgen.writeEndObject();
    }

    public void printRegistryEntryEntity(JsonGenerator jgen, IRegistryEntryEntity registryEntry)
            throws IOException {
        if (registryEntry != null) {
            if (registryEntry.getRecordYear() != null) {
                jgen.writeNumberField(REGISTRY_ENTRY_YEAR, registryEntry.getRecordYear());
            }
            if (registryEntry.getRecordSequenceNumber() != null) {
                jgen.writeNumberField(REGISTRY_ENTRY_SEQUENCE_NUMBER, registryEntry.getRecordSequenceNumber());
            }
            if (registryEntry.getRegistryEntryNumber() != null) {
                jgen.writeNumberField(REGISTRY_ENTRY_NUMBER, registryEntry.getRegistryEntryNumber());
            }
            printNullableMetadata(jgen, REGISTRY_ENTRY_TYPE, registryEntry.getRegistryEntryType());
            printNullableMetadata(jgen, REGISTRY_ENTRY_STATUS, registryEntry.getRegistryEntryStatus());
            printNullableDateTime(jgen, REGISTRY_ENTRY_DATE, registryEntry.getRecordDate());
            printNullable(jgen, CASE_RECORDS_MANAGEMENT_UNIT, registryEntry.getRecordsManagementUnit());
        }
    }
}
