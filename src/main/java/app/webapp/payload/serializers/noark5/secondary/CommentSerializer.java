package app.webapp.payload.serializers.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.secondary.ICommentEntity;
import app.webapp.payload.builder.noark5.secondary.CommentLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.secondary.CommentLinks;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing Comment object as JSON.
 */
@LinksPacker(using = CommentLinksBuilder.class)
@LinksObject(using = CommentLinks.class)
public class CommentSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject commentLinks, JsonGenerator jgen)
            throws IOException {
        ICommentEntity comment = (ICommentEntity) noarkEntity;
        jgen.writeStartObject();
        if (comment != null) {
            printSystemIdEntity(jgen, comment);
            printNullable(jgen, COMMENT_TEXT, comment.getCommentText());
            printNullableMetadata(jgen, COMMENT_TYPE, comment.getCommentType());
            printDateTime(jgen, COMMENT_DATE, comment.getCommentDate());
            printNullable(jgen, COMMENT_REGISTERED_BY, comment.getCommentRegisteredBy());
        }
        printLinks(jgen, commentLinks.getLinks(comment));
        jgen.writeEndObject();
    }
}
