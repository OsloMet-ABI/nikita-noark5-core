package app.webapp.payload.serializers.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.secondary.Keyword;
import app.webapp.payload.builder.noark5.secondary.KeywordLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.secondary.KeywordLinks;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.KEYWORD;

/**
 * Serialize an outgoing Keyword object as JSON.
 */
@LinksPacker(using = KeywordLinksBuilder.class)
@LinksObject(using = KeywordLinks.class)
public class KeywordSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject keywordLinks, JsonGenerator jgen)
            throws IOException {
        Keyword keyword = (Keyword) noarkEntity;
        jgen.writeStartObject();
        print(jgen, KEYWORD, keyword.getKeyword());
        printLinks(jgen, keywordLinks.getLinks(keyword));
        jgen.writeEndObject();
    }
}
