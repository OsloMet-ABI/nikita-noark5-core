package app.webapp.payload.serializers.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.secondary.Author;
import app.webapp.payload.builder.noark5.secondary.AuthorLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.secondary.AuthorLinks;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.AUTHOR;

/**
 * Serialize an outgoing Author object as JSON.
 */
@LinksPacker(using = AuthorLinksBuilder.class)
@LinksObject(using = AuthorLinks.class)
public class AuthorSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject authorLinks, JsonGenerator jgen)
            throws IOException {
        Author author = (Author) noarkEntity;
        jgen.writeStartObject();
        printNullable(jgen, AUTHOR, author.getAuthor());
        printLinks(jgen, authorLinks.getLinks(author));
        jgen.writeEndObject();
    }
}
