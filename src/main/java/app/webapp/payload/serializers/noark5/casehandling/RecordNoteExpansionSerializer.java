package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.casehandling.RecordNote;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;


/**
 * Serialize an outgoing expanded RecordNote object as JSON.
 */
public class RecordNoteExpansionSerializer
        extends RecordNoteSerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject recordNoteLinks,
                                     JsonGenerator jgen) throws IOException {
        RecordNote recordNote = (RecordNote) noarkEntity;
        jgen.writeStartObject();
        printRecordEntity(jgen, recordNote);
        printRecordNoteEntity(jgen, recordNote);
        printLinks(jgen, recordNoteLinks.getLinks(recordNote));
        jgen.writeEndObject();
    }
}
