package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.DocumentDescription;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing DocumentDescription object as JSON.
 */
public class DocumentDescriptionSerializer
        extends NoarkGeneralEntitySerializer
        implements IClassifiedPrint, ICrossReferencePrint, IDeletionPrint, IDisposalPrint, IDisposalUndertakenPrint,
        IElectronicSignaturePrint, IScreeningPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject documentDescriptionLinks,
                                     JsonGenerator jgen) throws IOException {
        DocumentDescription documentDescription = (DocumentDescription) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, documentDescription);
        printNullableMetadata(jgen, DOCUMENT_DESCRIPTION_DOCUMENT_TYPE, documentDescription.getDocumentType());
        printNullableMetadata(jgen, DOCUMENT_DESCRIPTION_STATUS, documentDescription.getDocumentStatus());
        printTitleAndDescription(jgen, documentDescription);
        printNullable(jgen, DOCUMENT_DESCRIPTION_DOCUMENT_NUMBER, documentDescription.getDocumentNumber());
        printNullableDateTime(jgen, DOCUMENT_DESCRIPTION_ASSOCIATED_DATE, documentDescription.getAssociationDate());
        if (documentDescription.getAssociatedBy() != null) {
            jgen.writeStringField(DOCUMENT_DESCRIPTION_ASSOCIATED_BY, documentDescription.getAssociatedBy());
        }
        printNullableMetadata(jgen, DOCUMENT_DESCRIPTION_ASSOCIATED_WITH_RECORD_AS,
                documentDescription.getAssociatedWithRecordAs());
        printCreateEntity(jgen, documentDescription);
        printModifiedEntity(jgen, documentDescription);
        printDisposal(jgen, documentDescription);
        printDisposalUndertaken(jgen, documentDescription);
        printDeletion(jgen, documentDescription);
        printScreening(jgen, documentDescription);
        printClassified(jgen, documentDescription);
        printNullable(jgen, STORAGE_LOCATION, documentDescription.getStorageLocation());
        printNullable(jgen, DOCUMENT_DESCRIPTION_EXTERNAL_REFERENCE, documentDescription.getExternalReference());
        printElectronicSignature(jgen, documentDescription);
        printBSM(jgen, documentDescription);
        printLinks(jgen, documentDescriptionLinks.getLinks(documentDescription));
        jgen.writeEndObject();
    }
}
