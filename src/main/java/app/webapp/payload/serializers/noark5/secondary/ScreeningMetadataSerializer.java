package app.webapp.payload.serializers.noark5.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.secondary.ScreeningMetadataLocal;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;


public class ScreeningMetadataSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject screeningMetadataLinks,
                                     JsonGenerator jgen)
            throws IOException {
        ScreeningMetadataLocal screeningMetadata = (ScreeningMetadataLocal) noarkEntity;
        jgen.writeStartObject();
        printMetadataEntity(jgen, screeningMetadata);
        printLinks(jgen, screeningMetadataLinks.getLinks(screeningMetadata));
        jgen.writeEndObject();
    }
}
