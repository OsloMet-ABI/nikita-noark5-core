package app.webapp.payload.serializers.application;

import app.webapp.spring.exception.ApiError;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

import static app.utils.constants.HATEOASConstants.*;

public class ApiErrorSerializer
        extends StdSerializer<ApiError> {

    public ApiErrorSerializer() {
        super(ApiError.class);
    }

    @Override
    public void serialize(ApiError apiError,
                          JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        jgen.writeStartObject();
        jgen.writeObjectFieldStart(ERROR);
        // This is what I believe the error message should look like:
        // jgen.writeNumberField(ERROR_CODE, apiError.getStatus());
        // jgen.writeStringField(ERROR_SOURCE, apiError.getUrlSource());
        // jgen.writeStringField(ERROR_METHOD, apiError.getMethod());
        // jgen.writeStringField(ERROR_DESCRIPTION, apiError.getMessage());
        // jgen.writeStringField(ERROR_TECHNICAL_DESCRIPTION, apiError.getDeveloperMessage());
        // jgen.writeStringField(ERROR_EXTENDED_DESCRIPTION, apiError.getStackTrace());
        // Perhaps we can update the standard at some time
        // This is what the standard expects
        jgen.writeNumberField(ERROR_CODE, apiError.getStatus());
        String message = apiError.getUrlSource() + ";" +
                apiError.getHttpMethod() + ";" +
                apiError.getMessage() + ";" +
                apiError.getDeveloperMessage() + ";" +
                apiError.getStackTrace();
        jgen.writeStringField(ERROR_DESCRIPTION, message);
        jgen.writeEndObject();
        jgen.writeEndObject();
    }
}