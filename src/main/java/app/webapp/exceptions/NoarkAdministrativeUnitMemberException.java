package app.webapp.exceptions;

public class NoarkAdministrativeUnitMemberException extends NikitaException {

    public NoarkAdministrativeUnitMemberException(final String message) {
        super(message);
    }
}
