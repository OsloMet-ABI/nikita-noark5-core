package app.webapp.startup;

import app.service.application.MetadataInsert;
import app.utils.InternalNameTranslator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PathPatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static app.utils.CommonUtils.FileUtils.addProductionToArchiveVersion;
import static app.utils.CommonUtils.FileUtils.setDefaultMimeTypesAsConvertible;
import static app.utils.CommonUtils.WebUtils.addRequestToMethodMap;
import static app.utils.constants.Constants.SLASH;
import static app.utils.constants.ErrorMessagesConstants.DOC_STORE_DIRECTORY_ERROR;
import static app.utils.constants.FileConstants.*;
import static java.lang.String.format;
import static org.springframework.http.HttpMethod.*;

/**
 * Create some basic required data for application.
 * <p>
 * This will import the content of all metadata classes, the Norwegian to
 * English mapping of attribute / class names
 */
@Service
public class AfterApplicationStartup {

    private static final Logger logger =
            LoggerFactory.getLogger(AfterApplicationStartup.class);

    private final RequestMappingHandlerMapping handlerMapping;
    private final ApplicationContext applicationContext;
    private final InternalNameTranslator internalNameTranslator;
    private final DomainModelMapper domainModelMapper;
    private final MetadataInsert metadataInsert;

    @Value("${nikita.startup.create-directory-store}")
    private final Boolean createDirectoryStore = false;
    @Value("${nikita.startup.show-endpoints:false}")
    private final Boolean showEndpointsAtStartup = false;
    @Value("${nikita.startup.directory-store-name}")
    private String directoryStoreName;
    @Value("${nikita.startup.incoming-directory}")
    private String incomingDirectoryName;

    public AfterApplicationStartup(@Qualifier("requestMappingHandlerMapping")
                                   RequestMappingHandlerMapping handlerMapping,
                                   MetadataInsert metadataInsert,
                                   ApplicationContext applicationContext,
                                   InternalNameTranslator internalNameTranslator,
                                   DomainModelMapper domainModelMapper) {
        this.handlerMapping = handlerMapping;
        this.metadataInsert = metadataInsert;
        this.applicationContext = applicationContext;
        this.internalNameTranslator = internalNameTranslator;
        this.domainModelMapper = domainModelMapper;
    }

    /**
     * Undertake some business logic after application starts. This is as
     * follows :
     * - mapEndpointsWithHttpMethods()
     * - populateTranslatedNames()
     * - createDemoUsers()
     */
    public void afterApplicationStarts() {
        mapEndpointsWithHttpMethods();
        mapProductionToArchiveMimeTypes();
        mapDomainModelClasses();
        populateTranslatedNames();
        setDefaultMimeTypesAsConvertible();

        metadataInsert.populateMetadataEntities();

        if (createDirectoryStore) {
            createDirectoryStoreIfNotExists();
        }
    }

    private void mapDomainModelClasses() {
        domainModelMapper.mapDomainModelClasses();
    }

    private void populateTranslatedNames() {
        internalNameTranslator.populateTranslatedNames();
    }

    /**
     * Check if the document storage directory exists, if not try to
     * create it. If there is a problem shut down the core.
     */
    private void createDirectoryStoreIfNotExists() {
        try {
            if (!Files.exists(Paths.get(directoryStoreName))) {
                Files.createDirectories(Paths.get(directoryStoreName));
            }
            if (!Files.exists(Paths.get(incomingDirectoryName))) {
                Files.createDirectories(Paths.get(incomingDirectoryName));
            }
        } catch (IOException e) {
            ((ConfigurableApplicationContext) applicationContext).close();
            logger.error(format(DOC_STORE_DIRECTORY_ERROR, directoryStoreName, incomingDirectoryName));
        }
    }


    private void mapProductionToArchiveMimeTypes() {

        addProductionToArchiveVersion(MIME_TYPE_ODT, FILE_EXTENSION_ODT,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_ODS, FILE_EXTENSION_ODS,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_ODP, FILE_EXTENSION_ODP,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_DOCX, FILE_EXTENSION_DOCX,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_DOC, FILE_EXTENSION_DOC,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_XLSX, FILE_EXTENSION_XLSX,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_XLS, FILE_EXTENSION_XLS,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_PPT, FILE_EXTENSION_PPT,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_PPTX, FILE_EXTENSION_PPTX,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_PNG, FILE_EXTENSION_PNG,
                MIME_TYPE_PDF);
        addProductionToArchiveVersion(MIME_TYPE_GIF, FILE_EXTENSION_GIF,
                MIME_TYPE_GIF);
        addProductionToArchiveVersion(MIME_TYPE_TEXT, FILE_EXTENSION_TEXT,
                MIME_TYPE_TEXT);
        addProductionToArchiveVersion(MIME_TYPE_PDF, FILE_EXTENSION_PDF,
                MIME_TYPE_PDF);
    }

    /**
     * map endpoints with their HTTP methods
     * <p>
     * Go through the list of endpoints and make a list of endpoints and the
     * HTTP methods they support. We were unable to get CORS implemented
     * properly and this approach was required to make to work correctly.
     * <p>
     * We shouldn't have to d this, it should be handled by spring. Out
     * application was over annotated and that could be the original cause of
     * the Cors problems. Eventually this code should be removed and Cors
     * handled by spring.
     * COMMENT : 2025-01-18. CORS is now handled correctly but this is still used to create ALLOWS headers
     * This needs to be removed with a better way of generating allows based on permissions
     */
    private void mapEndpointsWithHttpMethods() {

        for (Map.Entry<RequestMappingInfo, HandlerMethod> entry :
                handlerMapping.getHandlerMethods().entrySet()) {
            RequestMappingInfo requestMappingInfo = entry.getKey();
            PathPatternsRequestCondition patternsCondition =
                    requestMappingInfo.getPathPatternsCondition();
            if (patternsCondition != null) {
                Set<String> setServletPaths = patternsCondition.getPatternValues();
                for (String servletPath : setServletPaths) {
                    if (servletPath != null && !servletPath.contains("|")) {
                        // Adding a trailing slash as the incoming request may or may not have it
                        // This is done to be consistent when doing <a lookup
                        if (!servletPath.endsWith("/")) {
                            servletPath += SLASH;
                        }

                        Set<RequestMethod> httpMethodRequests = requestMappingInfo.getMethodsCondition().getMethods();
                        // RequestMethod and HTTPMethod are different types, have to convert them here
                        Set<HttpMethod> httpMethods = new TreeSet<>();
                        for (RequestMethod requestMethod : httpMethodRequests) {
                            if (requestMethod.equals(RequestMethod.GET)) {
                                httpMethods.add(GET);
                            } else if (requestMethod.equals(RequestMethod.DELETE)) {
                                httpMethods.add(DELETE);
                            } else if (requestMethod.equals(RequestMethod.OPTIONS)) {
                                httpMethods.add(OPTIONS);
                            } else if (requestMethod.equals(RequestMethod.HEAD)) {
                                httpMethods.add(HEAD);
                            } else if (requestMethod.equals(RequestMethod.PATCH)) {
                                httpMethods.add(PATCH);
                            } else if (requestMethod.equals(RequestMethod.POST)) {
                                httpMethods.add(POST);
                            } else if (requestMethod.equals(RequestMethod.PUT)) {
                                httpMethods.add(PUT);
                            } else if (requestMethod.equals(RequestMethod
                                    .TRACE)) {
                                httpMethods.add(TRACE);
                            }
                        }
                        if (showEndpointsAtStartup) {
                            logger.info("Adding {} methods {}", servletPath, httpMethods);
                        }
                        addRequestToMethodMap(servletPath, httpMethods);
                    }
                }
            } else {
                // This has never occurred. This code should be removed anyway
                logger.error("patternsCondition is null at startup");
            }
        }
    }
}
