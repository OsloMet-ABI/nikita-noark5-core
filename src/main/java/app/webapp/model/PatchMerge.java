package app.webapp.model;

import app.domain.noark5.NoarkEntity;
import app.webapp.payload.deserializers.patch.PatchMergeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.LinkedHashMap;
import java.util.Map;

@JsonDeserialize(using = PatchMergeDeserializer.class)
public class PatchMerge extends NoarkEntity {

    private final Map<String, Object> map = new LinkedHashMap<>();

    public PatchMerge() {
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void addValue(String key, Object object) {
        map.putIfAbsent(key, object);
    }

    public Object getValue(String key) {
        return map.get(key);
    }
}
