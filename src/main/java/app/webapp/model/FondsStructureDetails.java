package app.webapp.model;

import app.webapp.payload.serializers.application.APIDetailsSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static app.utils.constants.ODataConstants.SUPPORTED_ODATA_PARAMETERS;

@JsonSerialize(using = APIDetailsSerializer.class)
public class FondsStructureDetails extends APIDetails {

    public FondsStructureDetails(String publicUrlPath) {
        super();

        if (publicUrlPath.endsWith("/")) {
            publicUrlPath = publicUrlPath.substring(0, publicUrlPath.length()
                    - 1);
        }
        // Add Fonds
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_FONDS + SUPPORTED_ODATA_PARAMETERS,
                REL_FONDS_STRUCTURE + FONDS + SLASH,
                true
        ));

        // Add new-fonds
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_FONDS_STRUCTURE + SLASH + NEW_FONDS,
                REL_FONDS_STRUCTURE + NEW_FONDS + SLASH
        ));

        // Add FondsCreator
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_FONDS_STRUCTURE + SLASH + FONDS_CREATOR +
                        SUPPORTED_ODATA_PARAMETERS,
                REL_FONDS_STRUCTURE + FONDS_CREATOR + SLASH,
                true
        ));


        // Add new FondsCreator
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_FONDS_STRUCTURE + SLASH + NEW_FONDS_CREATOR,
                REL_FONDS_STRUCTURE + NEW_FONDS_CREATOR + SLASH
        ));

        // Add Series
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_SERIES + SUPPORTED_ODATA_PARAMETERS,
                REL_FONDS_STRUCTURE + SERIES + SLASH,
                true
        ));

        // Add Classification_system
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_FONDS_STRUCTURE + SLASH + CLASSIFICATION_SYSTEM +
                        SUPPORTED_ODATA_PARAMETERS,
                REL_FONDS_STRUCTURE + CLASSIFICATION_SYSTEM + SLASH,
                true
        ));

        // Add Class
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_CLASS + SUPPORTED_ODATA_PARAMETERS,
                REL_FONDS_STRUCTURE + CLASS + SLASH,
                true
        ));

        // Add File
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_FILE + SUPPORTED_ODATA_PARAMETERS,
                REL_FONDS_STRUCTURE + FILE + SLASH,
                true
        ));

        // Add Registration
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_RECORD + SUPPORTED_ODATA_PARAMETERS,
                REL_FONDS_STRUCTURE + RECORD + SLASH,
                true
        ));

        // Add DocumentDescription
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_FONDS_STRUCTURE + SLASH + DOCUMENT_DESCRIPTION +
                        SUPPORTED_ODATA_PARAMETERS,
                REL_FONDS_STRUCTURE + DOCUMENT_DESCRIPTION + SLASH,
                true
        ));

        // Add DocumentObject
        aPIDetails.add(new APIDetail(
                publicUrlPath + SLASH + HREF_BASE_DOCUMENT_OBJECT + SUPPORTED_ODATA_PARAMETERS,
                REL_FONDS_STRUCTURE + DOCUMENT_OBJECT + SLASH,
                true
        ));
    }
}
