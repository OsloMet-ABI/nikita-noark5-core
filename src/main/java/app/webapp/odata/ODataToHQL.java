package app.webapp.odata;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.odata.model.Comparison;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;

import static app.utils.constants.ODataConstants.*;

/**
 * Extending ODataWalker to handle events to convert OData filter
 * command to SQL.
 */
public class ODataToHQL
        extends ODataWalker
        implements IODataWalker {

    protected final NoarkHQLStatementBuilder statement;
    private final HibernateEntityReflections reflections;
    protected Comparison comparison = new Comparison();
    protected boolean right = false;

    public ODataToHQL(String dmlStatementType, String organisation) {
        statement = new NoarkHQLStatementBuilder(dmlStatementType, organisation);
        reflections = new HibernateEntityReflections();
    }

    public ODataToHQL(String organisation) {
        statement = new NoarkHQLStatementBuilder(organisation);
        reflections = new HibernateEntityReflections();
    }

    @Override
    public void processQueryEntity(String entity) {
        statement.addQueryEntity(entity);
    }

    @Override
    public void processAttribute(String attribute) {
        if (!right) {
            comparison.setLeft(attribute);
        } else {
            comparison.setRight(attribute);
        }
    }

    @Override
    public void processCountAsResource(Boolean includeResults) {
        statement.addCountAsResource(includeResults);
    }

    @Override
    public void processComparatorCommand(String aliasAndAttribute,
                                         String comparator, Object value) {
        statement.addCompareValue(aliasAndAttribute,
                translateComparator(comparator), value);
    }

    @Override
    public void processCompareMethod(String methodName, Object value) {
        switch (methodName) {
            case "contains" -> {
                comparison.setRight("%" + value + "%");
                comparison.setComparator("like");
            }
            case "startswith" -> {
                comparison.setRight(value + "%");
                comparison.setComparator("like");
            }
            case "endswith" -> {
                comparison.setRight("%" + value);
                comparison.setComparator("like");
            }
            default -> comparison.setRight(value);
        }
    }

    @Override
    public void processCompare(String aliasAndAttribute,
                               String comparisonOperator, Object value) {
        statement.addCompareValue(aliasAndAttribute,
                translateComparator(comparisonOperator), value);
    }

    @Override
    public void addClassNameForInheritanceClarification(
            String entityName, String originalEntityName) {
        statement.addPotentialTypeMapping(entityName, originalEntityName);
    }

    @Override
    public void addEntityToEntityJoin(String fromEntity, String toEntity) {
        String foreignKey;
        if (fromEntity.equals(toEntity)) {
            // 3rd argument tells to get reference to parent rather than child
            foreignKey = reflections.getForeignKeySubUnit(fromEntity);
        } else {
            foreignKey = reflections.getForeignKey(fromEntity, toEntity);
        }
        statement.addEntityToEntityJoin(fromEntity, foreignKey, toEntity);
    }

    @Override
    public void processParenthesis(String bracket) {
        statement.addBracket(bracket);
    }

    @Override
    public void processLogicalOperator(String logicalOperator) {
        statement.addLogicalOperator(logicalOperator);
    }

    @Override
    public void processComparator(String comparator) {
        comparison.setComparator(translateComparator(comparator));
    }

    @Override
    public void processStartRight() {
        right = true;
    }

    @Override
    public void processMethodExpression(String methodName) {
        if (methodName.equalsIgnoreCase("tolower")) {
            methodName = "lower";
        } else if (methodName.equalsIgnoreCase("toupper")) {
            methodName = "upper";
        }

        if (!right) {
            comparison.addLeftMethod(methodName);
        } else {
            comparison.addRightMethod(methodName);
        }
    }

    @Override
    public void processSkip(Integer skip) {
        statement.setLimitOffset(new AtomicInteger(skip));
    }

    @Override
    public void processTop(Integer top) {
        statement.setLimitHowMany(new AtomicInteger(top));
    }

    @Override
    public void startBoolComparison() {
        comparison = new Comparison();
        right = false;
    }

    @Override
    public void endBoolComparison() {
        StringBuilder leftFunctionNames = new StringBuilder();
        StringBuilder leftFunctionClose = new StringBuilder();
        for (Object function : comparison.getLeftMethods()) {
            leftFunctionNames.append(function).append("(");
            leftFunctionClose.append(")");
        }
        StringBuilder rightFunctionNames = new StringBuilder();
        StringBuilder rightFunctionClose = new StringBuilder();
        for (Object function : comparison.getRightMethods()) {
            rightFunctionNames.append(function).append("(");
            rightFunctionClose.append(")");
        }
        StringJoiner concatValues = comparison.getConcatValues();
        // 8 is the number of characters in concat()
        if (concatValues.length() > 8) {
            statement.addCompareValueFunction(concatValues.toString(), "",
                    comparison.getLeft(), comparison.getComparator(),
                    comparison.getRight(), rightFunctionNames.toString(), rightFunctionClose.toString());
        } else {
            statement.addCompareValueFunction(leftFunctionNames.toString(), leftFunctionClose.toString(),
                    comparison.getLeft(), comparison.getComparator(),
                    comparison.getRight(), rightFunctionNames.toString(), rightFunctionClose.toString());
        }
        processEndConcat();
    }

    @Override
    public void processStartConcat() {
        comparison.startConcat();
    }

    @Override
    public void processEndConcat() {
        comparison.endConcat();
    }

    @Override
    public void processOrderBy(String attribute, String sortOrder) {
        statement.addOrderBy(attribute, sortOrder);
    }

    public Query<INoarkEntity> getHqlStatement(Session session) {
        return statement.getQuery(session);
    }

    /**
     * Convert a OData comparator to a HQL comparator
     * - "eq" -> "="
     * - "gt" -> ">"
     * - "ge" -> ">="
     * - "lt" -> "<"
     * - "le" -> "<="
     * - "ne" -> "!="
     *
     * @param comparator The OData comparator
     * @return comparator used in HQL
     */
    private String translateComparator(String comparator)
            throws NikitaMalformedInputDataException {
        switch (comparator.toLowerCase()) {
            case ODATA_EQ -> {
                return HQL_EQ;
            }
            case ODATA_GT -> {
                return HQL_GT;
            }
            case ODATA_GE -> {
                return HQL_GE;
            }
            case ODATA_LT -> {
                return HQL_LT;
            }
            case ODATA_LE -> {
                return HQL_LE;
            }
            case ODATA_NE -> {
                return HQL_NE;
            }
        }
        throw new NikitaMalformedInputDataException(
                "Unrecognised comparator used in OData query (" +
                        comparator + ")");
    }

    public void processPrimitive(Object value) {
        if (comparison.getProcessConcat()) {
            value = "'" + value + "'";
        }
        if (!right) {
            comparison.setLeft(value);
        } else {
            comparison.setRight(value);
        }
    }

    public String getEntity() {
        return statement.getFromEntity();
    }
}
