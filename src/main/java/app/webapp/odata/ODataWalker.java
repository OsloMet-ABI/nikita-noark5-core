package app.webapp.odata;

import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.odata.base.ODataParser;
import app.webapp.odata.base.ODataParserBaseListener;
import app.webapp.payload.deserializers.noark5.interfaces.IParseDateTime;
import jakarta.validation.constraints.NotNull;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.UUID;

import static app.utils.CommonUtils.WebUtils.getEnglishNameDatabase;
import static app.utils.CommonUtils.WebUtils.getEnglishNameObject;
import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.SYSTEM_ID;
import static java.util.UUID.fromString;

public abstract class ODataWalker
        extends ODataParserBaseListener
        implements IODataWalker, IParseDateTime {

    private final Logger logger =
            LoggerFactory.getLogger(ODataWalker.class);
    protected String entity = "";
    protected String joinEntity = "";

    @Override
    public void enterMethodCallExpr(ODataParser.MethodCallExprContext ctx) {
        super.enterMethodCallExpr(ctx);
        processMethodExpression(ctx.getChild(0).getText());
    }

    @Override
    public void enterCompareMethodExpr(ODataParser.CompareMethodExprContext ctx) {
        super.enterCompareMethodExpr(ctx);
        processCompareMethod(ctx.getChild(0).getText(),
                getPrimitiveTypeObject((ODataParser.PrimitiveLiteralContext)
                        ctx.getChild(4).getChild(0)));
    }

    @Override
    public void enterCalenderMethodExp(ODataParser.CalenderMethodExpContext ctx) {
        super.enterCalenderMethodExp(ctx);
        processMethodExpression(ctx.getChild(0).getText());
    }

    @Override
    public void enterJoinEntities(ODataParser.JoinEntitiesContext ctx) {
        super.enterJoinEntities(ctx);
        this.joinEntity = processJoinEntitiesContext(ctx);
    }

    @Override
    public void enterAttributeName(ODataParser.AttributeNameContext ctx) {
        processAttribute(getAliasAndAttribute(
                this.joinEntity.isEmpty() ?
                        this.entity : this.joinEntity,
                getInternalNameObject(ctx.getText())));
    }

    @Override
    public void enterRightComparatorExpr(ODataParser.RightComparatorExprContext ctx) {
        super.enterRightComparatorExpr(ctx);
        processStartRight();
    }

    @Override
    public void enterComparisonOperator(ODataParser.ComparisonOperatorContext ctx) {
        super.enterComparisonOperator(ctx);
        processComparator(ctx.getChild(0).getText());
    }

    @Override
    public void enterConcatMethodExpr(ODataParser.ConcatMethodExprContext ctx) {
        super.enterConcatMethodExpr(ctx);
        processStartConcat();

    }

    @Override
    public void enterBoolExpression(ODataParser.BoolExpressionContext ctx) {
        super.enterBoolExpression(ctx);
        startBoolComparison();
    }

    @Override
    public void enterOrderByItem(ODataParser.OrderByItemContext ctx) {
        super.enterOrderByItem(ctx);
        String aliasAndAttribute =
                getAliasAndAttribute(this.entity,
                        getInternalNameObject(ctx.getChild(0).getText()));
        if (ctx.getChild(1) instanceof ODataParser.SortOrderContext) {
            processOrderBy(aliasAndAttribute,
                    ctx.getChild(1).getChild(0).getText());
        } else {
            processOrderBy(aliasAndAttribute, "");
        }
    }

    @Override
    public void enterPrimitiveLiteral(ODataParser.PrimitiveLiteralContext ctx) {
        super.enterPrimitiveLiteral(ctx);
        processPrimitive(getPrimitiveTypeObject(ctx));
    }

    @Override
    public void exitBoolExpression(ODataParser.BoolExpressionContext ctx) {
        super.exitBoolExpression(ctx);
        endBoolComparison();
        this.joinEntity = "";
    }

    protected String processJoinEntitiesContext(ODataParser.JoinEntitiesContext ctx) {
        List<ODataParser.EntityNameContext> entityNameContexts =
                ctx.getRuleContexts(ODataParser.EntityNameContext.class);
        // Join the from the entity applying the filter on e.g
        // mappe?$filter=klasse/klassifikasjonssystem ....
        // You have to first join mappe (File) to klasse (Class)
        String originalEntityName = getValue(ctx, ODataParser.EntityNameContext.class);
        String toEntity = getInternalNameObject(originalEntityName);
        addEntityToEntityJoin(this.entity, toEntity);
        addClassNameForInheritanceClarification(toEntity, originalEntityName);
        if (entityNameContexts.size() > 1) {
            for (int i = 0; i < entityNameContexts.size(); i++) {
                if (i < entityNameContexts.size() - 1) {
                    String fromEntity = getInternalNameObject
                            (getValue(ctx, ODataParser.EntityNameContext.class, i));
                    originalEntityName = getValue(ctx,
                            ODataParser.EntityNameContext.class, i + 1);
                    toEntity = getInternalNameObject(originalEntityName);
                    addEntityToEntityJoin(fromEntity, toEntity);
                    addClassNameForInheritanceClarification(toEntity,
                            originalEntityName);
                }
            }
        }
        return toEntity;
    }

    @Override
    public void enterBinaryExpression(ODataParser.BinaryExpressionContext ctx) {
        super.enterBinaryExpression(ctx);
    }

    @Override
    public void enterLogicalOperator(ODataParser.LogicalOperatorContext ctx) {
        super.enterLogicalOperator(ctx);
        processLogicalOperator(ctx.getText());
    }

    @Override
    public void enterOpenPar(ODataParser.OpenParContext ctx) {
        super.enterOpenPar(ctx);
        processParenthesis(ctx.getText());
    }

    @Override
    public void enterClosePar(ODataParser.CloseParContext ctx) {
        super.enterClosePar(ctx);
        processParenthesis(ctx.getText());
    }

    @Override
    public void enterResourcePath(ODataParser.ResourcePathContext ctx) {
        super.enterResourcePath(ctx);
        logger.debug("Entering ResourcePathContext. Found [" +
                ctx.getText() + "]");

        if (null != ctx.getChild(ODataParser.EntityContext.class, 0)) {
            this.entity = getInternalNameObject(getValue(
                    ctx.getChild(ODataParser.EntityContext.class, 0),
                    ODataParser.EntityNameContext.class));
        } else if (null != ctx.getChild(ODataParser.EntityCastContext.class, 0)) {
            this.entity = getInternalNameObject(getValue(
                    ctx, ODataParser.EntityCastContext.class));
        } else if (null != ctx.getChild(ODataParser.EntityUUIDContext.class, 0)) {
            this.entity = getInternalNameObject(getValue(
                    ctx, ODataParser.EntityNameContext.class));
            processEntityUUID(ctx.getChild(ODataParser.EntityUUIDContext.class, 0));
        } else if (null != ctx.getChild(ODataParser.EmbeddedEntitySetContext.class, 0)) {
            this.entity = processResourcePathJoin(
                    ctx.getChild(ODataParser.EmbeddedEntitySetContext.class, 0));
            processLogicalOperator("and");
        }
        processQueryEntity(this.entity);
    }

    private void processEntityUUID(ODataParser.EntityUUIDContext ctx) {
        UUID systemID = fromString(
                getValue(ctx, ODataParser.UuidIdValueContext.class));
        String aliasAndAttribute = getAliasAndAttribute(
                this.entity, getInternalNameObject(SYSTEM_ID));
        processComparatorCommand(aliasAndAttribute, "eq", systemID);
    }

    private String processResourcePathJoin(ODataParser.EmbeddedEntitySetContext ctx) {
        // Identify the last entity so we can use it as the starting point for
        // JOINs HERE!!!
        String entity = getFinalEntity(
                ctx.getRuleContexts(ODataParser.EntityContext.class));

        List<ODataParser.EntityUUIDContext> uuidContexts =
                ctx.getRuleContexts(ODataParser.EntityUUIDContext.class);

        if (!uuidContexts.isEmpty()) {
            String toEntity = getInternalNameObject(
                    uuidContexts.get(uuidContexts.size() - 1)
                            .getChild(0).getText());
            addEntityToEntityJoin(entity, toEntity);
            String toEntityUUID = uuidContexts.get(uuidContexts.size() - 1)
                    .getChild(2).getText();
            processCompare(getAliasAndAttribute(toEntity,
                            getInternalNameObject(SYSTEM_ID)),
                    "eq", fromString(toEntityUUID));
            for (int i = uuidContexts.size() - 1; i > 0; i--) {
                String fromEntity = getInternalNameObject(
                        uuidContexts.get(i).getChild(0).getText());
                toEntity = getInternalNameObject(
                        uuidContexts.get(i - 1).getChild(0).getText());
                toEntityUUID = uuidContexts.get(i - 1)
                        .getChild(2).getText();
                addEntityToEntityJoin(fromEntity, toEntity);
                processLogicalOperator("and");
                processCompare(getAliasAndAttribute(toEntity,
                        getInternalNameObject(SYSTEM_ID)),
                        "eq", fromString(toEntityUUID));
            }
        }
        return entity;
    }

    protected String getFinalEntity(
            List<ODataParser.EntityContext> entityContexts) {
        if (!entityContexts.isEmpty()) {
            return getInternalNameObject(entityContexts.get(
                    entityContexts.size() - 1).getText());
        }
        // Consider making this throw a 500 internal or 400 bad request
        return "";
    }

    @Override
    public void enterCountStatement(ODataParser.CountStatementContext ctx) {
        super.enterCountStatement(ctx);
        processCountAsResource(true);
    }

    /**
     * getInternalNameAttribute
     * <p>
     * Helper mechanism to convert Norwegian entity / attribute names to
     * English as English is used in classes and tables. Interacting with the
     * core is done in Norwegian but things have then to be translated to
     * English naming conventions.
     * <p>
     * Note, this will return the name of the database column
     * <p>
     * If we don't have a corresponding object, we choose just to return the
     * original object. This should force a database query error and expose
     * any missing objects. This strategy is OK in development, but later we
     * need a better way of handling it.
     *
     * @param norwegianName The name in Norwegian
     * @return the English version of the Norwegian name if it exists, otherwise
     * the original Norwegian name.
     */
    protected String getInternalNameAttribute(String norwegianName) {
        String englishName = getEnglishNameDatabase(norwegianName);
        if (englishName == null)
            return norwegianName;
        else
            return englishName;
    }

    /**
     * getInternalNameObject
     * <p>
     * Helper mechanism to convert Norwegian entity / attribute names to
     * English as English is used in classes and tables. Interacting with the
     * core is done in Norwegian but things have then to be translated to
     * English naming conventions.
     * <p>
     * Note, this will return the name of the class variable
     * <p>
     * If we don't have a corresponding object, we choose just to return the
     * original object. This should force a database query error and expose
     * any missing objects. This strategy is OK in development, but later we
     * need a better way of handling it.
     *
     * @param norwegianName The name in Norwegian
     * @return the English version of the Norwegian name if it exists, otherwise
     * the original Norwegian name.
     */
    protected String getInternalNameObject(String norwegianName) {
        String englishName = getEnglishNameObject(norwegianName);
        if (englishName == null)
            return norwegianName;
        else
            return englishName;
    }

    @Override
    public void enterTopStatement(ODataParser.TopStatementContext ctx) {
        super.enterTopStatement(ctx);
        processTop(Integer.valueOf(ctx.getChild(1).getText()));
    }

    @Override
    public void enterSkipStatement(ODataParser.SkipStatementContext ctx) {
        super.enterSkipStatement(ctx);
        processSkip(Integer.valueOf(ctx.getChild(1).getText()));
    }

    String getValue(ParserRuleContext context, Class klass) {
        ParseTree pContext = context.getChild(klass, 0);
        return pContext.getText();
    }

    String getValue(ParserRuleContext context, Class klass, int count) {
        ParserRuleContext context1 = (ParserRuleContext) context.getChild(klass, count);
        return context1.getText();
    }

    String getAliasAndAttribute(String entity, String attribute) {
        return entity.toLowerCase() + "_1." + firstLetterToLower(attribute);
    }

    private Object getPrimitiveTypeObject(ODataParser.PrimitiveLiteralContext context) {
        if (null != context.getChild(ODataParser.IntegerValueContext.class, 0)) {
            return Integer.valueOf(context
                    .getChild(ODataParser.IntegerValueContext.class, 0).getText());
        } else if (null != context.getChild(ODataParser.FloatValueContext.class, 0)) {
            return Double.valueOf(context
                    .getChild(ODataParser.FloatValueContext.class, 0).getText());
        } else if (null != context.getChild(ODataParser.DecimalLiteralContext.class, 0)) {
            return Double.valueOf(context
                    .getChild(ODataParser.DecimalLiteralContext.class, 0).getText());
        } else if (null != context.getChild(ODataParser.BooleanValueContext.class, 0)) {
            return Boolean.valueOf(context
                    .getChild(ODataParser.BooleanValueContext.class, 0).getText());
        } else if (null != context.getChild(ODataParser.QuotedStringContext.class, 0)) {
            String quotedString =
                    context.getChild(ODataParser.QuotedStringContext.class, 0).getText();
            String string = quotedString.substring(1,
                    quotedString.length() - 1);
            if (DATE_TIME_PATTERN.matcher(string).matches()) {
                return deserializeDateTime(string);
            } else if (DATE_PATTERN.matcher(string).matches()) {
                return deserializeDate(string);
            } else {
                return quotedString.substring(1, quotedString.length() - 1);
            }
        } else if (null != context.getChild(ODataParser.DquotedStringContext.class, 0)) {
            String quotedString =
                    context.getChild(ODataParser.DquotedStringContext.class, 0).getText();
            String string = quotedString.substring(1,
                    quotedString.length() - 1);
            if (DB_DATE_TIME_PATTERN.matcher(string).matches()) {
                return deserializeDateTime(string);
            } else if (DB_DATE_PATTERN.matcher(string).matches()) {
                return deserializeDate(string);
            } else {
                return quotedString.substring(1, quotedString.length() - 1);
            }
        } else if (null != context.getChild(ODataParser.QuotedUUIDContext.class, 0)) {
            String quotedUuidString =
                    context.getChild(ODataParser.QuotedUUIDContext.class, 0).getText();
            return fromString(quotedUuidString.substring(1,
                    quotedUuidString.length() - 1));
        } else if (null != context.getChild(ODataParser.NullTokenContext.class, 0) ||
                null != context.getChild(ODataParser.NullSpecLiteralContext.class, 0)) {
            return null;
        } else {
            String error = "Unknown primitive literal for context "
                    + context.getText();
            logger.error(error);
            throw new NikitaMalformedInputDataException(error);
        }
    }

    /**
     * Convert first letter to lowerCase. Useful when e.g., an Author is both
     * an Object and an attribute. referenceAuthor requires a capital
     * letter, while author as attribute requires small letters.
     *
     * @param originalString the string to convert
     * @return the converted string or the original
     */
    protected String firstLetterToLower(@NotNull String originalString) {
        if (originalString != null && !originalString.isBlank()) {
            char[] string = originalString.toCharArray();
            string[0] = Character.toLowerCase(string[0]);
            return new String(string);
        }
        return originalString;
    }
}
