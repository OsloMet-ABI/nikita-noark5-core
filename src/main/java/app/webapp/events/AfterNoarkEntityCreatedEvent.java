package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.admin.CreateLog;
import jakarta.validation.constraints.NotNull;

/**
 * Base class that can be used to identify CREATE events that can occur in
 * nikita.
 */
public class AfterNoarkEntityCreatedEvent
        extends AfterNoarkEntityEvent {
    final private CreateLog createLog;

    public AfterNoarkEntityCreatedEvent(@NotNull CreateLog createLog,
                                        @NotNull ISystemId entity) {
        super(entity);
        this.createLog = createLog;
    }

    public CreateLog getCreateLog() {
        return createLog;
    }
}
