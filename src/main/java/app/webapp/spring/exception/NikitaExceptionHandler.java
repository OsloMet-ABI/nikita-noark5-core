package app.webapp.spring.exception;


import app.webapp.exceptions.*;
import app.webapp.spring.security.NikitaAuthenticationEntryPoint;
import jakarta.servlet.http.HttpServletRequest;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

import static app.utils.constants.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.valueOf;

/**
 * Global exception handler for handling various exceptions in Nikita.
 * This class intercepts exceptions thrown during operation and wraps them in
 * a standardised response.
 * <p>
 * Note: 401 Not Authorized will not be handled here. See {@link NikitaAuthenticationEntryPoint}
 */
@ControllerAdvice
public class NikitaExceptionHandler {

    private static final Logger logger =
            LoggerFactory.getLogger(NikitaExceptionHandler.class);

    /**
     * Handles bad request exceptions (400 Bad Request).
     *
     * @param ex      the exception that occurred
     * @param request the current HTTP request
     * @return a ResponseEntity containing an ApiError object with details about the error
     */
    @ExceptionHandler({
            NikitaETAGMalformedHeaderException.class,
            NikitaMalformedHeaderException.class,
            NikitaMalformedInputDataException.class,
            NoarkInvalidStructureException.class,
            PatchMisconfigurationException.class,
            DataIntegrityViolationException.class,
            HttpMessageNotReadableException.class,
            MethodArgumentNotValidException.class
    })
    public ResponseEntity<ApiError> handleBadRequestPatch(
            final Exception ex, final HttpServletRequest request) {
        return ResponseEntity.status(BAD_REQUEST)
                .contentType(valueOf(NOARK5_V5_CONTENT_TYPE_JSON))
                .body(createApiError(BAD_REQUEST, ex, request));
    }

    /**
     * Handles concurrency exceptions (409 Conflict).
     *
     * @param ex      the concurrency exception that occurred
     * @param request the current HTTP request
     * @return a ResponseEntity containing an ApiError object with details about the error
     */
    @ExceptionHandler({
            NoarkConcurrencyException.class,
            ConstraintViolationException.class
    })
    public ResponseEntity<ApiError> handleConcurrencyException(
            final NoarkConcurrencyException ex,
            final HttpServletRequest request) {
        return ResponseEntity.status(CONFLICT)
                .contentType(valueOf(NOARK5_V5_CONTENT_TYPE_JSON))
                .body(createApiError(CONFLICT, ex, request));
    }

    /**
     * Handles unauthorised exceptions (401 Not Authorized).
     *
     * @param ex      the access denied exception that occurred
     * @param request the current HTTP request
     * @return a ResponseEntity containing an ApiError object with details about the error
     */
    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<ApiError> handleUnAuthorisedException(
            final Exception ex, final HttpServletRequest request) {
        return ResponseEntity.status(UNAUTHORIZED)
                .contentType(valueOf(NOARK5_V5_CONTENT_TYPE_JSON))
                .body(createApiError(UNAUTHORIZED, ex, request));
    }


    /**
     * Handles access denied exceptions (403 Forbidden).
     *
     * @param ex      the access denied exception that occurred
     * @param request the current HTTP request
     * @return a ResponseEntity containing an ApiError object with details about the error
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ApiError> handleAccessDeniedException(
            final Exception ex, final HttpServletRequest request) {
        return ResponseEntity.status(FORBIDDEN)
                .contentType(valueOf(NOARK5_V5_CONTENT_TYPE_JSON))
                .body(createApiError(FORBIDDEN, ex, request));
    }

    /**
     * Handles not found exceptions (404 Not Found).
     *
     * @param ex      the not found exception that occurred
     * @param request the current HTTP request
     * @return a ResponseEntity containing an ApiError object with details about the error
     */
    @ExceptionHandler(NoarkEntityNotFoundException.class)
    protected ResponseEntity<ApiError> handleNotFound(
            final RuntimeException ex, HttpServletRequest request) {
        return ResponseEntity.status(NOT_FOUND)
                .contentType(valueOf(NOARK5_V5_CONTENT_TYPE_JSON))
                .body(createApiError(NOT_FOUND, ex, request));
    }

    /**
     * Handles not acceptable exceptions (406 Not Acceptable).
     *
     * @param ex      the not acceptable exception that occurred
     * @param request the current HTTP request
     * @return a ResponseEntity containing an ApiError object with details about the error
     */
    @ExceptionHandler({
            UsernameExistsException.class,
            NoarkNotAcceptableException.class
    })
    protected ResponseEntity<ApiError> handleNotAcceptable(
            final RuntimeException ex, HttpServletRequest request) {
        return ResponseEntity.status(NOT_ACCEPTABLE)
                .contentType(valueOf(NOARK5_V5_CONTENT_TYPE_JSON))
                .body(createApiError(NOT_ACCEPTABLE, ex, request));
    }

    /**
     * Handles internal server errors (500 Internal Server Error)
     * <p>
     * This method captures a wide range of exceptions, including configuration issues,
     * runtime exceptions, and general exceptions, and returns a standardized error response
     * to the client.
     *
     * @param ex      the exception that occurred, which provides context for the error
     * @param request the current HTTP request, used to obtain additional context
     * @return a ResponseEntity containing an ApiError object with details about the error
     */
    @ExceptionHandler({
            NikitaMisconfigurationException.class,
            StorageException.class,
            NullPointerException.class,
            IllegalArgumentException.class,
            IllegalStateException.class,
            ClassCastException.class,
            ClassNotFoundException.class,
            NoSuchMethodException.class,
            IllegalAccessException.class,
            InvocationTargetException.class,
            InstantiationException.class,
            HttpMessageNotWritableException.class,
            Exception.class
    })
    public ResponseEntity<ApiError> handleInternalExceptions(
            final RuntimeException ex, HttpServletRequest request) {
        return ResponseEntity.status(OK)
                .contentType(valueOf(NOARK5_V5_CONTENT_TYPE_JSON))
                .body(createApiError(INTERNAL_SERVER_ERROR, ex, request));
    }

    /**
     * Creates an {@link ApiError} object based on the provided exception and HTTP status.
     *
     * @param httpStatusCode the HTTP status code to be returned in the API error response
     * @param ex             the exception that occurred, which provides context for the error
     * @param request        the current HTTP request, used to obtain the request URL
     * @return an {@link ApiError} object containing details about the error, including
     * the HTTP status, source URL, error message, root cause message, and exception details
     */
    private ApiError createApiError(final HttpStatusCode httpStatusCode, final Exception ex,
                                    final HttpServletRequest request) {
        logger.error("Exception occurred: {}", ex.getMessage());
        final String source = request.getRequestURL().toString();
        final String method = request.getMethod();
        final String errorMessage = Optional.ofNullable(ex.getMessage()).orElse(ex.getClass().getSimpleName());
        return new ApiError(httpStatusCode, source, errorMessage, getRootCauseMessage(ex), ex.toString(), method);
    }
}
