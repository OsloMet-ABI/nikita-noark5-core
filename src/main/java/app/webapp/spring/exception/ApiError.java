package app.webapp.spring.exception;

import app.webapp.payload.serializers.application.ApiErrorSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.http.HttpStatusCode;

@JsonSerialize(using = ApiErrorSerializer.class)
public class ApiError {

    private int status;
    private String message;

    private String urlSource;
    private String httpMethod;

    private String developerMessage;
    private String stackTrace;
    public ApiError(final HttpStatusCode status, final String message,
                    final String urlSource, final String developerMessage,
                    final String stackTrace, final String httpMethod) {
        super();

        this.status = status.value();
        this.message = message;
        this.urlSource = urlSource;
        this.developerMessage = developerMessage;
        this.stackTrace = stackTrace;
        this.httpMethod = httpMethod;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(final String developerMessage) {
        this.developerMessage = developerMessage;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String getUrlSource() {
        return urlSource;
    }

    public void setUrlSource(String urlSource) {
        this.urlSource = urlSource;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(status)
                .append(message)
                .append(developerMessage)
                .append(stackTrace)
                .toHashCode();
    }

    @Override
    public boolean equals(Object otherEntity) {
        if (otherEntity == null) {
            return false;
        }
        if (otherEntity == this) {
            return true;
        }
        if (otherEntity.getClass() != getClass()) {
            return false;
        }
        return new EqualsBuilder()
                .appendSuper(super.equals(otherEntity))
                .append(this.status, ((ApiError) otherEntity).status)
                .append(this.message, ((ApiError) otherEntity).message)
                .append(this.developerMessage,
                        ((ApiError) otherEntity).developerMessage)
                .append(this.stackTrace, ((ApiError) otherEntity).stackTrace)
                .isEquals();
    }

    // ignore stackTrace here!
    @Override
    public final String toString() {
        String builder = "ApiError [status=" + status +
                ", message=" + message +
                ", developerMessage=" + developerMessage +
                "]";
        return builder;
    }
}
