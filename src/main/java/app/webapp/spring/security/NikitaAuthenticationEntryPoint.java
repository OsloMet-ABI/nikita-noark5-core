package app.webapp.spring.security;

import app.webapp.spring.exception.ApiError;
import app.webapp.spring.exception.NikitaExceptionHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.io.IOException;
import java.util.Optional;

import static app.utils.constants.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

/**
 * Handles authentication failures by implementing the {@link AuthenticationEntryPoint} interface.
 * <p>
 * This class is responsible for returning a 401 Unauthorized response when an authentication
 * exception occurs.
 * <p>
 * Unlike a typical exception handler that would be defined under a {@link ControllerAdvice},
 * in {@link NikitaExceptionHandler} this class intercepts authentication failures at a lower
 * level in the Spring Security filter chain. This is needed as spring isn't allowing the
 * exception to reach the {@link NikitaExceptionHandler}.
 *
 * <p>
 * The {@link #commence(HttpServletRequest, HttpServletResponse, AuthenticationException)} method
 * is invoked whenever an authentication exception is thrown.
 * </p>
 */
@Component
public class NikitaAuthenticationEntryPoint
        implements AuthenticationEntryPoint {

    private static final Logger logger =
            LoggerFactory.getLogger(NikitaAuthenticationEntryPoint.class);

    private final ObjectMapper objectMapper;

    public NikitaAuthenticationEntryPoint(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    /**
     * Commences the authentication process by handling an authentication failure.
     * <p>
     * This method is invoked when an authentication exception occurs. It sets the
     * HTTP response status to 401 Unauthorized and prepares a JSON response containing
     * details about the error. The response includes the request URL, an error message,
     * and additional information about the exception.
     * <p>
     * The method performs the following actions:
     * <ol>
     *     <li>Sets the HTTP status code to 401 Unauthorized.</li>
     *     <li>Sets the content type of the response to the specified JSON type.</li>
     *     <li>Retrieves the request URL to include in the error response.</li>
     *     <li>Constructs an error message from the exception, defaulting to the exception's class name if no message is available.</li>
     *     <li>Creates an instance of {@link ApiError} containing the error details.</li>
     *     <li>Writes the {@link ApiError} instance as a JSON string to the response body.</li>
     * </ol>
     *
     * @param request  the {@link HttpServletRequest} that triggered the authentication failure.
     * @param response the {@link HttpServletResponse} to which the error response will be written.
     * @param ex       the {@link AuthenticationException} that occurred during the authentication process.
     * @throws IOException if an I/O error occurs while writing the response.
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException ex) throws IOException {
        logger.error("Exception occurred: {}", ex.getMessage());
        response.setStatus(UNAUTHORIZED.value());
        response.setContentType(NOARK5_V5_CONTENT_TYPE_JSON);
        final String source = request.getRequestURL().toString();
        final String errorMessage = Optional.ofNullable(ex.getMessage()).orElse(ex.getClass().getSimpleName());
        final String method = request.getMethod();
        ApiError apiError = new ApiError(UNAUTHORIZED, source, errorMessage, getRootCauseMessage(ex), ex.toString(), method);
        response.getWriter().write(objectMapper.writeValueAsString(apiError));
        response.getWriter().flush();
    }
}
