package app.webapp.spring.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.sql.DataSource;
import java.util.List;

import static app.utils.constants.Constants.ROLE_RECORDS_MANAGER;
import static app.utils.constants.Constants.SLASH;
import static app.utils.constants.N5ResourceMappings.FONDS;
import static app.utils.constants.PATHPatterns.PATTERN_METADATA_PATH;
import static app.utils.constants.PATHPatterns.PATTERN_NEW_FONDS_STRUCTURE_ALL;
import static org.springframework.http.HttpMethod.*;

@EnableWebSecurity
@Configuration
public class NikitaWebSecurityConfig {

    private final DataSource dataSource;
    private final NikitaAuthenticationEntryPoint authenticationEntryPoint;

    public NikitaWebSecurityConfig(DataSource dataSource,
                                   NikitaAuthenticationEntryPoint authenticationEntryPoint) {
        this.dataSource = dataSource;
        this.authenticationEntryPoint = authenticationEntryPoint;
    }

    // @formatter:off
        @Bean
        public SecurityFilterChain filterChain(HttpSecurity http)
                throws Exception {
            http
                    .cors(httpSecurityCorsConfigurer ->
                            httpSecurityCorsConfigurer.configurationSource(nikitaCorsConfiguration()))
                    .csrf(AbstractHttpConfigurer::disable)
                    .httpBasic(AbstractHttpConfigurer::disable)
                    .formLogin(AbstractHttpConfigurer::disable)
                    .exceptionHandling(exceptionHandlingConfigurer ->
                            exceptionHandlingConfigurer.authenticationEntryPoint(authenticationEntryPoint))
                    .oauth2ResourceServer(oauth2 -> oauth2
                            .jwt(jwt -> jwt
                                    .jwtAuthenticationConverter(JwtUtil::createJwtUser)
                            )
                    );
            // If the datasource is an H2 datasource, allow access to the database
            // user interface running in the browser. The log should show access URL.
            if (isDataSourceH2()) {
                configureH2Authorization(http);
            }
            configureGeneralAuthorization(http);
            return http.build();
        }
    // @formatter:on

    private void configureGeneralAuthorization(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers(GET, "/")
                        .permitAll()
                        .requestMatchers("/h2-console/**").permitAll()
                        // GET [api]/metadata/**, public to read basic structure
                        .requestMatchers(GET, PATTERN_METADATA_PATH)
                        .permitAll()
                        .requestMatchers(GET, "/v3/**")
                        .permitAll()
                        .requestMatchers(GET, "/**well-known/**").permitAll()
                        // POST GET [api]/arkivstruktur/ny-*, need role of record manager
                        .requestMatchers(HttpMethod.POST, PATTERN_NEW_FONDS_STRUCTURE_ALL)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(GET, PATTERN_NEW_FONDS_STRUCTURE_ALL)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        // POST PUT PATCH [api]/arkivstruktur/**, need role of record keeper
                        .requestMatchers(PUT, SLASH + FONDS + SLASH + "**")
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(PATCH, SLASH + FONDS + SLASH + "**")
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(DELETE, "/**")
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        // POST PUT PATCH DELETE [api]/metadata/**, need role of record keeper
                        .requestMatchers(PATCH, PATTERN_METADATA_PATH)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(PUT, PATTERN_METADATA_PATH)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(POST, PATTERN_METADATA_PATH)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(DELETE, PATTERN_METADATA_PATH)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .anyRequest().authenticated()
                );
    }

    @Bean
    public CorsConfigurationSource nikitaCorsConfiguration() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedOrigins(List.of("*"));
        corsConfiguration.setAllowedMethods(List.of("POST", "GET", "PUT", "PATCH", "DELETE"));
        corsConfiguration.setAllowedHeaders(List.of("*"));
        corsConfiguration.setMaxAge(3600L);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfiguration);
        return source;
    }

    /**
     * Configures authorization settings for the H2 database console.
     * <p>
     * This method sets up the security configuration to allow unrestricted access
     * to the H2 console located at the path "/h2-console/**". It also configures
     * the HTTP headers to allow the H2 console to be displayed in a frame from
     * the same origin.
     *
     * @param http the {@link HttpSecurity} object to configure security settings.
     * @throws Exception if an error occurs while configuring the security settings.
     */
    private void configureH2Authorization(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(authorize -> authorize
                .requestMatchers("/h2-console/**").permitAll());
        http.headers(headers -> headers.frameOptions(
                HeadersConfigurer.FrameOptionsConfig::sameOrigin));
    }

    /**
     * Checks if the current DataSource is an H2 database.
     * <p>
     * This method attempts to retrieve the JDBC URL of the current DataSource
     * and checks if it starts with "jdbc:h2". If the URL indicates that the
     * DataSource is an H2 database, the method returns true; otherwise, it
     * returns false. If an exception occurs while attempting to retrieve the
     * connection metadata, the method will catch the exception and return false.
     *
     * @return true if the DataSource is an H2 database; false otherwise.
     */
    private boolean isDataSourceH2() {
        try {
            String jdbcUrl = dataSource.getConnection().getMetaData().getURL();
            return jdbcUrl != null && jdbcUrl.startsWith("jdbc:h2");
        } catch (Exception e) {
            // Something goes wrong here it does not matter
            return false;
        }
    }

}
