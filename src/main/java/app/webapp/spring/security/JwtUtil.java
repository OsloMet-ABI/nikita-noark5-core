package app.webapp.spring.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static app.utils.constants.OIDCConstants.REALM_ACCESS_KEY;
import static app.utils.constants.OIDCConstants.RESOURCE_ACCESS_KEY;

public class JwtUtil {
    private JwtUtil() {
    }

    /**
     * Creates a JwtUser object from the provided Jwt instance.
     *
     * This method extracts claims from the Jwt and populates a list of
     * GrantedAuthority based on the roles defined in the claims. It checks
     * for specific claim keys related to realm and resource access.
     *
     * @param jwt the Jwt instance containing claims to extract roles from
     * @return a JwtUser object populated with the extracted roles and the original Jwt
     */
    public static JwtUser createJwtUser(Jwt jwt) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Map.Entry<String, Object> claim : jwt.getClaims().entrySet()) {
            Object value = claim.getValue();
            if (value instanceof Map<?, ?> valueMap) {

                String claimKey = claim.getKey().toLowerCase();
                if (claimKey.equals(REALM_ACCESS_KEY)) {
                    extractRoles(valueMap, authorities);
                } else if (claimKey.equals(RESOURCE_ACCESS_KEY)) {
                    extractResourceRoles(valueMap, authorities);
                }
            }
        }
        return new JwtUser(jwt, authorities);
    }

    /**
     * Extracts roles from a resource map and adds them to the provided list of authorities.
     *
     * This method iterates over the entries in the resource map, checking if each
     * entry's value is a map. If it is, it calls the extractRoles method to
     * extract roles from that map.
     *
     * @param resourceMap the map containing resource roles to extract
     * @param authorities the list to which the extracted GrantedAuthority objects will be added
     */
    private static void extractResourceRoles(Map<?, ?> resourceMap, List<GrantedAuthority> authorities) {
        for (Map.Entry<?, ?> resourceEntry : resourceMap.entrySet()) {
            Object resourceValue = resourceEntry.getValue();
            if (resourceValue instanceof Map) {
                extractRoles((Map<?, ?>) resourceValue, authorities);
            }
        }
    }

    /**
     * Extracts roles from a value map and adds them to the provided list of authorities.
     *
     * This method retrieves the list of roles from the value map and adds each
     * role as a GrantedAuthority to the authorities list. If the roles list is
     * null, no action is taken.
     *
     * @param valueMap the map containing roles to extract
     * @param authorities the list to which the extracted GrantedAuthority objects will be added
     */
    private static void extractRoles(Map<?, ?> valueMap, List<GrantedAuthority> authorities) {
        List<String> roles = (List<String>) valueMap.get("roles");
        if (roles != null) {
            for (String role : roles) {
                authorities.add(new SimpleGrantedAuthority(role));
            }
        }
    }
}
