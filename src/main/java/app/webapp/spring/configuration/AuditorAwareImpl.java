package app.webapp.spring.configuration;

import app.webapp.exceptions.NikitaMisconfigurationException;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static app.utils.constants.ErrorMessagesConstants.AUTHENTICATION_CONFIGURATION_PROBLEM;

/**
 * TODO: It is likely this class can be removed once we do startup values from an external script!
 */
@Component
public class AuditorAwareImpl
        implements AuditorAware<String> {

    /**
     * Note this can handle entities created during startup (system) or the
     * actual logged-in user.
     *
     * @return
     */
    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.of("system");
        }
        // TODO: Remove this! The anonymousUser part stems from not having a logged in user when running startup stuff
        //  internally in the application
        if (authentication.getPrincipal().equals("anonymousUser")) {
            return Optional.of("system");
        }
        Object userDetailsObject = authentication.getPrincipal();
        if (userDetailsObject instanceof Jwt jwt) {
            String username = (String) jwt.getClaims().get("preferred_username");
            return Optional.of(username);
        }
        throw new NikitaMisconfigurationException(AUTHENTICATION_CONFIGURATION_PROBLEM);
    }
}
