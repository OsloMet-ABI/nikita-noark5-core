package app.webapp.spring.filters;

import app.webapp.spring.exception.ApiError;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import static app.utils.constants.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static app.utils.constants.ErrorMessagesConstants.ETAG_ILLEGAL_VALUE;
import static app.utils.constants.ErrorMessagesConstants.REQUIRED_ETAG_MISSING;
import static java.lang.String.format;
import static org.springframework.http.HttpHeaders.ETAG;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * A filter that validates the presence and correctness of the ETag header
 * for incoming PUT and PATCH requests.
 *
 * <p>
 * The filter serves three main purposes:
 * <ol>
 *     <li>Ensures that each incoming PUT or PATCH request contains an ETag header.</li>
 *     <li>Validates that the ETag is a numeric value, as required by the service layer.</li>
 *     <li>By throwing errors early, it allows the service layer to assume that the ETag is not null and is numeric.</li>
 * </ol>
 * </p>
 *
 * <p>
 * If the ETag is missing or invalid, the filter responds with a 400 Bad Request status
 * and provides an error message in JSON format. This ensures that only valid requests reach the service layer.
 * </p>
 *
 * <p>
 * This filter is annotated with {@link org.springframework.stereotype.Component} to allow
 * it to be automatically detected and registered as a Spring bean.
 * </p>
 *
 * @see org.springframework.web.filter.OncePerRequestFilter
 * @see ApiError
 */
@Component
public class ETAGFilter
        extends OncePerRequestFilter {

    private static final Logger logger =
            LoggerFactory.getLogger(ETAGFilter.class);

    // ObjectMapper for converting objects to JSON
    private final ObjectMapper objectMapper;

    public ETAGFilter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    /**
     * Filters incoming HTTP requests to validate the presence and correctness of the ETag header
     * for PUT and PATCH requests. If the ETag is missing or invalid, an error response is generated
     * and further processing of the request is halted.
     *
     * @param request     the HttpServletRequest object that contains the request the client made to the server
     * @param response    the HttpServletResponse object that contains the response the server sends to the client
     * @param filterChain the FilterChain used to invoke the next filter or the resource
     * @throws IOException      if an input or output error occurs during the processing of the request
     * @throws ServletException if the request could not be handled
     */
    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain) throws IOException, ServletException {

        String method = request.getMethod();
        if (isPutOrPatch(method)) {
            String quotedETAG = request.getHeader(ETAG);
            if (quotedETAG == null) {
                handleError(request, response, format(REQUIRED_ETAG_MISSING, method));
                return; // Finish request chain if ETag is missing
            }

            String eTag = quotedETAG.replace("\"", ""); // Remove quotes
            if (!isValidETag(eTag)) {
                handleError(request, response, format(ETAG_ILLEGAL_VALUE, quotedETAG));
                return; // Finish request chain if ETag is invalid
            }
        }
        // If not a PUT or a PATCH, continue processing the filter chain
        filterChain.doFilter(request, response);
    }

    /**
     * Checks if the given HTTP method is either PUT or PATCH.
     *
     * @param method the HTTP method to check
     * @return true if the method is PUT or PATCH, false otherwise
     */
    private boolean isPutOrPatch(String method) {
        return PUT.matches(method) || PATCH.matches(method);
    }

    /**
     * Validates the provided ETag string. The ETag is considered valid if it is not null or blank
     * and can be parsed as a non-negative long value.
     *
     * @param eTag the ETag string to validate
     * @return true if the ETag is valid, false otherwise
     */
    private boolean isValidETag(String eTag) {
        if (eTag != null && !eTag.isBlank()) {
            try {
                return Long.parseLong(eTag) >= 0; // Check if ETag is non-negative
            } catch (NumberFormatException nfe) {
                return false; // Return false if parsing fails
            }
        }
        return false; // Return false if ETag is null or blank
    }

    /**
     * Handles errors that occur during request processing by logging the error message,
     * setting the HTTP response status to 400 Bad Request, and writing an error response
     * in JSON format to the client.
     *
     * <p>
     * This method constructs an {@link ApiError} object containing details about the error,
     * including the HTTP status, error message, request URL, and HTTP method. The error response
     * is then serialized to JSON and sent back to the client.
     * </p>
     *
     * @param request  the HttpServletRequest object that contains the request made by the client
     * @param response the HttpServletResponse object that contains the response to be sent to the client
     * @param message  the error message to be logged and included in the error response
     * @throws IOException if an input or output error occurs while writing the response
     */
    private void handleError(HttpServletRequest request,
                             HttpServletResponse response,
                             String message) throws IOException {
        logger.error("Exception occurred: {}", message);
        response.setStatus(BAD_REQUEST.value());
        response.setContentType(NOARK5_V5_CONTENT_TYPE_JSON);
        final String source = request.getRequestURL().toString();
        final String method = request.getMethod();
        ApiError apiError = new ApiError(BAD_REQUEST, message, source, "", "", method);
        response.getWriter().write(objectMapper.writeValueAsString(apiError));
        response.getWriter().flush();
    }
}
