package app.controller.noark5;

import app.domain.filehandling.LargeFileUploadRegister;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.secondary.Conversion;
import app.service.interfaces.IDocumentObjectService;
import app.webapp.exceptions.NikitaException;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NikitaMisconfigurationException;
import app.webapp.payload.links.DocumentObjectLinks;
import app.webapp.payload.links.secondary.ConversionLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.ErrorMessagesConstants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static java.lang.Long.parseLong;
import static java.lang.String.format;
import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = HREF_BASE_DOCUMENT_OBJECT)
public class DocumentObjectController {

    private final Logger logger =
            LoggerFactory.getLogger(DocumentObjectController.class);
    private final IDocumentObjectService documentObjectService;

    public DocumentObjectController(
            IDocumentObjectService documentObjectService) {
        this.documentObjectService = documentObjectService;
    }

    // API - All GET Requests (CRUD - READ)
    // Get a documentObject identified by systemID
    // GET [contextPath][api]/arkivstruktur/dokumentobjekt/{systemID}
    @Operation(summary = "Retrieves a single DocumentObject entity " +
            "identified by given a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DocumentObject returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + SYSTEM_ID_PARAMETER,
            produces = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<DocumentObjectLinks>
    findOneDocumentObjectBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the documentObject to retrieve",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemId) {
        DocumentObjectLinks documentObjectLinks =
                documentObjectService.findBySystemId(systemId);
        return ResponseEntity.status(OK)
                .body(documentObjectLinks);
    }

    // Get all documentObject
    // GET [contextPath][api]/arkivstruktur/dokumentobjekt/
    @Operation(summary = "Retrieves multiple DocumentObject entities limited " +
            "by ownership rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DocumentObject list found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(produces = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<DocumentObjectLinks> findAllDocumentObject() {
        DocumentObjectLinks documentObjectLinks =
                documentObjectService.findAll();
        return ResponseEntity.status(OK)
                .body(documentObjectLinks);
    }

    // Get a file identified by systemID retrievable with referanseFile
    // GET [contextPath][api]/arkivstruktur/dokumentobjekt/{systemID}/referanseFil
    @Operation(summary = "Downloads a file associated with the documentObject" +
            " identified by a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "File download successful"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + SYSTEM_ID_PARAMETER + SLASH + REFERENCE_FILE)
    public void handleFileDownload(HttpServletResponse response,
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the documentObject that has a file " +
                            "associated with it",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemId)
            throws IOException {
        Resource fileResource = documentObjectService.loadAsResource(systemId);
        try (InputStream filestream = fileResource.getInputStream()) {
            IOUtils.copyLarge(filestream,
                    response.getOutputStream());
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new NikitaMisconfigurationException(e.getMessage());
        }
        response.flushBuffer();
    }

    // GET [contextPath][api]/arkivstruktur/dokumentobjekt/{systemID}/konvertering
    @Operation(summary = "Return list of conversions related to the" +
            "documentObject identified by a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "List of Conversions returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + SYSTEM_ID_PARAMETER + SLASH + CONVERSION,
            produces = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<ConversionLinks>
    findAllConversionAssociatedWithDocumentObject(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the documentObject",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        return ResponseEntity.status(OK)
                .body(documentObjectService
                        .findAllConversionAssociatedWithDocumentObject(
                                systemID));
    }

    // GET [contextPath][api]/arkivstruktur/dokumentobjekt/{systemID}/konvertering/{subSystemID}
    @Operation(summary = "Return a conversion related to the documentObject " +
            "identified by a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Conversion returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + SYSTEM_ID_PARAMETER + SLASH +
            CONVERSION + SLASH + SUB_SYSTEM_ID_PARAMETER,
            produces = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<ConversionLinks>
    findAllConversionAssociatedWithDocumentObject(
            @Parameter(name = SYSTEM_ID,
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            @Parameter(name = "subSystemID",
                    required = true)
            @PathVariable("subSystemID") final UUID subSystemID) {
        return ResponseEntity.status(OK)
                .body(documentObjectService
                        .findConversionAssociatedWithDocumentObject
                                (systemID, subSystemID));
    }

    // GET [contextPath][api]/arkivstruktur/dokumentobject/{systemId}/ny-konvertering
    @Operation(summary = "Create a Conversion with default values")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Conversion returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + SYSTEM_ID_PARAMETER + SLASH + NEW_CONVERSION,
            produces = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<ConversionLinks> createDefaultConversion(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of documentObject to associate " +
                            "the conversion with.",
                    required = true)
            @PathVariable UUID systemID) {
        return ResponseEntity.status(OK)
                .body(documentObjectService
                        .generateDefaultConversion(systemID));
    }

    // API - All POST Requests (CRUD - CREATE)
    // POST [contextPath][api]/arkivstruktur/dokumentobject/{systemId}/ny-konvertering
    @Operation(summary = "Persists a Conversion object associated with " +
            "the given DocumentObject systemId",
            description = "Returns the newly created Conversion after it was " +
                    "associated with a DocumentObject object and  persisted " +
                    "to the database")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Conversion " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "Conversion " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type Conversion"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PostMapping(value = SLASH + SYSTEM_ID_PARAMETER + SLASH + NEW_CONVERSION,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<ConversionLinks>
    createConversionAssociatedWithDocumentObject(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of documentObject to associate " +
                            "the conversion with.",
                    required = true)
            @PathVariable UUID systemID,
            @Parameter(name = "conversion",
                    description = "Incoming documentObject object",
                    required = true)
            @RequestBody Conversion conversion)
            throws NikitaException {
        ConversionLinks conversionLinks = documentObjectService
                .createConversionAssociatedWithDocumentObject(
                        systemID, conversion);
        return ResponseEntity.status(CREATED)
                .body(conversionLinks);
    }

    // upload a file and associate it with a documentObject
    // POST [contextPath][api]/arkivstruktur/dokumentobjekt/{systemID}/referanseFil
    @Operation(summary = "Uploads a file and associates it with the " +
            "documentObject identified by a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "File uploaded successfully"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PostMapping(value = SLASH + SYSTEM_ID_PARAMETER + SLASH + REFERENCE_FILE,
            headers = "Accept=*/*",
            produces = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<DocumentObjectLinks> handleFileUpload(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the documentObject you wish to" +
                            " associate a file with",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            HttpServletRequest request)
            throws IOException {
        if (isNumericZero(request)) {
            checkStartOfLargeFileUploadHeaders(request);

            LargeFileUploadRegister largeFileUploadRegister = new LargeFileUploadRegister.Builder()
                    .setContentLength(getNumericValueOrThrow(request, CONTENT_LENGTH))
                    .setUploadContentType(request.getHeader(UPLOAD_CONTENT_TYPE))
                    .setUploadContentLength(getNumericValueOrThrow(request, UPLOAD_CONTENT_LENGTH))
                    .build();

            HttpHeaders headers = new HttpHeaders();
            headers.add("Location", "Great value");

            return ResponseEntity.status(OK).headers(headers).body(null);
        } else {
            DocumentObjectLinks documentObjectLinks =
                    documentObjectService.handleIncomingFile(systemID);
            return ResponseEntity.status(CREATED)
                    .body(documentObjectLinks);
        }
    }

    @PutMapping(value = SLASH + SYSTEM_ID_PARAMETER + SLASH + REFERENCE_FILE,
            headers = "Accept=*/*",
            produces = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<DocumentObjectLinks> handleFileChunkUpload(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the documentObject you wish to" +
                            " associate a file with",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID)
            throws IOException {
        DocumentObjectLinks documentObjectLinks =
                documentObjectService.handleIncomingFileChunk(systemID);

        // If not final, 200. If final 201
        return ResponseEntity.status(OK)
                .body(documentObjectLinks);
    }

    // Delete a DocumentObject identified by systemID
    // DELETE [contextPath][api]/arkivstruktur/dokumentobjekt/{systemId}
    @Operation(summary = "Deletes a single DocumentObject object")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "Conversion deleted"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteDocumentObjectBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the documentObject to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID)
            throws IOException {
        documentObjectService.deleteEntity(systemID);
        return ResponseEntity.status(NO_CONTENT).
                body(DELETE_RESPONSE);
    }

    // Delete a Conversion object identified by dokumentobjekt and
    // Conversion systemID
    // DELETE [contextPath][api]/arkivstruktur/dokumentobjekt/{systemId}/konvertering/{systemId}
    @Operation(summary = "Deletes a single Conversion object associated with " +
            "the given DocumentObject")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "Conversion deleted"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = SLASH + SYSTEM_ID_PARAMETER +
            SLASH + CONVERSION + SLASH + SUB_SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteConversionByDocumentObjectAndSystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the documentObject to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            @Parameter(name = "subSystemID",
                    required = true)
            @PathVariable("subSystemID") final UUID subSystemID) {
        documentObjectService.deleteConversion(systemID, subSystemID);
        return ResponseEntity.status(NO_CONTENT).
                body(DELETE_RESPONSE);
    }

    // Delete all DocumentObject
    // DELETE [contextPath][api]/arkivstruktur/dokumentobjekt/
    @Operation(summary = "Deletes all DocumentObject")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "Deleted all DocumentObject"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping
    public ResponseEntity<String> deleteAllDocumentObject()
            throws IOException {
        if (documentObjectService.deleteAll() > 0) {
            return ResponseEntity.status(MULTI_STATUS)
                    .body(BATCH_DELETE_DOCUMENT_OBJECT_FILE_MESSAGE);
        } else
            return ResponseEntity.status(NO_CONTENT)
                    .body(DELETE_RESPONSE);
    }

    // API - All PUT Requests (CRUD - UPDATE)
    // Update a DocumentObject
    // PUT [contextPath][api]/arkivstruktur/dokumentobjekt/{systemID}
    @Operation(summary = "Updates a DocumentObject object",
            description = "Returns the newly update DocumentObject object " +
                    "after it is persisted to the database")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DocumentObject " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "DocumentObject " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type DocumentObject"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})

    @PutMapping(value = SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<DocumentObjectLinks> updateDocumentObject(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of documentObject to update.",
                    required = true)
            @PathVariable(SYSTEM_ID) UUID systemID,
            @Parameter(name = "documentObject",
                    description = "Incoming documentObject object",
                    required = true)
            @RequestBody DocumentObject documentObject)
            throws NikitaException {
        DocumentObjectLinks documentObjectLinks =
                documentObjectService.handleUpdate(systemID, documentObject);
        return ResponseEntity.status(OK)
                .body(documentObjectLinks);
    }

    // PUT [contextPath][api]/arkivstruktur/dokumentobjekt/{systemID}/konvertering/{systemID}
    @Operation(summary = "Updates a Conversion object",
            description = "Returns the newly updateConversion object after it" +
                    " is persisted to the database")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Conversion " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "Conversion " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type Conversion"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = SLASH + SYSTEM_ID_PARAMETER + SLASH + CONVERSION +
            SLASH + SUB_SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<ConversionLinks> updateConversion(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of conversion to update.",
                    required = true)
            @PathVariable(SYSTEM_ID) UUID systemID,
            @Parameter(name = "subSystemID",
                    description = "systemId of conversion to update.",
                    required = true)
            @PathVariable("subSystemID") UUID subSystemID,
            @Parameter(name = "conversion",
                    description = "Incoming conversion object",
                    required = true)
            @RequestBody Conversion conversion) throws NikitaException {
        return ResponseEntity.status(OK)
                .body(documentObjectService.handleUpdateConversionBySystemId
                        (systemID, subSystemID, conversion));
    }

    /**
     * Utility method to get a Long value from an incoming header or throw a 400
     *
     * @param request    the incoming HTTP request
     * @param headerName the header name e.g., Content-Length
     * @return the value corresponding to the header e.g. 202 or an exception is thrown
     */

    private Long getNumericValueOrThrow(HttpServletRequest request,
                                        @NotNull String headerName) {
        try {
            return parseLong(request.getHeader(headerName));
        } catch (NumberFormatException e) {
            throw new NikitaMalformedInputDataException(
                    format(LENGTH_HEADER_MISSING, headerName));
        }
    }

    /**
     * Utility method to check if Content-Length header is greater than 0
     *
     * @param request the incoming HTTP request
     * @return true if the value is an integer and positive
     */
    private boolean isNumericZero(HttpServletRequest request) {
        try {
            String contentLength = request.getHeader(CONTENT_LENGTH);
            if (null != contentLength && parseLong(contentLength) == 0) {
                return true;
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return false;
    }

    /**
     * Utility method to check that the required headers are present and are not blank.
     *
     * @param request the incoming HTTP request to check
     */
    private void checkStartOfLargeFileUploadHeaders(HttpServletRequest request) {
        Map<String, String> headersToCheck = new HashMap<>();
        headersToCheck.put(CONTENT_LENGTH, LARGE_FILE_UPLOAD_HEADER_MISSING);
        headersToCheck.put(UPLOAD_CONTENT_TYPE, LARGE_FILE_UPLOAD_HEADER_MISSING);
        headersToCheck.put(UPLOAD_CONTENT_LENGTH, LARGE_FILE_UPLOAD_HEADER_MISSING);

        for (Map.Entry<String, String> entry : headersToCheck.entrySet()) {
            String headerName = entry.getKey();
            if (request.getHeader(headerName) == null) {
                throw new NikitaMalformedInputDataException(
                        format(entry.getValue(), headerName));
            }
            if (request.getHeader(headerName).isBlank()) {
                throw new NikitaMalformedInputDataException(
                        format(entry.getValue(), headerName));
            }
        }
    }

}
