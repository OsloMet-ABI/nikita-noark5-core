package app.controller.noark5.log;

import app.domain.noark5.admin.ChangeLog;
import app.service.interfaces.IChangeLogService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.ChangeLogLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_LOGGING + SLASH,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class ChangeLogController {

    private final IChangeLogService changeLogService;

    public ChangeLogController(IChangeLogService changeLogService) {
        this.changeLogService = changeLogService;
    }

    // GET [contextPath][api]/loggingogsporing/endringslogg/
    @Operation(
            summary = "Retrieves all ChangeLog entities limited by ownership " +
                    " rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ChangeLog found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)
    })
    @GetMapping(CHANGE_LOG)
    public ResponseEntity<ChangeLogLinks> findAllChangeLog() {
        ChangeLogLinks changeLogLinks = changeLogService.
                findChangeLogByOwner();
        return ResponseEntity.status(OK)
                .body(changeLogLinks);
    }

    // GET [contextPath][api]/loggingogsporing/endringslogg/{systemId}/
    @Operation(summary = "Retrieves a single changeLog entity given a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ChangeLog returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = CHANGE_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<ChangeLogLinks> findOne(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of changeLog to retrieve.",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        ChangeLogLinks changeLogLinks =
                changeLogService.findSingleChangeLog(systemID);
        return ResponseEntity.status(OK)
                .body(changeLogLinks);
    }

    // PUT [contextPath][api]/loggingogsporing/endringslogg/{systemId}/
    @Operation(
            summary = "Updates a ChangeLog object",
            description = "Returns the newly updated ChangeLog object after " +
                    "it is persisted to the database")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ChangeLog " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "ChangeLog " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type ChangeLog"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = CHANGE_LOG + SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<ChangeLogLinks> updateChangeLog(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of changeLog to update.",
                    required = true)
            @PathVariable(SYSTEM_ID) UUID systemID,
            @Parameter(name = "changeLog",
                    description = "Incoming changeLog object",
                    required = true)
            @RequestBody ChangeLog changeLog) throws NikitaException {
        ChangeLogLinks changeLogLinks =
                changeLogService.handleUpdate(systemID, changeLog);
        return ResponseEntity.status(OK)
                .body(changeLogLinks);
    }

    // DELETE [contextPath][api]/loggingogsporing/endringslogg/{systemId}/
    @Operation(
            summary = "Deletes a single ChangeLog entity identified by " +
                    "systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "ok message"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = CHANGE_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteChangeLogBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the changeLog to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        changeLogService.deleteEntity(systemID);
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
