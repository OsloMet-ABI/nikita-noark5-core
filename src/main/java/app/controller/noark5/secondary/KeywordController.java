package app.controller.noark5.secondary;

import app.domain.noark5.secondary.Keyword;
import app.service.interfaces.secondary.IKeywordService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.secondary.KeywordLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_FONDS_STRUCTURE + SLASH + KEYWORD,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class KeywordController {

    private final IKeywordService keywordService;

    public KeywordController(
            IKeywordService keywordService) {
        this.keywordService = keywordService;
    }

    // API - All GET Requests (CRUD - READ)
    // GET [contextPath][api]/arkivstruktur/noekkelord/
    // https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/noekkelord/
    @Operation(summary = "Retrieves multiple Keyword entities limited " +
            "by ownership rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Keyword found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping()
    public ResponseEntity<KeywordLinks> findAllKeyword() {
        KeywordLinks keywordLinks = keywordService.findAll();
        return ResponseEntity.status(OK)
                .body(keywordLinks);
    }

    // GET [contextPath][api]/arkivstruktur/noekkelord/{systemId}
    @Operation(summary = "Retrieves a single Keyword entity given a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Keyword returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<KeywordLinks> findKeywordBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the Keyword to retrieve",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        KeywordLinks keywordLinks = keywordService.findBySystemId(systemID);
        return ResponseEntity.status(OK)
                .body(keywordLinks);
    }

    // PUT [contextPath][api]/arkivstruktur/noekkelord/{systemId}
    @Operation(summary = "Updates a Keyword identified by a given systemId",
            description = "Returns the newly updated nationalIdentifierPerson")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Keyword " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "Keyword " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type Keyword"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<KeywordLinks> updateKeywordBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of Keyword to update",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            @Parameter(name = "Keyword",
                    description = "Incoming Keyword object",
                    required = true)
            @RequestBody Keyword keyword) throws NikitaException {
        KeywordLinks keywordLinks =
                keywordService.updateKeywordBySystemId(systemID,
                        keyword);
        return ResponseEntity.status(OK)
                .body(keywordLinks);
    }

    // DELETE [contextPath][api]/arkivstruktur/noekkelord/{systemID}/
    @Operation(summary = "Deletes a single Keyword entity identified by " +
            "systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "Keyword deleted"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteKeywordBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the keyword to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        keywordService.deleteKeywordBySystemId(systemID);
        return ResponseEntity.status(NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
