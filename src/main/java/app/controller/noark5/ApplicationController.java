package app.controller.noark5;


import app.service.application.VendorService;
import app.webapp.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static app.utils.constants.Constants.*;
import static org.springframework.http.HttpStatus.OK;

/**
 * REST controller that returns information about the Noark 5 cores
 * conformity to standards.
 */
@RestController
@RequestMapping(produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class ApplicationController {

    private final VendorService vendorService;

    public ApplicationController(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    /**
     * identify the interfaces the core supports
     *
     * @return the application details along with the correct response code
     * (200 OK, or 500 Internal Error)
     */
    // API - All GET Requests (CRUD - READ)
    @GetMapping
    public ResponseEntity<ApplicationDetails> identify() {
        return ResponseEntity.status(OK)
                .body(vendorService.getApplicationDetails());
    }

    @GetMapping(value = HREF_SYSTEM_INFORMATION)
    public ResponseEntity<SystemInformation> getSystemInformation() {
        return ResponseEntity.status(OK)
                .body(vendorService.getSystemInformation());
    }

    @GetMapping(value = HREF_BASE_FONDS_STRUCTURE)
    public ResponseEntity<FondsStructureDetails> getFondsStructure() {
        return ResponseEntity.status(OK)
                .body(vendorService.getFondsStructureDetails());
    }

    @GetMapping(value = HREF_BASE_METADATA)
    public ResponseEntity<MetadataDetails> getMetadataPath() {
        return ResponseEntity.status(OK)
                .body(vendorService.getMetadataDetails());
    }

    @GetMapping(value = HREF_BASE_ADMIN)
    public ResponseEntity<AdministrationDetails> getAdminPath() {
        return ResponseEntity.status(OK)
                .body(vendorService.getAdministrationDetails());
    }

    @GetMapping(value = HREF_BASE_CASE_HANDLING)
    public ResponseEntity<CaseHandlingDetails> getCaseHandling() {
        return ResponseEntity.status(OK)
                .body(vendorService.getCaseHandlingDetails());
    }

    @GetMapping(value = HREF_BASE_LOGGING)
    public ResponseEntity<LoggingDetails> getLogging() {
        return ResponseEntity.status(OK)
                .body(vendorService.getLoggingDetails());
    }
}
