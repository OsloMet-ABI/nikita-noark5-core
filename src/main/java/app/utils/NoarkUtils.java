package app.utils;

import app.domain.interfaces.IDeletion;
import app.domain.interfaces.IDocumentMedium;
import app.domain.interfaces.IScreening;
import app.domain.interfaces.entities.IDeletionEntity;
import app.domain.interfaces.entities.IFinalise;
import app.domain.noark5.metadata.DocumentMedium;
import app.domain.noark5.metadata.ScreeningMetadata;
import app.domain.noark5.secondary.Deletion;
import app.domain.noark5.secondary.Screening;
import app.domain.noark5.secondary.ScreeningMetadataLocal;
import app.service.interfaces.metadata.IMetadataService;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import jakarta.validation.constraints.NotNull;

import java.time.OffsetDateTime;

public final class NoarkUtils {

    // You shall not instantiate me!
    private NoarkUtils() {
    }

    public static final class NoarkEntity {

        public static final class Create {
            public static void setFinaliseEntityValues(
                    IFinalise finaliseEntity) {
                finaliseEntity.setFinalisedDate(null);
                finaliseEntity.setFinalisedBy(null);
            }

            public static void validateScreening(
                    IMetadataService service,
                    IScreening entity) {
                Screening screening = entity.getReferenceScreening();
                if (null != screening) {
                    validateScreening(service, screening);
                } else {
                    // TODO: Should an exception be thrown?
                }
            }

            public static void validateScreening(
                    @NotNull IMetadataService service,
                    @NotNull Screening screening) {
                for (ScreeningMetadataLocal screeningMetadataLocal :
                        screening.getReferenceScreeningMetadata()) {
                    ScreeningMetadata metadata = (ScreeningMetadata)
                            service.findValidMetadata(screeningMetadataLocal);
                    screeningMetadataLocal.setCodeName(metadata.getCodeName());
                }
            }

            public static void validateScreeningMetadata(
                    @NotNull IMetadataService service,
                    @NotNull ScreeningMetadata screeningMetadata) {
                ScreeningMetadata metadata = (ScreeningMetadata)
                        service.findValidMetadata(screeningMetadata);
                screeningMetadata.setCodeName(metadata.getCodeName());
            }

            public static void validateDocumentMedium(
                    IMetadataService service,
                    IDocumentMedium entity) {
                if (null != entity.getDocumentMedium()) {
                    DocumentMedium metadata = (DocumentMedium)
                            service.findValidMetadata(entity.getDocumentMedium());
                    entity.setDocumentMedium(metadata);
                }
            }

            public static void validateDeletion(IDeletionEntity entity) {
                if (null == entity)
                    return;
                if (OffsetDateTime.now().isBefore(entity.getDeletionDate())) {
                    String info = "Deletion date is in the future.";
                    throw new NikitaMalformedInputDataException(info);
                }
            }

            public static void updateDeletion(
                    @NotNull final IDeletion incoming,
                    @NotNull final IDeletion existing) {
                Deletion incomingDeletion = incoming.getReferenceDeletion();
                Deletion existingDeletion = existing.getReferenceDeletion();
                if (null == existingDeletion && null == incomingDeletion)
                    return;
                if (null != existingDeletion && null == incomingDeletion) {
                    existing.setReferenceDeletion(null);
                    return;
                }
                validateDeletion(incomingDeletion);
                existingDeletion.setDeletionBy(incomingDeletion.getDeletionBy());
                existingDeletion.setDeletionDate(incomingDeletion.getDeletionDate());
                existingDeletion.setDeletionType(incomingDeletion.getDeletionType());
            }

        }
    }
}
