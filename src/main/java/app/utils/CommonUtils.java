package app.utils;

import app.domain.annotation.ANationalIdentifier;
import app.webapp.exceptions.NikitaException;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NikitaMisconfigurationException;
import app.webapp.model.FileExtensionAndMimeType;
import app.webapp.model.ModelNames;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import java.lang.annotation.Annotation;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static app.utils.constants.Constants.*;
import static app.utils.constants.FileConstants.*;
import static app.utils.constants.N5ResourceMappings.SUB_SYSTEM_ID_PARAMETER;
import static app.utils.constants.N5ResourceMappings.SYSTEM_ID_PARAMETER;
import static org.springframework.http.HttpMethod.*;

public final class CommonUtils {

    public static final Map<String, Class<?>> entityMap = new HashMap<>();
    public static final Map<String, Class<?>> natIdentMap = new HashMap<>();
    /**
     * Holds a list of servletPaths and their HTTP methods
     */
    private static final Map<String, Set<HttpMethod>>
            requestMethodMap = new HashMap<>();
    /**
     * Holds a list of mimeType and boolean value stating if we should attempt
     * to convert a file of that mimeType to an archive version
     */
    private static final Map<String, Boolean>
            mimeTypeConversionMap = new HashMap<>();
    /**
     * Holds a mapping of Norwegian entity names to English entity names
     * e.g. mappe->file
     */
    private static final Map<String, ModelNames>
            nor2engEntityMap = new HashMap<>();
    /**
     * Holds a mapping of mimeTypes and their archive equivalent. The equivalent
     * contains both the equivalent mimeType and file extension.
     */
    private static final Map<String, FileExtensionAndMimeType>
            archiveVersion = new HashMap<>();

    // You shall not instantiate me!
    private CommonUtils() {
    }

    public static final class FileUtils {

        public static void addClassToMap(String simpleName, Class klass) {
            entityMap.put(simpleName, klass);
        }

        public static void addClassToNatIdentMap(
                Class klass, Annotation annotation) {
            natIdentMap.put(
                    ((ANationalIdentifier) annotation).name(), klass);
        }

        public static Class getClassFromName(String className) {
            return Optional.ofNullable(entityMap.get(className))
                    .orElseThrow(() -> new NikitaMisconfigurationException(
                            "Unsupported Noark class: " + className));
        }

        public static Boolean mimeTypeIsConvertible(@NotNull String mimeType) {
            return mimeTypeConversionMap.containsKey(mimeType);
        }

        public static void setDefaultMimeTypesAsConvertible() {
            // TODO At the moment there seems to be a problem with
            // unoconv and I don't have time to debug this to find out
            // why ...
            // doc, ppt, xls
            addMimeTypeAsConvertible(MIME_TYPE_DOC);
            addMimeTypeAsConvertible(MIME_TYPE_PPT);
            addMimeTypeAsConvertible(MIME_TYPE_XLS);
            //docx, pptx, xlsx
            addMimeTypeAsConvertible(MIME_TYPE_DOCX);
            addMimeTypeAsConvertible(MIME_TYPE_PPTX);
            addMimeTypeAsConvertible(MIME_TYPE_XLSX);
            //odt, odp, ods
            addMimeTypeAsConvertible(MIME_TYPE_ODT);
            addMimeTypeAsConvertible(MIME_TYPE_ODP);
            addMimeTypeAsConvertible(MIME_TYPE_ODS);
        }

        public static void addMimeTypeAsConvertible(@NotNull String mimeType) {
            mimeTypeConversionMap.put(mimeType, true);
        }

        public static void addProductionToArchiveVersion(
                @NotNull String productionMimeType,
                @NotNull String archiveFileExtension,
                @NotNull String archiveMimeType) {
            archiveVersion.put(productionMimeType,
                    new FileExtensionAndMimeType(archiveMimeType,
                            archiveFileExtension));
        }

        public static String getArchiveFileExtension(
                String productionMimeType) {
            String fileExtension = "unknown";
            if (null == productionMimeType) {
                return fileExtension;
            }
            FileExtensionAndMimeType fileExtensionAndMimeType =
                    archiveVersion.get(productionMimeType);
            if (null != fileExtensionAndMimeType) {
                fileExtension = archiveVersion.get(
                        productionMimeType).getFileExtension();
            } else {
                fileExtension = "unknown";
            }
            return fileExtension;
        }
    }

    public static final class WebUtils {

        private static final Logger logger =
                LoggerFactory.getLogger(WebUtils.class);

        public static void addNorToEnglishNameMap(
                @NotNull String norwegianName,
                @NotNull String englishNameDatabase,
                @NotNull String englishNameObject) {
            nor2engEntityMap.put(norwegianName,
                    new ModelNames(englishNameDatabase, englishNameObject));
        }

        /**
         * split a URL in two, using the API name as the point to split. This is
         * only undertaken if the URL starts with 'http://' or 'https://',
         * otherwise the url is returned as is.
         * <p>
         * The reason for this is to solve the problem where spring is
         * rejecting query parameters that contain a double '//' as in
         * 'http://', as the '//' is seen as a potential security issue.
         *
         * @param url the URL as a string to sanitise for OData internal use
         * @return the sanitised URL as a String
         */
        public static String sanitiseUrlForOData(String url) {
            if (url != null &&
                    (url.contains("http://") || url.contains("https" +
                            "://"))) {
                String[] split = url.split(HATEOAS_API_PATH);
                if (split.length != 2) {
                    throw new NikitaMalformedInputDataException(
                            "OData filtering problem with URL [" + url + "]");
                }
                return ODATA_PATH + split[1];
            } else
                return url;
        }

        public static String getEnglishNameObject(String norwegianName) {
            ModelNames names = nor2engEntityMap.get(norwegianName);
            if (null != names) {
                return names.getEnglishNameObject();
            } else
                return norwegianName;
        }

        public static String getEnglishNameObjectOrThrow(String norwegianName) {
            ModelNames names = nor2engEntityMap.get(norwegianName);
            if (null == names) {
                String error = norwegianName + " is not recognised attribute";
                logger.error(error);
                throw new NikitaMalformedInputDataException(error);
            }
            return names.getEnglishNameObject();
        }

        public static String getEnglishNameDatabase(String norwegianName) {
            return nor2engEntityMap.get(norwegianName).getEnglishNameDatabase();
        }

        /**
         * requestMethodMap maps servletPaths to HTTP methods. If a particular
         * servletPath supports
         * <p>
         * As servletPaths and corresponding methods are added they are formatted into a proper
         * Allows description. An example /api/arkivstruktur/ny-arkivskaper supports both
         * GET and POST. Therefore, the mapping of /api/arkivstruktur/ny-arkivskaper to its
         * methods should be a GET, HEAD or POST mapping.
         * <p>
         * Note GET automatically implies support for HEAD. Added.
         * <p>
         * Consider adding a list of allowed HTTP methods, but assuming spring will return allowed methods
         *
         * @param servletPath The incoming servletPath e.g. /api/arkivstruktur/
         * @param method      An instance of a single HTTP method
         */
        public static void addRequestToMethodMap(@NotNull String servletPath, @NotNull Set<HttpMethod> method) {
            Set<HttpMethod> methods = requestMethodMap.get(servletPath.toLowerCase());
            if (null == methods) {
                methods = new TreeSet<>();
            }
            // GET automatically implies HEAD
            if (method.contains(GET)) {
                methods.add(GET);
                methods.add(HEAD);
            }
            if (!method.contains(OPTIONS)) {
                methods.add(OPTIONS);
            }
            methods.addAll(method);
            requestMethodMap.put(servletPath.toLowerCase(), methods);
        }


        /**
         * Provides the ability to throw an Exception if this call fails.
         * This is just a helper to make the code more readable in other places.
         *
         * @param servletPath The servletPath
         */
        public static String getMethodsForRequestAsListOrThrow(
                @NotNull String servletPath) {
            HttpMethod[] methods = getMethodsForRequest(servletPath);
            if (null == methods) {
                String msg = "Error servletPath [" + servletPath
                        + "] has no known HTTP methods.";
                logger.error(msg);
                throw new NikitaException(msg);
            }
            return Arrays.stream(methods)
                    .map(HttpMethod::toString)
                    .collect(Collectors.joining(","));
        }

        /**
         * Provides the ability to throw an Exception if this call fails.
         * This is just a helper to make the code more readable in other places.
         *
         * @param servletPath The servletPath
         */

        public static HttpMethod[] getMethodsForRequestOrThrow(@NotNull String servletPath) {
            HttpMethod[] methods = getMethodsForRequest(servletPath);
            if (null == methods) {
                String msg = "Error servletPath [" +
                        servletPath + "] has no known HTTP methods.";
                logger.error(msg);
                throw new NikitaException(msg);
            }
            return methods;
        }

        /**
         * Provides the ability to throw an Exception if this call fails.
         * This is just a helper to make the code more readable in other places.
         *
         * @param servletPath The servletPath
         */
        public static HttpMethod[] getMethodsForRequest(@NotNull String servletPath) {
            // Adding a trailing slash as the map is set up with a trailing slash
            if (!servletPath.endsWith("/")) {
                servletPath += SLASH;
            }
            // Next, we have to replace the first occurrence of an
            // actual UUID with the word {systemID}, the next with
            // {subSystemID} for non-metadata entries, and {kode} or
            // {code}/{value} for metadata entries.
            String updatedServletPath;
            if (servletPath.startsWith(SLASH + HREF_BASE_METADATA + SLASH)) {
                // Anything to do with metadata, just let it through
                HttpMethod[] methods = new HttpMethod[5];
                methods[0] = GET;
                methods[1] = POST;
                methods[2] = DELETE;
                methods[3] = PUT;
                methods[4] = OPTIONS;
                return methods;
            } else {
                // The following pattern is taken from
                // https://stackoverflow.com/questions/136505/searching-for-uuids-in-text-with-regex#6640851
                Pattern pattern = Pattern.compile(
                        "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
                Matcher matcher = pattern.matcher(servletPath.toLowerCase());
                servletPath = matcher.replaceFirst(SYSTEM_ID_PARAMETER);
                matcher = pattern.matcher(servletPath.toLowerCase());
                updatedServletPath = matcher.replaceFirst(SUB_SYSTEM_ID_PARAMETER);
            }
            Set<HttpMethod> methods = requestMethodMap.get(
                    updatedServletPath.toLowerCase());
            if (methods == null) {
                return null;
            }
            return methods.toArray(new HttpMethod[0]);
        }
    }
}
