package app.utils.constants;

/**
 * Created by tsodring on 12/22/16.
 */
public class HATEOASConstants {
    public static final String LINKS = "_links";
    public static final String HREF = "href";
    public static final String REL = "rel";
    public static final String TEMPLATED = "templated";
    public static final String SELF = "self";
    public static final String OK_VAL = "200";
    public static final String CREATED_VAL = "201";
    public static final String NO_CONTENT_VAL = "204";
    public static final String UNAUTHORIZED_VAL = "401";
    public static final String FORBIDDEN_VAL = "403";
    public static final String NOT_FOUND_VAL = "404";
    public static final String CONFLICT_VAL = "409";
    public static final String INTERNAL_SERVER_ERROR_VAL = "500";
    public static final String CLAIM_ORGANISATION = "organisation";
    public static final String CLAIM_USERNAME = "preferred_username";
    public static final String CLAIM_FIRSTNAME = "firstname";
    public static final String CLAIM_LASTNAME = "lastname";
    public static final String CONTENT_LENGTH = "Content-Length";
    public static final String UPLOAD_CONTENT_TYPE = "X-Upload-Content-Type";
    public static final String CONTENT_RANGE = "Content-Range";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String UPLOAD_CONTENT_LENGTH = "X-Upload-Content-Length";
    public static final String ERROR = "feil";
    public static final String ERROR_CODE = "kode";
    public static final String ERROR_METHOD = "httpVerb";
    public static final String ERROR_SOURCE = "kilde";
    public static final String ERROR_DESCRIPTION = "beskrivelse";
    public static final String ERROR_TECHNICAL_DESCRIPTION = "tekniskBeskrivelse";
    public static final String ERROR_EXTENDED_DESCRIPTION = "utvidetBeskrivelse";

}
