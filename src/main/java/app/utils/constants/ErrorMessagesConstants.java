package app.utils.constants;

public final class ErrorMessagesConstants {
    public static final String ERROR_MAPPING_METADATA =
            "During startup, nikita found a metadata ";
    public static final String METADATA_ENTITY_MISSING =
            "Cannot find metadata entity of type: ";
    public static final String METADATA_ENTITY_CLASS_MISSING =
            "Unable to find metadata entity class type: ";
    public static final String CROSS_REFERENCE_BAD_SYSTEM_ID = "The systemID " +
            "for fromSystemID [%s] in the CrossReference you tried to create " +
            "/ update does not equal the one [%s] present on the URL of the " +
            "HTTP request";
    public static final String CROSS_REFERENCE_DUPLICATE = "Attempt to " +
            "create duplicate cross reference from [%s] to [%s]";
    public static final String ENTITY_ASSOCIATION_PROBLEM = "Problem " +
            "assigning organisation to a secondary entity reference. The " +
            "primary entity is of type [%s]. The cause is [%s].";
    public static final String STRING_IS_BLANK =
            "The [%s] string cannot contain only blank values.";
    public static final String MALFORMED_PAYLOAD =
            "The [%s] you tried to create is malformed. The following fields" +
                    " are not recognised as fields [%s]";
    public static final String DESERIALIZE_LEFTOVER =
            "The following fields are left over after deserialising [%s]. ";

    public static final String ETAG_ILLEGAL_VALUE = "Illegal eTag value [%s]. Nikita uses numeric ETAG values >= 0." +
            "ETAG values show version of an entity in the database. Note An eTag is assigned to you.";

    public static final String REQUIRED_ETAG_MISSING = "When undertaking a %s request, an ETAG header must be present.";

    public static final String CALL_TO_PARENT_DESERIALIZER_NOT_ALLOWED = "Somehow " +
            "a child called NoarkDeserializer. This should not happen";

    public static final String CALL_TO_PARENT_GET_TYPE_NOT_ALLOWED = "Somehow " +
            "a child called NoarkDeserializer::getType(). This should not happen";

    public static final String USER_NOT_FOUND = "Could not load data for user [%s]. This could be a result of the user " +
            "being registered in the external identity access management solution, but not registered in nikita.";

    public static final String AUTHENTICATION_CONFIGURATION_PROBLEM = "The http request [%s] has no authentication " +
            "object associated with it and nikita was unable to continue processing the http request";

    public static final String AUTHENTICATION_REQUEST_PROBLEM = "It is not possible to continue with the current http" +
            " request as there is a serious problem retrieving the Request object. [%s]";

    public static final String AUTHENTICATION_USER_PROBLEM = "Could not find a username in the the JWT token associated" +
            " with the http request [%s]. The username was expected to be in the [%s] claim.";

    public static final String BATCH_DELETE_DOCUMENT_OBJECT_FILE_MESSAGE = "During batch deleteAll call on DELETE " +
            "[contextPath][api]/arkivstruktur/dokumentobjekt/ there was an error deleting %s files associated with " +
            "documentObjects. See log file for further details";

    public static final String BATCH_DELETE_DOCUMENT_OBJECT_FILE_ERROR = "DocumentObject with systemID [%s] could not " +
            "be deleted during batch deletion because of error [%s]. This DocumentObject was not deleted. The batch " +
            "deletion process was not stopped in spite of this error";
    public static final String DOC_STORE_DIRECTORY_ERROR = "Unable to create document store directories! " +
            "directoryStoreName: [%s] incomingDirectoryName: [%s] Hint: Check write permissions / available space." +
            " Nikita cannot start without working storage! Exiting!";
    public static final String USER_ALREADY_EXISTS = "Cannot create user [%s] as a user with that username already exists";
    public static final String LARGE_FILE_UPLOAD_HEADER_MISSING = "Cannot start large file upload as required header [%s] is missing or empty";
    public static final String LENGTH_HEADER_MISSING = "Cannot start large file upload as required header [%s] has non numeric value";
    public static final String CONTENT_TYPE_BLANK = "Content-Type is missing or contains a blank value";
    public static final String CONTENT_RANGE_BLANK = "Content-Range is missing or contains a blank value";
    public static final String CONTENT_LENGTH_NEGATIVE = "Content-Length is invalid. It contains a number <= 0 [%s]";
    public static final String CONTENT_RANGE_NEGATIVE_VALUE = "Content-Range is invalid. All values must be >= 0 [%s], [%s], [%s]";
    public static final String CONTENT_RANGE_INVALID_NUM_VALUE = "Content-Range is invalid. from [%s] must be < to [%s]";
    public static final String CONTENT_RANGE_INVALID_PATTERN = "Content-Range is invalid. It must follow pattern 'bytes \\d+-\\d+/\\d+'. Provided was [%s]";
    public static final String FILE_NOT_WRITABLE = "The file ([%s]) is not writable server-side";
    public static final String ERROR_CHUNK_DIRECTORY = "Could not create chunk directory server-side for [%s]";
    public static final String ERROR_STORAGE_UNKNOWN = "Internal error when attempting to store a file [%s]";
    public static final String ERROR_CHECKSUM_UNKNOWN = "Internal error, could not load checksum algorithm [%s] when attempting to store a file";

}
