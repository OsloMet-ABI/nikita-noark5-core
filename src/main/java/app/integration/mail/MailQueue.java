package app.integration.mail;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("mailintegration")
@Configuration
public class MailQueue {


    @Value("${nikita.rabbitmq.queue_name_outgoing_email:nikita-outgoing-email}")
    private String outgoingQueue = "";

    /**
     * Create a queue to send EMail objects from nikita to mail queue.
     *
     * @return the queue
     */
    @Bean
    public Queue outgoingEmailQueue() {
        return new Queue(outgoingQueue);
    }
}
