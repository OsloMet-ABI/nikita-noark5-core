package app.integration.mail;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.casehandling.secondary.ContactInformation;
import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.domain.noark5.casehandling.secondary.CorrespondencePartPerson;
import app.domain.noark5.casehandling.secondary.CorrespondencePartUnit;
import app.domain.repository.admin.IUserRepository;
import app.domain.repository.noark5.v5.IRegistryEntryRepository;
import app.integration.mail.model.Email;
import app.service.ILocalUserDetails;
import app.service.IUrlDetails;
import app.service.application.IPatchService;
import app.service.interfaces.IRegistryEntryService;
import app.service.interfaces.ISequenceNumberGeneratorService;
import app.service.interfaces.admin.IAdministrativeUnitService;
import app.service.interfaces.casehandling.ISignOffService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.ICorrespondencePartService;
import app.service.interfaces.secondary.IDocumentFlowService;
import app.service.interfaces.secondary.IPrecedenceService;
import app.service.noark5.RegistryEntryService;
import app.webapp.payload.builder.interfaces.IRegistryEntryLinksBuilder;
import app.webapp.payload.links.casehandling.RegistryEntryLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static java.lang.String.format;

@Service
@Profile("mailintegration")
public class MailRegistryEntryService
        extends RegistryEntryService
        implements IRegistryEntryService {
    private static final Logger logger =
            LoggerFactory.getLogger(MailRegistryEntryService.class);
    private final RabbitTemplate rabbitTemplate;
    private final IMailFileService mailFileService;
    @Value("${nikita.download-dir.outgoing}")
    String pathnameDirIn;
    @Value("${nikita.mail-queue.outgoing:outgoing-email}")
    private String outgoingQueue;

    public MailRegistryEntryService(EntityManager entityManager, ApplicationEventPublisher applicationEventPublisher,
                                    IPatchService patchService,
                                    ICorrespondencePartService correspondencePartService,
                                    IDocumentFlowService documentFlowService, IPrecedenceService precedenceService,
                                    IMetadataService metadataService, IRegistryEntryRepository registryEntryRepository,
                                    IRegistryEntryLinksBuilder registryEntryLinksBuilder,
                                    ISequenceNumberGeneratorService numberGeneratorService,
                                    ISignOffService signOffService, IUserRepository userRepository,
                                    IAdministrativeUnitService administrativeUnitService, RabbitTemplate rabbitTemplate, IMailFileService mailFileService,
                                    ILocalUserDetails userDetails,
                                    IUrlDetails urlDetails) {
        super(entityManager, applicationEventPublisher, patchService, correspondencePartService,
                documentFlowService, precedenceService, metadataService, registryEntryRepository,
                registryEntryLinksBuilder, numberGeneratorService, signOffService, userRepository,
                administrativeUnitService, userDetails, urlDetails);
        this.rabbitTemplate = rabbitTemplate;
        this.mailFileService = mailFileService;
    }

    /**
     * When the publish event from super is ready, check to see if this is a RegistryEntry that is ready to send an
     * outgoing email. If it is, package everything up as an Email object and send it to the mail queue.
     *
     * @param registryEntry The registryEntry to create a mail object from
     */
    @Override
    public RegistryEntryLinks handleUpdate(
            @NotNull final UUID systemId, @NotNull final RegistryEntry incomingRegistryEntry) {
        if (incomingRegistryEntry.getRegistryEntryType().getCode().equalsIgnoreCase("u") &&
                incomingRegistryEntry.getRegistryEntryStatus().getCode().equalsIgnoreCase("k")) {
            try {
                Email outgoingEmail = getEmailDetails(incomingRegistryEntry);
                if (logger.isDebugEnabled()) {
                    logger.debug(format("Attempting to send following message to outgoing email queue [%s]", outgoingEmail));
                }
                rabbitTemplate.convertAndSend(outgoingQueue, outgoingEmail);
            } catch (AmqpException e) {
                logger.error(format("Error serialising outgoing email object when sending to queue. Reason [%s]",
                        e.getMessage()));
            }
        }
        return super.handleUpdate(systemId, incomingRegistryEntry);
    }

    /**
     * Create a Nikita Email object that can be put in the outgoing mail queue.
     *
     * @param registryEntry to create an email from
     * @return the outgoing Nikita formatted Email object
     */
    private Email getEmailDetails(RegistryEntry registryEntry) {
        Email outgoingEmail = new Email();
        outgoingEmail.setSubject("Svar på din hendvendelse");
        outgoingEmail.setFrom("orgpostmottak@proton.me");
        outgoingEmail.setMessageText("Svar på din hendvendelse. Se vedlagte dokumenter.");
        getCorrespondencePartEmails(outgoingEmail, registryEntry);
        getDocuments(outgoingEmail, registryEntry);
        return outgoingEmail;
    }

    /**
     * Given a RegistryEntry object, retrieve all documents that are in archive format. Note. There is an assumption
     * here that the caller will only use this on outgoing emails.
     *
     * @param outgoingEmail The email object to populate information with
     * @param registryEntry The registryEntry to get documents from
     */
    private void getDocuments(Email outgoingEmail, RegistryEntry registryEntry) {
        Set<DocumentDescription> documentDescriptions = registryEntry.getReferenceDocumentDescription();
        for (DocumentDescription documentDescription : documentDescriptions) {
            List<DocumentObject> documentObjects = documentDescription.getReferenceDocumentObject();
            for (DocumentObject documentObject : documentObjects) {
                String fileLocation = documentObject.getReferenceDocumentFile();
                String filename = documentObject.getOriginalFilename();
                if (documentObject.getVariantFormat().getCode().equalsIgnoreCase("a")) {
                    outgoingEmail.addAttachment(getToFile(documentObject).toString());
                }
            }
        }
    }

    private Path getToFile(DocumentObject documentObject) {
        return Paths.get("/data/nikita/storage/" +
                calculateDirectoryStructure(documentObject) +
                documentObject.getReferenceDocumentFile());
    }


    private String calculateDirectoryStructure(DocumentObject documentObject) {
        String checksum = documentObject.getChecksum();
        if (checksum.length() > 6) {
            return String.format("%s%s%s%s%s%s",
                    checksum.substring(0, 2), File.separator,
                    checksum.substring(2, 4), File.separator,
                    checksum.substring(4, 6), File.separator);
        }
        return "";
    }

    /**
     * Given a RegistryEntry object, retrieve all correspondence parts. If it is a type that has an email address, copy
     * the email address from the vale.
     *
     * @param outgoingEmail The email object to populate information with
     * @param registryEntry The registryEntry to get correspondence parts from
     */
    private void getCorrespondencePartEmails(Email outgoingEmail, RegistryEntry registryEntry) {
        List<CorrespondencePart> correspondenceParts = registryEntry.getReferenceCorrespondencePart();
        for (CorrespondencePart correspondencePart : correspondenceParts) {
            if (correspondencePart instanceof CorrespondencePartPerson) {
                ContactInformation contactInformation = ((CorrespondencePartPerson) correspondencePart)
                        .getContactInformation();
                outgoingEmail.addRecipient(contactInformation.getEmailAddress());
            } else if (correspondencePart instanceof CorrespondencePartUnit) {
                ContactInformation contactInformation = ((CorrespondencePartUnit) correspondencePart)
                        .getContactInformation();
                outgoingEmail.addRecipient(contactInformation.getEmailAddress());
            }
        }
    }
}
