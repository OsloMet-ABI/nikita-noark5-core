package app.spring.security;

import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static app.utils.TestConstants.TEST_USER;
import static app.utils.constants.Constants.ROLE_RECORDS_MANAGER;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation to create a mock JWT user for testing purposes.
 *
 * <p>This annotation can be applied to methods or types to indicate that
 * a mock JWT user should be created for the security context during
 * testing. It uses the {@link JwtSecurityContextFactory} to set up the
 * security context with the specified username and roles.</p>
 *
 * <p>By default, the username is set to {@code TEST_USER} and the roles
 * are set to {@code ROLE_RECORDS_MANAGER}. These defaults can be
 * overridden by specifying different values in the annotation.</p>
 *
 * @see JwtSecurityContextFactory
 */
@Target({METHOD, TYPE})
@Retention(RUNTIME)
@WithSecurityContext(factory = JwtSecurityContextFactory.class)
public @interface WithMockNikitaUser {
    String username() default TEST_USER;

    String[] roles() default {ROLE_RECORDS_MANAGER};
}
