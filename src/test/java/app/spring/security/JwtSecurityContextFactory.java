package app.spring.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A factory for creating a {@link SecurityContext} for mock JWT users
 * in unit tests.
 *
 * <p>This class implements the {@link WithSecurityContextFactory}
 * interface for the {@link WithMockNikitaUser} annotation. It provides
 * a way to set up a security context with a mock JWT token, to test
 * security-related components in Nikita.</p>
 *
 * <p>The factory creates a {@link SecurityContext} that includes
 * authentication information based on the roles and username of the
 * mock user. This  allows us to use various users and roles in tests, e.g.,
 * '@WithMockNikitaUser(username = "different_username", roles = {"LEADER", "CASE_HANDLER"})'
 * </p>
 */
public class JwtSecurityContextFactory implements
        WithSecurityContextFactory<WithMockNikitaUser> {

    /**
     * Creates a SecurityContext for the given mock JWT user.
     *
     * <p>This method initializes a SecurityContext with a mock JWT token
     * based on the provided {@link WithMockNikitaUser}. It extracts the roles
     * from the mock user, creates a list of authorities, and sets up the
     * authentication token within the SecurityContext.</p>
     *
     * @param mockUser The mock user for which the SecurityContext is to be
     *                 created. This user contains the username and roles
     *                 that will be used to set up the authentication.
     * @return A SecurityContext containing the authentication information
     * for the provided mock user.
     */
    @Override
    public SecurityContext createSecurityContext(WithMockNikitaUser mockUser) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        // Create a list of authorities based on the roles in the mockUser object
        Collection<SimpleGrantedAuthority> authorities = Stream.of(mockUser.roles())
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
        // Create a mock JWT token for use in tests
        Jwt jwt = createMockJwt(mockUser.username(), authorities);
        JwtAuthenticationToken authentication = new JwtAuthenticationToken(jwt, authorities);
        context.setAuthentication(authentication);
        return context;
    }

    /**
     * Creates a mock JWT (JSON Web Token) for testing purposes.
     *
     * <p>This method constructs a JWT with a predefined token value and
     * headers. It includes claims for the preferred username, subject,
     * and roles based on the provided authorities.</p>
     *
     * @param username    The username to be included in the JWT claims.
     * @param authorities A collection of {@link SimpleGrantedAuthority}
     *                    representing the roles associated with the user.
     *                    These roles will be included in the JWT claims.
     * @return A mock Jwt object containing the specified claims and headers.
     */
    private Jwt createMockJwt(String username, Collection<SimpleGrantedAuthority> authorities) {
        if (username == null || authorities == null) {
            throw new IllegalArgumentException("Username and authorities must not be null");
        }

        return Jwt.withTokenValue("jwt-test-token-nikita")
                .header("alg", "none")
                .header("typ", "JWT")
                .claim("preferred_username", username)
                .claim("sub", username)
                .claim("roles", authorities.stream()
                        .map(SimpleGrantedAuthority::getAuthority)
                        .collect(Collectors.toList()))
                .build();
    }
}

