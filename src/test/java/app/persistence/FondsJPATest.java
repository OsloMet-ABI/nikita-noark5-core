package app.persistence;

import app.domain.noark5.Fonds;
import app.domain.repository.noark5.v5.IFondsRepository;
import app.setup.TestDatabaseBase;
import app.webapp.exceptions.NikitaMisconfigurationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static app.utils.TestConstants.FONDS_SYSTEM_ID;
import static java.util.UUID.fromString;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * This class contains unit tests for the {@link IFondsRepository} class, which is responsible
 * for managing {@link Fonds} entities in the database. The tests validate the behavior of the
 * repository methods, ensuring that they handle various scenarios correctly.
 *
 * <p>Tests include:</p>
 * <ul>
 *     <li>Verifying that a {@link NikitaMisconfigurationException} is thrown when attempting to
 *     save a {@link Fonds} entity with missing mandatory fields.</li>
 *     <li>Checking that querying the repository for a non-existing fonds ID returns null.</li>
 *     <li>Asserting that the count of persisted fonds in the database is correct after
 *     initialization with DBUnit.</li>
 *     <li>Ensuring that a fonds can be retrieved from the database by its system ID after being
 *     persisted.</li>
 * </ul>
 *
 * <p>These tests are designed to ensure the integrity and correctness of the data access layer
 * for fonds entities, providing confidence that the repository behaves as expected under various
 * conditions.</p>
 */
@Transactional
@Sql("/db-tests/basic_structure.sql")
public class FondsJPATest
        extends TestDatabaseBase {

    @Autowired
    IFondsRepository fondsRepository;

    /**
     * Tests that a NikitaMisconfigurationException is thrown when an invalid fonds
     * is created (missing mandatory fields such as title).
     */
    @Test
    public void givenInvalidFonds_whenSaved_thenThrowsNikitaMisconfigurationException() {
        Assertions.assertThrows(NikitaMisconfigurationException.class, () -> {
            Fonds fonds = new Fonds();
            // Missing all mandatory fields
            fondsRepository.save(fonds);
        });
    }

    /**
     * Tests that querying the database for a fonds that does not exist returns null.
     */
    @Test
    public void givenNonExistingFondsId_whenRetrieved_thenReturnsNull() {
        assertThat(fondsRepository.findBySystemId(
                fromString("0c5e864c-3269-4e01-9430-17d55291dae7"))).isNull();
    }

    /**
     * Tests that the count of persisted fonds in the database is correct
     * after initialization with DBUnit.
     */
    @Test
    public void givenInitializedDatabase_whenRetrievedAllFonds_thenCheckSize() {
        List<Fonds> entities = (List<Fonds>) fondsRepository.findAll();
        assertThat(entities.size()).isEqualTo(1);
    }

    /**
     * Tests that a fonds can be retrieved from the database by its systemID
     * after being persisted.
     */
    @Test
    public void givenInitializedDatabase_whenRetrievedBySystemId_thenReturnsFonds() {
        Fonds fonds = fondsRepository.findBySystemId(
                fromString(FONDS_SYSTEM_ID));
        assertThat(fonds).isNotNull();
    }
}
