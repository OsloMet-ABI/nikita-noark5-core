package app.persistence;
/*
  This package contains the repository classes and their corresponding unit tests
  for the Nikita persistence layer. The tests validate the behavior of the
  repository methods, ensuring that they correctly interact with the underlying
  database and handle various scenarios appropriately.
 */