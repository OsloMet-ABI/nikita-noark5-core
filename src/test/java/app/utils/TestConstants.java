package app.utils;

import static app.utils.constants.Constants.HREF_BASE_FONDS_STRUCTURE;
import static app.utils.constants.Constants.SLASH;

public final class TestConstants {
    public static final String TEST_USER = "test_user@example.com";
    public static String NAME_TEST = "Frank Grimes";
    public static String CONTACT_NAME_TEST = "Hans Gruber";
    public static String D_NUMBER_TEST = "01018298765";
    public static String SOCIAL_SECURITY_NUMBER_TEST = "02029285333";
    public static String UNIT_NUMBER_TEST = "123456789";
    public static String EMAIL_ADDRESS_TEST = "frank@grimes.com";
    public static String MOBILE_NUMBER_TEST = "+4732791349";
    public static String TELEPHONE_NUMBER_TEST = "+4764940149";
    public static String SOURCE_TEST = "Source of information";
    public static String COUNTRY_CODE_TEST = "no";

    public static String POSTAL_ADDRESS_LINE_1_TEST = "Utsikten";
    public static String POSTAL_ADDRESS_LINE_2_TEST = "Kongensleilighet";
    public static String POSTAL_ADDRESS_LINE_3_TEST = "Kongensgate 11";
    public static String POSTAL_POSTAL_CODE_TEST = "5000";
    public static String POSTAL_POSTAL_TOWN_TEST = "Bergen";

    public static String RESIDING_ADDRESS_LINE_1_TEST = "Innsikten";
    public static String RESIDING_ADDRESS_LINE_2_TEST = "Dronningensleilighet";
    public static String RESIDING_ADDRESS_LINE_3_TEST = "Dronningssgate 19";
    public static String RESIDING_POSTAL_CODE_TEST = "0001";
    public static String RESIDING_POSTAL_TOWN_TEST = "Oslo";

    public static String BUSINESS_ADDRESS_LINE_1_TEST = "Oversikten";
    public static String BUSINESS_ADDRESS_LINE_2_TEST = "Prinsensleilighet";
    public static String BUSINESS_ADDRESS_LINE_3_TEST = "Prinsessegate 22";
    public static String BUSINESS_POSTAL_CODE_TEST = "7010";
    public static String BUSINESS_POSTAL_TOWN_TEST = "Trondheim";
    public static final String PATTERN_UUID = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$";
    public static String TITLE_TEST = "example test title";
    public static String DESCRIPTION_TEST = "example test description";
    // DocumentDescription constants
    public static String DOCUMENT_TYPE_CODE_TEST = "B";
    public static String DOCUMENT_TYPE_CODE_NAME_TEST = "Brev";
    public static String DOCUMENT_STATUS_CODE_TEST = "F";
    public static String DOCUMENT_STATUS_CODE_NAME_TEST =
            "Dokumentet er ferdigstilt";
    public static String DOCUMENT_ASS_REC_CODE_TEST = "H";
    public static String DOCUMENT_ASS_REC_CODE_NAME_TEST = "Hoveddokument";

    public static String AUTHOR_TEST = "Frank Grimes";
    public static String AUTHOR_TEST_UPDATED = "Henry Grimes II";

    public static String KEYWORD_TEST = "Cool";
    public static String KEYWORD_TEST_UPDATED = "Fabulous";

    public static String ES_VERIFIED_BY = "Hans Gruber";
    public static String ES_SIGNATURE_SECURITY_LEVEL_CODE = "SK";
    public static String ES_SIGNATURE_SECURITY_LEVEL_CODE_NAME = "Symmetrisk kryptert";
    public static String ES_SIGNATURE_VERIFIED_CODE = "V";
    public static String ES_SIGNATURE_VERIFIED_CODE_NAME = "Signatur påført og verifisert";

    public static String STORAGE_LOCATION_TEST = "Archive Room X";
    public static String STORAGE_LOCATION_TEST_UPDATED = "Archive Room XVI";

    // For BusinessSpecificMetatadata
    public static String BSM_DATE_NAME = "ppt-v1:datohenvist";
    public static String BSM_DATE_TIME_NAME = "ppt-v1:datotidvedtakferdig";
    public static String BSM_STRING_NAME = "ppt-v1:skolekontakt";
    public static String BSM_STRING_VALUE = "Harald Harfarge";
    public static String BSM_URI_NAME = "ppt-v1:refSkole";
    public static String BSM_URI_VALUE = "https://skole.eksempel.com";
    public static String BSM_BOOLEAN_NAME = "ppt-v1:sakferdig";
    public static Boolean BSM_BOOLEAN_VALUE = true;
    public static String BSM_DOUBLE_NAME = "ppt-v1:snittKarakter";
    public static Double BSM_DOUBLE_VALUE = 1.2;
    public static String BSM_INTEGER_NAME = "ppt-v1:antallDagerVurdert";
    public static Integer BSM_INTEGER_VALUE = 8;
    public static String BSM_DATE_TIME_VALUE = "2020-07-01T23:25:06+02:00";
    public static String BSM_DATE_TIME_VALUE_Z = "2020-07-01T21:25:06Z";
    public static String BSM_DATE_VALUE = "2020-07-01+02:00";
    public static String BSM_DATE_VALUE_Z = "2020-06-30Z";

    // For ElectronicSignature
    public static String VERIFIED_BY_VALUE = "Hans Gruber";
    public static String VERIFIED_DATE_VALUE = "2020-07-01T23:25:06+02:00";
    public static String VERIFIED_CODE_VALUE = "V";
    public static String VERIFIED_CODE_NAME_VALUE = "Signatur påført og verifisert";
    public static String VERIFIED_LEVEL_CODE_VALUE = "SK";
    public static String VERIFIED_LEVEL_CODE_NAME_VALUE = "Symmetrisk kryptert";

    // National Identifiers
    public static Integer BUILDING_NUMBER_VALUE = 99;
    public static Integer BUILDING_CHANGE_NUMBER_VALUE = 898;
    public static String PLAN_IDENTIFICATION_VALUE =
            "plan identification value";
    public static String PLAN_COUNTY_NUMBER_VALUE = "11";
    public static String PLAN_MUNICIPALITY_NUMBER_VALUE = "53";
    public static String PLAN_COUNTRY_CODE_VALUE = "NO";
    public static String PLAN_COUNTRY_CODE_NAME_VALUE = "Norge";
    public static String POSITION_CODE_VALUE = "EPSG:4326";
    public static String POSITION_CODE_NAME_VALUE = "WGS84";
    public static Double POSITION_X_VALUE = 1.01;
    public static Double POSITION_Y_VALUE = 2.02;
    public static Double POSITION_Z_VALUE = 3.03;
    public static String ORGANISATION_NUMBER_VALUE = "02020202022";
    public static String D_NUMBER_VALUE = "01010101011";
    public static String SS_NUMBER_VALUE = "02020202021";
    public static String MUNICIPALITY_NUMBER_VALUE = "0101";
    public static Integer HOLDING_NUMBER_VALUE = 1;
    public static Integer SUB_HOLDING_NUMBER_VALUE = 2;
    public static Integer LEASE_NUMBER_VALUE = 3;
    public static Integer SECTION_NUMBER_VALUE = 4;
    public static String TITLE_TEST_UPDATED = TITLE_TEST + " updated";
    // Constants to create URLS
    public static String hrefBaseFondsStructure = "/noark5v5/" + HREF_BASE_FONDS_STRUCTURE + SLASH;
    // Constants used in database
    public static String FONDS_SYSTEM_ID = "3318a63f-11a7-4ec9-8bf1-4144b7f281cf";
    public static String FILE_SYSTEM_ID = "48c81365-7193-4481-bc84-b025248fb310";
    public static String CLASS_SYSTEM_ID = "596c85fb-a6c4-4381-86b4-81df05234028";
    public static String RECORD_SYSTEM_ID = "99c2f1af-dd84-19e8-dd4f-cc21fe1578ff";
    public static String DOCUMENT_DESCRIPTION_SYSTEM_ID = "66b92e78-b75d-4b0f-9558-4204ab31c2d1";
    public static String DOCUMENT_OBJECT_SYSTEM_ID = "55d45e9a-c938-499e-b3da-6822ae508c8c";
}
