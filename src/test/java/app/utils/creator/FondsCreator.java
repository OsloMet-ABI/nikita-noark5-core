package app.utils.creator;

import app.domain.noark5.Fonds;
import app.domain.noark5.metadata.FondsStatus;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.FondsSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.StringWriter;

import static app.utils.constants.Constants.TEST_TITLE;
import static app.utils.constants.N5ResourceMappings.FONDS_STATUS_OPEN_CODE;

public final class FondsCreator {

    public static String createFondsAsJSON() throws IOException {
        return createFondsAsJSON(createFonds());
    }

    private static Fonds createFonds() {
        Fonds fonds = new Fonds();
        fonds.setTitle(TEST_TITLE);
        FondsStatus fondsStatus = new FondsStatus();
        fondsStatus.setCode(FONDS_STATUS_OPEN_CODE);
        fonds.setFondsStatus(fondsStatus);
        return fonds;
    }

    public static String createFondsAsJSON(Fonds Fonds) throws IOException {
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        FondsSerializer serializer = new FondsSerializer();
        serializer.serializeNoarkEntity(Fonds, new LinksNoarkObject(), jgen);
        jgen.close();
        return jsonWriter.toString();
    }

}
