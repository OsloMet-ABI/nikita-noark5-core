package app.utils.exceptions;

import app.webapp.exceptions.NikitaException;

public class DocumentationGenerationException
        extends NikitaException {
    public DocumentationGenerationException(String message) {
        super(message);
    }
}
