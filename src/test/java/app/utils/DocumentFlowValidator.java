package app.utils;

import org.springframework.test.web.servlet.ResultActions;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.HREF;
import static org.hamcrest.Matchers.endsWith;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


public final class DocumentFlowValidator {

    /**
     * @param resultActions with request result
     * @throws Exception if there is a problem
     */
    public static void validateDocumentFlow(ResultActions resultActions)
            throws Exception {
        /*
        resultActions
                .andExpect(jsonPath("$." + DOCUMENT_FLOW).value(DOCUMENT_FLOW_TEST));
        validateSELFLink(REL_FONDS_STRUCTURE_DOCUMENT_FLOW, resultActions);
                */
    }

    /**
     * @param resultActions with request result
     * @throws Exception if there is a problem
     */
    public static void validateUpdatedDocumentFlow(ResultActions resultActions)
            throws Exception {
        /*resultActions
                .andExpect(jsonPath("$." + DOCUMENT_FLOW)
                        .value(DOCUMENT_FLOW_TEST_UPDATED));
        validateSELFLink(REL_FONDS_STRUCTURE_DOCUMENT_FLOW, resultActions);
        */
    }

    /**
     * @param resultActions with request result
     * @throws Exception if there is a problem
     */
    public static void validateDocumentFlowForDocumentDescription(
            ResultActions resultActions)
            throws Exception {
        validateDocumentFlow(resultActions);
        validateDocumentDescriptionLink(resultActions);
    }

    /**
     * @param resultActions with request result
     * @throws Exception if there is a problem
     */
    public static void validateDocumentFlowForRecord(
            ResultActions resultActions)
            throws Exception {
        validateDocumentFlow(resultActions);
        validateRecordLink(resultActions);
    }

    public static void validateDocumentDescriptionLink(
            ResultActions resultActions)
            throws Exception {
        resultActions
                .andExpect(jsonPath("$._links['" +
                                REL_FONDS_STRUCTURE_DOCUMENT_DESCRIPTION +
                                "']['" + HREF + "']",
                        endsWith(HREF_BASE_DOCUMENT_DESCRIPTION + SLASH +
                                "66b92e78-b75d-4b0f-9558-4204ab31c2d1")));
    }

    public static void validateRecordLink(
            ResultActions resultActions)
            throws Exception {
        resultActions
                .andExpect(jsonPath("$._links['" +
                                REL_FONDS_STRUCTURE_RECORD +
                                "']['" + HREF + "']",
                        endsWith(HREF_BASE_RECORD + SLASH +
                                "dc600862-3298-4ec0-8541-3e51fb900054")));
    }
}
