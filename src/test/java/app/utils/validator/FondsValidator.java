package app.utils.validator;

import org.springframework.test.web.servlet.ResultActions;

import static app.utils.TestConstants.*;
import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.HREF;
import static app.utils.constants.HATEOASConstants.TEMPLATED;
import static app.utils.constants.N5ResourceMappings.*;
import static org.hamcrest.Matchers.endsWith;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


public final class FondsValidator {

    /**
     * Validates the response of a Fonds API call by checking the presence and value of specific JSON fields.
     * <p>
     * This method performs the following checks on the provided {@link ResultActions}:
     * <ul>
     *     <li>Verifies that the JSON response contains a field with the key defined by {@code SYSTEM_ID}.</li>
     *     <li>Checks that the value of the field defined by {@code TITLE} matches the expected value {@code TITLE_TEST}.</li>
     * </ul>
     * <p>
     * Additionally, it calls the {@link #validateFondsTemplate(ResultActions)} method to perform further validation.
     *
     * @param resultActions the {@link ResultActions} object containing the result of the API call to be validated
     * @throws Exception if an error occurs during the validation process
     */
    public static void validateFonds(ResultActions resultActions)
            throws Exception {
        resultActions
                .andExpect(jsonPath("$." + SYSTEM_ID).exists())
                .andExpect(jsonPath("$." + TITLE).value(TITLE_TEST));
        validateFondsLinks(resultActions);
    }

    /**
     * A Fonds template should look like the following
     * {
     * "dokumenttype": {"kode":"B","kodenavn":"Brev"},
     * "dokumentstatus": {"kode":"F","kodenavn":"Dokumentet er ferdigstilt"},
     * "tilknyttetRegistreringSom":{"kode":"H","kodenavn":"Hoveddokument"},
     * "_links":{
     * "https://rel.arkivverket.no/noark5/v5/api/metadata/dokumentmedium/":{
     * "href":"http://localhost:8092/noark5v5/api/metadata/dokumentmedium"},
     * "https://rel.arkivverket.no/noark5/v5/api/metadata/dokumentstatus/":{
     * "href":"http://localhost:8092/noark5v5/api/metadata/dokumentstatus"},
     * "https://rel.arkivverket.no/noark5/v5/api/metadata/dokumenttype/":{
     * "href":"http://localhost:8092/noark5v5/api/metadata/dokumenttype"},
     * "https://rel.arkivverket.no/noark5/v5/api/metadata/skjermingmetadata/":{
     * "href":"http://localhost:8092/noark5v5/api/metadata/skjermingmetadata"}}}
     *
     * @param resultActions with subset of request result
     * @throws Exception if there is a problem
     */
    public static void validateFondsTemplate(ResultActions resultActions)
            throws Exception {
        resultActions
                .andExpect(jsonPath("$." + DOCUMENT_TYPE +
                        "." + CODE).value(DOCUMENT_TYPE_CODE_TEST))
                .andExpect(jsonPath("$." + DOCUMENT_TYPE +
                        "." + CODE_NAME).value(DOCUMENT_TYPE_CODE_NAME_TEST))
                .andExpect(jsonPath("$." + DOCUMENT_STATUS +
                        "." + CODE).value(DOCUMENT_STATUS_CODE_TEST))
                .andExpect(jsonPath("$." + DOCUMENT_STATUS +
                        "." + CODE_NAME).value(DOCUMENT_STATUS_CODE_NAME_TEST))
                .andExpect(jsonPath("$." +
                        DOCUMENT_DESCRIPTION_ASSOCIATED_WITH_RECORD_AS +
                        "." + CODE).value(DOCUMENT_ASS_REC_CODE_TEST))
                .andExpect(jsonPath("$." +
                        DOCUMENT_DESCRIPTION_ASSOCIATED_WITH_RECORD_AS +
                        "." + CODE_NAME).value(DOCUMENT_ASS_REC_CODE_NAME_TEST));
        validateDocumentMediumLink(resultActions);
        validateDocumentStatusLink(resultActions);
        validateDocumentTypeLink(resultActions);
    }

    /*

                "_links": {
                    "self": {
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkiv/": {
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivdel/": {
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkivdel/": {
                        "href": "https://nikita.oslomet.no/noark5v5/api/arkivstruktur/arkiv/9e249206-9249-1559-8192-4eaa096b010b/ny-arkivdel"
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/underarkiv/": {
                        "href": "https://nikita.oslomet.no/noark5v5/api/arkivstruktur/arkiv/9e249206-9249-1559-8192-4eaa096b010b/underarkiv/{?$filter&$orderby&$top&$skip}",
                        "templated": true
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkiv/": {
                        "href": "https://nikita.oslomet.no/noark5v5/api/arkivstruktur/arkiv/9e249206-9249-1559-8192-4eaa096b010b/ny-arkiv"
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivskaper/": {
                        "href": "https://nikita.oslomet.no/noark5v5/api/arkivstruktur/arkiv/9e249206-9249-1559-8192-4eaa096b010b/arkivskaper/{?$filter&$orderby&$top&$skip}",
                        "templated": true
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkivskaper/": {
                        "href": "https://nikita.oslomet.no/noark5v5/api/arkivstruktur/arkiv/9e249206-9249-1559-8192-4eaa096b010b/ny-arkivskaper"
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/metadata/dokumentmedium/": {
                        "href": "https://nikita.oslomet.no/noark5v5/api/metadata/dokumentmedium"
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/metadata/arkivstatus/": {
                        "href": "https://nikita.oslomet.no/noark5v5/api/metadata/arkivstatus/{?$filter&$orderby&$top&$skip}",
                        "templated": true
                    },
                    "https://nikita.arkivlab.no/noark5/v5/ny-oppbevaringssted/": {
                        "href": "https://nikita.oslomet.no/noark5v5/api/arkivstruktur/registrering/9e249206-9249-1559-8192-4eaa096b010b/ny-oppbevaringssted"
                    },
                    "https://rel.arkivverket.no/noark5/v5/api/loggingogsporing/hendelseslogg/": {
                        "href": "https://nikita.oslomet.no/noark5v5/api/loggingogsporing/hendelseslogg?$filter=referanseArkivenhet eq '9e249206-9249-1559-8192-4eaa096b010b'/{?$filter&$orderby&$top&$skip}",
                        "templated": true
                    }
                }
            }


     */
    public static void validateFondsLinks(ResultActions resultActions)
            throws Exception {
        resultActions
                .andExpect(jsonPath("$._links['" + REL_LOGGING_EVENT_LOG + "']").exists())
                .andExpect(jsonPath("$._links['" + REL_LOGGING_EVENT_LOG + "']['" + HREF + "']").isNotEmpty())
                .andExpect(jsonPath("$._links['" + REL_LOGGING_EVENT_LOG + "']['" + TEMPLATED + "']").value(true));
        System.out.println("Here");
    }

    public static void validateDocumentMediumLink(ResultActions resultActions)
            throws Exception {
        resultActions
                .andExpect(jsonPath("$._links['" +
                                REL_METADATA_DOCUMENT_MEDIUM + "']['" + HREF + "']",
                        endsWith(HREF_BASE_METADATA + SLASH + DOCUMENT_MEDIUM)));
    }

    public static void validateDocumentTypeLink(ResultActions resultActions)
            throws Exception {
        resultActions
                .andExpect(jsonPath("$._links['" +
                                REL_METADATA_DOCUMENT_TYPE + "']['" + HREF + "']",
                        endsWith(HREF_BASE_METADATA + SLASH + DOCUMENT_TYPE)));
    }

    public static void validateDocumentStatusLink(ResultActions resultActions)
            throws Exception {
        resultActions
                .andExpect(jsonPath("$._links['" +
                                REL_METADATA_DOCUMENT_STATUS + "']['" + HREF + "']",
                        endsWith(HREF_BASE_METADATA + SLASH + DOCUMENT_STATUS)));
    }
}
