package app.utils;

import app.domain.noark5.metadata.ElectronicSignatureSecurityLevel;
import app.domain.noark5.metadata.ElectronicSignatureVerified;
import app.domain.noark5.secondary.ElectronicSignature;
import app.webapp.payload.serializers.noark5.interfaces.IElectronicSignaturePrint;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import static app.utils.TestConstants.*;
import static app.utils.constants.N5ResourceMappings.*;

public final class ElectronicSignatureCreator
        implements IElectronicSignaturePrint {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    /**
     * Create a default ElectronicSignature for testing purposes
     *
     * @return a ElectronicSignature with all values set
     */
    public static ElectronicSignature createElectronicSignature() {
        ElectronicSignature electronicSignature = new ElectronicSignature();

        ElectronicSignatureSecurityLevel esSecurityLevel =
                new ElectronicSignatureSecurityLevel();
        esSecurityLevel.setCode(ES_SIGNATURE_SECURITY_LEVEL_CODE);
        esSecurityLevel.setCodeName(ES_SIGNATURE_SECURITY_LEVEL_CODE_NAME);
        electronicSignature.setElectronicSignatureSecurityLevel(esSecurityLevel);

        ElectronicSignatureVerified esSignatureVerified =
                new ElectronicSignatureVerified();
        esSignatureVerified.setCode(ES_SIGNATURE_VERIFIED_CODE);
        esSignatureVerified.setCodeName(ES_SIGNATURE_VERIFIED_CODE_NAME);
        electronicSignature.setElectronicSignatureVerified(esSignatureVerified);

        electronicSignature.setVerifiedDate(OffsetDateTime.now());
        electronicSignature.setVerifiedBy(ES_VERIFIED_BY);
        return electronicSignature;
    }

    /**
     * Nikita has no ElectronicSignatureSerializer
     *
     * @return
     * @throws IOException
     */
    public static String createElectronicSignatureAsJSON() throws IOException {
        ElectronicSignature es = createElectronicSignature();
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        jgen.writeStartObject(ELECTRONIC_SIGNATURE);

        jgen.writeObjectFieldStart(ELECTRONIC_SIGNATURE_SECURITY_LEVEL_FIELD);
        jgen.writeStringField(CODE, es.getElectronicSignatureSecurityLevel().getCode());
        jgen.writeStringField(CODE_NAME, es.getElectronicSignatureSecurityLevel().getCodeName());
        jgen.writeEndObject();

        jgen.writeObjectFieldStart(ELECTRONIC_SIGNATURE_VERIFIED);
        jgen.writeStringField(CODE, es.getElectronicSignatureVerified().getCode());
        jgen.writeStringField(CODE_NAME, es.getElectronicSignatureVerified().getCodeName());
        jgen.writeEndObject();

        jgen.writeStringField(ELECTRONIC_SIGNATURE_VERIFIED_BY, es.getVerifiedBy());
        jgen.writeStringField(ELECTRONIC_SIGNATURE_VERIFIED_DATE, formatter.format(es.getVerifiedDate()));
        return jsonWriter.toString();
    }
}
