package app.setup;

import org.springframework.boot.test.context.SpringBootTest;


/**
 * Base class for tests that require database.
 * <p>
 * See:
 * <ul>
 *      <li>application.yml for H2</li>
 *      <li>application-local-postgres.yml to use a standalone postgres instance</li>
 * </ul>
 * </p>
 */
@SpringBootTest
public class TestDatabaseBase {

}