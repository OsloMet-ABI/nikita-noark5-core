package app.webapp.controller.controller.setup;

import app.setup.TestDatabaseBase;
import app.utils.exceptions.DocumentationGenerationException;
import app.webapp.exceptions.NikitaMisconfigurationException;
import app.webapp.spring.filters.ETAGFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.UnsupportedEncodingException;

import static app.utils.constants.HATEOASConstants.HREF;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

/**
 * Base class for integration tests of controllers in Nikita.
 *
 * <p>This class extends {@link TestDatabaseBase} and is annotated with
 * {@link SpringBootTest} to load the application context for testing. It
 * configures a {@link MockMvc} instance for testing web controllers,
 * allowing for the simulation of HTTP requests and responses.</p>
 *
 * <p>The class also integrates Spring REST Docs for generating
 * documentation snippets for the API endpoints. It sets up the
 * necessary configurations for request and response preprocessing,
 * making the generated documentation more readable.</p>
 *
 * <p>Additionally, this class provides utility methods for printing
 * documentation and retrieving HREFs from the response of performed
 * actions, enhancing the testing capabilities for controller
 * functionalities.</p>
 *
 * @see TestDatabaseBase
 * @see MockMvc
 * @see RestDocumentationExtension
 */
@SpringBootTest(webEnvironment = RANDOM_PORT)
@ExtendWith(RestDocumentationExtension.class)
@AutoConfigureMockMvc
public class TestControllerBase
        extends TestDatabaseBase {

    @Autowired
    protected MockMvc mockMvc;

    @Value("${server.servlet.context-path}")
    protected String contextPath;

    /**
     * Sets up the MockMvc instance for testing before each test method.
     *
     * <p>This method configures the MockMvc instance to use the provided
     * WebApplicationContext and RestDocumentationContextProvider. It applies
     * Spring Security configuration and sets up request and response
     * preprocessing for better readability in the generated documentation.</p>
     *
     * @param webApplicationContext The WebApplicationContext to set up the
     *                              MockMvc instance with the application context.
     * @param restDocumentation     The RestDocumentationContextProvider used for
     *                              configuring Spring REST Docs documentation.
     */
    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .addFilters(new ETAGFilter(new ObjectMapper()))
                .apply(documentationConfiguration(restDocumentation))
                .apply(springSecurity())
                .alwaysDo(document("{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())))
                .build();
    }

    /**
     * Prints the documentation for the given ResultActions by generating
     * a documentation snippet for various endpoints.
     *
     * <p>This method uses the Spring REST Docs framework to preprocess
     * the request and response, formatting them for better readability
     * before documenting the interaction.</p>
     *
     * @param resultActions The ResultActions object containing the result
     *                      of a performed action, which will be documented.
     * @throws DocumentationGenerationException If an error occurs during the documentation
     *                                          generation process.
     */
    protected void printDocumentation(ResultActions resultActions, String endpointName)
            throws DocumentationGenerationException {
        if (resultActions == null || endpointName == null || endpointName.isEmpty()) {
            throw new IllegalArgumentException("resultActions and endpointName must not be null or empty");
        }

        try {
            resultActions.andDo(document(endpointName,
                    preprocessRequest(prettyPrint()),
                    preprocessResponse(prettyPrint())));
        } catch (Exception e) {
            throw new DocumentationGenerationException("Failed to generate documentation for endpoint: " + endpointName);
        }
    }

    /**
     * Retrieves the HREF (Hypertext Reference) for a specified link from the
     * response of a given ResultActions object.
     *
     * <p>This method extracts the HREF from the JSON response content,
     * splits it based on the context path, and returns the full HREF.
     * If the HREF cannot be split correctly, a custom exception is thrown.</p>
     *
     * @param linkName      The name of the link for which the HREF is to be retrieved.
     * @param resultActions The ResultActions object containing the response
     *                      from which the HREF will be extracted.
     * @return The full HREF as a String, constructed by appending the
     * split part to the context path.
     * @throws UnsupportedEncodingException    If the character encoding is not
     *                                         supported during the extraction
     *                                         process.
     * @throws NikitaMisconfigurationException If the HREF cannot be split
     *                                         correctly based on the context
     *                                         path.
     */
    public String getHref(String linkName, ResultActions resultActions)
            throws NikitaMisconfigurationException, UnsupportedEncodingException {
        if (linkName == null || resultActions == null) {
            throw new IllegalArgumentException("linkName and resultActions must not be null");
        }

        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        String href = JsonPath.read(response.getContentAsString(),
                String.format("$._links.['%s']['%s']", linkName, HREF));

        if (href == null || href.isEmpty()) {
            throw new NikitaMisconfigurationException("HREF is null or empty for link: " + linkName);
        }

        String[] split = href.split(contextPath);
        if (split.length == 2) {
            return contextPath + split[1];
        }

        throw new NikitaMisconfigurationException("Unable to split HREF: " + href);
    }
}
