package app.webapp.controller;

import app.webapp.controller.controller.setup.TestControllerBase;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static app.utils.ElectronicSignatureCreator.createElectronicSignatureAsJSON;
import static app.utils.TestConstants.DOCUMENT_DESCRIPTION_SYSTEM_ID;
import static app.utils.TestConstants.hrefBaseFondsStructure;
import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_DESCRIPTION;
import static app.utils.constants.N5ResourceMappings.ELECTRONIC_SIGNATURE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@Sql(scripts = {"/db-tests/basic_structure.sql"})
public class ElectronicSignatureTest
        extends TestControllerBase {

    // Leaving the test commented out as it is a little unclear to me right now
    // how an electronic signature should be added
    // @Test
    // @WithMockNikitaUser
    public void addElectronicSignatureToExistingDocumentDescription()
            throws Exception {
        String url = hrefBaseFondsStructure + DOCUMENT_DESCRIPTION + SLASH +
                DOCUMENT_DESCRIPTION_SYSTEM_ID + SLASH + "ny-elektronisksignatur";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(CONTENT_TYPE_JSON_MERGE_PATCH)
                .content(createElectronicSignatureAsJSON()));

        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));

        printDocumentation(resultActions, ELECTRONIC_SIGNATURE);
    }
}
