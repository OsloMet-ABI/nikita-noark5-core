package app.webapp.controller;

import app.spring.security.WithMockNikitaUser;
import app.webapp.controller.controller.setup.TestControllerBase;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static app.utils.KeywordCreator.createKeywordAsJSON;
import static app.utils.KeywordCreator.createUpdatedKeywordAsJSON;
import static app.utils.KeywordValidator.*;
import static app.utils.TestConstants.*;
import static app.utils.constants.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static app.utils.constants.Constants.SLASH;
import static app.utils.constants.HATEOASConstants.SELF;
import static app.utils.constants.N5ResourceMappings.*;
import static app.utils.constants.ODataConstants.DOLLAR_FILTER;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Keyword related tests, that are run against a File, Class and Record (Keyword objects).
 * The tests adhere to the following pattern:
 * 1. Check the ability to do a GET ny-noekkelord from the keyword object
 * 2. Create a new keyword associated with the keyword object
 * 3. Retrieve a keyword associated with a keyword object
 * 4. Update a keyword associated with a keyword object
 * 5. Search for an object using a keyword
 */
@Transactional
@Sql(scripts = {"/db-tests/basic_structure.sql"})
public class KeywordTest
        extends TestControllerBase {

    /**
     * Check that the ny-noekkelord endpoint associated with a File will respond to a GET request. Currently, it
     * is unclear what should be returned so not checking for anything else than 200 OK and content-type.
     *
     * @throws Exception if something goes wrong. Note. All exceptions should be handled within the context of the
     *                   mockMvc request and resultActions
     */
    @Test
    @WithMockNikitaUser
    public void checkGetKeywordFromExistingFile() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(hrefBaseFondsStructure + FILE + SLASH + FILE_SYSTEM_ID + SLASH + NEW_KEYWORD)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the creation of a Keyword associated with a File.
     * Verifies that the Keyword is created successfully and validates the response.
     */
    @Test
    @WithMockNikitaUser
    public void createKeywordForFile() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(hrefBaseFondsStructure + FILE + SLASH + FILE_SYSTEM_ID + SLASH + NEW_KEYWORD)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createKeywordAsJSON()));
        resultActions.andExpect(status().isCreated());
        validateKeywordForFile(resultActions);
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the retrieval of an existing Keyword associated with a File.
     * Verifies that the Keyword is retrieved successfully and validates the response.
     */
    @Test
    @WithMockNikitaUser
    public void retrieveKeywordForFile() throws Exception {
        // Create a Keyword associated with a file first to retrieve it
        String urlKeyword = getHref(SELF, createKeywordAssociatedWithObject(FILE, FILE_SYSTEM_ID));
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        validateKeywordForFile(resultActions);
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the update of an existing Keyword associated with a File.
     * Verifies that the Keyword is updated successfully and validates the response.
     */
    @Test
    @WithMockNikitaUser
    public void updateKeywordForFile() throws Exception {
        // Create a Keyword first to update it
        String urlKeyword = getHref(SELF, createKeywordAssociatedWithObject(FILE, FILE_SYSTEM_ID));
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlKeyword)
                .contextPath(contextPath)
                .header("ETAG", "\"0\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createUpdatedKeywordAsJSON()));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        validateUpdatedKeyword(resultActions);
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the OData search for a File based on an existing Keyword.
     * Verifies that the search returns the expected results.
     */
    @Test
    @WithMockNikitaUser
    public void oDataSearchForFileByKeyword() throws Exception {
        // Create a Keyword associated with a File that has a known keyword value to search for
        ResultActions createResultActions = createKeywordAssociatedWithObject(FILE, FILE_SYSTEM_ID);
        createResultActions.andExpect(status().isCreated())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        // Create an OData query to search for
        String odata = "?" + DOLLAR_FILTER + "=" + KEYWORD + "/" + KEYWORD + " eq '" +
                KEYWORD_TEST + "'&$top=1";
        String urlFileKeywordSearch = hrefBaseFondsStructure + FILE + odata;
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlFileKeywordSearch)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(1)));
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Check that the ny-noekkelord endpoint associated with a Class will respond to a GET request. Currently, it
     * is unclear what should be returned so not checking for anything else than 200 OK and content-type.
     *
     * @throws Exception if something goes wrong. Note. All exceptions should be handled within the context of the
     *                   mockMvc request and resultActions
     */
    @Test
    @WithMockNikitaUser
    public void checkGetKeywordFromExistingClass() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(hrefBaseFondsStructure + CLASS + SLASH + CLASS_SYSTEM_ID + SLASH + NEW_KEYWORD)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the creation of a Keyword associated with a Class.
     * Verifies that the Keyword is created successfully and validates the response.
     */
    @Test
    @WithMockNikitaUser
    public void createKeywordForClass() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(hrefBaseFondsStructure + CLASS + SLASH + CLASS_SYSTEM_ID + SLASH + NEW_KEYWORD)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createKeywordAsJSON()));
        resultActions.andExpect(status().isCreated());
        validateKeywordForClass(resultActions);
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the retrieval of an existing Keyword associated with a Class.
     * Verifies that the Keyword is retrieved successfully and validates the response.
     */
    @Test
    @WithMockNikitaUser
    public void retrieveKeywordForClass() throws Exception {
        // Create a Keyword associated with a class first to retrieve it
        String urlKeyword = getHref(SELF, createKeywordAssociatedWithObject(CLASS, CLASS_SYSTEM_ID));
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        validateKeywordForClass(resultActions);
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the update of an existing Keyword associated with a Class.
     * Verifies that the Keyword is updated successfully and validates the response.
     */
    @Test
    @WithMockNikitaUser
    public void updateKeywordForClass() throws Exception {
        // Create a Keyword first to update it
        String urlKeyword = getHref(SELF, createKeywordAssociatedWithObject(CLASS, CLASS_SYSTEM_ID));
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlKeyword)
                .contextPath(contextPath)
                .header("ETAG", "\"0\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createUpdatedKeywordAsJSON()));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        validateUpdatedKeyword(resultActions);
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the OData search for a Class based on an existing Keyword.
     * Verifies that the search returns the expected results.
     */
    @Test
    @WithMockNikitaUser
    public void oDataSearchForClassByKeyword() throws Exception {
        // Create a Keyword associated with a Class that has a known keyword value to search for
        ResultActions createResultActions = createKeywordAssociatedWithObject(CLASS, CLASS_SYSTEM_ID);
        createResultActions.andExpect(status().isCreated())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        // Create an OData query to search for
        String odata = "?" + DOLLAR_FILTER + "=" + KEYWORD + "/" + KEYWORD + " eq '" +
                KEYWORD_TEST + "'&$top=1";
        String urlClassKeywordSearch = hrefBaseFondsStructure + CLASS + odata;
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlClassKeywordSearch)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(1)));
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Check that the ny-noekkelord endpoint associated with a Record will respond to a GET request. Currently, it
     * is unclear what should be returned so not checking for anything else than 200 OK and content-type.
     *
     * @throws Exception if something goes wrong. Note. All exceptions should be handled within the context of the
     *                   mockMvc request and resultActions
     */
    @Test
    @WithMockNikitaUser
    public void checkGetKeywordFromExistingRecord() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(hrefBaseFondsStructure + RECORD + SLASH + RECORD_SYSTEM_ID + SLASH + NEW_KEYWORD)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the creation of a Keyword associated with a Record.
     * Verifies that the Keyword is created successfully and validates the response.
     */
    @Test
    @WithMockNikitaUser
    public void createKeywordForRecord() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(hrefBaseFondsStructure + RECORD + SLASH + RECORD_SYSTEM_ID + SLASH + NEW_KEYWORD)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createKeywordAsJSON()));
        resultActions.andExpect(status().isCreated());
        validateKeywordForRecord(resultActions);
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the retrieval of an existing Keyword associated with a Record.
     * Verifies that the Keyword is retrieved successfully and validates the response.
     */
    @Test
    @WithMockNikitaUser
    public void retrieveKeywordForRecord() throws Exception {
        // Create a Keyword associated with a record first to retrieve it
        String urlKeyword = getHref(SELF, createKeywordAssociatedWithObject(RECORD, RECORD_SYSTEM_ID));
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlKeyword)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        validateKeywordForRecord(resultActions);
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the update of an existing Keyword associated with a Record.
     * Verifies that the Keyword is updated successfully and validates the response.
     */
    @Test
    @WithMockNikitaUser
    public void updateKeywordForRecord() throws Exception {
        // Create a Keyword first to update it
        String urlKeyword = getHref(SELF, createKeywordAssociatedWithObject(RECORD, RECORD_SYSTEM_ID));
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(urlKeyword)
                .contextPath(contextPath)
                .header("ETAG", "\"0\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createUpdatedKeywordAsJSON()));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        validateUpdatedKeyword(resultActions);
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Tests the OData search for a Record based on an existing Keyword.
     * Verifies that the search returns the expected results.
     */
    @Test
    @WithMockNikitaUser
    public void oDataSearchForRecordByKeyword() throws Exception {
        // Create a Keyword associated with a Record that has a known keyword value to search for
        ResultActions createResultActions = createKeywordAssociatedWithObject(RECORD, RECORD_SYSTEM_ID);
        createResultActions.andExpect(status().isCreated())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        // Create an OData query to search for
        String odata = "?" + DOLLAR_FILTER + "=" + KEYWORD + "/" + KEYWORD + " eq '" +
                KEYWORD_TEST + "'&$top=1";
        String urlRecordKeywordSearch = hrefBaseFondsStructure + RECORD + odata;
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlRecordKeywordSearch)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(1)));
        printDocumentation(resultActions, KEYWORD);
    }

    /**
     * Creates a new keyword associated with a specified object (Class, File or
     * Record) using its systemID.
     *
     * <p>This method performs a POST request to create a keyword for the
     * given object identified by the systemID. It constructs the request
     * URL using the provided object and systemID, sets the appropriate
     * content type and accepts headers, and sends the request with the
     * keyword data in JSON format.</p>
     *
     * <p>The method expects a successful creation response (HTTP status 201
     * Created) and returns the result of the performed action.</p>
     *
     * @param object         The name of the object to which the keyword is associated.
     * @param objectSystemId The systemID of the object to which the keyword
     *                       is associated.
     * @return A {@link ResultActions} object containing the result of the
     * performed action, allowing further assertions or actions to be
     * performed on the response.
     * @throws Exception If an error occurs while performing the request or
     *                   processing the response.
     */
    protected ResultActions createKeywordAssociatedWithObject(String object, String objectSystemId)
            throws Exception {
        ResultActions createResultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(hrefBaseFondsStructure + object + SLASH + objectSystemId + SLASH + NEW_KEYWORD)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createKeywordAsJSON()));
        createResultActions.andExpect(status().isCreated());
        return createResultActions;
    }
}
