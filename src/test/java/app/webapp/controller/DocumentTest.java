package app.webapp.controller;

import app.spring.security.WithMockNikitaUser;
import app.webapp.controller.controller.setup.TestControllerBase;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static app.utils.TestConstants.DOCUMENT_OBJECT_SYSTEM_ID;
import static app.utils.TestConstants.hrefBaseFondsStructure;
import static app.utils.constants.Constants.*;
import static app.utils.constants.FileConstants.MIME_TYPE_ODT;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_OBJECT;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@Sql("/db-tests/basic_structure.sql")
public class DocumentTest
        extends TestControllerBase {
    @Test
    @WithMockNikitaUser
    /**
     * Initiates the multi-stage document upload process and verifies the response.
     *
     * This method performs a POST request to start the upload of a document in multiple stages.
     * It checks that the response status is OK (200) and verifies that the "Location" header
     * is present and not empty, indicating where the next stage of the upload should occur.
     *
     * @throws Exception if an error occurs during the request or response validation
     */
    public void initiateMultiStageDocumentUpload() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(hrefBaseFondsStructure + DOCUMENT_OBJECT + SLASH + DOCUMENT_OBJECT_SYSTEM_ID + SLASH + REFERENCE_FILE)
                .header(CONTENT_LENGTH, "0")
                .header(UPLOAD_CONTENT_TYPE, MIME_TYPE_ODT)
                .header(UPLOAD_CONTENT_LENGTH, "1200")
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isOk())
                .andExpect(result -> {
                    String headerValue = result.getResponse().getHeader("Location");
                    assertNotNull("Location header should not be null", headerValue);
                    assertFalse("Location header should not be empty", headerValue.isEmpty());
                })
                .andDo(document(DOCUMENT_OBJECT,
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));
        printDocumentation(resultActions, DOCUMENT_OBJECT);
    }
}
