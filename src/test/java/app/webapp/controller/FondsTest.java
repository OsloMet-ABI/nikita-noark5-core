package app.webapp.controller;

import app.spring.security.WithMockNikitaUser;
import app.webapp.controller.controller.setup.TestControllerBase;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static app.utils.TestConstants.PATTERN_UUID;
import static app.utils.TestConstants.hrefBaseFondsStructure;
import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static app.utils.creator.FondsCreator.createFondsAsJSON;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.matchesPattern;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@Sql("/db-tests/basic_structure.sql")
public class FondsTest
        extends TestControllerBase {

    /**
     * Tests the endpoint for retrieving a list of fonds.
     * <p>
     * This test verifies that when a request is made to the fonds endpoint,
     * the response is successful (HTTP 200 OK) and contains the expected
     * content type. It checks that the response includes a results array
     * with exactly one item, and that the item's fields match the expected
     * values, including a valid UUID format for the system ID and a
     * predefined title.
     * </p>
     *
     * <p>
     * The test is executed with a mock user having the role of
     * {@code TEST_USER} to simulate authenticated access.
     * </p>
     *
     * <p>
     * The following assertions are made:
     * <ul>
     *     <li>The response status is 200 OK.</li>
     *     <li>The content type of the response is {@code NOARK5_V5_CONTENT_TYPE_JSON}.</li>
     *     <li>The results array contains exactly one item.</li>
     *     <li>The system ID of the first result matches the UUID pattern defined by {@code PATTERN_UUID}.</li>
     *     <li>The title of the first result matches the expected value {@code TEST_TITLE}.</li>
     * </ul>
     * </p>
     *
     * <p>
     * The request and response are documented using Spring REST Docs.
     * </p>
     *
     * @throws Exception if an error occurs during the request execution or assertion.
     */
    @Test
    @WithMockNikitaUser
    public void givenFondsCheckListIsReturnedAndContents() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get(hrefBaseFondsStructure + FONDS)
                        .contextPath(contextPath)
                        .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.results", hasSize(1)))
                .andExpect(jsonPath("$.results[0]." + SYSTEM_ID).value(matchesPattern(PATTERN_UUID)))
                .andExpect(jsonPath("$.results[0]." + TITLE).value(TEST_TITLE));
        printDocumentation(resultActions, FONDS);
    }

    /**
     * Tests the creation of a Fonds and verifies the returned contents.
     *
     * <p>This test method performs a POST request to create a new Fonds
     * and checks that the response status is 201 Created. It also verifies
     * that the returned JSON contains the expected fields, including that
     * systemID matches a UUID pattern and a title that matches the
     * predefined test title.</p>
     *
     * <p>The method uses the {@link WithMockNikitaUser} annotation to simulate
     * a JWT user for authentication during the test.</p>
     *
     * @throws Exception If an error occurs while performing the request or
     *                   processing the response.
     */
    @Test
    @WithMockNikitaUser
    public void createFondsCheckReturnedContents() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(hrefBaseFondsStructure + NEW_FONDS)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createFondsAsJSON()));

        resultActions.andExpect(status().isCreated())
                .andExpect(jsonPath("$." + SYSTEM_ID).value(matchesPattern(PATTERN_UUID)))
                .andExpect(jsonPath("$." + TITLE).value(TEST_TITLE))
                .andDo(document(FONDS,
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));
        printDocumentation(resultActions, FONDS);
    }
}
