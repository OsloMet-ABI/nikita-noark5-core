package app.webapp.controller;

import app.domain.noark5.casehandling.secondary.CorrespondencePartPerson;
import app.webapp.controller.controller.setup.TestControllerBase;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.CorrespondencePartPersonSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.io.StringWriter;

import static app.utils.CorrespondencePartCreator.createCorrespondencePartPerson;
import static app.utils.CorrespondencePartCreator.createCorrespondencePartPersonAsJSON;
import static app.utils.CorrespondencePartValidator.validateCorrespondencePartPerson;
import static app.utils.CorrespondencePartValidator.validateCorrespondencePartPersonLink;
import static app.utils.KeywordCreator.createKeywordAsJSON;
import static app.utils.KeywordValidator.validateKeywordForFile;
import static app.utils.TestConstants.*;
import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static com.jayway.jsonpath.JsonPath.read;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@Sql(scripts = {"/db-tests/basic_structure.sql"})
public class CorrespondencePartTest
        extends TestControllerBase {

    /**
     * Test that it is possible to create a CorrespondencePartPerson and test
     * that the values and _links are correct. Then retrieve the systemID and
     * make sure the object can be retrieved independently of the database.
     *
     * @throws Exception if required
     */
    //@Test
    //@WithMockNikitaUser
    public void addCorrespondencePartPersonToExistingRecord() throws Exception {
        String url = hrefBaseFondsStructure + RECORD + SLASH + RECORD_SYSTEM_ID +
                SLASH + NEW_CORRESPONDENCE_PART_PERSON;

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createCorrespondencePartPersonAsJSON()));

        resultActions.andExpect(status().isCreated())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON));
        validateCorrespondencePartPerson(resultActions);
        validateCorrespondencePartPersonLink(resultActions);
        printDocumentation(resultActions, CORRESPONDENCE_PART_PERSON);
    }

    //@Test
    //@WithMockNikitaUser
    public void checkFilePartContains()
            throws Exception {

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(hrefBaseFondsStructure + FILE + SLASH + FILE_SYSTEM_ID + SLASH + NEW_PART_PERSON)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createKeywordAsJSON()));
        resultActions.andExpect(status().isCreated());
        validateKeywordForFile(resultActions);
        printDocumentation(resultActions, KEYWORD);

        String url = "/noark5v5/odata/api/arkivstruktur/mappe?$filter=contains" +
                "(part/navn, 'Gruber')";

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results[0]." + SYSTEM_ID)
                        .value("f1677c47-99e1-42a7-bda2-b0bbc64841b7"))
                .andExpect(jsonPath("$.results[1]." + SYSTEM_ID)
                        .value("43d305de-b3c8-4922-86fd-45bd26f3bf01"));

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));
    }

    /**
     * Check that it is possible to search based on files with an associated
     * partRoll that has a codename 'Advokat'
     * <p>
     * Make sure result corresponds to the file with systemID
     * f1677c47-99e1-42a7-bda2-b0bbc64841b7
     * and that only one result has been returned
     *
     * @throws Exception if required
     */
    //@Test
    //@WithMockNikitaUser
    public void checkFilePartWithPartRoleCodeName()
            throws Exception {
        String url = "/noark5v5/odata/api/arkivstruktur/mappe?$filter=" +
                "part/partRolle/kodenavn eq 'Advokat'";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results[0]." + SYSTEM_ID)
                        .value("f1677c47-99e1-42a7-bda2-b0bbc64841b7"))
                .andExpect(jsonPath("$.results", hasSize(1)));

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));
    }

    /**
     * Check that it is possible to search files based on associated partUnit
     * value. In this case search all partUnit that contain name equal to
     * 'Hans Gruber'
     * <p>
     * mappe?$filter=partEnhet/navn eq 'Hans Gruber'
     * <p>
     * Make sure result corresponds to the file with systemID
     * f1677c47-99e1-42a7-bda2-b0bbc64841b7
     * and that only one result has been returned
     *
     * @throws Exception if required
     */
    //@Test
    //@WithMockNikitaUser
    public void searchFileWithPartUnit()
            throws Exception {
        String attributeName = "partEnhet/navn";
        String compareValue = "Hans Gruber";

        String url = "/noark5v5/odata/api/arkivstruktur/mappe?$filter=" +
                attributeName + " eq '" + compareValue + "'";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results[0]." + SYSTEM_ID)
                        .value("f1677c47-99e1-42a7-bda2-b0bbc64841b7"))
                .andExpect(jsonPath("$.results", hasSize(1)));

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));
    }

    /**
     * Check that it is possible to search files based on associated partUnit
     * value. In this case search all partUnit that contain name equal to
     * 'Hans Gruber'
     * <p>
     * mappe?$filter=partPerson/navn eq 'Hans Gruber'
     * <p>
     * Make sure result corresponds to the file with systemID
     * 43d305de-b3c8-4922-86fd-45bd26f3bf01
     * and that only one result has been returned
     *
     * @throws Exception if required
     */
    //@Test
    //@WithMockNikitaUser
    public void searchFileWithPartPerson()
            throws Exception {
        String attributeName = "partPerson/navn";
        String compareValue = "Hans Gruber";

        String url = "/noark5v5/odata/api/arkivstruktur/mappe?$filter=" +
                attributeName + " eq '" + compareValue + "'";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results[0]." + SYSTEM_ID)
                        .value("43d305de-b3c8-4922-86fd-45bd26f3bf01"))
                .andExpect(jsonPath("$.results", hasSize(1)));

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));
    }

    /**
     * Check that it is possible to search based on files with an associated
     * partRoll that has a code 'ADV'
     * <p>
     * Make sure result corresponds to the file with systemID
     * f1677c47-99e1-42a7-bda2-b0bbc64841b7
     * and that only one result has been returned
     *
     * @throws Exception if required
     */
    //@Test
    //@WithMockNikitaUser
    public void checkFilePartWithPartRoleCode()
            throws Exception {
        String url = "/noark5v5/odata/api/arkivstruktur/mappe?$filter=" +
                "part/partRolle/kode eq 'ADV'";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.results[0]." + SYSTEM_ID)
                        .value("f1677c47-99e1-42a7-bda2-b0bbc64841b7"))
                .andExpect(jsonPath("$.results", hasSize(1)));

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));
    }


    /**
     * Test that it is possible to create a CorrespondencePartPerson and test
     * that the values and _links are correct. Then retrieve the systemID and
     * make sure the object can be retrieved independently of the database.
     *
     * @throws Exception if required
     */
    //@Test
    //@WithMockNikitaUser
    public void addElectronicSignatureToExistingRecord() throws Exception {
        String url = "/noark5v5/api/arkivstruktur/registrering/" +
                "dc600862-3298-4ec0-8541-3e51fb900054/" +
                "ny-elektro";

        CorrespondencePartPerson correspondencePart = createCorrespondencePartPerson();
        var jsonWriter = new StringWriter();
        var jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializerSM = new CorrespondencePartPersonSerializer();
        serializerSM.serializeNoarkEntity(correspondencePart, new LinksNoarkObject(), jgen);
        jgen.close();

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(jsonWriter.toString()));

        MockHttpServletResponse response =
                resultActions.andReturn().getResponse();
        System.out.println(response.getContentAsString());

        resultActions.andExpect(status().isCreated());
        validateCorrespondencePartPerson(resultActions);
        validateCorrespondencePartPersonLink(resultActions);

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        url = "/noark5v5/api/arkivstruktur/korrespondansepartperson/" +
                read(response.getContentAsString(), "$." + SYSTEM_ID);

        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        resultActions.andExpect(status().isOk());
        validateCorrespondencePartPerson(resultActions);
        validateCorrespondencePartPersonLink(resultActions);
    }
}