package app.webapp.controller;

import app.spring.security.WithMockNikitaUser;
import app.webapp.controller.controller.setup.TestControllerBase;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithAnonymousUser;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.SELF;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ApplicationRootTest
        extends TestControllerBase {

    /**
     * Tests that the appropriate links are present in the response when a user is not logged in.
     * <p>
     * This test performs a GET request to the root ("/") of the application and verifies that:
     * <ul>
     *     <li>The response status is 200 OK.</li>
     *     <li>The response content type is {@code NOARK5_V5_CONTENT_TYPE_JSON}.</li>
     * </ul>
     * Additionally, it documents the response using Spring REST Docs, including:
     * <ul>
     *     <li>Preprocessing the request and response for pretty printing.</li>
     *     <li>Link relations for OIDC login information and self-reference.</li>
     * </ul>
     * The documented links include:
     * <ul>
     *     <li><strong>login</strong>: Provides login information via OIDC.</li>
     *     <li><strong>self</strong>: A self-reference link to the current resource.</li>
     * </ul>
     *
     * @throws Exception if the request fails or the assertions do not hold.
     */
    @Test
    @WithAnonymousUser
    public void checkLinksArePresentWithNoUser() throws Exception {
        mockMvc.perform(get("/")
                        .accept(NOARK5_V5_CONTENT_TYPE_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON))
                .andDo(document("home",
                                preprocessRequest(prettyPrint()),
                                preprocessResponse(prettyPrint()),
                                links(halLinks(),
                                        linkWithRel(REL_LOGIN_OIDC).
                                                description("Get login information"),
                                        linkWithRel(SELF).
                                                description("Self REL")
                                )

                        )
                );

    }

    /**
     * Tests that the appropriate links are present in the response when a user is logged in.
     * <p>
     * This test performs a GET request to the root ("/") of the application while simulating
     * an authenticated user with the username "admin@example.com". It verifies that:
     * <ul>
     *     <li>The response status is 200 OK.</li>
     *     <li>The response content type is {@code NOARK5_V5_CONTENT_TYPE_JSON}.</li>
     * </ul>
     * Additionally, it documents the response using Spring REST Docs, including:
     * <ul>
     *     <li>Preprocessing the request and response for pretty printing.</li>
     *     <li>Link relations for various functionalities available to the logged-in user.</li>
     * </ul>
     * The documented links include:
     * <ul>
     *     <li><strong>administration</strong>: Provides information about administrative functions.</li>
     *     <li><strong>case handling</strong>: Provides information about case handling.</li>
     *     <li><strong>fonds structure</strong>: Provides information about fonds structure.</li>
     *     <li><strong>logging</strong>: Provides information about logging.</li>
     *     <li><strong>login</strong>: Provides login information via OIDC.</li>
     *     <li><strong>metadata</strong>: Provides information about metadata.</li>
     *     <li><strong>self</strong>: A self-reference link to the current resource.</li>
     * </ul>
     *
     * @throws Exception if the request fails or the assertions do not hold.
     */
    @Test
    @WithMockNikitaUser
    public void checkLinksArePresentWhenUserLoggedIn() throws Exception {
        mockMvc.perform(get("/")
                        .accept(NOARK5_V5_CONTENT_TYPE_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON))
                .andDo(document("home",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        links(halLinks(),
                                linkWithRel(REL_ADMINISTRATION).
                                        description("Get information about administrative functions"),
                                linkWithRel(REL_CASE_HANDLING).
                                        description("Get information about case handling"),
                                linkWithRel(REL_FONDS_STRUCTURE).
                                        description("Get information about fonds structure"),
                                linkWithRel(REL_LOGGING).
                                        description("Get information about logging"),
                                linkWithRel(REL_LOGIN_OIDC).
                                        description("Get login information"),
                                linkWithRel(REL_METADATA).
                                        description("Get information about metadata"),
                                linkWithRel(SELF).
                                        description("Self REL")
                        )
                ));
    }
}