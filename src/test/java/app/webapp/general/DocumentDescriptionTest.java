package app.webapp.general;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static app.utils.DocumentDescriptionCreator.createDocumentDescriptionAsJSON;
import static app.utils.DocumentDescriptionValidator.validateDocumentDescription;
import static app.utils.DocumentDescriptionValidator.validateDocumentDescriptionTemplate;
import static app.utils.constants.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Internal testing currently suspended in the codebase.
 * <p>
 * Note: In the upgrade to Spring Boot 3 we experienced problems with the domain model. There is no problem with the
 * domain model when running spring boot as an application, but there seems to be a problem with the domain model
 * is used in testing. Typically, the problem is that Hibernate is not able to deal with JOINED inheritance as it is
 * specified in nikita, and prints a Wrong entity retrieved exception stating that it expects a File when it was given
 * a Record (We tried to retrieve a Record). We have tried using a discriminator column, forcing the use of a
 * discriminator column but in test mode we are not able to get the Nikita domain model
 * <p>
 * We need to finish this Spring Boot 3 branch and move forward. All tests that will not run are currently commented
 * out until we have time to fix it.
 */
public class DocumentDescriptionTest
        extends BaseTest {

    protected MockMvc mockMvc;


    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())))
                .build();
    }

    /**
     * Check that it is possible to create a DocumentDescription
     *
     * @throws Exception Serialising or validation exception
     */
    @Sql("/db-tests/basic_structure.sql")
    @WithMockUser("test_user_admin@example.com")
    public void addAuthorWhenCreatingDocumentDescription() throws Exception {
        // First get template to create / POST DocumentDescription
        String urlNewDocumentDescription = "/noark5v5/api/arkivstruktur/registrering" +
                "/dc600862-3298-4ec0-8541-3e51fb900054/ny-dokumentbeskrivelse";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewDocumentDescription)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isOk());
        validateDocumentDescriptionTemplate(resultActions);

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));

        // Create a JSON object to POST
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewDocumentDescription)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createDocumentDescriptionAsJSON()));

        resultActions.andExpect(status().isCreated());
        validateDocumentDescription(resultActions);

        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));
    }
}
