package app.webapp.exceptions;

import app.spring.security.WithMockNikitaUser;
import app.webapp.controller.controller.setup.TestControllerBase;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static app.utils.TestConstants.FONDS_SYSTEM_ID;
import static app.utils.TestConstants.hrefBaseFondsStructure;
import static app.utils.constants.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static app.utils.constants.Constants.SLASH;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.FONDS;
import static java.util.UUID.randomUUID;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
public class ExceptionsTest
        extends TestControllerBase {
    /**
     * Validates the error payload of a response from a Spring MVC test.
     * <p>
     * This method checks that the response contains the expected HTTP status code,
     * content type, and the structure of the error payload. It verifies that the
     * error object exists and that it contains the expected error code and description.
     * <p>
     * The method performs the following checks:
     * <ol>
     *     <li>Asserts that the HTTP status code of the response matches the expected status code.</li>
     *     <li>Asserts that the content type of the response is the expected JSON type.</li>
     *     <li>Checks that the error object exists in the response JSON.</li>
     *     <li>Validates that the error code in the error object matches the provided HTTP status code.</li>
     *     <li>Ensures that the error description is not null.</li>
     * </ol>
     *
     * @param resultActions  the result actions from the mock MVC request, which contains the response to be validated.
     * @param httpStatusCode the expected HTTP status code that should be present in the response.
     * @throws Exception if an error occurs during the validation process, such as an assertion failure.
     */
    private static void checkErrorPayload(ResultActions resultActions, long httpStatusCode) throws Exception {
        resultActions.andExpect(status().is((int) httpStatusCode))
                .andExpect(content().contentType(NOARK5_V5_CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$." + ERROR).exists())
                .andExpect(jsonPath("$." + ERROR + "." + ERROR_CODE).value(httpStatusCode))
                .andExpect(jsonPath("$." + ERROR + "." + ERROR_DESCRIPTION).value(notNullValue()));
    }

    private static String getUpdatedFondsContent() {
        return "{\"tittel\":\"Oppdatert\", \"arkivstatus\": {\"kode\":\"O\", \"kodenavn\":\"Opprettet\"}}";
    }

    /**
     * Tests the behavior of the API when a requested object is missing.
     * <p>
     * This test simulates a scenario where a client attempts to retrieve a resource
     * that does not exist, expecting a 404 Not Found response. It verifies that the
     * response payload contains the appropriate error information, including the error
     * code and description.
     * <p>
     * The test uses the {@link WithMockNikitaUser} annotation to simulate an authenticated
     * user context for the request.
     *
     * @throws Exception if an error occurs during the execution of the test
     */
    @Test
    @WithMockNikitaUser
    public void givenMissingObjectCheck404() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get(hrefBaseFondsStructure + FONDS + SLASH + randomUUID())
                        .contextPath(contextPath)
                        .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        checkErrorPayload(resultActions, 404);
        printDocumentation(resultActions, ERROR);
    }

    /**
     * Tests the behavior of the API when a PUT request is made to update a Fonds resource
     * without providing an ETag header. This scenario is expected to result in a 400 Bad Request
     * response due to the missing ETag
     *
     * <p>
     * The test performs the following steps:
     * <ul>
     *     <li>Executes a PUT request to the specified Fonds resource URL.</li>
     *     <li>Sets the context path and content type to JSON as per the API specifications.</li>
     *     <li>Includes the updated content for the Fonds resource in the request body.</li>
     *     <li>Checks that the response status is 400 (Bad Request) indicating that the request was invalid.</li>
     *     <li>Validates the error payload returned in the response.</li>
     *     <li>Prints the documentation for the error response for further analysis.</li>
     * </ul>
     * </p>
     *
     * <p>
     * This test is annotated with {@link WithMockNikitaUser} to simulate an authenticated user
     * with the necessary permissions to perform the update operation.
     * </p>
     *
     * @throws Exception if an error occurs during the execution of the test.
     */
    @Test
    @WithMockNikitaUser
    public void givenMissingETAGCheck400() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(hrefBaseFondsStructure + FONDS + SLASH + FONDS_SYSTEM_ID)
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(getUpdatedFondsContent()));

        checkErrorPayload(resultActions, 400);
        printDocumentation(resultActions, ERROR);
    }

    /**
     * Tests the behavior of the API when a PUT request is made to update a Fonds resource
     * with an incorrect ETag header. The value should be "0" in this case, but is set to an arbitrary "99".
     * This scenario is expected to result in a 409 Conflict response due to the mismatch between the provided
     * ETag and the current ETag of the resource.
     *
     * <p>
     * The test performs the following steps:
     * <ul>
     *     <li>Executes a PUT request to the specified Fonds resource URL.</li>
     *     <li>Sets the context path and content type to JSON as per the API specifications.</li>
     *     <li>Includes an incorrect ETag header ("99") in the request.</li>
     *     <li>Includes the updated content for the Fonds resource in the request body.</li>
     *     <li>Checks that the response status is 409 (Conflict) indicating that the ETag provided
     *         does not match the current ETag of the resource.</li>
     *     <li>Validates the error payload returned in the response.</li>
     *     <li>Prints the documentation for the error response for further analysis.</li>
     * </ul>
     * </p>
     *
     * <p>
     * This test is annotated with {@link WithMockNikitaUser} to simulate an authenticated user
     * with the necessary permissions to perform the update operation. It also uses the {@link Sql}
     * annotation to set up the database state before the test runs, ensuring that the necessary
     * data is present for the test to execute correctly.
     * </p>
     *
     * @throws Exception if an error occurs during the execution of the test.
     */
    @Test
    @WithMockNikitaUser
    @Sql("/db-tests/basic_structure.sql")
    public void givenWrongETAGCheck409() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put(hrefBaseFondsStructure + FONDS + SLASH + FONDS_SYSTEM_ID)
                .contextPath(contextPath)
                .header("ETAG", "\"99\"")
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(getUpdatedFondsContent()));

        checkErrorPayload(resultActions, 409);
        printDocumentation(resultActions, ERROR);
    }

    /**
     * Tests the behavior of the application when a non-logged-in user attempts to access a protected resource.
     * <p>
     * This test simulates an anonymous user trying to access a specific endpoint that requires authentication.
     * It expects the server to respond with a 401 Unauthorized status code, indicating that the user is not
     * authorized to access the requested resource.
     * <p>
     * The test performs the following steps:
     * <ol>
     *     <li>Performs a GET request to the specified URL, which includes a randomly generated UUID.</li>
     *     <li>Checks that the response payload contains the expected error structure and status code.</li>
     *     <li>Prints the documentation for the response, including any error details.</li>
     * </ol>
     *
     * @throws Exception if an error occurs during the execution of the test, such as a failure to perform the
     *                   request or an unexpected response from the server.
     */
    @Test
    @WithAnonymousUser
    public void givenNonLoggedInUserCheck401() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get(hrefBaseFondsStructure + FONDS + SLASH + randomUUID())
                        .contextPath(contextPath)
                        .accept(NOARK5_V5_CONTENT_TYPE_JSON));
        checkErrorPayload(resultActions, 401);
        printDocumentation(resultActions, ERROR);
    }
}
