-- systemID for User objects
-- insert into system_id_entity(system_id, created_date, created_by, owned_by, version)
--values ('a63a38c5-58d4-4346-8e4d-279889c13b3b', '2019-04-08 00:00:00', 'admin@example.com', 'admin@example.com', 0);
-- insert into system_id_entity(system_id, created_date, created_by, owned_by, version)
--values ('59536d1e-c0a6-4a30-95c0-a81a887f0758', '2019-04-08 00:00:00', 'admin@example.com', 'admin@example.com', 0);
-- insert into system_id_entity(system_id, created_date, created_by, owned_by, version)
--values ('7a7ee349-1e05-4096-bf39-acabba2e120c', '2019-04-08 00:00:00', 'admin@example.com', 'admin@example.com', 0);
-- insert into system_id_entity(system_id, created_date, created_by, owned_by, version)
--values ('7c33ed39-4c87-41a4-8ca1-e6a4d021bdca', '2019-04-08 00:00:00', 'admin@example.com', 'admin@example.com', 0);

-- User objects
-- insert into ad_user (system_id, lastname, firstname, account_non_expired, credentials_non_expired, account_non_locked, enabled, password, username) values ('a63a38c5-58d4-4346-8e4d-279889c13b3b', 'Szyslak','Moe', true, true, true, true, 'password', 'casehandler@example.com');
-- insert into ad_user (system_id, lastname, firstname, account_non_expired, credentials_non_expired, account_non_locked, enabled, password, username) values ('59536d1e-c0a6-4a30-95c0-a81a887f0758', 'Burns', 'Charles Montgomery Plantagenet Schicklgruber', true, true, true, true, 'password', 'leader@example.com');
-- insert into ad_user (system_id, lastname, firstname, account_non_expired, credentials_non_expired, account_non_locked, enabled, password, username) values ('7a7ee349-1e05-4096-bf39-acabba2e120c', 'Smithers', 'Waylon Joseph', true, true, true, true, 'password', 'recordskeeper@example.com');
-- insert into ad_user (system_id, lastname, firstname, account_non_expired, credentials_non_expired, account_non_locked, enabled, password, username) values ('7c33ed39-4c87-41a4-8ca1-e6a4d021bdca', 'Grimes', 'Frank', true, true, true, true, 'password', 'admin1@example.com');

-- INSERT INTO ad_authority (id, authority_name) VALUES (1, 'CASE_HANDLER');
-- INSERT INTO ad_authority (id, authority_name) VALUES (2, 'LEADER');
-- INSERT INTO ad_authority (id, authority_name) VALUES (3, 'RECORDS_KEEPER');
-- INSERT INTO ad_authority (id, authority_name) VALUES (4, 'RECORDS_MANAGER');

-- INSERT INTO ad_administrative_unit_user(f_pk_administrative_unit_id, f_pk_user_id) VALUES ('c3d4affc-66a0-4663-b63a-6ecc4f3d6009', 'a63a38c5-58d4-4346-8e4d-279889c13b3b');
-- INSERT INTO ad_administrative_unit_user(f_pk_administrative_unit_id, f_pk_user_id) VALUES ('c3d4affc-66a0-4663-b63a-6ecc4f3d6009', '59536d1e-c0a6-4a30-95c0-a81a887f0758');
-- INSERT INTO ad_administrative_unit_user(f_pk_administrative_unit_id, f_pk_user_id) VALUES ('c3d4affc-66a0-4663-b63a-6ecc4f3d6009', '7a7ee349-1e05-4096-bf39-acabba2e120c');
-- INSERT INTO ad_administrative_unit_user(f_pk_administrative_unit_id, f_pk_user_id) VALUES ('c3d4affc-66a0-4663-b63a-6ecc4f3d6009', '7c33ed39-4c87-41a4-8ca1-e6a4d021bdca');

-- INSERT INTO ad_user_authority(`f_pk_user_id`,  `authority_id`) VALUES ('a63a38c5-58d4-4346-8e4d-279889c13b3b', 1);
-- INSERT INTO ad_user_authority(`f_pk_user_id`,  `authority_id`) VALUES ('59536d1e-c0a6-4a30-95c0-a81a887f0758', 2);
-- INSERT INTO ad_user_authority(`f_pk_user_id`,  `authority_id`) VALUES ('7a7ee349-1e05-4096-bf39-acabba2e120c', 3);
-- INSERT INTO ad_user_authority(`f_pk_user_id`,  `authority_id`) VALUES ('7c33ed39-4c87-41a4-8ca1-e6a4d021bdca', 4);
