-- Build a given arkivstruktur for testing
--
-- Note. The '' is not postgres syntax. Changing database will require dealing with duplicate
--

-- Default AdministrativeUnit
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('c3d4affc-66a0-4663-b63a-6ecc4f3d6009', '2020-07-01 22:25:06', 'test_user@example.com',
        'default', 0, 'AdministrativeUnit');

-- Default AdministrativeUnit
insert into ad_administrative_unit (system_id, administrative_unit_name, default_administrative_unit, short_name)
values ('c3d4affc-66a0-4663-b63a-6ecc4f3d6009', 'example test administrative unit', true, 'test');

-- Default admin user
INSERT
INTO system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('03fd9ab1-e5fd-44b6-9c63-6afd08569a46', '2019-04-08 00:00:00', 'test_user@example.com', null, 0,
        'Organisation');

-- Default admin user
-- Note. The '' is not postgres syntax. Changing database will require dealing with duplicate

insert
into ad_user(system_id, username, firstname, lastname)
values ('03fd9ab1-e5fd-44b6-9c63-6afd08569a46', 'test_user@example.com', 'test_user_firstname', 'test_user_lastname');

-- 3
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('3318a63f-11a7-4ec9-8bf1-4144b7f281cf', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'Fonds');

-- 4
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'Series');

-- 5 system_id:file objects
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('f1677c47-99e1-42a7-bda2-b0bbc64841b7', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'File');

-- 6
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('43d305de-b3c8-4922-86fd-45bd26f3bf01', '2020-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'File');

-- 7
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('fed888c6-83e1-4ed0-922a-bd5770af3fad', '2020-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'File');

-- 8 system_id:classification system object
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('2d0b2dc1-f3bb-4239-bf04-582b1085581c', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'ClassificationSystem');

-- 9
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('dc600862-3298-4ec0-8541-3e51fb900054', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'RecordEntity');

-- 10
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('7f000101-730c-1c94-8173-0c0ded71003c', '2020-07-01 22:25:06', 'test_user@example.com',
        'default', 0, 'CorrespondencePart');

-- 11
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('8131049d-dcac-43d8-bee4-656e72842da9', '2020-07-01 22:25:06', 'test_user@example.com',
        'default', 0, 'Part');

-- 12  Record / RegistryEntry
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('8f6b084f-d727-4b46-bbe2-14bed2135fa9', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'RecordEntity');

-- 13 Record / RecordNote
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('11b32a9e-802d-43de-9bb5-c951e3bbe95b', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'RecordEntity');

-- 14 DocumentFlow / RegistryEntry
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('cf0f41f7-65e8-4471-85f3-18ff223cbdb0', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'DocumentFlow');

-- 15
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('5f117c28-8a33-4d65-bc5a-8e74b3ba635b', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'DocumentFlow');

-- 16
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('554e1e9e-db86-4f1c-a505-43bc76207b09', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'DocumentFlow');

-- 18 system_id:documentdescription objects
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('66b92e78-b75d-4b0f-9558-4204ab31c2d1', '2020-07-01 22:25:06', 'test_user@example.com',
        'default', 0, 'DocumentDescription');

insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('9493c357-1345-4a55-98fd-fcba13b8a6dd', '2020-07-01 22:25:06', 'test_user@example.com',
        'default', 0, 'DocumentDescription');

insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('ebcefc44-73e5-485e-94c9-1b210359c125', '2020-07-01 22:25:06', 'test_user@example.com',
        'default', 0, 'Part');

insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('55d45e9a-c938-499e-b3da-6822ae508c8c', '2020-07-01 22:25:06', 'test_user@example.com',
        'default', 0, 'DocumentObject');


--
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('f32c1fa0-8e42-4236-8f40-e006940ea70b', '2020-07-01 22:25:06', 'test_user@example.com',
        'default', 0, 'Series');
-- Record
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('99c2f1af-dd84-19e8-dd4f-cc21fe1578ff', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'RecordEntity');
-- File
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('48c81365-7193-4481-bc84-b025248fb310', '2020-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'File');
-- Class
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('596c85fb-a6c4-4381-86b4-81df05234028', '2020-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'Klass');

-- Keyword for Record
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('6bcd4138-3d7b-46d6-9f93-cb1565730212', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'Keyword');

-- Keyword for File
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('81cea881-1203-4e3f-943c-c0294e81e528', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'Keyword');

-- Keyword for Class
insert into system_id_entity(system_id, created_date, created_by, organisation, version, dtype)
values ('1e29bf5c-f6d8-4b5b-aa6e-b2272e34a2ad', '2019-04-08 00:00:00', 'test_user@example.com',
        'default', 0, 'Keyword');

insert into as_fonds (system_id, title, description, fonds_status_code, fonds_status_code_name)
values ('3318a63f-11a7-4ec9-8bf1-4144b7f281cf', 'example test title', 'example test description', 'O', 'Opprettet');

insert into as_series (system_id, title, description, series_status_code, series_status_code_name, series_fonds_id)
values ('f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d', 'test title bravo', 'test description 2', 'O', 'Opprettet',
        '3318a63f-11a7-4ec9-8bf1-4144b7f281cf');

insert into as_series (system_id, title, description, series_status_code, series_status_code_name, series_fonds_id)
values ('f32c1fa0-8e42-4236-8f40-e006940ea70b', 'test title charlie', 'test description charlie', 'O', 'Opprettet',
        '3318a63f-11a7-4ec9-8bf1-4144b7f281cf');

insert into as_classification_system (system_id, title, description, classification_type_code,
                                      classification_type_code_name)
values ('2d0b2dc1-f3bb-4239-bf04-582b1085581c', 'test title classification system',
        'test description classification system', 'PNR', 'Fødselsnummer');

insert into as_series_classification_system(f_pk_series_id, f_pk_classification_system_id)
values ('f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d', '2d0b2dc1-f3bb-4239-bf04-582b1085581c');


insert into as_class (system_id, title, class_id, class_classification_system_id)
values ('596c85fb-a6c4-4381-86b4-81df05234028', 'test title class', 'class_id_value',
        '2d0b2dc1-f3bb-4239-bf04-582b1085581c');

-- file objects
insert into as_file (system_id, title, description, file_series_id)
values ('43d305de-b3c8-4922-86fd-45bd26f3bf01', 'test title charlie', 'test description 2',
        'f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d');
insert into as_file (system_id, title, description, file_series_id)
values ('fed888c6-83e1-4ed0-922a-bd5770af3fad', 'test title charlie', 'test description 2',
        'f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d');

-- 18
insert into as_file (system_id, title, description, file_series_id)
values ('f1677c47-99e1-42a7-bda2-b0bbc64841b7', 'test title bravo', 'test description 2',
        'f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d');

insert into as_file (system_id, title, description, file_series_id)
values ('48c81365-7193-4481-bc84-b025248fb310', 'test title bravo', 'test description 2',
        'f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d');


insert into as_record_entity (system_id, title, description, record_file_id)
values ('99c2f1af-dd84-19e8-dd4f-cc21fe1578ff', 'test title record', 'test description record',
        'f1677c47-99e1-42a7-bda2-b0bbc64841b7');


insert into as_record_entity (system_id, title, description, record_file_id)
values ('8f6b084f-d727-4b46-bbe2-14bed2135fa9', 'test title record', 'test description record',
        'f1677c47-99e1-42a7-bda2-b0bbc64841b7');

INSERT INTO sa_registry_entry (system_id, document_date, due_date, freedom_assessment_date, number_of_attachments,
                               record_date, record_sequence_number, record_year, records_management_unit,
                               registry_entry_number, registry_entry_status_code, registry_entry_status_code_name,
                               registry_entry_type_code, registry_entry_type_code_name, sent_date)
VALUES ('8f6b084f-d727-4b46-bbe2-14bed2135fa9', '2019-04-08 00:00:00', '2019-04-08 00:00:00', '2019-04-08 00:00:00', 1,
        '2019-04-08 00:00:00', 2, '2021', 'records_management_unit ', '22', 'J ', 'Journalført ', 'I',
        'Inngående dokument', '2019-04-08 00:00:00');

insert into as_record_entity (system_id, title, description, record_file_id)
values ('11b32a9e-802d-43de-9bb5-c951e3bbe95b', 'test title record', 'test description record',
        'f1677c47-99e1-42a7-bda2-b0bbc64841b7');

INSERT INTO sa_record_note (system_id, document_date, due_date, freedom_assessment_date,
                            number_of_attachments, sent_date)
VALUES ('11b32a9e-802d-43de-9bb5-c951e3bbe95b', '2019-04-08 00:00:00', '2019-04-08 00:00:00', '2019-04-08 00:00:00', 1,
        '2019-04-08 00:00:00');

INSERT INTO as_document_flow (system_id, flow_comment, flow_from, flow_received_date, flow_sent_date, flow_status_code,
                              flow_status_code_name, flow_to, document_flow_registry_entry_id)
VALUES ('cf0f41f7-65e8-4471-85f3-18ff223cbdb0', 'Please consider this', 'flow_from', '2019-04-08 00:00:00',
        '2019-04-08 00:00:00',
        'F', 'Til fordeling', 'flow_to_a', '8f6b084f-d727-4b46-bbe2-14bed2135fa9'),
       ('554e1e9e-db86-4f1c-a505-43bc76207b09', 'Please consider this', 'flow_from', '2019-04-08 00:00:00',
        '2019-04-08 00:00:00',
        'F', 'Til fordeling', 'flow_to_b', '8f6b084f-d727-4b46-bbe2-14bed2135fa9'),
       ('5f117c28-8a33-4d65-bc5a-8e74b3ba635b', 'Great stuff', 'flow_from', '2019-04-08 00:00:00',
        '2019-04-08 00:00:00',
        'G', 'Godkjent', 'flow_to', '8f6b084f-d727-4b46-bbe2-14bed2135fa9');

insert into as_record_entity (system_id, title, description, record_file_id)
values ('dc600862-3298-4ec0-8541-3e51fb900054', 'test title record', 'test description record',
        'f1677c47-99e1-42a7-bda2-b0bbc64841b7');

insert into as_document_description (system_id, title, description, document_type_code, document_type_code_name,
                                     associated_with_record_as_code, associated_with_record_as_code_name,
                                     document_number, association_date)
values ('66b92e78-b75d-4b0f-9558-4204ab31c2d1', 'test title bravo', 'test description bravo', 'B', 'Brev',
        'H', 'Hoveddokument', 1, '2020-04-08');


insert into as_document_description (system_id, title, description, document_type_code, document_type_code_name,
                                     associated_with_record_as_code, associated_with_record_as_code_name,
                                     document_number, association_date)
values ('9493c357-1345-4a55-98fd-fcba13b8a6dd', 'test title charlie', 'test description charlie', 'B', 'Brev',
        'H', 'Hoveddokument', 1, '2020-04-08');

insert into as_document_object (system_id, version_number, file_size, original_filename,
                                checksum, checksum_algorithm,
                                format_code, format_code_name, mime_type,
                                reference_document_file, storage_path,
                                variant_format_code, variant_format_code_name)
values ('55d45e9a-c938-499e-b3da-6822ae508c8c', 1, 4444444, 'The life and times of scrooge mcduck.odt',
        '9dc9fe5ae7da9770e9801d1711abb7553d38227cd90b90f0f94811778f29ea95', 'SHA-256',
        'fmt/136', 'OpenDocument Text (odt) Version 1.0', 'application/vnd.oasis.opendocument.text',
        '1111111-2222-3333-4444-555555555', '/somewhere/over/the/rainbow',
        'P', 'Produksjonsformat');





-- Keyword for Record
INSERT INTO as_keyword (keyword, system_id)
VALUES ('keywordX for record', '6bcd4138-3d7b-46d6-9f93-cb1565730212');

-- Keyword for File
INSERT INTO as_keyword (keyword, system_id)
VALUES ('keywordX for file', '81cea881-1203-4e3f-943c-c0294e81e528');

-- Keyword for Class
INSERT INTO as_keyword (keyword, system_id)
VALUES ('keywordX for class', '1e29bf5c-f6d8-4b5b-aa6e-b2272e34a2ad');

-- Join table associations
INSERT INTO as_class_keyword (f_pk_class_id, f_pk_keyword_id)
VALUES ('596c85fb-a6c4-4381-86b4-81df05234028', '1e29bf5c-f6d8-4b5b-aa6e-b2272e34a2ad');

INSERT INTO as_record_keyword (f_pk_record_id, f_pk_keyword_id)
VALUES ('99c2f1af-dd84-19e8-dd4f-cc21fe1578ff', '6bcd4138-3d7b-46d6-9f93-cb1565730212');

INSERT INTO as_file_keyword (f_pk_file_id, f_pk_keyword_id)
VALUES ('48c81365-7193-4481-bc84-b025248fb310', '81cea881-1203-4e3f-943c-c0294e81e528');


-- Joins
-- Join users to AdminUnit
insert into ad_administrative_unit_user (f_pk_administrative_unit_id, f_pk_user_id)
values ('c3d4affc-66a0-4663-b63a-6ecc4f3d6009', '03fd9ab1-e5fd-44b6-9c63-6afd08569a46');

insert into as_record_document_description (f_pk_record_id, f_pk_document_description_id)
values ('dc600862-3298-4ec0-8541-3e51fb900054', '66b92e78-b75d-4b0f-9558-4204ab31c2d1');

insert into as_record_document_description (f_pk_record_id, f_pk_document_description_id)
values ('dc600862-3298-4ec0-8541-3e51fb900054', '9493c357-1345-4a55-98fd-fcba13b8a6dd');


insert into as_correspondence_part(system_id, correspondence_part_type_code,
                                   correspondence_part_type_code_name, f_pk_record_id)
values ('7f000101-730c-1c94-8173-0c0ded71003c', 'EA', 'Avsender', 'dc600862-3298-4ec0-8541-3e51fb900054');

insert into as_correspondence_part_person(system_id, name, social_security_number)
values ('7f000101-730c-1c94-8173-0c0ded71003c', 'Hans Gruber II', '987654321369852');

--
-- insert into as_part(system_id, name, part_role_code, part_role_code_name, title)
-- values ('8131049d-dcac-43d8-bee4-656e72842da9', 'Hans Gruber', 'KLI', 'Klient', 'title');
--
-- insert into as_part_person(system_id, social_security_number)
-- values ('8131049d-dcac-43d8-bee4-656e72842da9', '1234567895655');
--
-- insert into as_part(system_id, name, part_role_code, part_role_code_name, title)
-- values ('ebcefc44-73e5-485e-94c9-1b210359c125', 'Hans Gruber', 'ADV', 'Advokat', 'title');
--
-- insert into as_part_unit(system_id, organisation_number)
-- values ('ebcefc44-73e5-485e-94c9-1b210359c125', '02020202022');
--
-- insert into as_file_part(f_pk_file_id, f_pk_part_id)
-- values ('f1677c47-99e1-42a7-bda2-b0bbc64841b7', 'ebcefc44-73e5-485e-94c9-1b210359c125');
--
-- insert into as_file_part(f_pk_file_id, f_pk_part_id)
-- values ('43d305de-b3c8-4922-86fd-45bd26f3bf01', '8131049d-dcac-43d8-bee4-656e72842da9');
--
-- insert into as_record_part(f_pk_record_id, f_pk_part_id)
-- values ('dc600862-3298-4ec0-8541-3e51fb900054', '8131049d-dcac-43d8-bee4-656e72842da9');
--
-- -- Make the existing File a Case File. Requires an associated administrativeUnit
--
-- insert into sa_case_file (system_id, case_date, case_responsible, case_sequence_number, case_status_code,
--                           case_status_code_name, case_year, case_file_administrative_unit_id)
-- values ('fed888c6-83e1-4ed0-922a-bd5770af3fad', '2021-04-24 14:55:13', 'test_user@example.com', '1', 'R',
--         'Opprettet av saksbehandler', '2021', 'c3d4affc-66a0-4663-b63a-6ecc4f3d6009');
--
-- insert into sa_sequence_generator(reference_administrative_unit, year, administrative_unit_name, record_sequence_number,
--                                   case_file_sequence_number)
-- values ('c3d4affc-66a0-4663-b63a-6ecc4f3d6009', '2021', 'example test administrative unit', '1', '1');
