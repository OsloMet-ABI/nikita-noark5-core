LOGIN_OK = 'Login successful. Token expires in {}s. Token is {}'
LOGIN_NOT_OK = 'Did not receive a 200 OK when logging in'
REALM_CREATE_OK = 'Creation of realm [{}] OK. Returned status [{}]'
REALM_CREATE_NOT_OK = 'Creation of realm [{}] failed. Returned status [{}]'
ROLE_CREATE_OK = 'Creation of role [{}] OK. Returned status [{}]'
ROLE_CREATE_NOT_OK = 'Creation of role [{}] failed. Returned status [{}]'
CLIENT_CREATE_OK = 'Creation of client [{}] OK. Returned status [{}]'
CLIENT_CREATE_NOT_OK = 'Creation of client [{}] failed. Returned status [{}]'
USER_CREATE_OK = 'Creation of user [{}] OK. Returned status [{}]'
USER_CREATE_NOT_OK = 'Creation of user [{}] failed. Returned status [{}]'
GET_ROLE_OK = 'Retrieval of role [{}] OK. Returned status [{}]'
GET_ROLE_NOT_OK = 'Retrieval of role [{}] failed. Returned status [{}]'
TOO_MANY_USERS = 'Too many users [{}] returned when searching for [{}]. Returned status [{}]'
GET_USER_OK = 'Retrieval of user [{}] OK. Returned status [{}]'
GET_USER_NOT_OK = 'Retrieval of user [{}] failed. Returned status [{}]'
ADD_USER_ROLE_OK = 'Assignment of user [{}] to role [{}] OK. Returned status [{}]'
ADD_USER_ROLE_NOT_OK = 'Assignment of user [{}] to role [{}] failed. Returned status [{}]'
NIKITA_USER_CREATE_OK = 'Creation of Nikita user [{}] OK. Returned status [{}]'
NIKITA_USER_CREATE_NOT_OK = 'Creation of Nikita user [{}] failed. Returned status [{}]'

roles = ['RECORDS_MANAGER', 'ADMIN', 'RECORDS_KEEPER', 'CASE_HANDLER',
         'LEADER']

# Note there is no real difference between a nikita admin user and a general user, but there will
# be one day. That is why they are differentiated here
nikita_admin_username = 'admin@example.com'
nikita_admin_password = 'password'

keycloak_admin_username = 'admin'
keycloak_admin_password = 'password'

nikita_client = 'nikita-client'
admin_client = 'admin-cli'

realm_name = 'recordkeeping'

keycloak_base = 'http://localhost:38080/'
keycloak_base = 'https://nikita.oslomet.no/keycloak/'
admin_login = 'realms/master/protocol/openid-connect/token'
nikita_login = 'realms/' + realm_name + '/protocol/openid-connect/token'
nikita_base_url = 'http://localhost:8092/noark5v5'
nikita_base_url = 'https://nikita.oslomet.no/noark5v5'
nikita_application_root_url = nikita_base_url + '/api/arkivstruktur'
nikita_create_user_url = nikita_base_url + '/api/admin/ny-bruker'
nikita_create_metadata_url = nikita_base_url + '/api/metadata/ny-virksomhetsspesifikkeMetadata'

url_realm = keycloak_base + 'admin/realms/'
url_admin_login = keycloak_base + admin_login
url_nikita_user_login = keycloak_base + nikita_login

url_add_user_to_role = url_realm + '{}/users/{}/role-mappings/realm'
nikita_username_template = 'bruker{}@example.com'
nikita_password_template = 'bruker{}pass'
total_nikita_user_to_create = 70

# Nikita specific messages
NIKITA_APPLICATION_ROOT_OK = 'Retrieval of application root [{}] OK. Returned status [{}]'
NIKITA_APPLICATION_ROOT_NOT_OK = 'Retrieval of application root [{}] failed. Returned status [{}]'
NIKITA_CREATE_FONDS_OK = 'Creation of fonds [{}] OK. Returned status [{}]'
NIKITA_CREATE_FONDS_NOT_OK = 'Creation of fonds [{}] failed. Returned status [{}]'
NIKITA_CREATE_FONDS_CREATOR_OK = 'Creation of fonds creator [{}] OK. Returned status [{}]'
NIKITA_CREATE_FONDS_CREATOR_NOT_OK = 'Creation of fonds creator [{}] failed. Returned status [{}]'
NIKITA_CREATE_SERIES_OK = 'Creation of series [{}] OK. Returned status [{}]'
NIKITA_CREATE_SERIES_NOT_OK = 'Creation of series [{}] failed. Returned status [{}]'

# Nikita specific URLs
LINKS = '_links'
HREF = 'href'
CONTENT_TYPE_NOARK = 'application/vnd.noark5+json'
NEW_FONDS_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkiv/'
NEW_FONDS_CREATOR_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkivskaper/'
NEW_SERIES_REL = 'https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkivdel/'

