import json
import logging

from constants import *
from net import *

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def create_nikita_general_users(nikita_admin_header):
    for user_id in range(1, total_nikita_user_to_create):
        create_nikita_user(nikita_admin_header,
                           nikita_username_template.format(str(user_id)),
                           nikita_password_template.format(str(user_id)))


def create_nikita_user(nikita_admin_header, username, password):
    user_payload = create_nikita_user_payload(username, password)
    do_create_nikita_user(nikita_admin_header, user_payload)


def create_nikita_user_payload(username, password):
    return {
        "brukerNavn": username,
        "passord": password,
        "fornavn": "Frank",
        "etternavn": "Grimes"
    }


def do_create_nikita_user(nikita_admin_header, user_payload):
    response = do_post_request(nikita_create_user_url, json.dumps(user_payload),
                               nikita_admin_header)
    if response.status == 201:
        logging.info(NIKITA_USER_CREATE_OK.format(user_payload['brukerNavn'], response.status))
    else:
        logging.error(NIKITA_USER_CREATE_NOT_OK.format(user_payload['brukerNavn'], response.status))
