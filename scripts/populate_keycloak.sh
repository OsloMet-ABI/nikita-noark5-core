#!/bin/sh
# Populate a keycloak instance with default values required by nikita
# Developed with and tested on 22.0.0, tested on 26.0.6.

if [ -z "${KEYCLOAK_ADMIN}" ] ; then
    export KEYCLOAK_ADMIN=admin
fi

if [ -z "${KEYCLOAK_ADMIN_PASSWORD}" ] ; then
    export KEYCLOAK_ADMIN_PASSWORD=admin
fi

realm_name='recordkeeping'
role_name='RECORDS_MANAGER'
username='admin@example.com'
password='password'
client_name='nikita-client'
# Get an admin access token to use for all requests
access_token=$(curl -d "username=${KEYCLOAK_ADMIN}" -d "password=${KEYCLOAK_ADMIN_PASSWORD}" -d "grant_type="password -d "client_id=admin-cli" "http://localhost:8080/realms/master/protocol/openid-connect/token" | jq -r ' "\(.token_type) \(.access_token)"') && echo "Access token is "$access_token
# Create a realm
curl -s -o /dev/null -w "%{http_code} for create realm ($realm_name) \n" -H "Content-Type: application/json" -H "Authorization: $access_token" -d '{ "id": "'$realm_name'", "realm": "'$realm_name'", "displayName": "Recordkeeping Realm", "enabled": true, "accessTokenLifespan": 36000, "sslRequired": "external", "registrationAllowed": false, "loginWithEmailAllowed": true, "duplicateEmailsAllowed": false, "resetPasswordAllowed": false, "editUsernameAllowed": false, "bruteForceProtected": true }' -X POST "http://localhost:8080/admin/realms"
# Create a role
curl -s -o /dev/null -w "%{http_code} for create role ($role_name) \n" -H "Content-Type: application/json" -H "Authorization: $access_token" -d '{ "name":"'$role_name'","description":"Records manager role" }' -X POST "http://localhost:8080/admin/realms/$realm_name/roles"
# Create a client
curl -s -o /dev/null -w "%{http_code} for create client ($client_name) \n" -H "Content-Type: application/json" -H "Authorization: $access_token" -d '{ "id":"nikita-client", "name":"nikita-client","description":"General nikita client", "directAccessGrantsEnabled": true, "publicClient": true}' -X POST "http://localhost:8080/admin/realms/$realm_name/clients"
# Create a user
curl -s -o /dev/null -w "%{http_code} for create user ($username) \n" -H "Content-Type: application/json" -H "Authorization: $access_token" -d '{ "username":"'$username'",  "enabled":true,  "emailVerified":true,  "firstName":"Frank",  "lastName":"Grimes",  "email":"'$username'",  "credentials":[ { "type":"password",  "value":"'$password'",  "temporary":false}], "realmRoles":[ "'$role_name'"], "clientRoles": { "account": [ "view-profile", "manage-account", "'$role_name'" ] }}' -X POST "http://localhost:8080/admin/realms/$realm_name/users"
user_id=$(curl -H	"Authorization: $access_token" -X GET "http://localhost:8080/admin/realms/$realm_name/users?q=username=$username" | jq -r '.[] | .id' ) && echo "User id is "$user_id
rk_role_id=$(curl -H "Authorization: $access_token" -X GET "http://localhost:8080/admin/realms/$realm_name/roles/$role_name"  | jq -r '.id') && echo "Role id is "$rk_role_id
# Add a user to the given role
curl -s -o /dev/null -w "%{http_code} for add user ($username) to role ($role_name) \n" -H "Content-Type: application/json" -H "Authorization: $access_token" -d '[{"id":"'$rk_role_id'", "name":"'$role_name'", "description": "Records manager role", "composite": false, "clientRole": false, "containerId": "'$realm_name'"}]' -X POST "http://localhost:8080/admin/realms/$realm_name/users/$user_id/role-mappings/realm"

# Commands to remove what was created, if required
#curl -s -o /dev/null -I -w "%{http_code} \n"  -H "Authorization: $access_token" -X DELETE "http://localhost:8080/admin/realms/$realm_name/users/$user_id/role-mappings/realm" -H 'content-type: application/json'
#curl -s -o /dev/null -I -w "%{http_code} \n"  -H "Authorization: $access_token" -X DELETE "http://localhost:8080/admin/realms/$realm_name/users/$user_id"
#curl -s -o /dev/null -I -w "%{http_code} \n"  -H "Authorization: $access_token" -X DELETE "http://localhost:8080/admin/realms/$realm_name/clients/$client_name"
#curl -s -o /dev/null -I -w "%{http_code} \n"  -H "Authorization: $access_token" -X DELETE "http://localhost:8080/admin/realms/$realm_name/roles/$role_name"
#curl -s -o /dev/null -I -w "%{http_code} \n"  -H "Authorization: $access_token" -X DELETE "http://localhost:8080/admin/realms/$realm_name/"
curl -d "username=$admin_username" -d "password=$admin_password" -d "grant_type="password -d "client_id=admin-cli" "http://localhost:8080/realms/master/protocol/openid-connect/token"