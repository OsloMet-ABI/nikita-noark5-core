MVN = mvn

MVNOPTS := -Dmaven.repo.local=`pwd`/maven-repo

all: build

build:
	$(MVN) $(MVNOPTS) clean validate install
run: build
	curl http://localhost:8080/realms/recordkeeping/.well-known/openid-configuration | jq .authorization_endpoint | grep -q /protocol/ || echo "error: Unable to reach Keycloak API.  Is it running and configured?"
	$(MVN) $(MVNOPTS) -f pom.xml spring-boot:run
clean:
	$(MVN) $(MVNOPTS) -DskipTests=true clean
check:
	$(MVN) $(MVNOPTS) test
check-postgres:
	$(MVN) $(MVNOPTS) test -DDB_PROFILE=local-postgres

